/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "meshcoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MeshCoil::MeshCoil(){

	}

	// constructor with input
	MeshCoil::MeshCoil(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShMeshCoilPr MeshCoil::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<MeshCoil>();
	}

	// factory with input
	ShMeshCoilPr MeshCoil::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<MeshCoil>(frame, area);
	}

	// extract surface mesh
	ShSurfacePr MeshCoil::create_surface() const{
		// create coil surface
		ShSurfaceCoilPr surf = SurfaceCoil::create();
		
		// regular setupp
		setup_surface(surf);

		// set properties
		surf->set_material(get_material()->copy());
		surf->set_operating_current(operating_current_);
		surf->set_number_turns(number_turns_);

		// return surface
		return surf;
	}

	// set operating current
	void MeshCoil::set_operating_current(const double operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	double MeshCoil::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void MeshCoil::set_number_turns(const double number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	double MeshCoil::get_number_turns() const{
		return number_turns_;
	}

	// set material
	void MeshCoil::set_material(ShMaterialPr material){
		assert(material!=NULL);
		material_ = material;
	}

	// get material
	ShMaterialPr MeshCoil::get_material() const{
		assert(material_!=NULL);
		return material_;
	}

	// set use volume elements
	void MeshCoil::set_use_volume_elements(const bool use_volume_elements){
		use_volume_elements_ = use_volume_elements;
	}

	// set softening factor
	void MeshCoil::set_softening(const double softening){
		softening_ = softening;
	}

	// set softening factor
	void MeshCoil::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}

	// get all current elements
	void MeshCoil::get_current_elements(
		arma::Mat<double> &Rl, arma::Mat<double> &dRl, 
		arma::Row<double> &Acrss, const arma::sword num_gauss) const{

		// forward calculation including all cross sections
		get_current_elements(Rl,dRl,Acrss,num_gauss,0,n_.n_cols/area_->get_num_elements()-1);
	}

	// get element direction vectors
	// from cross section with idx1 to 
	// the cross section with idx2
	void MeshCoil::get_current_elements(
		arma::Mat<double> &Rl, arma::Mat<double> &dRl, 
		arma::Row<double> &Acrss, const arma::sword num_gauss, 
		const arma::uword idx1, const arma::uword idx2) const{

		// check input
		assert(!n_.is_empty()); assert(!R_.is_empty());
		assert(idx2>=idx1); assert(num_gauss!=0);

		// get counters
		const arma::uword num_cross_elements = area_->get_num_elements();
		const arma::uword num_cross = idx2-idx1+1;
		const arma::uword num_elements = num_cross*num_cross_elements;

		// real number of gauss points
		arma::uword ng = std::abs(num_gauss);

		// create gauss points
		cmn::Gauss gp(num_gauss);
		arma::Row<double> xg = gp.get_abscissae();
		arma::Row<double> wg = gp.get_weights();
		
		// setup grid around singularity
		// arma::Mat<double> Rqgrd; arma::Row<double> wgrd;
		// cmn::Hexahedron::setup_source_grid(Rqgrd, wgrd, arma::Col<double>{1,1,1}, xg, wg);
		// assert(std::abs(arma::sum(wgrd)-1.0)<1e-6);

		// number of gauss points
		const arma::uword ng_element = std::pow(ng,n_dim_);

		// length scaling
		arma::Mat<double> Rqgrd(n_dim_,ng_element);
		arma::Row<double> ell_scale(ng_element);
		arma::Row<double> a_scale(ng_element);

		// setup quadrilateral grid coordinates
		arma::uword cnt = 0;

		// for line elements
		if(n_dim_ == 1){
			for(arma::uword k=0;k<ng;k++){
				Rqgrd.col(cnt) = xg(k);
				ell_scale(cnt) = wg(k)/2; a_scale(cnt) = 1.0;
				cnt++;
			}

			// check scales
			assert(std::abs(arma::accu(ell_scale)-1.0)<1e-6);
			assert(std::abs(arma::accu(a_scale)-ng)<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-1.0)<1e-6);
		}

		// for quadrilateral elements
		else if(n_dim_ == 2){
			for(arma::uword k=0;k<ng;k++){
				for(arma::uword l=0;l<ng;l++){
					Rqgrd.col(cnt) = arma::Col<double>::fixed<2>{xg(k),xg(l)};
					ell_scale(cnt) = wg(l)/2; a_scale(cnt) = wg(k)/2;
					cnt++;
				}
			}

			// check scales
			assert(std::abs(arma::accu(ell_scale)-ng)<1e-6);
			assert(std::abs(arma::accu(a_scale)-ng)<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-1.0)<1e-6);
		}

		// for hexahedron elements
		else if(n_dim_ == 3){
			for(arma::uword k=0;k<ng;k++){
				for(arma::uword l=0;l<ng;l++){
					for(arma::uword m=0;m<ng;m++){
						Rqgrd.col(cnt) = arma::Col<double>::fixed<3>{xg(k),xg(l),xg(m)};
						ell_scale(cnt) = wg(m)/2; a_scale(cnt) = wg(k)*wg(l)/4;
						cnt++;
					}
				}
			}
			
			// check scales
			assert(std::abs(arma::accu(ell_scale)-ng*ng)<1e-6);
			assert(std::abs(arma::accu(a_scale)-ng)<1e-6);
			assert(std::abs(arma::accu(ell_scale%a_scale)-1.0)<1e-6);
		}

		// quadrilateral coord
		// const arma::Col<double>::fixed<2> Rqmid(arma::fill::zeros);

		// calculate cross section area	
		const arma::Row<double> cross_area = area_->calculate_areas();

		// allocate one line current for each element
		Rl.set_size(3,num_elements*ng_element);
		dRl.set_size(3,num_elements*ng_element);
		Acrss.set_size(num_elements*ng_element);

		// walk over elements
		for(arma::uword i=0;i<num_elements;i++){
			// find cable intersections at middle of faces/edges
			// const arma::Col<double>::fixed<3> R1 = 
			// 	cmn::Quadrilateral::quad2cart(
			// 	R_.cols(n_.submat(0,i,n_.n_rows/2-1,i)),Rqmid);
			// const arma::Col<double>::fixed<3> R2 = 
			// 	cmn::Quadrilateral::quad2cart(
			// 	R_.cols(n_.submat(n_.n_rows/2,i,n_.n_rows-1,i)),Rqmid);

			// get element index
			const arma::uword element_index = idx1*num_cross_elements + i;

			// find cable intersections at middle of faces/edges
			const arma::Col<double>::fixed<3> R1 = arma::mean(R_.cols(n_.submat(0,element_index,n_.n_rows/2-1,element_index)),1);
			const arma::Col<double>::fixed<3> R2 = arma::mean(R_.cols(n_.submat(n_.n_rows/2,element_index,n_.n_rows-1,element_index)),1);

			// create weighted direction vectors
			arma::Mat<double> dRtmp(3,ng_element);
			dRtmp.each_col() = R2-R1;
			dRtmp.each_row() %= ell_scale;

			// calculate points in carthesian coordinates
			if(n_dim_==1)Rl.cols(i*ng_element,(i+1)*ng_element-1) = cmn::Line::quad2cart(R_.cols(n_.col(element_index)),Rqgrd);;
			if(n_dim_==2)Rl.cols(i*ng_element,(i+1)*ng_element-1) = cmn::Quadrilateral::quad2cart(R_.cols(n_.col(element_index)),Rqgrd);
			if(n_dim_==3)Rl.cols(i*ng_element,(i+1)*ng_element-1) = cmn::Hexahedron::quad2cart(R_.cols(n_.col(element_index)),Rqgrd);

			dRl.cols(i*ng_element,(i+1)*ng_element-1) = dRtmp;
			Acrss.cols(i*ng_element,(i+1)*ng_element-1) = a_scale*cross_area(i%num_cross_elements);
		}
	}


	// create line elements
	fmm::ShCurrentSourcesPr MeshCoil::create_current_sources(const arma::sword num_gauss) const{

		// get line current elements
		arma::Mat<double> Rl,dRl; arma::Row<double> Acrss;
		//get_current_elements(Rl,dRl,Acrss,num_gauss*std::pow(2,3-n_dim_));
		get_current_elements(Rl,dRl,Acrss,num_gauss*std::exp2(3-n_dim_));

		// set currents to elements
		const arma::Row<double> Il = Acrss*calc_current_density();

		// calculate softening factor from length
		//const arma::Row<double> epsl = softening_*0.5*std::sqrt(2.0)*cmn::Extra::vec_norm(dRl); 
		//const arma::Row<double> epsl = arma::pow(Acrss*cmn::Extra::vec_norm(dRl)*(3.0/4.0)/arma::datum::pi,1.0/3);
		const arma::Row<double> epsl = softening_*arma::max(
			std::sqrt(2.0)*arma::sqrt(Acrss/arma::datum::pi),
			0.5*std::sqrt(2.0)*cmn::Extra::vec_norm(dRl));

		// create source object and return
		return fmm::CurrentSources::create(Rl,dRl,Il,epsl);
	}

	// create line elements
	fmm::ShSourcesPr MeshCoil::create_current_mesh(const arma::sword num_gauss) const{
		// get line current elements
		arma::Mat<double> Rl,dRl; arma::Row<double> Acrss;
		get_current_elements(Rl,dRl,Acrss,1);

		// calculate current distribution in cross section
		const double Jop = calc_current_density();

		// current density vectors for elements
		const arma::Mat<double> Je = Jop*(dRl.each_row()/cmn::Extra::vec_norm(dRl));

		// allocate
		fmm::ShSourcesPr mesh = NULL;

		// line elements
		if(n_dim_==1){
			// use regular elements for now
			// this could be replaced by something
			// more advanced in the future
			mesh = create_current_sources(num_gauss);
		}

		// surface elements
		else if(n_dim_==2){
			fmm::ShCurrentSurfacePr surf = fmm::CurrentSurface::create();
			surf->set_mesh(R_,n_,Je*area_->get_hidden_dim());
			surf->set_num_gauss(2*num_gauss);
			mesh = surf;
		}

		// volume elements
		else if(n_dim_==3){
			fmm::ShCurrentMeshPr vol = fmm::CurrentMesh::create();
			vol->set_mesh(R_,n_,Je);
			vol->set_num_gauss(num_gauss);
			mesh = vol;
		}

		// otherwise
		else{
			rat_throw_line("number of dimensions not recognised");
		}

		// return mesh
		return mesh;
	}

	// default create sources function to be overridden
	fmm::ShSourcesPrList MeshCoil::create_sources(const arma::sword num_gauss, const arma::sword) const{
		// allocate
		fmm::ShSourcesPrList srcs(1);

		// line elements
		if(use_volume_elements_==false){
			srcs(0) = create_current_sources(num_gauss);
		}

		// volume elements
		else{
			srcs(0) = create_current_mesh(num_gauss);
		}

		// return the sources list
		return srcs;
	}

	// current density
	double MeshCoil::calc_current_density() const{
		// calculate cross section areas
		const double cross_area = area_->calculate_area();

		// calculate current distribution in cross section
		const double Jop = number_turns_*operating_current_/cross_area;

		// return current density
		return Jop;
	}

	// get curret density at nodes
	arma::Mat<double> MeshCoil::get_nodal_current_density() const{
		return L_*calc_current_density();
	}

	// calculate field angle
	arma::Row<double> MeshCoil::calc_alpha(const arma::Mat<double> &B) const{
		// check number of columns
		assert(B.n_cols==get_num_nodes());

		// calculate components along L, N and D
		const arma::Row<double> Bl = cmn::Extra::dot(B,L_);
		const arma::Row<double> Bn = cmn::Extra::dot(B,N_);
		const arma::Row<double> Bd = cmn::Extra::dot(B,D_);

		// calculate angle
		const arma::Row<double> alpha_rad = arma::atan2(arma::sqrt(Bl%Bl + Bd%Bd),Bn);

		// return angle
		return alpha_rad;
	}

	// calculate engineering current density
	arma::Row<double> MeshCoil::calc_Je(
		const arma::Mat<double> &B, 
		const arma::Row<double> &T) const{
		// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);

		// calculate engineering current density and return
		arma::Row<double> Jc;
		if(enable_current_sharing_){
			Jc = material_->calc_critical_current_density(Bm, T, alpha, area_);
		}else{
			Jc = material_->calc_critical_current_density(Bm, T, alpha);
		}

		// return critical current
		return Jc;
	}

	// calculate engineering current density
	arma::Row<double> MeshCoil::calc_Tc(
		const arma::Mat<double> &J, 
		const arma::Mat<double> &B) const{
		// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);
		arma::Row<double> Jm = cmn::Extra::vec_norm(J);

		// calculate engineering current density and return
		arma::Row<double> Tc;
		if(enable_current_sharing_){
			Tc = material_->calc_critical_temperature(Jm, Bm, alpha, area_);
		}else{
			Tc = material_->calc_critical_temperature(Jm, Bm, alpha);
		}

		// return critical temperature
		return Tc;
	}

	// calculate electric field
	arma::Mat<double> MeshCoil::calc_E(
		const arma::Mat<double> &J, 
		const arma::Mat<double> &B, 
		const arma::Row<double> &T) const{
			// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);
		arma::Row<double> Jm = cmn::Extra::vec_norm(J);

		// calculate electric field
		arma::Mat<double> E;
		if(enable_current_sharing_){
			E = L_.each_row()%material_->calc_electric_field(Jm,Bm,T,alpha, area_);	
		}else{
			E = L_.each_row()%material_->calc_electric_field(Jm,Bm,T,alpha);	
		}

		// return field
		return E;
	}

	// copy constructor
	ShMeshPr MeshCoil::copy() const{
		return std::make_shared<MeshCoil>(*this);
	}

	// get type
	std::string MeshCoil::get_type(){
		return "mdl::meshcoil";
	}

	// method for serialization into json
	void MeshCoil::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Mesh::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// material of this coil
		js["material"] = cmn::Node::serialize_node(material_,list);

		// settings
		js["operating_current"] = operating_current_;
		js["enable_current_sharing"] = enable_current_sharing_;
		js["number_turns"] = number_turns_;
		js["use_volume_elements"] = use_volume_elements_;
		js["softening"] = softening_;
	}

	// method for deserialisation from json
	void MeshCoil::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Mesh::deserialize(js,list,factory_list);

		// material
		set_material(cmn::Node::deserialize_node<Material>(js["material"],list,factory_list));

		// settings
		set_operating_current(js["operating_current"].asDouble());
		set_enable_current_sharing(js["enable_current_sharing"].asBool());
		set_number_turns(js["number_turns"].asDouble());
		set_use_volume_elements(js["use_volume_elements"].asBool());
		set_softening(js["softening"].asDouble());
	}

}}