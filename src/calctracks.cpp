/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calctracks.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcTracks::CalcTracks(){
		fmm_stngs_->set_num_exp(7);
		set_name("tracks");
	}

	// constructor
	CalcTracks::CalcTracks(ShModelPr source_model, ShModelPr target_model, ShEmitterPr emitter){
		set_source_model(source_model); set_target_model(target_model);
		set_emitter(emitter); fmm_stngs_->set_num_exp(7); set_name("tracks");
	}

	// factory
	ShCalcTracksPr CalcTracks::create(){
		return std::make_shared<CalcTracks>();
	}

	// factory
	ShCalcTracksPr CalcTracks::create(ShModelPr source_model, ShModelPr target_model, ShEmitterPr emitter){
		return std::make_shared<CalcTracks>(source_model, target_model, emitter);
	}

	// set emitter
	void CalcTracks::set_emitter(ShEmitterPr emitter){
		if(emitter==NULL)rat_throw_line("emitter points to NULL");
		emitter_ = emitter;
	}

	// set step size
	void CalcTracks::set_stepsize(const double step_size){
		if(step_size<=0)rat_throw_line("step size must be larger than zero");
		step_size_ = step_size; 
	}

	// system function
	arma::Mat<double> CalcTracks::system_function(
		const double, const arma::Mat<double> &RV, 
		const arma::Row<double> &q, const arma::Row<double> &m,
		const ShVTKUnstrPr &vtk_mesh){

		// calculate magnetic field at positions [T]
		const arma::Mat<double> Bp = vtk_mesh->interpolate_field(RV.rows(0,2),"Mgn. Flux Density [T]");

		// calculate force 
		const arma::Mat<double> F = cmn::Extra::cross(RV.rows(3,5), Bp).each_row()%q;

		// calculiate change
		arma::Mat<double> dXp(RV);
		dXp.rows(0,2) = RV.rows(3,5); // change of position is just velocity
		dXp.rows(3,5) = F.each_row()/m; // acceleration: newton second law

		// check for out of scope
		dXp.cols(arma::find(arma::all(Bp==0,0))).fill(0);

		// return change
		return dXp;
	}

	// Runge Kutta algorithm fourth order 
	arma::Mat<double> CalcTracks::runge_kutta(const double t, const double dt, 
		const arma::Mat<double> &RV, const arma::Row<double> &q, const arma::Row<double> &m,
		const ShVTKUnstrPr &vtk_mesh){

		// calc coefficients
		const arma::Mat<double> k1 = system_function(t, RV, q, m, vtk_mesh);
		const arma::Mat<double> k2 = system_function(t + dt/2, RV + (dt/2)*k1, q, m, vtk_mesh);
		const arma::Mat<double> k3 = system_function(t + dt/2, RV + (dt/2)*k2, q, m, vtk_mesh);
		const arma::Mat<double> k4 = system_function(t + dt, RV + dt*k3, q, m, vtk_mesh);

		// change
		arma::Mat<double> dRV = dt*(k1 + 2*k2 + 2*k3 + k4)/6.0;

		// particle fallen off grid
		dRV.cols(arma::find(arma::all(k1==0,0) || arma::all(k2==0,0) || 
			arma::all(k3==0,0) || arma::all(k4==0,0))).fill(0);

		// calculate change of x
		return dRV;
	}

	// run tracking code
	void CalcTracks::calculate(cmn::ShLogPr lg){
		// check emitter
		if(emitter_==NULL)rat_throw_line("emitter not set");
		
		// check mesh
		if(!has("H"))rat_throw_line("interpolation mesh needs magnetic field");

		// check settings
		if(step_size_==0)rat_throw_line("stepsize can not be zero");

		// calculate map
		CalcFieldMap::calculate(lg);

		// header
		lg->msg("%s%s--- CALCULATING PARTICLE TRACKS ---%s\n",KBLD,KGRN,KNRM);

		// check field calculation
		if(!has_field())rat_throw_line("field not calculated");

		// create VTK data object
		ShVTKUnstrPr vtk_mesh = VTKUnstr::create();

		// Create mesh and elements
		for(arma::uword i=0;i<num_objects_;i++){
			arma::uword type = 0;
			if(n_(i).n_rows==2)type = 3; // line
			if(n_(i).n_rows==4)type = 9; // quadrilateral
			if(n_(i).n_rows==8)type = 12; // hexahedron
			vtk_mesh->set_mesh(Rt_,n_(i),type); // 12 stands for hexahedrons
		}
		
		// magnetic field data	
		const arma::Mat<double> Bn = get_field("B");
		vtk_mesh->set_nodedata(Bn,"Mgn. Flux Density [T]");

		// export to mesh
		vtk_mesh->setup_interpolation();

		// header
		lg->msg(2,"%s%sPARTICLE TRACING%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sTIME STEPS%s\n",KBLD,KGRN,KNRM);

		// construct vtk mesh
		//vtkSmartPointer<vtkUnstructuredGrid> vtk_mesh = create_vtk_ugrid(lg);

		// get particles from emitter
		lg->msg(2,"%sspawn particles%s\n",KBLU,KNRM);
		particles_ = emitter_->spawn_particles(num_particles_);
		lg->msg("number of particles: %05llu\n",num_particles_);
		lg->msg(-2,"\n");
		
		// tracking direction
		for(arma::sword dir = -1;dir<=1;dir+=2){
			// time and initial time step
			double dt = dir*step_size_/arma::datum::c_0;
			
			// set time and iteration to zero
			double t = 0; arma::uword iter = 0;

			// start time stepping
			for(;;){
				// walk over particles and find alive
				arma::Row<arma::uword> is_alive(num_particles_,arma::fill::zeros);
				for(arma::uword i=0;i<num_particles_;i++){
					if(dir==1){
						is_alive(i) = particles_(i).is_alive_end();
					}else{
						is_alive(i) = particles_(i).is_alive_start();
					}
				}
				
				// find alive indexes
				const arma::Row<arma::uword> idx_alive = 
					arma::find(is_alive).t();
				const arma::uword num_alive = idx_alive.n_elem;

				// is there any track alive
				if(num_alive==0)break;

				// print header on first iteration 
				// after it is confirmed that there are alive
				// particles
				if(iter==0){
					// header
					if(dir==1){
						lg->msg(2,"%strace particles forward%s\n",KBLU,KNRM);
					}else{
						lg->msg(2,"%strace particles backward%s\n",KBLU,KNRM);
					}
					lg->msg("%s%4s %9s %5s%s\n",KBLD,"iter","time","alive",KNRM);			
				}

				// display time
				if(iter%5==0)lg->msg("%04llu %+9.2e %05llu\n",iter,t,num_alive);

				// allocate coordinate, velocity matrix
				arma::Mat<double> RV(6,num_alive);
				arma::Row<double> m(num_alive), q(num_alive);
				for(arma::uword i=0;i<num_alive;i++){
					// get particle index from alive list
					const arma::uword idx = idx_alive(i);

					// get particle position and velocity
					if(dir==1){
						RV.submat(0,i,2,i) = particles_(idx).get_last_coord(); // [m]
						RV.submat(3,i,5,i) = particles_(idx).get_last_velocity()*arma::datum::c_0; // [m/s]
					}else{
						RV.submat(0,i,2,i) = particles_(idx).get_first_coord(); // [m]
						RV.submat(3,i,5,i) = particles_(idx).get_first_velocity()*arma::datum::c_0; // [m/s]
					}

					// calculate Lorentz factor
					const double vmag = arma::as_scalar(cmn::Extra::vec_norm(RV.submat(3,i,5,i)));
					const double gamma = 1.0/std::sqrt(1.0 - (vmag*vmag)/(arma::datum::c_0*arma::datum::c_0));
					
					// calculate rest mass [kg]
					const double rest_mass = particles_(idx).get_rest_mass()*
						1e9*arma::datum::eV/(arma::datum::c_0*arma::datum::c_0);
					
					// calculate relativistic mass [kg]
					m(i) = gamma*rest_mass;

					// calculate particle charge in [C]
					q(i) = particles_(idx).get_charge()*arma::datum::ec;
				}
				
				// calculate next position and time using fourth order runge kutta
				const arma::Mat<double> dRV = runge_kutta(t, dt, RV, q, m, vtk_mesh);
				RV += dRV; t += dt;

				// adapt stepsize
				dt *= step_size_/arma::max(cmn::Extra::vec_norm(dRV.rows(0,2)));

				// store to particles
				for(arma::uword i=0;i<num_alive;i++){
					// get particle index
					const arma::uword idx = idx_alive(i);

					// particle is alive insert next position and velocity
					if(arma::any(dRV.col(i)!=0)){
						const arma::Col<double>::fixed<3> Rn = RV.submat(0,i,2,i);
						const arma::Col<double>::fixed<3> Vn = RV.submat(3,i,5,i)/arma::datum::c_0;
						if(dir==1){
							particles_(idx).set_next_coord(t,Rn,Vn);
						}else{
							particles_(idx).set_prev_coord(t,Rn,Vn);
						}
					}

					// the particle went off the grid
					else{
						if(dir==1){
							particles_(idx).terminate_end();
						}else{
							particles_(idx).terminate_start();
						}
					}
				}

				// increment iteration
				iter++;
			}

			// done
			if(iter>0){
				lg->msg("%s= tracing complete%s\n",KCYN,KNRM);
				lg->msg(-2,"\n");
			}
		}

		// end
		lg->msg(-4);
	}

	// get a particle from the list
	Particle CalcTracks::get_particle(const arma::uword idx) const{
		return particles_(idx);
	}

	// write surface to VTK file
	ShVTKUnstrPr CalcTracks::export_vtk_tracks(cmn::ShLogPr lg) const{
		// header
		lg->msg(2,"%s%sEXPORT PARTICLE TRACKS%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sconstructing mesh%s",KBLU,KNRM);

		// create VTK data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// allocate field arrays
		lg->msg("combine trajectories\n");
		arma::field<arma::Mat<double> > Rfld(1,num_particles_);
		arma::field<arma::Mat<arma::uword> > nfld(1,num_particles_);
		arma::uword idx_offset = 0;
		for(arma::uword i=0;i<num_particles_;i++){
			Rfld(i) = particles_(i).get_track_coords();
			//std::cout<<Rfld(i).t()<<std::endl;
			if(Rfld(i).n_cols>1){
				nfld(i) = arma::join_vert(
					arma::regspace<arma::Row<arma::uword> >(0,Rfld(i).n_cols-2),
					arma::regspace<arma::Row<arma::uword> >(1,Rfld(i).n_cols-1));
			}else{
				nfld(i) = arma::Mat<arma::uword>(2,0);
			}
			nfld(i) += idx_offset; idx_offset += Rfld(i).n_cols;
		}

		// combine
		const arma::Mat<double> R = cmn::Extra::field2mat(Rfld);
		const arma::Mat<arma::uword> n = cmn::Extra::field2mat(nfld);

		// Create a unstructured grid object
		vtkSmartPointer<vtkUnstructuredGrid> ugrid =	
			vtkSmartPointer<vtkUnstructuredGrid>::New();

		// export points as well
		lg->msg("coordinates\n");
		vtk_data->set_nodes(R);

		// create mesh
		lg->msg("line elements\n");
		const arma::uword line_type = 3;
		vtk_data->set_elements(n,line_type);
		
		// mesh setup done
		lg->msg(-2);

		// end
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

	// write output file
	void CalcTracks::write(cmn::ShLogPr lg){
		// check if data directory set
		if(output_dir_.empty())return;

		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES:%s\n",KBLD,KGRN,KNRM);
		
		// check output directory
		if(output_times_.is_empty())rat_throw_line("output times are not set");

		// create output directory
		cmn::Extra::create_directory(output_dir_);

		// report
		lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

		// settings report
		display_settings(lg);

		// report
		lg->msg(2, "%swriting meshdata%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// output filename
		std::string fname  = output_fname_;

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKUnstrPr vtk_mesh = export_vtk();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_mesh->write(output_dir_ + fname + "_tmsh");
		}

		// return
		lg->msg(-2,"\n");

		// header
		lg->msg(2, "%swriting trackdata%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// update name
		fname  = output_fname_;

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKUnstrPr vtk_tracks = export_vtk_tracks();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_tracks->write(output_dir_ + fname + "_trck");
		}

		// return
		lg->msg(-6,"\n"); 
	}

	// get type
	std::string CalcTracks::get_type(){
		return "mdl::calctracks";
	}

	// method for serialization into json
	void CalcTracks::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		CalcMesh::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["num_particles"] = (unsigned int)num_particles_;
		js["step_size"] = step_size_;

		// subnodes
		js["emitter"] = cmn::Node::serialize_node(emitter_, list);
	}

	// method for deserialisation from json
	void CalcTracks::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		CalcMesh::deserialize(js,list,factory_list);

		// properties
		num_particles_ = js["num_particles"].asUInt64();
		step_size_ = js["step_size"].asDouble();

		// subnodes
		emitter_ = cmn::Node::deserialize_node<Emitter>(js["emitter"], list, factory_list);
	}


}}
