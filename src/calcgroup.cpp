/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcGroup::CalcGroup(){

	}

	// factory
	ShCalcGroupPr CalcGroup::create(){
		return std::make_shared<CalcGroup>();
	}

	// add claculations
	void CalcGroup::add_calculation(ShCalcPr calc){
		// check input
		if(calc==NULL)rat_throw_line("calc points to zero");

		// get number of calcs
		const arma::uword num_calcs = calcs_.n_elem;

		// allocate new source list
		ShCalcPrList new_calcs(num_calcs + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_calcs;i++)new_calcs(i) = calcs_(i);
		new_calcs(num_calcs) = calc;
		
		// set new source list
		calcs_ = new_calcs;
	}

	// calculate and write
	void CalcGroup::calculate_and_write(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->calculate_and_write(lg);
	}

	// run calculation
	void CalcGroup::calculate(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->calculate(lg);
	}

	// run calculation
	void CalcGroup::write(cmn::ShLogPr lg){
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			calcs_(i)->write(lg);
	}

	// get type
	std::string CalcGroup::get_type(){
		return "mdl::calcgroup";
	}

	// method for serialization into json
	void CalcGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// properties
		js["type"] = get_type();
		
		// subnodes
		for(arma::uword i=0;i<calcs_.n_elem;i++)
			js["calcs"].append(cmn::Node::serialize_node(calcs_(i), list));
	}

	// method for deserialisation from json
	void CalcGroup::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// subnodes
		const arma::uword num_calcs = js["calcs"].size();
		calcs_.set_size(num_calcs);
		for(arma::uword i=0;i<num_calcs;i++)
			calcs_(i) = cmn::Node::deserialize_node<Calc>(js["calcs"].get(i,0), list, factory_list);
	}

}}
