/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcharmonics.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcHarmonics::CalcHarmonics(){
		fmm_stngs_->set_num_exp(8); set_name("harm");
	}

	// constructor
	CalcHarmonics::CalcHarmonics(ShModelPr source_model, ShPathPr base){
		set_source_model(source_model); set_base(base);
		fmm_stngs_->set_num_exp(8); set_name("harm");
	}

	// factory
	ShCalcHarmonicsPr CalcHarmonics::create(){
		return std::make_shared<CalcHarmonics>();
	}

	// factory
	ShCalcHarmonicsPr CalcHarmonics::create(ShModelPr source_model, ShPathPr base){
		return std::make_shared<CalcHarmonics>(source_model, base);
	}


	// set path
	void CalcHarmonics::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base path points to zero");
		base_ = base;
	}

	// set radius
	void CalcHarmonics::set_radius(const double radius){
		if(radius<=0)rat_throw_line("radius must be larger than zero");
		radius_ = radius;
	}

	// set ntheta
	void CalcHarmonics::set_num_theta(const arma::uword num_theta){
		if(num_theta<=0)rat_throw_line("number of azymuthal elements must be larger than zero");
		num_theta_ = num_theta;
	}

	// set radius
	void CalcHarmonics::set_compensate_curvature(const bool compensate_curvature){
		compensate_curvature_ = compensate_curvature;
	}

	// harmonics calculation
	void CalcHarmonics::setup(cmn::ShLogPr lg){
		// check settings
		if(radius_==0)rat_throw_line("radius is not set");
		if(base_==NULL)rat_throw_line("base is not set");
			
		// header
		lg->msg(2,"%s%sSETTING UP HARMONICS CALCULATION%s\n",KBLD,KGRN,KNRM);
		
		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("radius: %s%2.2f [mm]%s\n",KYEL,1e3*radius_,KNRM);
		lg->msg("number of points: %s%llu%s\n",KYEL,num_theta_,KNRM);
		lg->msg(-2,"\n");

		// create circle
		theta_ = arma::linspace<arma::Row<double> >
			(0,(((double)num_theta_-1.0)/(double)num_theta_)*2*arma::datum::pi,num_theta_);
		const arma::Row<double> rho = arma::Row<double>(num_theta_,arma::fill::ones)*radius_;
		const arma::Row<double> u = rho%arma::cos(theta_);
		const arma::Row<double> v = rho%arma::sin(theta_);

		// create frame for base line
		ShFramePr gen = base_->create_frame();

		// combine sections
		gen->combine();

		// get line properties
		Rb_ = gen->get_coords(0);
		Lb_ = gen->get_direction(0);
		Nb_ = gen->get_normal(0);
		Db_ = gen->get_transverse(0);

		// calculate length
		const arma::Row<double> dl = arma::sqrt(arma::sum(arma::pow(arma::diff(Rb_,1,1),2),0));
		ell_ = arma::join_horiz(arma::Row<double>{0},arma::cumsum(dl));

		// number of nodes in basepath
		num_ell_ = Rb_.n_cols;

		// allocate position matrices
		arma::Mat<double> x(num_ell_,num_theta_);
		arma::Mat<double> y(num_ell_,num_theta_);
		arma::Mat<double> z(num_ell_,num_theta_);
		
		// walk over nodes
		for(arma::uword i=0;i<num_ell_;i++){
			x.row(i) = Rb_(0,i) + Db_(0,i)*v + Nb_(0,i)*u;
			y.row(i) = Rb_(1,i) + Db_(1,i)*v + Nb_(1,i)*u;
			z.row(i) = Rb_(2,i) + Db_(2,i)*v + Nb_(2,i)*u;
		}

		// get length
		const arma::Mat<double> ell_diff = arma::sqrt(
			arma::diff(x,1,0)%arma::diff(x,1,0) + 
			arma::diff(y,1,0)%arma::diff(y,1,0) + 
			arma::diff(z,1,0)%arma::diff(z,1,0)); 
		ellt_ = arma::reshape((arma::join_vert(arma::Row<double>(num_theta_,arma::fill::zeros),ell_diff) + 
			arma::join_vert(ell_diff,arma::Row<double>(num_theta_,arma::fill::zeros)))/2,1,num_ell_*num_theta_);

		// convert to coordinates
		Rt_.set_size(3,num_ell_*num_theta_);
		Rt_.row(0) = arma::reshape(x,1,num_ell_*num_theta_);
		Rt_.row(1) = arma::reshape(y,1,num_ell_*num_theta_);
		Rt_.row(2) = arma::reshape(z,1,num_ell_*num_theta_);
		
		// create targets
		set_field_type("H",arma::Row<arma::uword>{3});

		// display input path
		lg->msg(2,"%sInput Path%s\n",KBLU,KNRM);
		lg->msg("number of points: %s%llu%s\n",KYEL,num_ell_,KNRM);
		lg->msg("total path length: %s%2.2f%s\n",KYEL,arma::sum(dl),KNRM);
		lg->msg(-2);

		// done
		lg->msg(-2,"\n");
	}

	// set the time
	void CalcHarmonics::set_time(const double time){
		time_ = time; post_process();
	}

	// post process
	void CalcHarmonics::post_process(){
		// check if calculation was done
		if(!has_field())rat_throw_line("field was not calculated");

		// get results
		arma::Mat<double> B = get_field("B");
		assert(B.n_cols==num_ell_*num_theta_);

		// calculate quasi-integrated harmonics
		if(compensate_curvature_)B.each_row() %= ellt_;

		// reshape to matrices
		const arma::Mat<double> Bx = arma::reshape(B.row(0),num_ell_,num_theta_);
		const arma::Mat<double> By = arma::reshape(B.row(1),num_ell_,num_theta_);
		const arma::Mat<double> Bz = arma::reshape(B.row(2),num_ell_,num_theta_);
		
		// get in plane components of the field
		// we ignore the out of plane component here
		// this is why it is called pseudo-harmonics
		arma::Mat<double> Bu(num_ell_,num_theta_);
		arma::Mat<double> Bv(num_ell_,num_theta_);
		for(arma::uword i=0;i<num_ell_;i++){
			// allocate
			arma::Mat<double> Bt(3,num_theta_); 
			arma::Mat<double> Dext(3,num_theta_), Next(3,num_theta_);

			// get magetic field
			Bt.row(0) = Bx.row(i); 
			Bt.row(1) = By.row(i); 
			Bt.row(2) = Bz.row(i);
			
			// get orientation vectors
			Dext.each_col() = Db_.col(i); 
			Next.each_col() = Nb_.col(i);
			
			// calculate dot products
			Bu.row(i) = cmn::Extra::dot(Bt,Next); 
			Bv.row(i) = cmn::Extra::dot(Bt,Dext);
		}

		// calculate radial component
		const arma::Mat<double> Br = 
			Bu.each_row()%arma::cos(theta_) + 
			Bv.each_row()%arma::sin(theta_);

		// check radial field
		assert(Br.is_finite());

		// fourier transform (applies to columns)
		const arma::Mat<std::complex<double> > M = arma::fft(Br.t()).st();
		An_ = (2.0/num_theta_)*arma::real(M.cols(0,num_theta_/2-1));
		Bn_ = (2.0/num_theta_)*arma::imag(M.cols(0,num_theta_/2-1));

		// divide by length to keep units of [T]
		if(compensate_curvature_){
			arma::Col<double> ell = arma::diff(ell_.t(),1,0);
			ell = (arma::join_vert(arma::Col<double>{0}, ell) + 
				arma::join_vert(ell,arma::Col<double>{0}))/2; 
			An_.each_col()/=ell; Bn_.each_col()/=ell;
		}
	}

	// display harmonics in terminal/log
	void CalcHarmonics::display(cmn::ShLogPr lg, const arma::uword nmax) const{
		// header
		lg->msg(2,"%s%sTERMINAL OUTPUT%s\n",KBLD,KGRN,KNRM);
		
		// walk over time steps
		lg->msg(2,"%sintegrated harmonic content%s\n",KBLU,KNRM);
		lg->msg("%s%3s %8s %8s %8s %8s%s\n",KBLD,"n","An","Bn","an","bn",KNRM);
		arma::Row<double> An,Bn; get_harmonics(An,Bn);
		const arma::uword idx = arma::index_max(arma::max(arma::abs(An),arma::abs(Bn)));
		const double ABmax = std::max(std::abs(An(idx)),std::abs(Bn(idx)));
		const arma::Row<double> an = 1e4*An/ABmax; 
		const arma::Row<double> bn = 1e4*Bn/ABmax;
		assert(nmax<Bn.n_elem);
		for(arma::uword i=1;i<=nmax;i++){
			if(idx==i){
				if(std::abs(An(idx))>std::abs(Bn(idx)))lg->msg("%03i %s%+8.2e%s %+8.2e %+08.1f %+08.1f\n",i,KYEL,An(i),KNRM,Bn(i),an(i),bn(i));
				else lg->msg("%03i %+8.2e %s%+8.2e%s %+08.1f %+08.1f\n",i,An(i),KYEL,Bn(i),KNRM,an(i),bn(i));
			}
			else{
				lg->msg("%03i %+8.2e %+8.2e %+08.1f %+08.1f\n",i,An(i),Bn(i),an(i),bn(i));
			}
		}
		lg->msg("* in [Tm] at radius %2.2f [mm]\n",1e3*radius_);
		lg->msg(-2,"\n");
		lg->msg(-2,"\n");
	}


	// get harmonic content
	void CalcHarmonics::get_harmonics(
		arma::Row<double> &ell, arma::Mat<double> &An, arma::Mat<double> &Bn) const{
		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// copy matrices to output
		ell = ell_; An = An_; Bn = Bn_;
	}

	// getting integrated harmonics
	void CalcHarmonics::get_harmonics(arma::Row<double> &An, arma::Row<double> &Bn) const{
		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// integrate along ell
		An = arma::trapz(ell_,An_,0);
		Bn = arma::trapz(ell_,Bn_,0); 
	}

	// write surface to VTK file
	ShVTKTablePr CalcHarmonics::export_vtk_table(cmn::ShLogPr lg) const{
		// header
		lg->msg(2,"%s%sVTK HARMONICS TABLE EXPORT%s\n",KBLD,KGRN,KNRM);
		
		// header for data preparation
		lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// create table
		ShVTKTablePr vtk_table = VTKTable::create();

		// set table size
		vtk_table->set_num_rows(num_ell_);

		// add length
		lg->msg("longitudinal coordinates\n");
		vtk_table->set_data(ell_.t(),"ell");
		
		// add harmonic content
		lg->msg("skew harmonic components\n");
		for(arma::uword i=1;i<=num_max_;i++){
			std::string data_name; 
			if(i<10)data_name = "A0" + std::to_string(i) + " [T]";
			else data_name = "A" + std::to_string(i) + " [T]";
			vtk_table->set_data(An_.col(i),data_name);
		}
		
		// add harmonic content
		lg->msg("main harmonic components\n");
		for(arma::uword i=1;i<=num_max_;i++){
			std::string data_name; 
			if(i<10)data_name = "B0" + std::to_string(i) + " [T]";
			else data_name = "B" + std::to_string(i) + " [T]";
			vtk_table->set_data(Bn_.col(i),data_name);
		}

		// done preparing data
		lg->msg(-2);

		// done exporting
		lg->msg(-2,"\n");

		// return table object
		return vtk_table;
	}

	// export points/lines to vtk file
	ShVTKUnstrPr CalcHarmonics::export_vtk_coord(cmn::ShLogPr lg) const{
		// header
		lg->msg(2,"%s%sVTK HARMONICS COORDINATES EXPORT%s\n",KBLD,KGRN,KNRM);
		
		// header for data preparation
		lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

		// create vtk data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// check if harmonics where calculated
		if(An_.is_empty())rat_throw_line("harmonics not calculated");
		if(Bn_.is_empty())rat_throw_line("harmonics not calculated");

		// export node coordinates
		lg->msg("coordinates\n");
		vtk_data->set_nodes(Rt_);

		// create line elements
		lg->msg("line elements\n");
		arma::Mat<arma::uword> n(2,num_theta_*num_ell_);
		for(arma::uword i=0;i<num_ell_;i++){
			// connect intermediate
			for(arma::uword j=0;j<num_theta_-1;j++)
				n.col(i*num_theta_ + j) = arma::Col<arma::uword>::fixed<2>{j*num_ell_+i,(j+1)*num_ell_+i};

			// connect last to first
			n.col(i*num_theta_ + num_theta_-1) = 
				arma::Col<arma::uword>::fixed<2>{(num_theta_-1)*num_ell_+i,i};
		}
		const arma::uword line_type = 3;
		vtk_data->set_elements(n,line_type);

		// magnetic field data
		lg->msg("magnetic flux density\n");
		const arma::Mat<double> B = get_field("B");
		vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");

		// harmonic content
		arma::Mat<double> MA(num_theta_*num_ell_,num_max_+1);
		arma::Mat<double> MB(num_theta_*num_ell_,num_max_+1);
		for(arma::uword j=0;j<num_ell_;j++){
			for(arma::uword k=0;k<num_theta_;k++){
				MA.row(k*num_ell_ + j) = An_.submat(j,0,j,num_max_);
				MB.row(k*num_ell_ + j) = Bn_.submat(j,0,j,num_max_);
			}
		}

		// convert harmonics to VTK
		// skew harmonics
		lg->msg("skew harmonics\n");
		for(arma::uword j=1;j<=num_max_;j++){
			std::string name; 
			if(j<10)name = "A0" + std::to_string(j) + " [T]";
			else name = "A" + std::to_string(j) + " [T]";
			vtk_data->set_nodedata(MA.col(j).t(),name);
		}

		// main harmonics
		lg->msg("main harmonics\n");
		for(arma::uword j=1;j<=num_max_;j++){
			std::string name; 
			if(j<10)name = "B0" + std::to_string(j) + " [T]";
			else name = "B" + std::to_string(j) + " [T]";
			vtk_data->set_nodedata(MB.col(j).t(),name);
		}

		// mesh done
		lg->msg(-2);

		// done exporting
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

	// export a vtk with times
	void CalcHarmonics::write(cmn::ShLogPr lg){
		// check if data directory set
		if(output_dir_.empty())return;

		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
		// check output directory
		if(output_times_.is_empty())rat_throw_line("output times are not set");

		// create output directory
		cmn::Extra::create_directory(output_dir_);

		// output filename
		std::string fname  = output_fname_;

		// set time
		set_time(output_times_(0));

		// display harmonics at first time
		display(lg);

		// report
		lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

		// settings report
		display_settings(lg);

		// report
		lg->msg(2, "%swriting harmonics geometry%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKUnstrPr vtk_harm = export_vtk_coord();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_harm->write(output_dir_ + output_fname_ + "_hmsh");
		}
		lg->msg(-2,"\n");

		lg->msg(2, "%swriting harmonics table%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKTablePr vtk_table = export_vtk_table();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_table->write(output_dir_ + output_fname_ + "_htb");
		}

		// return
		lg->msg(-6,"\n"); 
	}

	// get type
	std::string CalcHarmonics::get_type(){
		return "mdl::calcharmonics";
	}

	// method for serialization into json
	void CalcHarmonics::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		CalcFieldMap::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["radius"] = radius_;
		js["num_theta"] = (unsigned int)num_theta_;
		js["compensate_curvature"] = compensate_curvature_;

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
	}

	// method for deserialisation from json
	void CalcHarmonics::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// serialize fieldmap
		CalcFieldMap::deserialize(js,list,factory_list);

		// properties
		radius_ = js["radius"].asDouble();
		num_theta_ = js["num_theta"].asUInt64();
		compensate_curvature_ = js["compensate_curvature"].asBool();

		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list);
	}


}}
