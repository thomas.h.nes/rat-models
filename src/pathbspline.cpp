/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathbspline.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathBSpline::PathBSpline(){

	}

	// constructor
	PathBSpline::PathBSpline(
		const arma::Col<double>::fixed<3> pstart, 
		const arma::Col<double>::fixed<3> lstart,
		const arma::Col<double>::fixed<3> nstart,
		const arma::Col<double>::fixed<3> pend, 
		const arma::Col<double>::fixed<3> lend,
		const arma::Col<double>::fixed<3> nend,
		const double str1, const double str2, 
		const double element_size, const double offset){

		// check input
		assert(element_size>0); assert(str1>0); assert(str2>0);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(lend))-1.0)<1e-6);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(nend))-1.0)<1e-6);

		// set values
		pstart_ = pstart; lstart_ = lstart; nstart_ = nstart;
		pend_ = pend; lend_ = lend; nend_ = nend;
		str1_ = str1; str2_ = str2; 
		element_size_ = element_size;
		offset_ = offset;
	}

	// constructor
	PathBSpline::PathBSpline(
		const double pstartx, const double pstarty,	const double pstartz,
		const double lstartx, const double lstarty,	const double lstartz,
		const double nstartx, const double nstarty,	const double nstartz,
		const double pendx, const double pendy,	const double pendz,
		const double lendx, const double lendy,	const double lendz,
		const double nendx, const double nendy,	const double nendz,
		const double str1, const double str2, 
		const double element_size, const double offset){

		// make vectors
		pstart_ = arma::Col<double>::fixed<3>{pstartx,pstarty,pstartz};
		lstart_ = arma::Col<double>::fixed<3>{lstartx,lstarty,lstartz};
		nstart_ = arma::Col<double>::fixed<3>{nstartx,nstarty,nstartz};
		pend_ = arma::Col<double>::fixed<3>{pendx,pendy,pendz};
		lend_ = arma::Col<double>::fixed<3>{lendx,lendy,lendz};
		nend_ = arma::Col<double>::fixed<3>{nendx,nendy,nendz};

		// check input
		assert(element_size>0); assert(str1>0); assert(str2>0);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(lend_))-1.0)<1e-6);
		assert(std::abs(arma::as_scalar(cmn::Extra::vec_norm(nend_))-1.0)<1e-6);

		// set values
		str1_ = str1; str2_ = str2; 
		element_size_ = element_size;
		offset_ = offset;
	}



	// factory
	ShPathBSplinePr PathBSpline::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathBSpline>();
	}

	// factory with dimension input
	ShPathBSplinePr PathBSpline::create(
		const arma::Col<double>::fixed<3> pstart, 
		const arma::Col<double>::fixed<3> lstart,
		const arma::Col<double>::fixed<3> nstart,
		const arma::Col<double>::fixed<3> pend, 
		const arma::Col<double>::fixed<3> lend,
		const arma::Col<double>::fixed<3> nend,
		const double str1, const double str2, 
		const double element_size, const double offset){
		return std::make_shared<PathBSpline>(pstart,lstart,nstart,pend,lend,nend,str1,str2,element_size,offset);
	}

	// factory with dimension input
	ShPathBSplinePr PathBSpline::create(
		const double pstartx, const double pstarty,	const double pstartz,
		const double lstartx, const double lstarty,	const double lstartz,
		const double nstartx, const double nstarty,	const double nstartz,
		const double pendx, const double pendy,	const double pendz,
		const double lendx, const double lendy,	const double lendz,
		const double nendx, const double nendy,	const double nendz,
		const double str1, const double str2, 
		const double element_size, const double offset){
		return std::make_shared<PathBSpline>(
			pstartx,pstarty,pstartz,lstartx,lstarty,lstartz,nstartx,nstarty,nstartz,
			pendx,pendy,pendz,lendx,lendy,lendz,nendx,nendy,nendz,str1,str2,element_size,offset);
	}


	// method for creating the frame
	ShFramePr PathBSpline::create_frame() const{
		// get spline length
		const double ell = get_length();

		// calculate number of points
		const arma::uword num_points = std::max(2,(int)std::ceil(ell/element_size_)+1);
		
		// make regular time array
		const arma::Row<double> treq = arma::linspace<arma::Row<double> >(0,1,num_points);

		// get regular times
		arma::Mat<double> R,L,N,D;
		get_frame_from_time(R,L,N,D,treq,true);

		// check if frame are truely orthogonal
		assert(arma::all(cmn::Extra::dot(L,N)<1e-6));
		assert(arma::all(cmn::Extra::dot(L,D)<1e-6));
		assert(arma::all(cmn::Extra::dot(D,N)<1e-6));

		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// calculate curve length (with offset)
	double PathBSpline::get_length() const{
		// regular time array
		arma::Row<double> t = arma::linspace<arma::Row<double> >(0,1,Nreg_);

		// get frame
		arma::Mat<double> R,L,N,D;
		get_frame_from_time(R,L,N,D,t,false);

		// offset frame
		arma::Mat<double> Roffset = R + offset_*N;

		// calculate length
		double ell1 = arma::as_scalar(arma::sum(arma::sqrt(arma::sum(arma::pow(arma::diff(Roffset,1,1),2),0)),1));
		double ell2 = arma::as_scalar(arma::sum(arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0)),1));

		// return length
		return std::max(ell1,ell2);
	}

	// calculation
	void PathBSpline::get_frame_from_time(
		arma::Mat<double> &R, arma::Mat<double> &L, 
		arma::Mat<double> &N, arma::Mat<double> &D,
		const arma::Row<double> &t, const bool make_regular) const{

		// check input
		assert(str1_>0);
		assert(str2_>0);

		// get anchor 1 and anchor 2 coordinates
		arma::Col<double>::fixed<3> anchor1 = pstart_ + bc_*str1_*lstart_;
		arma::Col<double>::fixed<3> anchor2 = pend_ - bc_*str2_*lend_;

		// calculate parameters connecting the shapes
		arma::Col<double>::fixed<3> c = 3*(anchor1-pstart_);
		arma::Col<double>::fixed<3> b = 3*(anchor2-anchor1)-c;
		arma::Col<double>::fixed<3> a = pend_-pstart_-c-b;  

		// use regular time arrays
		arma::Row<double> tt;
		if(make_regular)tt = regular_times(a,b,c,t); else tt = t;

		// calculate length
		R = calculate_coords(a,b,c,tt);
		L = calculate_direction(a,b,c,tt);
		
		// normalization
		L.each_row()/=cmn::Extra::vec_norm(L);

		// apply offset
		R.each_col()+=pstart_;
		
		// allocate acceleration vectors
		arma::Mat<double> A(3,R.n_cols);

		// walk over nodes
		if(!in_plane_){
			// walk over nodes and set acceleration
			A.col(0) = -nstart_;
			for(arma::uword i=1;i<R.n_cols;i++){
				A.col(i) = cmn::Extra::cross(cmn::Extra::cross(A.col(i-1),-L.col(i)),L.col(i));
			}
		}else{
			// get out of plane vector
			arma::Col<double>::fixed<3> plane_vec = cmn::Extra::cross(lstart_,nstart_);
			assert(std::abs(arma::as_scalar(cmn::Extra::dot(plane_vec,lend_))-1.0)>1e-6);

			// walk over nodes and set acceleration
			for(arma::uword i=0;i<R.n_cols;i++){
				A.col(i) = cmn::Extra::cross(L.col(i),plane_vec);
			}
		}

		// normal vector
		// arma::Mat<double> A = calculate_acceleration(a,b,c,tt);
		D = cmn::Extra::cross(L,A);

		// normalization
		L.each_row()/=cmn::Extra::vec_norm(L);
		D.each_row()/=cmn::Extra::vec_norm(D);

		// calculate normal vectors
		N = cmn::Extra::cross(L,D);

		// calculate direction of rotation
		double sign1 = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(L.head_cols(1),cmn::Extra::cross(N.head_cols(1), nstart_))));
		double sign2 = cmn::Extra::sign(arma::as_scalar(cmn::Extra::dot(L.tail_cols(1),cmn::Extra::cross(N.tail_cols(1), nend_))));

		// apply rotation if needed
		double rot1 = sign1*std::acos(arma::as_scalar(cmn::Extra::dot(N.head_cols(1),nstart_)));
		double rot2 = sign2*std::acos(arma::as_scalar(cmn::Extra::dot(N.tail_cols(1),nend_)));

		// add extra twists
		rot2 += 2*num_twist_*arma::datum::pi;

		// check if rotation needed
		if(std::abs(rot1)>1e-9 || std::abs(rot2)>1e-9){
			// calculate rotation to apply using spline fit
			double brot = 3*(rot2-rot1);
			double arot = rot2-rot1-brot;
			arma::Row<double> rot = arot*t%t%t + brot*t%t + rot1;

			// apply rotation
			for(arma::uword i=0;i<t.n_elem;i++){
				arma::Mat<double>::fixed<3,3> M = cmn::Extra::create_rotation_matrix(L.col(i),rot(i));
				N.col(i) = M*N.col(i); D.col(i) = M*D.col(i);
			}
		}


	}

	// calculate coordinates
	arma::Row<double> PathBSpline::calculate_lengths(
		const arma::Col<double>::fixed<3> &a,
		const arma::Col<double>::fixed<3> &b,
		const arma::Col<double>::fixed<3> &c,
		const arma::Row<double> &t) const{

		// calculate
		arma::Mat<double> R = calculate_coords(a,b,c,t);

		// calculate length
		arma::Row<double> ell = arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0));

		// return coordinates
		return ell;
	}

	// regular times
	arma::Row<double> PathBSpline::regular_times(
		const arma::Col<double>::fixed<3> &a,
		const arma::Col<double>::fixed<3> &b,
		const arma::Col<double>::fixed<3> &c,
		const arma::Row<double> &treq) const{

		// calculate line with regular spaced times
		arma::Row<double> treg = arma::linspace<arma::Row<double> >(0,1,Nreg_);

		// calculate lengths for regular spaced times
		arma::Row<double> ell = calculate_lengths(a,b,c,treg);

		// calculate accumalation of length
		arma::Row<double> rp(Nreg_,arma::fill::zeros);
		rp.tail_cols(Nreg_-1) = arma::cumsum(ell)/arma::sum(ell);

		// allocate output
		arma::Col<double> tset;

		// use interpolation
		arma::interp1(rp.t(),treg.t(),treq.t(),tset,"linear");
		
		// fix first and last point
		tset(0) = 0; tset.tail(1) = 1;
		
		// check if sorted
		assert(tset.is_sorted("ascend"));
		
		// return time
		return tset.t();
	}


	// calculate coordinates
	arma::Mat<double> PathBSpline::calculate_coords(
		const arma::Col<double>::fixed<3> &a,
		const arma::Col<double>::fixed<3> &b,
		const arma::Col<double>::fixed<3> &c,
		const arma::Row<double> &t) const{

		// calculate
		arma::Mat<double> R(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			R.row(i) = a(i)*t%t%t + b(i)*t%t + c(i)*t;

		// return coordinates
		return R;
	}

	// calculate direction
	arma::Mat<double> PathBSpline::calculate_direction(
		const arma::Col<double>::fixed<3> &a,
		const arma::Col<double>::fixed<3> &b,
		const arma::Col<double>::fixed<3> &c,
		const arma::Row<double> &t) const{

		// calculate
		arma::Mat<double> L(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			L.row(i) = 3*a(i)*t%t + 2*b(i)*t + c(i);

		// return velocity
		return L;    
	}

	// calculate acceleration
	arma::Mat<double> PathBSpline::calculate_acceleration(
		const arma::Col<double>::fixed<3> &a,
		const arma::Col<double>::fixed<3> &b,
		const arma::Col<double>::fixed<3> &,
		const arma::Row<double> &t) const{

		// calculate
		arma::Mat<double> A(3,t.n_elem);
		for(arma::uword i=0;i<3;i++)
			A.row(i) = 6*a(i)*t + 2*b(i);

		// return velocity
		return A;    
	}

	// set number of additional twists
	void PathBSpline::set_num_twist(const arma::sword num_twist){
		num_twist_ = num_twist;
	}

	// in plane
	void PathBSpline::set_in_plane(const bool in_plane){
		in_plane_ = in_plane;
	}

	// set path offset
	void PathBSpline::set_element_size(const double element_size){
		element_size_ = element_size;
	}

	// set path offset
	void PathBSpline::set_offset(const double offset){
		offset_ = offset;
	}

}}