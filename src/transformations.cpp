/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Transformations::Transformations(){

	}

	// factory
	ShTransformationsPr Transformations::create(){
		return std::make_shared<Transformations>();
	}

	// add transformation
	void Transformations::add_transformation(ShTransPr trans){
		// check input
		assert(trans!=NULL); 

		// allocate new source list
		arma::field<ShTransPr> new_trans(trans_.n_elem + 1);

		// set old and new sources
		for(arma::uword i=0;i<trans_.n_elem;i++)new_trans(i) = trans_(i);
		new_trans(trans_.n_elem) = trans;

		// set new source list
		trans_ = new_trans;
	}

	// add list of transformations
	void Transformations::add_transformation(arma::field<ShTransPr> trans){
		// allocate new source list
		arma::field<ShTransPr> new_trans(trans_.n_elem + trans.n_elem);

		// set old and new sources
		for(arma::uword i=0;i<trans_.n_elem;i++)new_trans(i) = trans_(i);
		for(arma::uword i=0;i<trans.n_elem;i++)new_trans(trans_.n_elem+i) = trans(i);

		// set new source list
		trans_ = new_trans;
	}

	// rotation arround vector
	void Transformations::add_rotation(const arma::Col<double>::fixed<3> &V, const double alpha){
		add_transformation(TransRotate::create(V,alpha));
	}

	// add a translation to this object
	void Transformations::add_translation(const double dx, const double dy, const double dz){
		add_transformation(TransTranslate::create(dx,dy,dz));
	}

	// add a translation to this object
	void Transformations::add_translation(const arma::Col<double>::fixed<3> &dR){
		add_transformation(TransTranslate::create(dR));
	}

	// rotation
	void Transformations::add_rotation(const double phi, const double theta, const double psi){
		add_transformation(TransRotate::create(phi,theta,psi));
	}

	// rotation arround vector
	void Transformations::add_rotation(const double ux, const double uy, const double uz, const double alpha){
		add_transformation(TransRotate::create(ux,uy,uz,alpha));
	}

	// rotation arround vector
	void Transformations::add_reflect_xy(){
		add_transformation(TransReflect::create(0,0,1));
	}

	// rotation arround vector
	void Transformations::add_reflect_yz(){
		add_transformation(TransReflect::create(1,0,0));
	}

	// rotation arround vector
	void Transformations::add_reflect_xz(){
		add_transformation(TransReflect::create(0,1,0));
	}

	// add full reverse of the path
	void Transformations::add_reverse(){
		add_transformation(TransReverse::create());
	}

	// flip cable
	void Transformations::add_flip(){
		add_transformation(TransFlip::create());
	}

	// get type
	std::string Transformations::get_type(){
		return "mdl::transformations";
	}

	// method for serialization into json
	void Transformations::serialize(Json::Value &js, cmn::SList &list) const{
		// set type
		js["type"] = get_type();

		// transformations
		for(arma::uword i=0;i<trans_.n_elem;i++){
			js["trans"].append(cmn::Node::serialize_node(trans_(i), list));
		}
	}

	// method for deserialisation from json
	void Transformations::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		const arma::uword num_trans = js["trans"].size();
		trans_.set_size(num_trans);
		for(arma::uword i=0;i<num_trans;i++){
			trans_(i) = cmn::Node::deserialize_node<Trans>(js["trans"].get(i,0), list, factory_list);
		}
	}
}}