/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcinductance.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcInductance::CalcInductance(){
		set_name("ind");
	}

	// constructor with model input
	CalcInductance::CalcInductance(ShModelPr model){
		set_model(model); set_name("ind");
	}

	// factory
	ShCalcInductancePr CalcInductance::create(){
		return std::make_shared<CalcInductance>();
	}

	// factory with model input
	ShCalcInductancePr CalcInductance::create(ShModelPr model){
		return std::make_shared<CalcInductance>(model);
	}

	// set model
	void CalcInductance::set_model(ShModelPr model){
		if(model==NULL)rat_throw_line("model points to NULL");
		model_ = model;
	}

	// set inductance type
	void CalcInductance::set_type(const IndType type){
		type_ = type;
	}

	// calculate mutual inductance between ms1 and ms2
	void CalcInductance::calculate(cmn::ShLogPr lg){
		// report
		lg->msg("%s%s--- INDUCTANCE CALCULATION ---%s\n",KBLD,KGRN,KNRM);

		// create timer
		arma::wall_clock timer;

		// set timer
		timer.tic();

		// report
		lg->msg(2,"%s%sGEOMETRY SETUP:%s\n",KBLD,KGRN,KNRM);

		// report
		lg->msg(2,"%s%sSOURCE MESHES%s\n",KBLD,KGRN,KNRM);

		// get coil meshes
		ShMeshPrList meshlist = model_->create_mesh();
		ShMeshCoilPrList meshes = Extra::filter<MeshCoil>(meshlist);

		// number of coils
		const arma::uword num_coils = meshes.n_elem;

		// create table with stats for each coil
		Mesh::display(lg, meshlist);

		// extract currents and set dIdt
		operating_currents_.set_size(num_coils);
		for(arma::uword i=0;i<num_coils;i++){
			operating_currents_(i) = meshes(i)->get_operating_current();
			//meshes(i)->set_operating_current(dIdt_);
		}

		// in case of drives
		if(type_==DRIVE){
			// report
			lg->msg(2,"%sgrouping by drive%s\n",KBLU,KNRM);

			// walk over meshes and extract drive
			arma::Row<arma::uword> coil_drive(num_coils);
			for(arma::uword i=0;i<num_coils;i++)
				coil_drive(i) = meshes(i)->get_drive_id();

			// get unique drives
			const arma::Row<arma::uword> drives = arma::unique(coil_drive);

			// get number of drives
			const arma::uword num_drives = drives.n_elem;

			// allocate source and target lists
			coil_groups_.set_size(num_drives);

			// find coils per drive
			for(arma::uword i=0;i<num_drives;i++){
				coil_groups_(i) = arma::find(coil_drive==drives(i));
				assert(!coil_groups_(i).is_empty());
			}
		}

		// in case of single coils
		else if(type_==COIL){
			// report
			lg->msg(2,"%susing single coils%s\n",KBLU,KNRM);

			// allocate source and target lists
			coil_groups_.set_size(num_coils);

			// set source and target coils
			for(arma::uword i=0;i<num_coils;i++)
				coil_groups_(i) = arma::Col<arma::uword>{i};

		}

		// in case of total calculation
		else if(type_==TOTAL){
			// report
			lg->msg(2,"%sgrouping all%s\n",KBLU,KNRM);

			// allocate source and target lists
			coil_groups_.set_size(1);

			// fill list
			coil_groups_(0) = arma::regspace<arma::Col<arma::uword> >(0,num_coils-1);
		}

		// return
		lg->msg(-6,"\n");

		// get number of groups
		const arma::uword num_groups = coil_groups_.n_elem;

		// allocate matrix
		M_.zeros(num_groups,num_groups);

		// walk over coil combinations and assemble list
		arma::Mat<arma::uword> ijlist(num_groups*num_groups,2);
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_groups;i++){
			for(arma::uword j=0;j<num_groups;j++){
				ijlist(cnt,0) = i; ijlist(cnt,1) = j; cnt++;
			}
		}

		// sanity check
		assert(cnt==num_groups*num_groups);

		// drop lower triangle
		ijlist = ijlist.rows(arma::find(ijlist.col(0)>=ijlist.col(1)));

		// disable log when parallel
		cmn::ShLogPr fmmlg = lg;
		if(use_parallel_)fmmlg = cmn::NullLog::create();
		
		// walk over list of coil combinations
		//for(arma::uword k=0;k<ijlist.n_rows;k++){
		cmn::parfor(0,ijlist.n_rows,use_parallel_,[&](int k, int){
			// get i and j
			const arma::uword i=ijlist(k,0); // source group
			const arma::uword j=ijlist(k,1); // target group

			// report
			lg->msg(2,"%s%sCALCULATING INDUCTANCE GRP%llu TO GRP%llu%s\n",KBLD,KGRN,i,j,KNRM);

			// count number of source and target coils
			const arma::uword num_source_coils = coil_groups_(i).n_elem;
			const arma::uword num_target_coils = coil_groups_(j).n_elem;

			// allocate sources
			fmm::ShCurrentSourcesPrList srcs(num_source_coils);

			// create source objects
			for(arma::uword l=0;l<num_source_coils;l++)
				srcs(l) = meshes(coil_groups_(i)(l))->create_current_sources(num_gauss_);

			// combine current sources
			fmm::ShCurrentSourcesPr combined_sources = fmm::CurrentSources::create(srcs);

			// allocate targets
			arma::field<arma::Mat<double> > Rt(1,num_target_coils);

			// get node coordinates of each target coil
			for(arma::uword l=0;l<num_target_coils;l++)
				Rt(l) = meshes(coil_groups_(j)(l))->get_coords();

			// combine target points into single target object
			fmm::ShMgnTargetsPr combined_targets = fmm::MgnTargets::create(cmn::Extra::field2mat(Rt));

			// set calculation to vector potential only
			combined_targets->set_field_type("A",3);

			// create multilevel fast multipole method object
			fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create();

			// add sources and targets to the calculation
			mlfmm->set_sources(combined_sources);
			mlfmm->set_targets(combined_targets);
			
			// setup multipole method
			mlfmm->setup(fmmlg);

			// run multipole method
			mlfmm->calculate(fmmlg);
			
			// get field and store 
			const arma::Mat<double> dAdt = combined_targets->get_field("A");
			
			// calculate induced voltage for each of the coils
			arma::uword cnt2 = 0;
			arma::Row<double> Vcoil(num_target_coils);
			for(arma::uword m=0;m<num_target_coils;m++){
				// get coil index
				const arma::uword idx = coil_groups_(j)(m);

				// get number of targets
				const arma::uword num_nodes = meshes(idx)->get_num_nodes();
				const arma::uword num_elements = meshes(idx)->get_num_elements();
				
				// get number of turns
				const arma::uword num_turns = meshes(idx)->get_number_turns();

				// get direction vector
				const arma::Mat<double> Ln = meshes(idx)->get_direction();
				assert(arma::all(arma::abs(cmn::Extra::vec_norm(Ln)-1.0)<1e-6));

				// get volume of elements
				const arma::Row<double> Ve = meshes(idx)->calc_volume();
				assert(arma::all(Ve>0));

				// get elements
				const arma::Mat<arma::uword> n = meshes(idx)->get_elements();

				// get cross sectional area
				//const double Acrss = meshes(idx)->get_total_area_crss();
				const double Acrss = meshes(idx)->get_area()->calculate_area();

				// calcute electric field in direction of longitudinal vector
				const arma::Row<double> En = cmn::Extra::dot(
					Ln, dAdt.cols(cnt2,cnt2+num_nodes-1)); // [V/m]

				// calculate elemental electric field
				arma::Row<double> Ee(num_elements);
				for(arma::uword l=0;l<num_elements;l++)
					Ee(l) = arma::as_scalar(arma::mean(En.cols(n.col(l)),1));

				// convert to voltage contribution
				const arma::Row<double> V = num_turns*Ee%Ve/Acrss;

				// calculate coil voltage
				Vcoil(m) = arma::accu(V);

				// increment counter
				cnt2 += num_nodes;
			}

			// calculate induced voltage
			// still need scale with cross area
			const double Vgrp = arma::accu(Vcoil);

			// calculate inductance
			M_(i,j) = Vgrp/operating_currents_(i);
			M_(j,i) = M_(i,j);

			// return
			lg->msg(-2);
		});

		// get calculation time
		double calculation_time = timer.toc();

		// report results
		lg->msg(2,"%s%sRESULTS%s\n",KBLD,KGRN,KNRM);

		// report matrix
		lg->msg(2,"%sInductance Matrix [mH]%s\n",KBLU,KNRM);

		// print matrix header
		lg->msg("%2s "," ");
		for(arma::uword i=0;i<M_.n_cols;i++){
			lg->msg(0,"%s%05llu%s ",KBLD,i,KNRM);
		}
		lg->msg(0,"\n");
		
		// print matrix data
		for(arma::uword i=0;i<M_.n_rows;i++){
			lg->msg("%s%02llu%s ",KBLD,i,KNRM);
			for(arma::uword j=0;j<M_.n_cols;j++){
				lg->msg(0,"%05.2f ",1e3*M_(i,j));
			}
			lg->msg(0,"\n");
		}

		// return
		lg->msg(-2,"\n");

		// print other data
		lg->msg(2,"%sStatistics%s\n",KBLU,KNRM);
		lg->msg("calculation time: %s%6.2f [s]%s\n",
			KYEL,calculation_time,KNRM);
		lg->msg("stored energy: %s%6.2f [kJ]%s\n",
			KYEL,1e-3*calc_stored_energy(),KNRM);

		// return
		lg->msg(-4,"\n");
	}

	// get inductance
	arma::Mat<double> CalcInductance::get_inductance_matrix() const{
		return M_;
	}

	// get total inductance
	double CalcInductance::get_inductance() const{
		return arma::accu(M_);
	}

	// get stored energy
	double CalcInductance::calc_stored_energy() const{
		// number of groups
		const arma::uword num_groups = coil_groups_.n_elem;

		// get currents
		arma::Row<double> Igrp(num_groups);
		for(arma::uword i=0;i<num_groups;i++)
			Igrp(i) = operating_currents_(coil_groups_(i)(0));

		// source currents
		arma::Mat<double> I1(num_groups,num_groups);
		arma::Mat<double> I2(num_groups,num_groups);

		// set matrices
		I1.each_row() = Igrp;
		I2.each_col() = Igrp.t();

		// calculate energy
		return arma::accu(0.5*M_%I1%I2);
	}

	// write surface to VTK file
	ShVTKTablePr CalcInductance::export_vtk_table(cmn::ShLogPr) const{
		// number of groups
		const arma::uword num_groups = coil_groups_.n_elem;

		// create table
		ShVTKTablePr vtk_table = VTKTable::create();

		// dump data
		for(arma::uword i=0;i<num_groups;i++){
			vtk_table->set_data(M_.col(i),"group " + std::to_string(i));
		}

		// return table
		return vtk_table;
	}

	// write output
	void CalcInductance::write(cmn::ShLogPr){
		M_.save(output_dir_ + output_fname_ + "_ind.txt", arma::csv_ascii);
		ShVTKTablePr vtk_table = export_vtk_table();
		vtk_table->write(output_dir_ + output_fname_ + "_ind");
	}

	// get type
	std::string CalcInductance::get_type(){
		return "mdl::calcinductance";
	}

	// method for serialization into json
	void CalcInductance::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Calc::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["indtype"] = type_;

		// subnodes
		js["model"] = cmn::Node::serialize_node(model_, list);
	}

	// method for deserialisation from json
	void CalcInductance::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Calc::deserialize(js,list,factory_list);

		// properties
		type_ = (IndType)js["indtype"].asInt();

		// subnodes
		model_ = cmn::Node::deserialize_node<Model>(js["model"], list, factory_list);
	}



}}