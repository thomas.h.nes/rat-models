/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "vtkunstr.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKUnstr::VTKUnstr(){
		vtk_ugrid_ = vtkSmartPointer<vtkUnstructuredGrid>::New();
	}

	// factory
	ShVTKUnstrPr VTKUnstr::create(){
		return std::make_shared<VTKUnstr>();
	}

	// write mesh
	void VTKUnstr::set_mesh(const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n,
		const arma::uword element_type){
		set_nodes(Rn); set_elements(n,element_type);
	}

	// write nodes
	void VTKUnstr::set_nodes(const arma::Mat<double> &Rn){
		// check input
		assert(Rn.n_rows==3);

		// set number of nodes
		num_nodes_ = Rn.n_cols;

		// create list of node coordinates
		vtkSmartPointer<vtkPoints> points = 
			vtkSmartPointer<vtkPoints>::New();
		points->SetDataTypeToDouble();
		
		// copy points
		points->Allocate(Rn.n_elem);
		for(arma::uword i=0;i<num_nodes_;i++)
			points->InsertNextPoint(Rn(0,i),Rn(1,i),Rn(2,i));

		// set points to vtk grid
		vtk_ugrid_->SetPoints(points);
	}

	// element types
	int VTKUnstr::get_element_type(const arma::uword num_rows){
		int type = 0;
		if(num_rows==1)type = 1; // points
		else if(num_rows==2)type = 3; // line
		else if(num_rows==4)type = 9; // quadrilateral
		else if(num_rows==8)type = 12; // hexahedron
		else rat_throw_line("unknown element type");
		return type;
	}

	// write elements
	void VTKUnstr::set_elements(
		const arma::Mat<arma::uword> &n, const arma::uword element_type){

		// create vtk element array
		vtkSmartPointer<vtkCellArray> nvtk = 
			vtkSmartPointer<vtkCellArray>::New();

		// set number of nodes
		num_elements_ = n.n_cols;

		// convert to signed integer
		const arma::Mat<arma::sword> ns = 
			arma::conv_to<arma::Mat<arma::sword> >::from(n);

		// insert cell elements
		nvtk->Allocate(ns.n_elem + ns.n_cols);
		for(arma::uword i=0;i<ns.n_cols;i++)
			nvtk->InsertNextCell(ns.n_rows,ns.colptr(i));
		
		// set elements to vtk grid
		vtk_ugrid_->SetCells(element_type,nvtk); // 12 stands for hexahedrons
	}

	// write elements with different types
	void VTKUnstr::set_elements(
		const arma::field<arma::Mat<arma::uword> > &n,
		const arma::Row<arma::uword> &element_types){
		// check input
		assert(element_types.n_elem==n.n_elem);

		// create vtk element array
		vtkSmartPointer<vtkCellArray> nvtk = 
			vtkSmartPointer<vtkCellArray>::New();

		// count number of elements
		num_elements_ = 0; arma::uword num_point_id = 0;
		for(arma::uword i=0;i<n.n_elem;i++){
			num_elements_+= n(i).n_cols;
			num_point_id += n(i).n_elem;
		}

		// allocate memory
		vtk_ugrid_->Allocate(num_elements_ + num_point_id,0);

		// walk over sets
		for(arma::uword i=0;i<n.n_elem;i++){
			// convert to signed integer
			const arma::Mat<arma::sword> ns = 
				arma::conv_to<arma::Mat<arma::sword> >::from(n(i));

			// insert cell elements
			for(arma::uword j=0;j<ns.n_cols;j++){
				vtk_ugrid_->InsertNextCell((int)element_types(i),(vtkIdType)ns.n_rows,(vtkIdType*)ns.colptr(j));
				// vtk_ugrid_->InsertNextCell(ns.n_rows,ns.colptr(j));
			}
		}
	}


	// write data at nodes
	void VTKUnstr::set_nodedata(
		const arma::Mat<double> &v, 
		const std::string &data_name){
		// check input
		if(v.n_cols!=num_nodes_)rat_throw_line("data does not match number of nodes");

		// create vtk array
		vtkSmartPointer<vtkDoubleArray> vvtk = 
			vtkSmartPointer<vtkDoubleArray>::New();

		// add name to data
		vvtk->SetName(data_name.c_str());

		// allocate
		vvtk->SetNumberOfComponents(v.n_rows);
		vvtk->SetNumberOfValues(v.n_elem);
		
		// insert data elements
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// set data to vtk grid
		vtk_ugrid_->GetPointData()->AddArray(vvtk);
	}

	// set time
	void VTKUnstr::set_time(const double time){
		// Add "time" value to a VTK dataset.
		vtkSmartPointer<vtkDoubleArray> timestamp = 
			vtkSmartPointer<vtkDoubleArray>::New();
		timestamp->SetName("TIME");
		timestamp->SetNumberOfTuples(1);
		timestamp->SetTuple1(0, time);

		// add to grid
		vtk_ugrid_->GetFieldData()->AddArray(timestamp);
	}

	// set cycle
	// 	// Add "cycle" value to a VTK dataset.
	// void
	// AddCycleToVTK(vtkDataSet *ds, int cycle)
	// {
	//     vtkIntArray *c = vtkIntArray::New();
	//     c->SetName("CYCLE");
	//     c->SetNumberOfTuples(1);
	//     c->SetTuple1(0, cycle);
	//     ds->GetFieldData()->AddArray(c);
	// }

	// setup interpolation
	// creates oct-tree
	void VTKUnstr::setup_interpolation(){
	// creating probe
		vtk_probe_ = vtkSmartPointer<vtkProbeFilter>::New();
		vtk_probe_->SetSourceData(vtk_ugrid_);
	}


	// interpolation function
	arma::Mat<double> VTKUnstr::interpolate_field(
		const arma::Mat<double> &Rp, const std::string &field_name){

		// counters
		const arma::uword num_particles = Rp.n_cols;

		// copy points to VTK
		vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
		points->SetDataTypeToDouble();
		points->Allocate(Rp.n_elem);
		for(arma::uword i=0;i<Rp.n_cols;i++)
			points->InsertNextPoint(Rp(0,i),Rp(1,i),Rp(2,i));

		// Create a dataset from the grid points
		vtkSmartPointer<vtkPolyData> pd = vtkSmartPointer<vtkPolyData>::New();
		pd->SetPoints(points);

		// run interpolation
		vtk_probe_->SetInputData(pd);
		vtk_probe_->Update();
		
		// get interpolated data "Mgn. Flux Density [T]"
		vtkDataArray* data = vtk_probe_->GetOutput()->GetPointData()->GetVectors(field_name.c_str());
		vtkDoubleArray* doubleData = dynamic_cast<vtkDoubleArray*>(data);

		// check output
		assert(doubleData->GetNumberOfTuples()==(vtkIdType)num_particles);
		assert(doubleData->GetNumberOfComponents()==3);

		// get field at points
		arma::Mat<double> Bp(3,num_particles,arma::fill::zeros);
		for(arma::uword i=0;i<num_particles;i++)
			for(arma::uword j=0;j<3;j++)
				Bp(j,i) = doubleData->GetComponent(i,j);
		
		// get invalid id's
		arma::Row<arma::uword> mark(num_particles,arma::fill::ones);
		vtkIdTypeArray* idx_valid = vtk_probe_->GetValidPoints();
		for(vtkIdType i=0;i<idx_valid->GetNumberOfTuples();i++)
			mark(idx_valid->GetValue(i)) = 0;
		Bp.cols(arma::find(mark)).fill(0);

		// free pointers?
		// free data;

		// return field
		return Bp;
	}

	// write output file
	void VTKUnstr::write(const std::string fname, cmn::ShLogPr lg){
		// create extended filename
		const std::string fname_ext = fname + ".vtu";

		// display file settings and type
		lg->msg(2,"%s%sWriting VTK File%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sFile Settings%s\n",KBLU,KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.c_str(),KNRM);
		lg->msg("type: unstructed grid (vtu)\n");

		// create unstructured grid writer and set it up
		vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =  
			vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
		writer->SetFileName(fname_ext.c_str());
		writer->SetInputData(vtk_ugrid_);

		// perform write
		writer->Write();

		// done writing VTK unstructured grid
		lg->msg(-2);
		lg->msg(-2,"\n");
	}

}}