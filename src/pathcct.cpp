/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathcct.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCCT::PathCCT(){

	}

	// constructor with input
	PathCCT::PathCCT(const arma::uword num_poles, const double radius, const double a, 
		const double omega, const double num_turns, const arma::uword num_nodes_per_turn){
		set_num_poles(num_poles); set_radius(radius); set_amplitude(a); 
		set_pitch(omega); set_num_turns(num_turns); set_num_nodes_per_turn(num_nodes_per_turn);
	}

	// factory
	ShPathCCTPr PathCCT::create(){
		return std::make_shared<PathCCT>();
	}

	// factory
	ShPathCCTPr PathCCT::create(const arma::uword num_poles, const double radius, const double a, 
		const double omega, const double num_turns, const arma::uword num_nodes_per_turn){
		return std::make_shared<PathCCT>(num_poles,radius,a,omega,num_turns,num_nodes_per_turn);
	}

	// set the direction of the layer
	void PathCCT::set_is_reverse(const bool is_reverse){
		is_reverse_ = is_reverse;
	}

	// set twisting
	void PathCCT::set_twist(const double twist){
		twist_ = twist;
	}

	// set amplitude
	void PathCCT::set_amplitude(const double a){
		a_ = a;
	}

	// set winding pitch
	void PathCCT::set_pitch(const double omega){
		omega_ = omega;
	}

	// set number of turns
	void PathCCT::set_num_turns(const double num_turns){
		num_turns_ = num_turns;
	}

	// set radius
	void PathCCT::set_radius(const double radius){
		if(radius<=0)rat_throw_line("radius must be positive");
		radius_ = radius;
	}

	// set scaling in x
	void PathCCT::set_scale_x(const double scale_x){
		scale_x_ = scale_x;
	}
	
	// set scaling in y
	void PathCCT::set_scale_y(const double scale_y){
		scale_y_ = scale_y;
	}

	// set number of poles
	void PathCCT::set_num_poles(const arma::uword num_poles){
		if(num_poles==0)rat_throw_line("number of poles must be positive");
		num_poles_ = num_poles;
	}

	// set number of poles
	void PathCCT::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		if(num_nodes_per_turn==0)rat_throw_line("number of nodes per turn must be positive");
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// calculate skew angle
	double PathCCT::calc_alpha() const{
		return num_poles_*std::atan(radius_/a_);
	}

	// setup coordinates and orientation vectors
	ShFramePr PathCCT::create_frame() const{
		// calculate number of sections per half
		const arma::uword num_sect_per_turn = num_poles_*4;
		const double sectsize = 2*arma::datum::pi/num_sect_per_turn;

		// calculate maximum theta
		const double thetamin = -num_turns_*arma::datum::pi;
		const double thetamax = +num_turns_*arma::datum::pi;
		
		arma::uword num_int = std::floor(thetamax/sectsize);
		if(std::abs(thetamax-num_int*sectsize)<1e-8)num_int--;

		// calculate number of sections
		const arma::uword num_sections = 2*(num_int+1);

		// allocate theta
		arma::Row<double> theta_start(num_sections);
		arma::Row<double> theta_end(num_sections);

		// set theta start positions
		theta_start(0) = thetamin;
		theta_start.cols(1,num_sections-1) = arma::linspace<arma::Row<double> >(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		
		// set theta end positions
		theta_end.cols(0,num_sections-2) = arma::linspace<arma::Row<double> >(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		theta_end(num_sections-1) = thetamax;

		//std::cout<<arma::join_horiz(theta_start.t(),theta_end.t())<<std::endl;

		// allocate
		arma::field<arma::Mat<double> > R(1,num_sections);
		arma::field<arma::Mat<double> > L(1,num_sections);
		arma::field<arma::Mat<double> > N(1,num_sections); 
		arma::field<arma::Mat<double> > D(1,num_sections);

		// allocate section and turn indexes
		arma::Row<arma::uword> section(num_sections);
		arma::Row<arma::uword> turn(num_sections); 

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// calculate section
			const double theta1 = theta_start(i);
			const double theta2 = theta_end(i);
			assert(theta2>theta1);

			// calculate number of nodes
			const arma::uword num_nodes = std::max(2,(int)std::ceil(num_nodes_per_turn_*(theta2-theta1)/(2*arma::datum::pi)));

			// create theta (taking into account last section
			const arma::Row<double> theta = arma::linspace<arma::Row<double> >(theta1, theta2, num_nodes);

			// create coordinates
			const arma::Row<double> x = scale_x_*radius_*arma::cos(theta);
			const arma::Row<double> y = scale_y_*radius_*arma::sin(theta);
			//arma::Row<double> z = (radius_/(num_poles_*std::tan(calc_alpha())))*arma::sin(num_poles_*theta);
			arma::Row<double> z = (a_/num_poles_)*arma::sin(num_poles_*theta);
			
			// add turn increments
			if(is_reverse_==false)z += (omega_/(2*arma::datum::pi))*theta;
			else z -= (omega_/(2*arma::datum::pi))*theta;
			
			// create direction vectors
			const arma::Row<double> vx = -scale_x_*radius_*arma::sin(theta);
			const arma::Row<double> vy = scale_y_*radius_*arma::cos(theta);
			// arma::Row<double> vz = (num_poles_*(radius_/(num_poles_*std::tan(calc_alpha())))*arma::cos(num_poles_*theta) + 
			// 	num_poles_*omega_/(2*arma::datum::pi))*(2*arma::datum::pi/num_nodes_per_turn_);
			arma::Row<double> vz = a_*arma::cos(num_poles_*theta);
			if(is_reverse_==false)vz += omega_/(2*arma::datum::pi);
			else vz -= omega_/(2*arma::datum::pi);

			// acceleration
			const arma::Row<double> ax = -scale_x_*radius_*arma::cos(theta);
			const arma::Row<double> ay = -scale_y_*radius_*arma::sin(theta);
			arma::Row<double> az = -a_*num_poles_*arma::sin(num_poles_*theta);

			// jerk
			const arma::Row<double> jx = scale_x_*radius_*arma::sin(theta);
			const arma::Row<double> jy = -scale_y_*radius_*arma::cos(theta);
			arma::Row<double> jz = -a_*num_poles_*num_poles_*arma::cos(num_poles_*theta);

			// reverse axial direction if needed
			if(is_reverse_){
				z *= -1; vz *= -1; az *= -1; jz *= -1;
			}

			// create darboux frame
			// Darboux db(
			// 	arma::join_vert(vx,vy,vz),
			// 	arma::join_vert(ax,ay,az),
			// 	arma::join_vert(jx,jy,jz));
			// db.setup(true);

			// store position vectors
			// R(i).set_size(3,num_nodes);
			// R(i).row(0) = x; R(i).row(1) = y; R(i).row(2) = z;
			R(i) = arma::join_vert(x,y,z);

			// store longitudinal vectors
			L(i) = arma::join_vert(vx,vy,vz);

			// calculate radial vector
			arma::Mat<double> Vax(3,num_nodes,arma::fill::zeros);
			Vax.row(2).fill(1.0);
			D(i) = cmn::Extra::cross(L(i),Vax);

			// create normal vectors
			N(i) = cmn::Extra::cross(L(i),D(i));
			
			// normalize
			L(i).each_row()/=cmn::Extra::vec_norm(L(i));
			D(i).each_row()/=cmn::Extra::vec_norm(D(i));
			N(i).each_row()/=cmn::Extra::vec_norm(N(i));

			// get darboux
			// L(i) = db.get_longitudinal();
			// D(i) = db.get_transverse();
			// N(i) = db.get_normal();

			
			// check size
			assert(R(i).n_rows==3);	assert(L(i).n_rows==3);
			assert(N(i).n_rows==3);	assert(D(i).n_rows==3);

			// set section and turn
			section(i) = i%num_sect_per_turn;
			turn(i) = i/num_sect_per_turn;

			// add aditional twist
			if(twist_!=0){
				arma::Row<double> twist = arma::sin(num_poles_*theta)*twist_;
				if(is_reverse_)twist*=-1;
				D(i) = D(i).each_row()%arma::cos(twist) + N(i).each_row()%arma::sin(twist);
				N(i) = N(i).each_row()%arma::cos(twist) - D(i).each_row()%arma::sin(twist);
			}
		}

		// reverse frame
		if(is_reverse_){
			cmn::Extra::reverse_field(R); cmn::Extra::reverse_field(L);
			cmn::Extra::reverse_field(D); cmn::Extra::reverse_field(N);
			for(arma::uword i=0;i<L.n_elem;i++)L(i) *= -1;
		}

		// create frame
		ShFramePr gen = Frame::create(R,L,N,D);

		// set section and turn indices
		gen->set_location(section,turn);

		// transformations
		gen->apply_transformations(trans_);

		// create frame
		return gen;
	}

	// get type
	std::string PathCCT::get_type(){
		return "mdl::pathcct";
	}

	// method for serialization into json
	void PathCCT::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["num_poles"] = (unsigned int)num_poles_; 
		js["is_reverse_"] = is_reverse_;
		js["radius"] = radius_;
		js["a"] = a_; 
		js["omega"] = omega_; 
		js["num_turns"] = num_turns_; 
		js["num_nodes_per_turn"] = (unsigned int)num_nodes_per_turn_; 
		js["twist"] = twist_; 
	}

	// method for deserialisation from json
	void PathCCT::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Transformations::deserialize(js,list,factory_list);

		// settings
		set_num_poles(js["num_poles"].asUInt64()); 
		set_is_reverse(js["is_reverse_"].asBool());
		set_radius(js["radius"].asDouble());
		set_amplitude(js["a"].asDouble()); 
		set_pitch(js["omega"].asDouble()); 
		set_num_turns(js["num_turns"].asDouble()); 
		set_num_nodes_per_turn(js["num_nodes_per_turn"].asUInt64()); 
		set_twist(js["twist"].asDouble());
	}

}}


