/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// get name function
	std::string Nameable::get_name() const{
		return myname_;
	}

	// set name function
	void Nameable::set_name(const std::string &name){
		myname_ = name;
	}

	// append name
	void Nameable::append_name(const std::string &append){
		myname_ = append + myname_;
	}

	// get type
	std::string Nameable::get_type(){
		return "mdl::nameable";
	}

	// method for serialization into json
	void Nameable::serialize(Json::Value &js, cmn::SList &) const{
		js["name"] = myname_;
	}

	// method for deserialisation from json
	void Nameable::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		myname_ = js["name"].asString();
	}
	
}}