/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "meshbar.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MeshBar::MeshBar(){

	}

	// constructor with input
	MeshBar::MeshBar(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
		if(n_dim_!=3)rat_throw_line("bar mesh must be volumetric (3 dim)");
	}

	// factory
	ShMeshBarPr MeshBar::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<MeshBar>();
	}

	// factory with input
	ShMeshBarPr MeshBar::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<MeshBar>(frame,area);
	}

	// set use volume elements
	void MeshBar::set_use_volume_elements(const bool use_volume_elements){
		use_volume_elements_ = use_volume_elements;
	}

	// set softening factor
	void MeshBar::set_softening(const double softening){
		softening_ = softening;
	}

	// set magnetisation
	void MeshBar::set_magnetisation(const arma::Col<double>::fixed<3> &Mf){
		Mf_  = Mf;
	}

	// default create sources function to be overridden
	fmm::ShSourcesPrList MeshBar::create_sources(const arma::sword num_gauss, const arma::sword num_gauss_surface) const{
		// allocate
		fmm::ShSourcesPrList srcs(3);

		// calculate nodal magnetisation
		const arma::Mat<double> Mn = L_*Mf_(0) + N_*Mf_(1) + D_*Mf_(2);

		// create volume current
		fmm::ShCurrentMeshPr cmsh = fmm::CurrentMesh::create();
		cmsh->set_mesh(R_,n_);
		cmsh->set_magnetisation_nodes(Mn);
		cmsh->set_num_gauss(num_gauss);
		srcs(0) = cmsh;

		// find indexes of unique nodes
		const arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s_,1,s_.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(get_num_nodes(),arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		const arma::Mat<arma::uword> s = arma::reshape(
			node_indexing(arma::reshape(s_,1,s_.n_elem)),s_.n_rows,s_.n_cols);

		// create surface mesh
		fmm::ShCurrentSurfacePr smsh = fmm::CurrentSurface::create();
		smsh->set_mesh(R_.cols(idx_node_surface),s);
		smsh->set_magnetisation_nodes(Mn.cols(idx_node_surface));
		smsh->set_num_gauss(num_gauss_surface);
		srcs(1) = smsh;

		// create interpolation for magnetisation
		fmm::ShInterpPr ipmsh = fmm::Interp::create();
		ipmsh->set_mesh(R_,n_);
		ipmsh->set_interpolation_values("M",Mn);
		srcs(2) = ipmsh;

		// return the sources list
		return srcs;
	}

	// copy constructor
	ShMeshPr MeshBar::copy() const{
		return std::make_shared<MeshBar>(*this);
	}

}}