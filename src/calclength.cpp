/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calclength.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLength::CalcLength(){
		set_name("length");
	}

	// constructor
	CalcLength::CalcLength(ShModelPr model){
		set_model(model); set_name("length");
	}

	// factory
	ShCalcLengthPr CalcLength::create(){
		return std::make_shared<CalcLength>();
	}

	// factory
	ShCalcLengthPr CalcLength::create(ShModelPr model){
		return std::make_shared<CalcLength>(model);
	}

	// set input model
	void CalcLength::set_model(ShModelPr model){
		model_ = model;
	}

	// calculation
	void CalcLength::calculate(cmn::ShLogPr lg){
		// header
		lg->msg("%s%s--- CALCULATING LENGTHS ---%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sGEOMETRY SETUP:%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sSOURCE MESHES%s\n",KBLD,KGRN,KNRM);\

		// create edges
		ShMeshPrList meshes = model_->create_mesh();

		// display meshes
		Mesh::display(lg, meshes);

		// get number of edges
		const arma::uword num_coils = meshes.n_elem;

		// allocate conductor length array
		name_.set_size(num_coils);
		volume_.set_size(num_coils);
		area_.set_size(num_coils);
		num_turns_.set_size(num_coils);
		ell_gen_.set_size(num_coils);

		// walk over coils
		for(arma::uword i=0;i<num_coils;i++){
			// get name
			name_(i) = meshes(i)->get_name();

			// get volume
			volume_(i) = meshes(i)->calc_total_volume();

			// get area
			area_(i) = meshes(i)->get_area()->calculate_area();

			// get number turns
			num_turns_(i) = meshes(i)->get_number_turns();

			// get length from frame
			ell_gen_(i) = meshes(i)->get_frame()->calc_total_ell();
		}

		// calculate ell from volume
		ell_ = num_turns_%volume_/area_;

		// return
		lg->msg(-2,"\n");
		lg->msg(-2);
	}

	// write output files
	void CalcLength::write(cmn::ShLogPr lg){
		// output to terminal
		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
		// header
		lg->msg(2,"%s%sTERMINAL OUTPUT%s\n",KBLD,KGRN,KNRM);
		
		// walk over time steps
		lg->msg(2,"%sconductor length table%s\n",KBLU,KNRM);
		lg->msg("%s%8s %8s %8s %8s %8s%s\n",KBLD,"name","nt [#]","frm [m]","V [m^3]","ell [m]",KNRM);
		
		// walk over coils
		for(arma::uword i=0;i<name_.n_elem;i++){
			lg->msg("%8s %8.4f %8.4f %8.4e %8.4f\n", 
				name_(i).c_str(), num_turns_(i), ell_gen_(i), volume_(i), ell_(i));
		}

		// return terminal
		lg->msg(-2,"\n");
		lg->msg(-2);
		lg->msg(-2);

		// write to VTK table
	}

	// get type
	std::string CalcLength::get_type(){
		return "mdl::calclength";
	}

	// method for serialization into json
	void CalcLength::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Calc::serialize(js,list);

		// properties
		js["type"] = get_type();
		
		// subnodes
		js["model"] = cmn::Node::serialize_node(model_, list);
	}

	// method for deserialisation from json
	void CalcLength::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Calc::deserialize(js,list,factory_list);

		// subnodes
		model_ = cmn::Node::deserialize_node<Model>(js["model"], list, factory_list);
	}

}}