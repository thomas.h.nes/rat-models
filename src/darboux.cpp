/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Darboux::Darboux(){

	}

	// constructor
	Darboux::Darboux(const arma::Mat<double> &V, const arma::Mat<double> &A, const arma::Mat<double> &J){
		assert(V.n_cols==A.n_cols); assert(V.n_cols==J.n_cols);
		assert(V.n_rows==3); assert(A.n_rows==3); assert(J.n_rows==3);
		V_ = V; A_ = A; J_ = J;
	}

	// factory
	ShDarbouxPr Darboux::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Darboux>();
	}

	// factory
	ShDarbouxPr Darboux::create(const arma::Mat<double> &V, const arma::Mat<double> &A, const arma::Mat<double> &J){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Darboux>(V,A,J);
	}

	// calculate frame
	void Darboux::setup(const bool analytic_frame){
		// number of points
		const arma::uword num_times = V_.n_cols;

		// normalize velocity
		const arma::Row<double> normV = cmn::Extra::vec_norm(V_);
		T_ = V_.each_row()/normV; // previously called T
		
		// setup frame
		B_ = cmn::Extra::cross(V_,A_); 
		const arma::Row<double> normB = cmn::Extra::vec_norm(B_);

		// analytic version
		if(analytic_frame){
			// calculate 
			kappa_ = normB/(normV%normV%normV);
			tau_ = cmn::Extra::dot(B_,J_)/(normB%normB);

			// normalize B
			B_.each_row() /= normB;

			// frame	
			D_ = tau_%T_.each_row() + kappa_%B_.each_row();
		}

		// numeric
		else{
			// normalize 
			const arma::Mat<double> Anrm = A_.each_row()/cmn::Extra::vec_norm(A_);

			// create frame
			D_ = cmn::Extra::cross(Anrm.head_cols(num_times-1),Anrm.tail_cols(num_times-1));

			// extend
			D_ = (arma::join_horiz(D_.head_cols(1),D_) + arma::join_horiz(D_,D_.tail_cols(1)))/2;
		}

		// normalize darboux vectors
		D_.each_row() /= cmn::Extra::vec_norm(D_);

		// calculate angle
		alpha_ = arma::acos(cmn::Extra::dot(-T_,D_));

		// Scale frame
		D_.each_row() /= arma::sin(alpha_);
	}

	// correct sign flips
	void Darboux::correct_sign(const arma::Col<double>::fixed<3> &dstart){
		// check sign flips
		const arma::Row<double> sf = 
			cmn::Extra::dot(arma::join_horiz(
			dstart,D_.head_cols(D_.n_cols-1)),D_);

		// flip D when curvature changes from positive to negative
		arma::sword flipstate = 1;
		for(arma::uword i=0;i<D_.n_cols;i++){
			// check whether flip is needed
			if(sf(i)<0)flipstate*=-1;

			// flip vectors (if needed)
			D_.col(i)*=flipstate;
			B_.col(i)*=flipstate;

			// stability fix
			if(std::abs(sf(i))<1e-7){
				D_.col(i)=D_.col(i-1);
				B_.col(i)=B_.col(i-1);
			}
		}
	}

	// get orientation vectors
	arma::Mat<double> Darboux::get_longitudinal() const{
		assert(!T_.is_empty());
		return T_;
	}

	arma::Mat<double> Darboux::get_transverse() const{
		assert(!D_.is_empty());
		return D_;
	}

	arma::Mat<double> Darboux::get_normal() const{
		return cmn::Extra::cross(T_,B_);
	}

}}