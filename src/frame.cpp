/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Frame::Frame(){

	}

	// constructor with dimension input
	Frame::Frame(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D,
		const arma::Mat<double> &B){
		
		// allocate
		R_.set_size(1,1); L_.set_size(1,1); 
		N_.set_size(1,1); D_.set_size(1,1);
		B_.set_size(1,1);

		// set to self
		R_(0) = R; L_(0) = L; N_(0) = N; D_(0) = D; B_(0) = B;

		// since it is single section we can set 0,0
		section_ = arma::Row<arma::uword>{0};
		turn_ = arma::Row<arma::uword>{0};
	}

	// constructor with dimension input and section division
	Frame::Frame(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D,
		const arma::Mat<double> &B,
		const arma::uword num_sections){
		
		// number of elements
		const arma::uword num_elements = R.n_cols;

		// create field arrays
		R_.set_size(1,num_sections); L_.set_size(1,num_sections), 
		N_.set_size(1,num_sections); D_.set_size(1,num_sections), 
		B_.set_size(1,num_sections);

		// create indexes at which we want to split the sections
		arma::Row<arma::uword> idx_divide = 
			arma::floor(arma::regspace<arma::Row<arma::uword> >(0,num_sections)*
			(double)num_elements/num_sections);
		idx_divide(num_sections) = num_elements-1;

		// split sections
		for(arma::uword i=0;i<num_sections;i++){
			const arma::uword idx1 = idx_divide(i);
			const arma::uword idx2 = idx_divide(i+1);
			R_(i) = R.cols(idx1,idx2); L_(i) = L.cols(idx1,idx2);
			N_(i) = N.cols(idx1,idx2); D_(i) = D.cols(idx1,idx2);
			B_(i) = B.cols(idx1,idx2);
		}

		// since it is single section we can set 0,0
		section_ = arma::regspace<arma::Row<arma::uword> >(0,R_.n_elem-1);
		turn_ = arma::Row<arma::uword>(R_.n_elem,arma::fill::zeros);
	}


	// constructor with field arrays containing multiple sections
	Frame::Frame(
		const arma::field<arma::Mat<double> > &R, 
		const arma::field<arma::Mat<double> > &L, 
		const arma::field<arma::Mat<double> > &N, 
		const arma::field<arma::Mat<double> > &D,
		const arma::field<arma::Mat<double> > &B){
		
		// check number of sections for vectors
		assert(L.n_elem==R.n_elem);
		assert(N.n_elem==R.n_elem);
		assert(D.n_elem==R.n_elem);
		assert(B.n_elem==R.n_elem);

		// set to self
		R_ = R;	L_ = L;	N_ = N;	D_ = D; B_ = B;

		// since it is single section we can set 0,0
		section_ = arma::regspace<arma::Row<arma::uword> >(0,R_.n_elem-1);
		turn_ = arma::Row<arma::uword>(R_.n_elem,arma::fill::zeros);
	}

	// constructor with dimension input
	Frame::Frame(const ShFramePrList &frame){
		// check if input empty
		assert(!frame.is_empty());

		// check all types
		for(arma::uword i=0;i<frame.n_elem;i++)
			if(frame(0)->get_type()!=frame(i)->get_type())
				rat_throw_line("combined frame are of different type");

		// get type from first generator
		conductor_type_ = frame(0)->get_conductor_type();

		// number of frame provided
		const arma::uword num_frame = frame.n_elem;

		// calculate number of sections
		arma::Row<arma::uword> num_sections_each(num_frame);

		// walk over generator list
		for(arma::uword i=0;i<num_frame;i++)
			num_sections_each(i) = frame(i)->get_num_sections();
		
		// total number of sections
		const arma::uword num_sections = arma::sum(num_sections_each);

		// allocate space for the sections
		R_.set_size(1,num_sections); L_.set_size(1,num_sections);
		N_.set_size(1,num_sections); D_.set_size(1,num_sections);
		B_.set_size(1,num_sections);
		section_.set_size(num_sections);

		// copy sections from the given frame
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_frame;i++){
			for(arma::uword j=0;j<num_sections_each(i);j++){
				R_(cnt) = frame(i)->get_coords(j);
				L_(cnt) = frame(i)->get_direction(j);
				N_(cnt) = frame(i)->get_normal(j);
				D_(cnt) = frame(i)->get_transverse(j);
				B_(cnt) = frame(i)->get_block(j);
				cnt++;
			}
		}

		// since it is single section we can set 0,0
		section_ = arma::regspace<arma::Row<arma::uword> >(0,num_sections-1);
		turn_ = arma::Row<arma::uword>(num_sections,arma::fill::zeros);

		// sanity check
		assert(cnt==num_sections);
	}

	// override automatic numbering of sections and turns
	// this is relevant when dealing with cables and coils etc.
	void Frame::set_location(
		const arma::Row<arma::uword> &section,
		const arma::Row<arma::uword> &turn){
		assert(section.n_elem==get_num_sections());
		assert(turn.n_elem==get_num_sections());
		section_ = section; turn_ = turn;
	}


	// factory
	ShFramePr Frame::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Frame>();
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D){
		return std::make_shared<Frame>(R,L,N,D,N);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D,
		const arma::uword num_sections){
		return std::make_shared<Frame>(R,L,N,D,N,num_sections);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::field<arma::Mat<double> > &R, 
		const arma::field<arma::Mat<double> > &L, 
		const arma::field<arma::Mat<double> > &N, 
		const arma::field<arma::Mat<double> > &D){
		return std::make_shared<Frame>(R,L,N,D,N);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D,
		const arma::Mat<double> &B){
		return std::make_shared<Frame>(R,L,N,D,B);
	}

	// factory with section splitting
	ShFramePr Frame::create(
		const arma::Mat<double> &R, 
		const arma::Mat<double> &L, 
		const arma::Mat<double> &N, 
		const arma::Mat<double> &D,
		const arma::Mat<double> &B,
		const arma::uword num_sections){
		return std::make_shared<Frame>(R,L,N,D,B,num_sections);
	}

	// factory with single section input
	ShFramePr Frame::create(
		const arma::field<arma::Mat<double> > &R, 
		const arma::field<arma::Mat<double> > &L, 
		const arma::field<arma::Mat<double> > &N, 
		const arma::field<arma::Mat<double> > &D,
		const arma::field<arma::Mat<double> > &B){
		return std::make_shared<Frame>(R,L,N,D,B);
	}

	// factory creating frame from a set of other frame
	ShFramePr Frame::create(const ShFramePrList &frame){
		return std::make_shared<Frame>(frame);
	}

	// count number of sections
	arma::uword Frame::get_num_sections() const{
		return R_.n_elem;
	}

	// count number of sections
	arma::uword Frame::get_num_nodes(const arma::uword index) const{
		return R_(index).n_cols;
	}

	// get number of nodes in each section
	arma::Row<arma::uword> Frame::get_num_nodes() const{
		arma::Row<arma::uword> nn(get_num_sections());
		for(arma::uword i=0;i<get_num_sections();i++)
			nn(i) = get_num_nodes(i);
		return nn;
	}

	// get coordinates
	const arma::field<arma::Mat<double> >& Frame::get_coords() const{
		return R_;
	}

	// get coordinates
	const arma::field<arma::Mat<double> >& Frame::get_direction() const{
		return L_;
	}

	// get coordinates
	const arma::field<arma::Mat<double> >& Frame::get_normal() const{
		return N_;
	}

	// get coordinates
	const arma::field<arma::Mat<double> >& Frame::get_transverse() const{
		return D_;
	}

	// get coordinates
	const arma::field<arma::Mat<double> >& Frame::get_block() const{
		return B_;
	}

	// get coordinates for section
	const arma::Mat<double>& Frame::get_coords(const arma::uword section) const{
		assert(section<R_.n_elem);
		return R_(section);
	}

	// get coordinates for section
	const arma::Mat<double>& Frame::get_direction(const arma::uword section) const{
		assert(section<L_.n_elem);
		return L_(section);
	}

	// get coordinates for section
	const arma::Mat<double>& Frame::get_normal(const arma::uword section) const{
		assert(section<N_.n_elem);
		return N_(section);
	}

	// get coordinates for section
	const arma::Mat<double>& Frame::get_transverse(const arma::uword section) const{
		assert(section<D_.n_elem);
		return D_(section);
	}

	// get coordinates for section
	const arma::Mat<double>& Frame::get_block(const arma::uword section) const{
		assert(section<B_.n_elem);
		return B_(section);
	}

	// get coordinates for section
	arma::Row<double> Frame::calc_ashear(const arma::uword section) const{
		assert(section<B_.n_elem);
		assert(B_(section).is_finite());
		assert(N_(section).is_finite());

		// find direction of the angle
		const arma::Row<double> dir = arma::sign(cmn::Extra::dot(L_(section),cmn::Extra::cross(B_(section),N_(section))));

		// calculate shear angle
		const arma::Row<double> ashear = dir%arma::acos(arma::clamp(arma::abs(cmn::Extra::dot(B_(section),N_(section))/
			(cmn::Extra::vec_norm(B_(section))%cmn::Extra::vec_norm(N_(section)))),0.0,1.0));

		// check output
		assert(ashear.is_finite());

		// calculate angle and return
		return ashear;
	}

	// calculate length
	arma::Row<double> Frame::calc_ell() const{
		// get counters
		arma::uword num_sections = get_num_sections();

		// allocate length per section
		arma::Row<double> ell(num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++)
			ell(i) = arma::accu(cmn::Extra::vec_norm(arma::diff(R_(i),1,1)));

		// return sum of lengths
		return ell;
	}

	// calculate length
	double Frame::calc_total_ell() const{
		return arma::accu(calc_ell());
	}

	// get section indexes
	arma::Row<arma::uword> Frame::get_section() const{
		return section_;
	}

	// get section indexes
	arma::Row<arma::uword> Frame::get_turn() const{
		return turn_;
	}


	// align sections using first section as a reference
	void Frame::align_sections(){
		// take first coordinate of first section as a reference
		align_sections(
			R_(0).col(0), L_(0).col(0),
			N_(0).col(0), D_(0).col(0));
	}

	// method for alligning all sections using start point as a reference
	void Frame::align_sections(
		const arma::Col<double>::fixed<3> &R0, 
		const arma::Col<double>::fixed<3> &L0, 
		const arma::Col<double>::fixed<3> &N0, 
		const arma::Col<double>::fixed<3> &D0){

		// get counters
		const arma::uword num_sections = get_num_sections();

		// check input
		assert(num_sections>0);
		assert(R_.n_elem==num_sections); assert(L_.n_elem==num_sections);
		assert(N_.n_elem==num_sections); assert(D_.n_elem==num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate reference point
			arma::Col<double>::fixed<3> r1, l1, n1, d1;

			// for first section reference point is provided
			if(i==0){
				l1 = L0; n1 = N0; d1 = D0;
			}

			// get last orientation vectors of this section
			else{
				l1 = L_(i-1).tail_cols(1);
				n1 = N_(i-1).tail_cols(1);
				//d1 = D_(i-1).tail_cols(1);
				d1 = cmn::Extra::cross(n1,l1);
			}

			// get last orientation vectors of this section
			const arma::Col<double>::fixed<3> l2 = L_(i).head_cols(1);
			const arma::Col<double>::fixed<3> n2 = N_(i).head_cols(1);
			//const arma::Col<double>::fixed<3> d2 = D_(i).head_cols(1);

			// calculate third component not using D as this one may be non-orthogonal
			const arma::Col<double>::fixed<3> d2 = cmn::Extra::cross(n2,l2);

			// std::cout<<arma::as_scalar(cmn::Extra::dot(d1,cmn::Extra::cross(l1,n1)))<<std::endl;
			// std::cout<<arma::as_scalar(cmn::Extra::dot(d2,cmn::Extra::cross(l2,n2)))<<std::endl;

			// check if L and N orthogonal
			if(std::abs(arma::as_scalar(cmn::Extra::dot(l1,n1)))>1e-6)rat_throw_line("L1 and N1 are not orthogonal");
			if(std::abs(arma::as_scalar(cmn::Extra::dot(l2,n2)))>1e-6)rat_throw_line("L2 and N2 are not orthogonal");

			// check if unit vectors
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(l1))-1.0)>1e-6)rat_throw_line("L1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(n1))-1.0)>1e-6)rat_throw_line("N1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(d1))-1.0)>1e-6)rat_throw_line("D1 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(l2))-1.0)>1e-6)rat_throw_line("L2 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(n2))-1.0)>1e-6)rat_throw_line("N2 is not unit vector");
			if(std::abs(arma::as_scalar(cmn::Extra::vec_norm(d2))-1.0)>1e-6)rat_throw_line("D2 is not unit vector");

			// check that both coordinate systems have same handedness
			if(!(arma::as_scalar(cmn::Extra::dot(d1,cmn::Extra::cross(l1,n1))) 
				-arma::as_scalar(cmn::Extra::dot(d2,cmn::Extra::cross(l2,n2)))<1e-6))
				rat_throw_line("sections do not have same handedness");

			// create matrix
			const arma::Mat<double>::fixed<3,3> M = 
				arma::join_horiz(l1,d1,n1)*arma::join_vert(l2.t(),d2.t(),n2.t());

			// check vectors
			if(!(std::abs(arma::det(M)-1.0)<1e-6))rat_throw_line("system is not right handed");
			if(!arma::all(arma::abs(M*l2 - l1)<1e-6))rat_throw_line("can not align sections in L");
			if(!arma::all(arma::abs(M*n2 - n1)<1e-6))rat_throw_line("can not align sections in N");
			if(!arma::all(arma::abs(M*d2 - d1)<1e-6))rat_throw_line("can not align sections in D");

			// apply matrix to second section in 
			// order to align it with the first one
			R_(i) = M*R_(i); L_(i) = M*L_(i);
			N_(i) = M*N_(i); D_(i) = M*D_(i);
			B_(i) = M*B_(i);

			// get position of the origins of the two coordinate systems
			if(i==0){
				r1 = R0;
			}

			// last point of previous section
			else{
				r1 = R_(i-1).tail_cols(1);
			}

			// first point of this section
			const arma::Col<double>::fixed<3> r2 = R_(i).head_cols(1);

			// shift to location
			R_(i).each_col() += r1 - r2;
		}
	}

	// check if the sections are closed in on themselves
	arma::Row<arma::uword> Frame::is_closed() const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// enclosing box
		arma::Col<double>::fixed<3> Rmin = R_(0).col(0);
		arma::Col<double>::fixed<3> Rmax = R_(0).col(0);

		// find minium and maximum coordinates
		for(arma::uword i=0;i<num_sections;i++){
			Rmin = arma::min(arma::min(R_(i),1),Rmin);
			Rmax = arma::max(arma::max(R_(i),1),Rmax);
		}

		// find maximum span
		arma::Col<double>::fixed<3> Rdim = Rmax-Rmin;

		// typical size
		double dim = arma::max(Rdim);
		
		// allocate
		arma::Row<arma::uword> closed(num_sections);

		// calculate vector between start and end
		for(arma::uword i=0;i<num_sections;i++){
			arma::Col<double>::fixed<3> dR = 
				R_(i).tail_cols(1) - R_(i).head_cols(1);

			// distance between start and end
			double dist = arma::as_scalar(cmn::Extra::vec_norm(dR));

			// check if distance within tolerance
			closed(i) = dist<(dim*tol_closed_);
		}

		// return closed
		return closed;
	}

	// check if the first section is connected to the last section
	bool Frame::is_loop() const{
		// get counters
		const arma::uword num_sections = get_num_sections();

		// enclosing box
		arma::Col<double>::fixed<3> Rmin = R_(0).col(0);
		arma::Col<double>::fixed<3> Rmax = R_(0).col(0);

		// find minium and maximum coordinates
		for(arma::uword i=0;i<num_sections;i++){
			Rmin = arma::min(arma::min(R_(i),1),Rmin);
			Rmax = arma::max(arma::max(R_(i),1),Rmax);
		}

		// find maximum span
		arma::Col<double>::fixed<3> Rdim = Rmax-Rmin;

		// typical size
		double dim = arma::max(Rdim);

		// get coordinates
		arma::Col<double>::fixed<3> R1 = R_(0).col(0);
		arma::Col<double>::fixed<3> R2 = R_(num_sections-1).tail_cols(1);
		arma::Col<double>::fixed<3> dR = R2-R1;

		// distance between start and end
		double dist = arma::as_scalar(cmn::Extra::vec_norm(dR));

		// check if distance is within tolerance
		return dist<(dim*tol_closed_);
	}

	// combine sections into single set of frame
	void Frame::combine(){
		// get counters
		const arma::uword num_sections = get_num_sections();

		// remove last node for all sections but last
		for(arma::uword i=0;i<num_sections-1;i++){
			R_(i) = R_(i).head_cols(R_(i).n_cols-1); L_(i) = L_(i).head_cols(L_(i).n_cols-1);
			N_(i) = N_(i).head_cols(N_(i).n_cols-1); D_(i) = D_(i).head_cols(D_(i).n_cols-1);
			B_(i) = B_(i).head_cols(B_(i).n_cols-1);
		}

		// combine field arrays
		const arma::Mat<double> R = cmn::Extra::field2mat(R_);
		const arma::Mat<double> L = cmn::Extra::field2mat(L_);
		const arma::Mat<double> N = cmn::Extra::field2mat(N_);
		const arma::Mat<double> D = cmn::Extra::field2mat(D_);
		const arma::Mat<double> B = cmn::Extra::field2mat(B_);

		// set size
		R_.set_size(1); L_.set_size(1); N_.set_size(1); D_.set_size(1); 

		// combine field arrays
		R_(0) = R; L_(0) = L; N_(0) = N; D_(0) = D; B_(0) = B;

		// collapse sections (no longer relevant)
		section_ = arma::Row<arma::uword>{0}; 
		turn_ = arma::Row<arma::uword>{0};
	}

	// split sections into smaller sections with num_sub elements
	void Frame::split(const arma::uword num_sub){
		// get counters
		const arma::uword num_sections = get_num_sections();

		// count number of sections
		const arma::Row<arma::uword> num_nodes = get_num_nodes();
		const arma::Row<arma::uword> num_elements = num_nodes-1;
		arma::Row<arma::uword> num_sub_sections = num_elements/num_sub;

		// extend if last section incomplete
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::modulus(num_elements, num_sub)!=0).t();
		if(!idx.is_empty())num_sub_sections(idx) = num_sub_sections(idx)+1;

		// get total number of sub sections
		const arma::uword total_num_sub_sections = arma::accu(num_sub_sections);

		// allocate matrices
		arma::field<arma::Mat<double> > R(1,total_num_sub_sections);
		arma::field<arma::Mat<double> > L(1,total_num_sub_sections);
		arma::field<arma::Mat<double> > N(1,total_num_sub_sections);
		arma::field<arma::Mat<double> > D(1,total_num_sub_sections);
		arma::field<arma::Mat<double> > B(1,total_num_sub_sections);

		// allocate section and turn arrays
		arma::Row<arma::uword> section(total_num_sub_sections);;
		arma::Row<arma::uword> turn(total_num_sub_sections);

		// walk over sections
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_sections;i++){
			for(arma::uword j=0;j<num_sub_sections(i);j++){
				// get cross section indexes (within section)
				const arma::uword idx1 = j*num_sub;
				arma::uword idx2 = (j+1)*num_sub;

				// limit last index
				if(idx2>=R_(i).n_cols)idx2 = R_(i).n_cols-1;

				// copy
				R(cnt) = R_(i).cols(idx1,idx2);
				L(cnt) = L_(i).cols(idx1,idx2);
				N(cnt) = N_(i).cols(idx1,idx2);
				D(cnt) = D_(i).cols(idx1,idx2);
				B(cnt) = B_(i).cols(idx1,idx2);

				// section and turn
				section(cnt) = section_(i);
				turn(cnt) = turn_(i);

				// increment counter
				cnt++;
			}
		}

		// sanity check
		assert(cnt==total_num_sub_sections);

		// set to self
		R_ = R; L_ = L; N_ = N; D_ = D; B_ = B;
		section_ = section; turn_ = turn;
	}

	// apply multiple transformations
	void Frame::apply_transformations(const ShTransPrList &trans){
		for(arma::uword i=0;i<trans.n_elem;i++)
			apply_transformation(trans(i));
	}

	// apply transformation
	void Frame::apply_transformation(const ShTransPr &trans){
		// apply to vectors	
		trans->apply_vectors(R_,L_,'L');
		trans->apply_vectors(R_,N_,'N');
		trans->apply_vectors(R_,D_,'D');
		trans->apply_vectors(R_,B_,'B');

		// apply to coordinates
		trans->apply_coords(R_);
	}

	// reverse the path
	void Frame::reverse(){
		// reverse field arrays
		cmn::Extra::reverse_field(R_); cmn::Extra::reverse_field(L_);
		cmn::Extra::reverse_field(N_); cmn::Extra::reverse_field(D_);
		cmn::Extra::reverse_field(B_);

		// flip sections
		section_ = arma::fliplr(section_); 
		turn_ = arma::fliplr(turn_);

		// flip L and D
		for(arma::uword i=0;i<L_.n_elem;i++){
			L_(i) *= -1; D_(i) *= -1;
		}
	}

	// create loft extrusion of area mesh to volume mesh
	// ShCoilMeshPr Frame::create_loft(const ShAreaPr &area) const{
	// 	// allocate
	// 	arma::Mat<double> R,L,N,D;
	// 	arma::Mat<arma::uword> n,s;
		
	// 	// get nodes
	// 	const arma::Mat<double> R2d = area->get_nodes();
	// 	const arma::Mat<arma::uword> n2d = area->get_elements();

	// 	// edge matrix construction
	// 	arma::Mat<arma::uword> s2d,i2d;	
	// 	area->create_edge_matrix(s2d,i2d);

	// 	// call internal loft function
	// 	create_loft(R,L,N,D,n,s,R2d,n2d,s2d);

	// 	// create coil mesh
	// 	ShCoilMeshPr mesh = CoilMesh::create(R,L,N,D,n,s); 

	// 	// set area and number of cross-sectional elements
	// 	mesh->set_num_cross(area->get_num_elements());
	// 	mesh->set_cross_area(area->calculate_areas());
		
	// 	// return mesh
	// 	return mesh;
	// }

	// set type for the frame
	void Frame::set_conductor_type(const DbType conductor_type){
		conductor_type_ = conductor_type;
	}

	// get type for the frame
	DbType Frame::get_conductor_type() const{
		return conductor_type_;
	}


	// loft core function
	void Frame::create_loft(arma::Mat<double> &R, 
		arma::Mat<double> &L, arma::Mat<double> &N, 
		arma::Mat<double> &D, arma::Mat<arma::uword> &n, 
		arma::Mat<arma::uword> &s, const arma::Mat<double> &R2d, 
		const arma::Mat<arma::uword> &n2d,
		const arma::Mat<arma::uword> &s2d) const{

		// get counters
		const arma::uword num_sections = get_num_sections();

		// check whether the path forms a closed loop
		const arma::Row<arma::uword> close = is_closed();

		// check if path is reversed
		double sign = arma::as_scalar(
			cmn::Extra::dot(cmn::Extra::cross(N_(0).col(0),L_(0).col(0)),D_(0).col(0)));
		bool is_reversed = sign>0;

		// get counters for 2D mesh
		const arma::uword Ne2d = n2d.n_cols; // number of elements
		const arma::uword Nn2d = R2d.n_cols; // number of nodes
		const arma::uword Nnpe = n2d.n_rows; // number of nodes per element
		const arma::uword Nse2d = s2d.n_cols; // number of surface elements
		const arma::uword Nnpse = s2d.n_rows; // number of nodes per surface element

		// allocate 3D mesh field arrays
		arma::field<arma::Mat<double> > R3d(1,num_sections);
		arma::field<arma::Mat<arma::uword> > n3d(1,num_sections);
		arma::field<arma::Mat<arma::uword> > s3d(1,num_sections);

		// allocate 3D orientation vectors
		arma::field<arma::Mat<double> > L3d(1,num_sections);
		arma::field<arma::Mat<double> > N3d(1,num_sections);
		arma::field<arma::Mat<double> > D3d(1,num_sections);

		// keep track of node shift with the sections
		arma::uword node_offset = 0;

		// walk over sections
		for(arma::uword j=0;j<num_sections;j++){
			// number of nodes in the base path
			const arma::uword num_base = R_(j).n_cols;

			// allocate coordinate matrices
			arma::Mat<double> x(Nn2d,num_base), y(Nn2d,num_base), z(Nn2d,num_base);

			// walk over 2D coordinates and lay out along path
			for(arma::uword i=0;i<Nn2d;i++){
				// transformation for block type using block vector
				if(conductor_type_==block){
					x.row(i) = R_(j).row(0) + B_(j).row(0)*R2d(0,i) + D_(j).row(0)*R2d(1,i);
					y.row(i) = R_(j).row(1) + B_(j).row(1)*R2d(0,i) + D_(j).row(1)*R2d(1,i);
					z.row(i) = R_(j).row(2) + B_(j).row(2)*R2d(0,i) + D_(j).row(2)*R2d(1,i);
				}

				// other types presently use normal vector
				else{
					x.row(i) = R_(j).row(0) + N_(j).row(0)*R2d(0,i) + D_(j).row(0)*R2d(1,i);
					y.row(i) = R_(j).row(1) + N_(j).row(1)*R2d(0,i) + D_(j).row(1)*R2d(1,i);
					z.row(i) = R_(j).row(2) + N_(j).row(2)*R2d(0,i) + D_(j).row(2)*R2d(1,i);
				}	
			}

			// allocate orientation matrices
			arma::Mat<double> lx(Nn2d,num_base), dx(Nn2d,num_base), nx(Nn2d,num_base);
			arma::Mat<double> ly(Nn2d,num_base), dy(Nn2d,num_base), ny(Nn2d,num_base);
			arma::Mat<double> lz(Nn2d,num_base), dz(Nn2d,num_base), nz(Nn2d,num_base);

			// setup node orientation vectors
			for(arma::uword i=0;i<Nn2d;i++){
				lx.row(i) = L_(j).row(0); dx.row(i) = D_(j).row(0); nx.row(i) = N_(j).row(0);
				ly.row(i) = L_(j).row(1); dy.row(i) = D_(j).row(1); ny.row(i) = N_(j).row(1);
				lz.row(i) = L_(j).row(2); dz.row(i) = D_(j).row(2); nz.row(i) = N_(j).row(2);
			}

			// close last section to first
			arma::uword num_base_used = num_base;
			if(close(j))num_base_used--;

			// generate coordinate vector
			R3d(j).set_size(3,Nn2d*num_base_used);
			R3d(j).row(0) = arma::reshape(x,1,Nn2d*num_base_used);
			R3d(j).row(1) = arma::reshape(y,1,Nn2d*num_base_used);
			R3d(j).row(2) = arma::reshape(z,1,Nn2d*num_base_used);

			// allocate mesh elements
			n3d(j).set_size(2*Nnpe,Ne2d*(num_base_used-1));
			s3d(j).set_size(2*Nnpse,Nse2d*(num_base_used-1));

			// generate mesh elements for volume
			if(Ne2d>0){
				for(arma::uword i=0;i<num_base_used-1;i++){
					n3d(j).submat(0,i*Ne2d,Nnpe-1,(i+1)*Ne2d-1) = n2d + i*Nn2d;
					n3d(j).submat(Nnpe,i*Ne2d,2*Nnpe-1,(i+1)*Ne2d-1) = n2d + (i+1)*Nn2d;
				}
			}

			// generate mesh elements for surface
			if(Nse2d>0){
				for(arma::uword i=0;i<num_base_used-1;i++){
					s3d(j).submat(0,i*Nse2d,Nnpse-1,(i+1)*Nse2d-1) = s2d + i*Nn2d;
					s3d(j).submat(Nnpse,i*Nse2d,2*Nnpse-1,(i+1)*Nse2d-1) = s2d + (i+1)*Nn2d;
				}
			}
		
			// close mesh for closed paths
			if(close(j)){
				n3d(j) = arma::join_horiz(n3d(j),
					arma::join_vert(n2d + (num_base_used-1)*Nn2d,n2d));
				s3d(j) = arma::join_horiz(s3d(j),
					arma::join_vert(s2d + (num_base_used-1)*Nn2d,s2d));
			}
		
			// generate longitudinal orientation vector
			L3d(j).set_size(3,Nn2d*num_base_used);
			L3d(j).row(0) = arma::reshape(lx,1,Nn2d*num_base_used);
			L3d(j).row(1) = arma::reshape(ly,1,Nn2d*num_base_used);
			L3d(j).row(2) = arma::reshape(lz,1,Nn2d*num_base_used);

			// generate normal orientation vector
			N3d(j).set_size(3,Nn2d*num_base_used);
			N3d(j).row(0) = arma::reshape(nx,1,Nn2d*num_base_used);
			N3d(j).row(1) = arma::reshape(ny,1,Nn2d*num_base_used);
			N3d(j).row(2) = arma::reshape(nz,1,Nn2d*num_base_used);

			// generate transverse orientation vector
			D3d(j).set_size(3,Nn2d*num_base_used);
			D3d(j).row(0) = arma::reshape(dx,1,Nn2d*num_base_used);
			D3d(j).row(1) = arma::reshape(dy,1,Nn2d*num_base_used);
			D3d(j).row(2) = arma::reshape(dz,1,Nn2d*num_base_used);

			// invert mesh elements
			// in case path was defined in the "wrong" direction
			if(is_reversed){
				// flip face 1
				n3d(j).rows(0,n3d(j).n_rows/2-1) = 
					arma::flipud(n3d(j).rows(0,n3d(j).n_rows/2-1));

				// flip face 2
				n3d(j).rows(n3d(j).n_rows/2,n3d(j).n_rows-1) = 
					arma::flipud(n3d(j).rows(n3d(j).n_rows/2,n3d(j).n_rows-1));

				// flip face 1
				s3d(j).rows(0,s3d(j).n_rows/2-1) = 
					arma::flipud(s3d(j).rows(0,s3d(j).n_rows/2-1));

				// flip face 2
				s3d(j).rows(s3d(j).n_rows/2,s3d(j).n_rows-1) = 
					arma::flipud(s3d(j).rows(s3d(j).n_rows/2,s3d(j).n_rows-1));
			}

			// cap start and end for quadrilateral surface of hexahedron mesh
			if(!close(j) && n.n_rows==4){
				arma::Mat<arma::uword> ncap = n2d.rows(arma::Col<arma::uword>::fixed<4>{0,1,3,2});
				//s3d(j) = arma::join_horiz(ncap,s3d(j));
				s3d(j) = arma::join_horiz(ncap,s3d(j),ncap+(num_base_used-1)*Nn2d);
			}

			// apply node offset
			n3d(j) += node_offset;
			s3d(j) += node_offset;
			node_offset += Nn2d*num_base_used;
		}

		// combine arrays for output
		R = cmn::Extra::field2mat(R3d); L = cmn::Extra::field2mat(L3d);
		N = cmn::Extra::field2mat(N3d); D = cmn::Extra::field2mat(D3d);
		n = cmn::Extra::field2mat(n3d); s = cmn::Extra::field2mat(s3d);

		// fix quadrilateral elements 
		// by flipping second set of rows
		if(n.n_rows==4)n.swap_rows(2,3);
		if(s.n_rows==4)s.swap_rows(2,3);
	}

	// perform coordinate shift along normal direction
	arma::Mat<double> Frame::perform_normal_shift(
		const arma::uword section, const arma::Row<double> &n) const{
		// allocate output
		arma::Mat<double> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<double> ashear = calc_ashear(section);

			// perform shift with squeeze mapping
			//Rs = R_(section) + B_(section).each_row()%n;
			Rs = R_(section) + B_(section).each_row()%(n/arma::cos(ashear)); // ?
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section).each_row()%n;
		}

		// return shifted vectors
		return Rs;
	}

	// perform coordinate shift along 
	// normal and transverse direction
	arma::Mat<double> Frame::perform_shift(
		const arma::uword section, const arma::Row<double> &n, const arma::Row<double> &d) const{
		// allocate output
		arma::Mat<double> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<double> ashear = calc_ashear(section);

			// perform shift with shear and squeeze mapping
			Rs = R_(section) + 
				B_(section).each_row()%(n/arma::cos(ashear) + arma::sin(ashear)%(d%arma::cos(ashear))) + 
				D_(section).each_row()%(d%arma::cos(ashear));
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section).each_row()%n + D_(section).each_row()%d;
		}

		// return shifted vectors
		return Rs;
	}

	// perform coordinate shift along 
	// normal and transverse direction
	arma::Mat<double> Frame::perform_shift(
		const arma::uword section, const double n, const double d) const{
		// allocate output
		arma::Mat<double> Rs;

		// perform shift 
		if(conductor_type_==block){
			// calculate shear angle
			const arma::Row<double> ashear = calc_ashear(section);

			// perform shift with shear and squeeze mapping
			Rs = R_(section) + 
				B_(section).each_row()%(n/arma::cos(ashear) + arma::sin(ashear)%(d*arma::cos(ashear))) + 
				D_(section).each_row()%(d*arma::cos(ashear));
		}else{
			// perform normal shift
			Rs = R_(section) + N_(section)*n + D_(section)*d;
		}

		// return shifted vectors
		return Rs;
	}

	// get sub frame
	ShFramePr Frame::get_sub(const arma::uword section, const arma::uword idx1, const arma::uword idx2) const{
		// create sub frame
		ShFramePr frm = Frame::create();

		// copy coordinates and orientation vectors
		frm->R_.set_size(1); frm->R_(0) = R_(section).cols(idx1,idx2);
		frm->L_.set_size(1); frm->L_(0) = L_(section).cols(idx1,idx2);
		frm->N_.set_size(1); frm->N_(0) = N_(section).cols(idx1,idx2);
		frm->D_.set_size(1); frm->D_(0) = D_(section).cols(idx1,idx2);
		frm->B_.set_size(1); frm->B_(0) = B_(section).cols(idx1,idx2);

		// set location
		frm->section_.set_size(1); frm->section_(0) = section_(section);
		frm->turn_.set_size(1); frm->turn_(0) = turn_(section);

		// copy type
		frm->conductor_type_ = conductor_type_;

		// return subframe
		return frm;
	}

	// get sub frame
	ShFramePr Frame::get_sub(const arma::uword section) const{
		// create sub frame
		ShFramePr frm = Frame::create();

		// copy coordinates and orientation vectors
		frm->R_.set_size(1); frm->R_(0) = R_(section);
		frm->L_.set_size(1); frm->L_(0) = L_(section);
		frm->N_.set_size(1); frm->N_(0) = N_(section);
		frm->D_.set_size(1); frm->D_(0) = D_(section);
		frm->B_.set_size(1); frm->B_(0) = B_(section);

		// set location
		frm->section_.set_size(1); frm->section_(0) = section_(section);
		frm->turn_.set_size(1); frm->turn_(0) = turn_(section);

		// copy type
		frm->conductor_type_ = conductor_type_;

		// return subframe
		return frm;
	}

	// get type
	std::string Frame::get_type(){
		return "mdl::frame";
	}

	// method for serialization into json
	void Frame::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// type
		js["type"] = get_type();
		
		// node coordinates
		for(arma::uword k=0;k<R_.n_elem;k++){
			Json::Value sect;
			for(arma::uword i=0;i<R_(k).n_cols;i++){
				Json::Value jsr,jsl,jsn,jsd,jsb;
				for(arma::uword j=0;j<R_(k).n_rows;j++){
					jsr.append(R_(k)(j,i)); jsl.append(L_(k)(j,i));
					jsn.append(N_(k)(j,i)); jsd.append(D_(k)(j,i));
					jsb.append(B_(k)(j,i));
				}
				sect["R"].append(jsr); sect["L"].append(jsl);
				sect["N"].append(jsn); sect["D"].append(jsd);
				sect["B"].append(jsb);
			}
			js["sect"].append(sect);
		}

		// section and turn
		for(arma::uword i=0;i<section_.n_elem;i++){
			js["section"].append((int)section_(i));
			js["turn"].append((int)turn_(i));
		}

		// conductor type
		js["conductor_type"] = (int)conductor_type_;
	}

	// method for deserialisation from json
	void Frame::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		// get number of sections
		const arma::uword num_sections = js["sect"].size();
		R_.set_size(1,num_sections); L_.set_size(1,num_sections);
		N_.set_size(1,num_sections); D_.set_size(1,num_sections);
		B_.set_size(1,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// get section
			Json::Value sect = js["sect"].get(i,0);

			// get number of nodes in this section
			const arma::uword num_nodes = sect["R"].size();

			// allocate
			R_(i).set_size(3,num_nodes); L_(i).set_size(3,num_nodes);
			N_(i).set_size(3,num_nodes); D_(i).set_size(3,num_nodes);
			B_(i).set_size(3,num_nodes);

			// walk over nodes
			for(arma::uword j=0;j<num_nodes;j++){
				// get node from json
				Json::Value jsr = sect["R"].get(j,0); Json::Value jsl = sect["L"].get(j,0);
				Json::Value jsn = sect["N"].get(j,0); Json::Value jsd = sect["D"].get(j,0);
				Json::Value jsb = sect["B"].get(j,0);

				// walk over xyz
				for(arma::uword k=0;k<3;k++){
					R_(i)(k,j) = jsr.get(k,0).asDouble(); L_(i)(k,j) = jsl.get(k,0).asDouble();
					N_(i)(k,j) = jsn.get(k,0).asDouble(); D_(i)(k,j) = jsd.get(k,0).asDouble();
					B_(i)(k,j) = jsb.get(k,0).asDouble();
				}
			}
		}

		// number of sections
		section_.set_size(num_sections);
		turn_.set_size(num_sections);
		for(arma::uword i=0;i<num_sections;i++){
			section_(i) = js["section"].get(i,0).asUInt64();
			turn_(i) = js["turn"].get(i,0).asUInt64();
		}

		// conductor type
		set_conductor_type((DbType)js["conductor_type"].asInt());
	}

}}