/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "emitterbeam.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	EmitterBeam::EmitterBeam(){
		// protons are default
		set_proton();
	}

	// factory
	ShEmitterBeamPr EmitterBeam::create(){
		return std::make_shared<EmitterBeam>();
	}
				
	// create correlated data
	arma::Mat<double> EmitterBeam::correlated_normdist(
		const double correlation, const arma::uword num_cols){
		// create correlation atrix for normal direction
		arma::Mat<double>::fixed<2,2> M;
		M.row(0) = arma::Row<double>{1.0, correlation};
		M.row(1) = arma::Row<double>{correlation, 1.0};
		const arma::Mat<double>::fixed<2,2> Mc = arma::chol(M).t();
		return Mc*arma::Mat<double>(2,num_cols,arma::fill::randn);
	}

	// set protons
	void EmitterBeam::set_proton(){
		rest_mass_ = 0.938; // [GeV/C^2] 
		charge_ = 1; // elementary charge
	}

	// set electrons
	void EmitterBeam::set_electron(){
		rest_mass_ = 0.51099895000e-3; // [GeV/C^2] 
		charge_ = -1; // elementary charge
	}

	// set rest mass
	void EmitterBeam::EmitterBeam::set_rest_mass(const double rest_mass){
		if(rest_mass<=0)rat_throw_line("rest mass must be larger than zero");
		rest_mass_ = rest_mass;
	}

	// set charge
	void EmitterBeam::set_charge(const double charge){
		charge_ = charge;
	}

	// set beam energy
	void EmitterBeam::set_beam_energy(const double beam_energy){
		if(beam_energy<=0)rat_throw_line("beam energy must be larger than zero");
		beam_energy_ = beam_energy;
	}

	// set particle lifetime
	void EmitterBeam::set_lifetime(const arma::uword lifetime){
		if(lifetime<=0)rat_throw_line("lifetime must be larger than zero");
		lifetime_ = lifetime; 
	}

	// set starting index
	void EmitterBeam::set_start_idx(const arma::uword start_idx){
		start_idx_ = start_idx;
	}

	// getting the rest mass of the particles
	double EmitterBeam::get_rest_mass() const{
		return rest_mass_;
	}

	// set orientation
	void EmitterBeam::set_spawn_coord(
		const arma::Col<double>::fixed<3> &R, 
		const arma::Col<double>::fixed<3> &L, 
		const arma::Col<double>::fixed<3> &N, 
		const arma::Col<double>::fixed<3> &D){

		R_ = R;
		L_ = L.each_row()/cmn::Extra::vec_norm(L); 
		N_ = N.each_row()/cmn::Extra::vec_norm(N);
		D_ = D.each_row()/cmn::Extra::vec_norm(D);
	} 

	// set position xx
	void EmitterBeam::set_xx(const double corr_xx, const double sigma_x, const double sigma_xa){
		if(corr_xx<0)rat_throw_line("correlation must be larger than zero");
		if(corr_xx>1)rat_throw_line("correlation must be smaller than one");
		if(sigma_x<0)rat_throw_line("beam size must be positive");
		corr_xx_ = corr_xx; sigma_x_ = sigma_x; sigma_xa_ = sigma_xa;
	}

	// set position yy
	void EmitterBeam::set_yy(const double corr_yy, const double sigma_y, const double sigma_ya){
		if(corr_yy<0)rat_throw_line("correlation must be larger than zero");
		if(corr_yy>1)rat_throw_line("correlation must be smaller than one");
		if(sigma_y<0)rat_throw_line("beam size must be positive");
		corr_yy_ = corr_yy; sigma_y_ = sigma_y; sigma_ya_ = sigma_ya;
	}


	// particle creation
	arma::field<Particle> EmitterBeam::spawn_particles(const arma::uword num_particles) const{
		// calculate relativistic velocity in fraction of lightspeed
		// [
		if(rest_mass_==0)rat_throw_line("rest mass is not set");
		if(beam_energy_==0)rat_throw_line("beam energy is not set");
		
		// calculate relativistic velocity
		// const double V = std::sqrt(1.0 - ));
		// if(V>1.0)rat_throw_line("can not exceed the speed of light sherlock");


		const double gamma = beam_energy_/rest_mass_;
		const double V = std::sqrt(1.0 - 1.0/(gamma*gamma));

		// generate data in x
		const arma::Mat<double> XX = correlated_normdist(corr_xx_, num_particles);
		const arma::Mat<double> YY = correlated_normdist(corr_yy_, num_particles);

		// calculate angles
		const arma::Row<double> alpha = sigma_xa_*XX.row(1);
		const arma::Row<double> beta = sigma_ya_*YY.row(1);

		// get position in local coordinates
		const arma::Row<double> u = sigma_x_*XX.row(0);
		const arma::Row<double> v = sigma_y_*YY.row(0);
		const arma::Row<double> w(num_particles,arma::fill::zeros);

		// get velocity in local coordinates
		const arma::Row<double> du = V*arma::sin(alpha);
		const arma::Row<double> dv = V*arma::sin(beta);
		const arma::Row<double> dw = V*arma::cos(alpha)%arma::cos(beta);
		
		// create start positions
		arma::Mat<double> R0(3,num_particles);
		arma::Mat<double> V0(3,num_particles);

		// calculate cartesian coordinates
		for(arma::uword i=0;i<3;i++){
			R0.row(i) = R_(i) + L_(i)*w + N_(i)*u + D_(i)*v;
			V0.row(i) = L_(i)*dw + N_(i)*du + D_(i)*dv;
		}	

		// allocate list of particles
		arma::field<Particle> particles(num_particles);

		// create all particles
		for(arma::uword i=0;i<num_particles;i++){
			// set maximum number of steps
			particles(i).set_num_steps(lifetime_);

			// set start coordinate and velocity
			particles(i).set_startcoord(R0.col(i),V0.col(i));

			// set start index
			particles(i).set_start_index(start_idx_);

			// set mass
			particles(i).set_rest_mass(rest_mass_);
			particles(i).set_charge(charge_);

			// setup internal storage
			particles(i).setup();
		}

		// return list of particles
		return particles;
	}

	// get type
	std::string EmitterBeam::get_type(){
		return "mdl::emitterbeam";
	}

	// method for serialization into json
	void EmitterBeam::serialize(Json::Value &js, cmn::SList &) const{
		// properties
		js["type"] = get_type();

		// beam parameters
		js["beam_energy"] = beam_energy_;
		js["rest_mass"] = rest_mass_;
		js["charge"] = charge_;

		// position orientation
		js["position"] = Node::serialize_arma(R_.t());
		js["longitudinal"] = Node::serialize_arma(L_.t());
		js["normal"] = Node::serialize_arma(N_.t());
		js["transverse"] = Node::serialize_arma(D_.t());

		// beam
		js["sigma"] = sigma_;
		js["corr_xx"] = corr_xx_;
		js["sigma_x"] = sigma_x_;
		js["sigma_xa"] = sigma_xa_;
		js["corr_yy"] = corr_yy_;
		js["sigma_y"] = sigma_y_;
		js["sigma_ya"] = sigma_ya_;

		// tracking
		js["lifetime"] = (unsigned int)lifetime_;
		js["start_idx"] = (unsigned int)start_idx_;
	}

	// method for deserialisation from json
	void EmitterBeam::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// beam parameters
		beam_energy_ = js["beam_energy"].asDouble();
		rest_mass_ = js["rest_mass"].asDouble();
		charge_ = js["charge"].asDouble();

		// position orientation
		R_ = Node::deserialize_arma(js["position"]).t();
		L_ = Node::deserialize_arma(js["longitudinal"]).t();
		N_ = Node::deserialize_arma(js["normal"]).t();
		D_ = Node::deserialize_arma(js["transverse"]).t();

		// beam
		sigma_ = js["sigma"].asDouble();
		corr_xx_ = js["corr_xx"].asDouble();
		sigma_x_ = js["sigma_x"].asDouble();
		sigma_xa_ = js["sigma_xa"].asDouble();
		corr_yy_ = js["corr_yy"].asDouble();
		sigma_y_ = js["sigma_y"].asDouble();
		sigma_ya_ = js["sigma_ya"].asDouble();

		// tracking
		lifetime_ = js["lifetime"].asUInt64();
		start_idx_ = js["start_idx"].asUInt64();
	}


}}
