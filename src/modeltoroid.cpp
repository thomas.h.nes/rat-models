/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modeltoroid.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelToroid::ModelToroid(){

	}

	// constructor with setting of the base path
	ModelToroid::ModelToroid(ShModelPr base_model){
		set_base_model(base_model);
	}

	// factory
	ShModelToroidPr ModelToroid::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelToroid>();
	}

	// factory with setting of the base path
	ShModelToroidPr ModelToroid::create(ShModelPr base_model){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelToroid>(base_model);
	}


	// setting the number of coils
	void ModelToroid::set_num_coils(const arma::uword num_coils){
		assert(num_coils>0);
		if(num_coils<=0)rat_throw_line("number of coils must be larger than zero");
		num_coils_ = num_coils;
	}

	// setting the radius
	void ModelToroid::set_radius(const double radius){
		if(radius<0)rat_throw_line("number of coils must be zero or larger");
		radius_ = radius;
	}

	// set basemodel
	void ModelToroid::set_base_model(ShModelPr base_model){
		if(base_model==NULL)rat_throw_line("base model points to zero");
		base_model_ = base_model;
	}

	// get number of coils stored
	// in this model and all child models/coils
	arma::uword ModelToroid::get_num_objects() const{
		return num_coils_*base_model_->get_num_objects();
	}

	// set parallel setup
	void ModelToroid::set_parallel_setup(const bool parallel_setup){
		parallel_setup_ = parallel_setup;
	}

	// mesh generation
	ShMeshPrList ModelToroid::create_mesh() const{
		// check
		if(num_coils_==0)rat_throw_line("number of coils is not set");
		
		// get coil mesh from base
		ShMeshPrList base_mesh = base_model_->create_mesh();

		// move and rotate coil into position
		for(arma::uword i=0;i<base_mesh.n_elem;i++){
			base_mesh(i)->apply_transformation(TransRotate::create(1,0,0,arma::datum::pi/2));
			//base_mesh(i)->apply_transformation(TransRotate::create(0,0,1,arma::datum::pi/2));
			base_mesh(i)->apply_transformation(TransTranslate::create(radius_,0,0));
		}

		// set counter for number of coils to zero
		const arma::uword num_base = base_mesh.n_elem;
		const arma::uword num_meshes = num_base*num_coils_;

		// allocate output
		ShMeshPrList toroid_mesh(num_meshes);

		// walk over coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,num_coils_,parallel_setup_,[&](int i, int){
			for(arma::uword j=0;j<num_base;j++){
				// deep copy base (also adds coolant, circuit and conductor)
				toroid_mesh(i*num_base + j) = base_mesh(j)->copy();

				// rotate coil into position
				toroid_mesh(i*num_base + j)->apply_transformation(
					TransRotate::create(0,0,1,i*2*arma::datum::pi/num_coils_));

				// apply global transformations
				toroid_mesh(i*num_base + j)->apply_transformations(trans_);

				// append name
				toroid_mesh(i*num_base + j)->append_name(myname_);
			}
		});

		// return combined mesh
		return toroid_mesh;
	}

	// mesh generation
	ShEdgesPrList ModelToroid::create_edge() const{
		// check
		if(num_coils_==0)rat_throw_line("number of coils is not set");
		
		// get coil mesh from base
		ShEdgesPrList base_edges = base_model_->create_edge();

		// move and rotate coil into position
		for(arma::uword i=0;i<base_edges.n_elem;i++){
			base_edges(i)->apply_transformation(TransRotate::create(1,0,0,arma::datum::pi/2));
			//base_mesh(i)->apply_transformation(TransRotate::create(0,0,1,arma::datum::pi/2));
			base_edges(i)->apply_transformation(TransTranslate::create(radius_,0,0));
		}

		// set counter for number of coils to zero
		const arma::uword num_base = base_edges.n_elem;
		const arma::uword num_meshes = num_base*num_coils_;

		// allocate output
		ShEdgesPrList toroid_edges(num_meshes);

		// walk over coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,num_coils_,parallel_setup_,[&](int i, int){
			for(arma::uword j=0;j<num_base;j++){
				// deep copy base
				toroid_edges(i*num_base + j) = base_edges(j)->copy();

				// rotate coil into position
				toroid_edges(i*num_base + j)->apply_transformation(
					TransRotate::create(0,0,1,i*2*arma::datum::pi/num_coils_));

				// apply global transformations
				toroid_edges(i*num_base + j)->apply_transformations(trans_);

				// append name
				toroid_edges(i*num_base + j)->append_name(myname_);
			}
		});

		// set tags
		//add_tags(toroid_edges);

		// return combined mesh
		return toroid_edges;
	}

	// get type
	std::string ModelToroid::get_type(){
		return "mdl::modeltoroid";
	}

	// method for serialization into json
	void ModelToroid::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Nameable::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["num_coils"] = (unsigned int)num_coils_;
		js["radius"] = radius_;
		js["psetup"] = parallel_setup_;

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_model_, list);
	}

	// method for deserialisation from json
	void ModelToroid::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Nameable::deserialize(js,list,factory_list);
		Transformations::deserialize(js,list,factory_list);

		// settings
		set_num_coils(js["num_coils"].asUInt64());
		set_radius(js["radius"].asDouble());
		set_parallel_setup(js["psetup"].asBool());

		// subnodes
		base_model_ = cmn::Node::deserialize_node<Model>(js["base"], list, factory_list);
	}

}}