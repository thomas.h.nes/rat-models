/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "drivedc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveDC::DriveDC(){

	}

	// constructor
	DriveDC::DriveDC(const double scaling, const arma::uword id){
		set_scaling(scaling); set_id(id);
	}

	// factory
	ShDriveDCPr DriveDC::create(){
		return std::make_shared<DriveDC>();
	}

	// factory
	ShDriveDCPr DriveDC::create(const double scaling, const arma::uword id){
		return std::make_shared<DriveDC>(scaling,id);
	}

	// set current
	void DriveDC::set_scaling(const double scaling){
		scaling_ = scaling;
	}

	// get current
	double DriveDC::get_scaling(const double) const{
		return scaling_;
	}

	// get current derivative
	double DriveDC::get_dscaling(const double /*time*/) const{
		return 0;
	}

	// get type
	std::string DriveDC::get_type(){
		return "mdl::drivedc";
	}

	// method for serialization into json
	void DriveDC::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["scaling"] = scaling_;
	}

	// method for deserialisation from json
	void DriveDC::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		Drive::deserialize(js,list,factory_list);
		set_scaling(js["scaling"].asDouble());
	}

}}