/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathRectangle::PathRectangle(){
		
	}

	// constructor with dimension input
	PathRectangle::PathRectangle(
		const double width, const double height, 
		const double radius, const double element_size){

		// initial user input checking
		if(width<0)rat_throw_line("can not set negative length");
		if(height<0)rat_throw_line("can not set negative length");
		if(element_size<0)rat_throw_line("element size must be positive");
		if(radius<0)rat_throw_line("radius must be positive");
		
		// set parameters
		set_width(width); set_height(height); 
		set_radius(radius); set_element_size(element_size); 
	}

	// factory
	ShPathRectanglePr PathRectangle::create(){
		return std::make_shared<PathRectangle>();
	}

	// factory with dimension input
	ShPathRectanglePr PathRectangle::create(
		const double width, const double height, 
		const double radius, const double element_size){
		return std::make_shared<PathRectangle>(width,height,radius,element_size);
	}

	// set coil width
	void PathRectangle::set_width(const double width){
		if(width<=0)rat_throw_line("coil width must be larger than zero");
		width_ = width;
	}

	// set coil height
	void PathRectangle::set_height(const double height){
		if(height<=0)rat_throw_line("coil height must be larger than zero");
		height_ = height;
	}

	// set radius
	void PathRectangle::set_radius(const double radius){
		if(radius<=0)rat_throw_line("coil radius must be larger than zero");
		radius_ = radius;
	}

	// set element size 
	void PathRectangle::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// path offset
	void PathRectangle::set_offset(const double offset){
		offset_ = offset;
	}

	// set bending radius
	void PathRectangle::set_bending_radius(const double bending_radius){
		bending_radius_ = bending_radius;
	}

	// get frame
	ShFramePr PathRectangle::create_frame() const{
		// check input
		if(width_==0)rat_throw_line("coil width is not set");
		if(height_==0)rat_throw_line("coil height is not set");
		if(radius_==0)rat_throw_line("coil radius is not set");
		if(element_size_==0)rat_throw_line("element size is not set");

		// calculate lengths of straight sections
		const double ell1 = height_-2*radius_;
		const double ell2 = width_-2*radius_;

		// final shape check
		if(ell1<-1e-9)rat_throw_line("height must be larger than two radii");
		if(ell2<-1e-9)rat_throw_line("width must be larger than two radii");

		// create an arc
		ShPathArcPr path_arc = PathArc::create(radius_,arma::datum::pi/2,element_size_,offset_);

		// create straight section
		ShPathPr path_straight1A, path_straight1B;
		if(bending_radius_==0){
			if(ell1>0){
				path_straight1A = PathStraight::create(ell1/2, element_size_);
				path_straight1B = path_straight1A;
			}
		}

		// when straight section is curved
		else{
			const double arc_length = ell1/(2*bending_radius_);
			const double radA = bending_radius_-ell2/2-radius_;
			const double radB = bending_radius_+ell2/2+radius_;
			if(radA<=0)rat_throw_line("bending radius not feasible");
			if(radB<=0)rat_throw_line("bending radius not feasible");
			path_straight1A = PathArc::create(radA, -arc_length, element_size_);
			path_straight1B = PathArc::create(radB, arc_length, element_size_);
		}
		

		// second straight path
		ShPathStraightPr path_straight2;
		if(ell2>0)path_straight2 = PathStraight::create(ell2/2,element_size_);

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1A);
		}
		pathgroup->add_path(path_arc);
		if(ell2>1e-9){
			pathgroup->add_path(path_straight2); 
			pathgroup->add_path(path_straight2);
		}
		pathgroup->add_path(path_arc);
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1B);
			pathgroup->add_path(path_straight1B);
		}
		pathgroup->add_path(path_arc);
		if(ell2>1e-9){
			pathgroup->add_path(path_straight2);
			pathgroup->add_path(path_straight2);
		}
		pathgroup->add_path(path_arc);
		if(ell1>1e-9){
			pathgroup->add_path(path_straight1A);
		}
		pathgroup->add_translation(width_/2,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame();

		// apply transformations
		frame->apply_transformations(trans_);

		// return frame
		return frame;
	}

	// get type
	std::string PathRectangle::get_type(){
		return "mdl::pathrectangle";
	}

	// method for serialization into json
	void PathRectangle::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["width"] = width_;
		js["height"] = height_;
		js["radius"] = radius_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
		js["bending_radius"] = bending_radius_;
	}

	// method for deserialisation from json
	void PathRectangle::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Transformations::deserialize(js,list,factory_list);
		
		// properties
		set_width(js["width"].asDouble());
		set_height(js["height"].asDouble());
		set_radius(js["radius"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_offset(js["offset"].asDouble());
		set_bending_radius(js["bending_radius"].asDouble());
	}

}}
