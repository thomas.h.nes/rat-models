/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathcable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathCable::PathCable(){

	}

	// constructor with input of base path
	PathCable::PathCable(ShPathPr base){
		set_base(base);
	}

	// factory methods
	ShPathCablePr PathCable::create(){
		return std::make_shared<PathCable>();
	}

	// factory with input of base path
	ShPathCablePr PathCable::create(ShPathPr base){
		return std::make_shared<PathCable>(base);
	}

	// set base path
	void PathCable::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base points to zero");
		base_ = base;
	}

	// reverse winding direction
	void PathCable::set_reverse_base(const bool reverse_base){
		reverse_base_ = reverse_base;
	}

	// set number of turns in coil block
	void PathCable::set_num_turns(const arma::uword num_turns){
		if(num_turns<=0)rat_throw_line("number of turns must be larger than zero");
		num_turns_ = num_turns;
	}

	// set the thickness of the cable
	void PathCable::set_turn_step(const double turn_step){
		if(turn_step<=0)rat_throw_line("turn step must be larger than zero");
		turn_step_ = turn_step;
	}

	// set the thickness of the insulation layer
	void PathCable::set_idx_start(const arma::uword idx_start){
		idx_start_ = idx_start;
	}

	// set number of sections to add
	void PathCable::set_num_add(const arma::sword num_add){
		num_add_ = num_add;
	}

	// set increment position
	void PathCable::set_idx_incr(const arma::uword idx_incr){
		idx_incr_ = idx_incr;
	}

	// set cable offset
	void PathCable::set_num_offset(const arma::sword num_offset){
		num_offset_ = num_offset;
	}

	// set cable offset
	void PathCable::set_offset(const double offset){
		offset_ = offset;
	}

	// allow for disabling increment
	void PathCable::set_disable_increment(
		const bool disable_increment){
		disable_increment_ = disable_increment;
	}

	// update function
	ShFramePr PathCable::create_frame() const{
		// check input
		if(base_==NULL)rat_throw_line("base is not set");
		
		// create frame
		ShFramePr frame = base_->create_frame();

		// reverse frame if requested
		if(reverse_base_==true)frame->reverse();	

		// number of sections in base
		const arma::uword num_base_sections = frame->get_num_sections();

		// calculate number of sections in cable
		const arma::uword num_cable_sections = num_base_sections*num_turns_ + num_add_;

		// check user input
		if(idx_start_>=num_base_sections)rat_throw_line("start index exceeds number of base sections");
		if(idx_incr_>=num_base_sections)rat_throw_line("increment index exceeds number of base sections");
		if(-num_add_>=(arma::sword)num_cable_sections)rat_throw_line("can not remove all sections");

		// allocate cable coordinate system
		arma::field<arma::Mat<double> > R(1,num_cable_sections); 
		arma::field<arma::Mat<double> > L(1,num_cable_sections);
		arma::field<arma::Mat<double> > D(1,num_cable_sections); 
		arma::field<arma::Mat<double> > N(1,num_cable_sections);
		arma::field<arma::Mat<double> > B(1,num_cable_sections);

		// allocate section and turn indexes
		arma::Row<arma::uword> section_idx(num_cable_sections);
		arma::Row<arma::uword> turn_idx(num_cable_sections);

		// calculate increment position
		// unfortunately we must use sword here to allow negative
		arma::sword incr_real = (arma::sword)idx_incr_ - (arma::sword)idx_start_;
		if(incr_real<0)incr_real = num_base_sections + incr_real;
		arma::uword idx_incr_real = (arma::uword)incr_real;

		// walk over number of turns
		for(arma::uword i=0;i<(num_turns_ + std::ceil((double)num_add_/num_base_sections));i++){
			// number of sections in this turn
			arma::uword num_sect;
			if((num_cable_sections-i*num_base_sections)<num_base_sections)
				num_sect = num_cable_sections-i*num_base_sections;
			else num_sect = num_base_sections;

			// walk over sections
			for(arma::uword j=0;j<num_sect;j++){
				// sector index
				const arma::uword isect = i*num_base_sections + j;

				// index into base
				arma::uword ibase = j+idx_start_;
				if(ibase>=num_base_sections)ibase-=num_base_sections;

				
				// number of nodes in this section
				arma::uword Nsn = frame->get_num_nodes(ibase);
				// num_nodes_(isect) = Nsn;

				// calculate position of non-incremented turn
				double pn = ((arma::sword)i+num_offset_)*turn_step_ + offset_;

				// check for turn increment
				arma::Row<double> nshift(Nsn);
				if(disable_increment_==false){
					if(j==idx_incr_real)nshift = arma::linspace<arma::Row<double> >(pn, pn + turn_step_, Nsn);
					else if(j>idx_incr_real)nshift.fill(pn + turn_step_);
					else nshift.fill(pn);
				}else{
					assert(num_add_==0);
					nshift.fill(pn);
				}

				// calculate shear angle
				//const arma::Row<double> ashear = frame->calc_ashear(ibase); 

				// generate coordinates by shifting in the block normal direction 
				// R(isect) = frame->get_coords(ibase) + 
				// 	frame->get_block(ibase).each_row()%(nshift/arma::cos(ashear));

				// generate coordinates by shifting in the normal direction
				//R(isect) = Rb(ibase) + Nb(ibase).each_row()%dN;
				R(isect) = frame->perform_normal_shift(ibase, nshift);

				// calculate direction vector
				arma::Mat<double> dL = arma::diff(R(isect),1,1);

				// extend direction vector and store in D
				L(isect).zeros(3,Nsn);
				L(isect).cols(1,Nsn-2) = 
					(dL.cols(1,Nsn-2) + dL.cols(0,Nsn-3))/2;
				L(isect).col(0) = dL.col(0); 
				L(isect).col(Nsn-1) = dL.col(Nsn-2); 
				
				// normalize direction vector
				L(isect).each_row() /= cmn::Extra::vec_norm(L(isect));

				// for the longitudinal vector also take first and 
				// last vectors from base path this ensures proper 
				// current lead connections
				L(isect).head_cols(1) = frame->get_direction(ibase).head_cols(1);
				L(isect).tail_cols(1) = frame->get_direction(ibase).tail_cols(1);

				// copy radial vector
				D(isect) = frame->get_transverse(ibase);

				// copy normal vector (do not use cross here
				// otherwise cables are not touching)
				N(isect) = frame->get_normal(ibase);

				// get block direction vector
				B(isect) = frame->get_block(ibase);

				// set index
				section_idx(isect) = ibase;
				turn_idx(isect) = i;
			}
		}

		// reverse frame
		// if(reverse_base_==true){
		// 	Frame::reverse_field(R); Frame::reverse_field(L);
		// 	Frame::reverse_field(N); Frame::reverse_field(D);
		// 	for(arma::uword i=0;i<L.n_elem;i++)L(i) *= -1;
		// }

		// create frame
		ShFramePr cable_frame = Frame::create(R,L,N,D,B);

		// set cable flag
		cable_frame->set_conductor_type(cable);

		// override location
		cable_frame->set_location(section_idx,turn_idx);

		// reverse frame
		if(reverse_base_==true)cable_frame->reverse();

		// apply transformations
		cable_frame->apply_transformations(trans_);

		// create frame and return
		return cable_frame;
	}

	// get type
	std::string PathCable::get_type(){
		return "mdl::pathcable";
	}

	// method for serialization into json
	void PathCable::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["num_turns"] = (unsigned int)num_turns_;
		js["turn_step"] = turn_step_;
		js["idx_start"] = (unsigned int)idx_start_;
		js["idx_incr"] = (unsigned int)idx_incr_;
		js["num_add"] = (int)num_add_;
		js["num_offset"] = (int)num_offset_;
		js["offset"] = offset_;
		js["disable_increment"] = disable_increment_;
		js["reverse_base"] = disable_increment_;

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
	}

	// method for deserialisation from json
	void PathCable::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Transformations::deserialize(js,list,factory_list);

		// properties
		set_num_turns(js["num_turns"].asUInt64());
		set_turn_step(js["turn_step"].asDouble());
		set_idx_start(js["idx_start"].asUInt64());
		set_idx_incr(js["idx_incr"].asUInt64());
		set_num_add(js["num_add"].asInt64());
		set_num_offset(js["num_offset"].asInt64());
		set_offset(js["offset"].asDouble());
		set_disable_increment(js["disable_increment"].asBool());
		set_reverse_base(js["reverse_base"].asBool());
		
		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list);
	}

}}
