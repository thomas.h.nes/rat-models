/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathaxis.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathAxis::PathAxis(const char axis, const char dir, const double ell, const arma::Col<double>::fixed<3> &Rc, const double dl){
		if(ell<=0)rat_throw_line("length must be larger than zero");
		if(axis!='x' && axis!='y' && axis!='z')rat_throw_line("axis must be x,y or z");
		if(dir!='x' && dir!='y' && dir!='z')rat_throw_line("direction must be x,y or z");
		if(dl<=0)rat_throw_line("element size must be larger than zero");
		axis_ = axis; dir_ = dir; ell_ = ell; Rc_ = Rc; dl_ = dl;
	}

	// factory
	ShPathAxisPr PathAxis::create(const char axis, const char dir, const double ell, const arma::Col<double>::fixed<3> &Rc, const double dl){
		return std::make_shared<PathAxis>(axis,dir,ell,Rc,dl);
	}

	// factory
	ShPathAxisPr PathAxis::create(const char axis, const char dir, const double ell, const double x, const double y, const double z, const double dl){
		return std::make_shared<PathAxis>(axis,dir,ell,arma::Col<double>::fixed<3>{x,y,z},dl);
	}

	// get frame
	ShFramePr PathAxis::create_frame() const{
		// axis of magnet for field quality calculation
		ShPathGroupPr pth = PathGroup::create();
		ShPathStraightPr str = PathStraight::create(ell_,dl_);
		str->set_element_divisor(element_divisor_);
		pth->add_path(str);
		pth->add_translation(0,-ell_/2,0);

		// set line start location, direction vector and orientation (N-vector)
		// depending on the axis and direction chosen (note that D-is pointing in dir)
		if(axis_=='x' && dir_=='y'){
			pth->add_rotation(0,0,1,-arma::datum::pi/2);
			pth->add_rotation(1,0,0,-arma::datum::pi/2);
		}
		else if(axis_=='x' && dir_=='z'){
			pth->add_rotation(0,0,1,-arma::datum::pi/2);
			
		}
		else if(axis_=='y' && dir_=='x'){
			pth->add_rotation(0,1,0,-arma::datum::pi/2);
		}
		else if(axis_=='y' && dir_=='z'){
			// default orientation
		}
		else if(axis_=='z' && dir_=='x'){
			pth->add_rotation(1,0,0,arma::datum::pi/2);
			pth->add_rotation(0,0,1,arma::datum::pi/2);
		}
		else if(axis_=='z' && dir_=='y'){
			pth->add_rotation(1,0,0,arma::datum::pi/2);
			pth->add_rotation(0,0,1,arma::datum::pi);
		}
		else{
			rat_throw_line("axis and direction combination unrecognised");
		}
		
		// add translation
		pth->add_translation(Rc_);

		// create frame
		ShFramePr cable_gen = pth->create_frame();

		// apply transformations
		cable_gen->apply_transformations(trans_);

		// return frame from this path
		return cable_gen;
	}

	// set divisor
	void PathAxis::set_element_divisor(const arma::uword element_divisor){
		if(element_divisor<=0)rat_throw_line("divisor must be larger than zero");
		element_divisor_ = element_divisor;
	}

	// get type
	std::string PathAxis::get_type(){
		return "mdl::pathaxis";
	}

	// method for serialization into json
	void PathAxis::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["axis"] = axis_;
		js["dir"] = dir_;
		js["ell"] = ell_;
		for(arma::uword i=0;i<3;i++)
			js["Rc"].append(Rc_(i));
		js["dl"] = dl_;
	}

	// method for deserialisation from json
	void PathAxis::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Transformations::deserialize(js,list,factory_list);

		// properties
		axis_ = js["axis"].asInt();
		dir_ = js["dir"].asInt();
		ell_ = js["ell"].asDouble();
		for(arma::uword i=0;i<3;i++)
			Rc_(i) = js["Rc"].get(i,0).asDouble();
		dl_ = js["dl"].asDouble();
	}

}}

