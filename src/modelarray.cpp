/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelarray.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	ModelArray::ModelArray(){

	}

	// constructor with setting of the base path
	ModelArray::ModelArray(ShModelPr base_model){
		set_base_model(base_model);
	}

	// constructor with setting of the base path
	ModelArray::ModelArray(ShModelPr base_model,
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z, 
		const double dx, const double dy, const double dz){
		set_base_model(base_model);
		set_grid_size(num_x,num_y,num_z);
		set_grid_spacing(dx,dy,dz);
	}

	// factory
	ShModelArrayPr ModelArray::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>();
	}

	// factory with setting of the base path
	ShModelArrayPr ModelArray::create(ShModelPr base_model){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>(base_model);
	}

	// factory with setting of the base path
	ShModelArrayPr ModelArray::create(ShModelPr base_model,
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z, 
		const double dx, const double dy, const double dz){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<ModelArray>(base_model,num_x,num_y,num_z,dx,dy,dz);
	}

	// set grid size
	void ModelArray::set_grid_size(
		const arma::uword num_x, const arma::uword num_y, const arma::uword num_z){
		num_x_ = num_x; num_y_ = num_y; num_z_ = num_z;
	}

	// set grid spacing
	void ModelArray::set_grid_spacing(
		const double dx, const double dy, const double dz){
		dx_ = dx; dy_ = dy; dz_ = dz;
	}

	// set basemodel
	void ModelArray::set_base_model(ShModelPr base_model){
		if(base_model==NULL)rat_throw_line("base model points to zero");
		base_model_ = base_model;
	}

	// get number of coils stored
	// in this model and all child models/coils
	arma::uword ModelArray::get_num_objects() const{
		return num_x_*num_y_*num_z_*base_model_->get_num_objects();
	}

	// set parallel setup
	void ModelArray::set_parallel_setup(const bool parallel_setup){
		parallel_setup_ = parallel_setup;
	}

	// mesh generation
	ShMeshPrList ModelArray::create_mesh() const{
		// get coil mesh from base
		ShMeshPrList base_mesh = base_model_->create_mesh();

		// set counter for number of coils to zero
		const arma::uword num_base = base_mesh.n_elem;
		const arma::uword num_meshes = num_base*num_x_*num_y_*num_z_;

		// allocate output
		ShMeshPrList array_meshes(num_meshes);

		// create a grid
		arma::Mat<arma::uword> idx(num_x_*num_y_*num_z_,3);
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_x_;i++)
			for(arma::uword j=0;j<num_y_;j++)
				for(arma::uword k=0;k<num_z_;k++)
					idx.row(cnt++) = arma::Row<arma::uword>::fixed<3>{i,j,k};

		// walk over coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,idx.n_rows,parallel_setup_,[&](int n, int){
			// get indexes
			const arma::uword i = idx(n,0),j = idx(n,1),k = idx(n,2);

			// walk over base meshes
			for(arma::uword m=0;m<num_base;m++){
				// deep copy base (also adds coolant, circuit and conductor)
				array_meshes(n*num_base + m) = base_mesh(m)->copy();

				// rotate coil into position
				array_meshes(n*num_base + m)->apply_transformation(
					TransTranslate::create(i*dx_,j*dy_,k*dz_));

				// apply global transformations
				array_meshes(n*num_base + m)->apply_transformations(trans_);

				// append name
				array_meshes(n*num_base + m)->append_name(myname_);
			}
		});

		// return combined mesh
		return array_meshes;
	}

	// mesh generation
	ShEdgesPrList ModelArray::create_edge() const{
		// get coil mesh from base
		ShEdgesPrList base_edges = base_model_->create_edge();

		// set counter for number of coils to zero
		const arma::uword num_base = base_edges.n_elem;
		const arma::uword num_meshes = num_base*num_x_*num_y_*num_z_;

		// allocate output
		ShEdgesPrList array_edges(num_meshes);

		// create a grid
		arma::Mat<arma::uword> idx(num_x_*num_y_*num_z_,3);
		arma::uword cnt = 0;
		for(arma::uword i=0;i<num_z_;i++)
			for(arma::uword j=0;j<num_y_;j++)
				for(arma::uword k=0;k<num_x_;k++)
					idx.row(cnt++) = arma::Row<arma::uword>::fixed<3>{i,j,k};

		// walk over coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,idx.n_rows,parallel_setup_,[&](int n, int){
			// get indexes
			const arma::uword i = idx(n,0),j = idx(n,1),k = idx(n,2);

			// walk over base edges
			for(arma::uword m=0;m<num_base;m++){
				// deep copy base
				array_edges(n*num_base + m) = base_edges(m)->copy();

				// rotate coil into position
				array_edges(n*num_base + m)->apply_transformation(
					TransTranslate::create(i*dx_,j*dy_,k*dz_));

				// apply global transformations
				array_edges(n*num_base + m)->apply_transformations(trans_);

				// append name
				array_edges(n*num_base + m)->append_name(myname_);
			}
		});

		// set tags
		//add_tags(toroid_edges);

		// return combined mesh
		return array_edges;
	}

	// get type
	std::string ModelArray::get_type(){
		return "mdl::modelarray";
	}

	// method for serialization into json
	void ModelArray::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Nameable::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// settings
		js["psetup"] = parallel_setup_;

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_model_, list);
	}

	// method for deserialisation from json
	void ModelArray::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Nameable::deserialize(js,list,factory_list);
		Transformations::deserialize(js,list,factory_list);

		// settings
		set_parallel_setup(js["psetup"].asBool());

		// subnodes
		base_model_ = cmn::Node::deserialize_node<Model>(js["base"], list, factory_list);
	}

}}