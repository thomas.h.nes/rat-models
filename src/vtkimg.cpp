/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "vtkimg.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKImg::VTKImg(){
		vtk_igrid_ = vtkSmartPointer<vtkImageData>::New();
	}

	// factory
	ShVTKImgPr VTKImg::create(){
		return std::make_shared<VTKImg>();
	}

	// write mesh
	void VTKImg::set_dims(
		const double x1, const double x2, const arma::uword num_x,
		const double y1, const double y2, const arma::uword num_y,
		const double z1, const double z2, const arma::uword num_z){
		
		// calculate spacing
		double dx=0,dy=0,dz=0;
		if(num_x>1)dx = (x2-x1)/(num_x-1);
		if(num_y>1)dy = (y2-y1)/(num_y-1);
		if(num_z>1)dz = (z2-z1)/(num_z-1);

		// setup grid
		vtkSmartPointer<vtkImageData> voxels = vtkImageData::New();
		vtk_igrid_->SetDimensions(num_x,num_y,num_z);
		vtk_igrid_->SetSpacing(dx,dy,dz);
		vtk_igrid_->SetOrigin(x1,y1,z1);
	}

	// write data at nodes
	void VTKImg::set_nodedata(const arma::Mat<double> &v, const std::string &data_name){
		// create vtk array
		vtkSmartPointer<vtkDoubleArray> vvtk = 
			vtkSmartPointer<vtkDoubleArray>::New();

		// add name to data
		vvtk->SetName(data_name.c_str());

		// allocate
		vvtk->SetNumberOfComponents(v.n_rows);
		vvtk->SetNumberOfValues(v.n_elem);
		
		// insert data elements
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// set data to vtk grid
		vtk_igrid_->GetPointData()->AddArray(vvtk);
	}

	// write output file
	void VTKImg::write(const std::string fname, cmn::ShLogPr lg){
		// create extended filename
		const std::string fname_ext = fname + ".vti";

		// display file settings and type
		lg->msg(2,"%s%sWriting VTK File%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sFile Settings%s\n",KBLU,KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.c_str(),KNRM);
		lg->msg("type: image data (vti)\n");

		// Write the file
		vtkSmartPointer<vtkXMLImageDataWriter> writer =  
			vtkSmartPointer<vtkXMLImageDataWriter>::New();
		writer->SetFileName(fname_ext.c_str());
		writer->SetInputData(vtk_igrid_);
		
		// write data
		writer->Write();

		// done writing VTK image data
		lg->msg(-2);
		lg->msg(-2,"\n");
	}

}}