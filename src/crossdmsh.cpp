/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "crossdmsh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	CrossDMsh::CrossDMsh(const double h0, dm::ShDistFunPr fd, dm::ShDistFunPr fh){
		if(fd==NULL)rat_throw_line("distance function points to null");
		if(fh==NULL)rat_throw_line("scaling function points to null");
		fd_ = fd; fh_ = fh; h0_ = h0;
	}

	// factory methods
	ShCrossDMshPr CrossDMsh::create(const double h0, dm::ShDistFunPr fd, dm::ShDistFunPr fh){
		return std::make_shared<CrossDMsh>(h0,fd,fh);
	}

	// setting fixed points
	void CrossDMsh::set_fixed(const arma::Mat<double> &pfix_ext){
		assert(pfix_ext.n_cols==2);
		pfix_ext_ = pfix_ext;
	}

	// setup
	ShAreaPr CrossDMsh::create_area() const{
		// check if functions set
		if(fd_==NULL)rat_throw_line("distance function points to null");
		if(fh_==NULL)rat_throw_line("scaling function points to null");

		// call distmesh2d
		dm::ShDistMesh2DPr dmsh = dm::DistMesh2D::create();

		// setup distmesher
		dmsh->set_quad(quad_);
		if(quad_)dmsh->set_h0(std::sqrt(2.0)*h0_); else dmsh->set_h0(h0_);
		dmsh->set_distfun(fd_);
		dmsh->set_scalefun(fh_);
		dmsh->set_fixed(pfix_ext_);

		// run mesher
		dmsh->setup();

		// get nodes and elements
		const arma::Mat<double> Rn = dmsh->get_nodes().t();
		arma::Mat<arma::uword> n = dmsh->get_elements().t();

		// circle area
		ShAreaPr area = Area::create(Rn,n);

		// return positive
		return area;
	}

	// surface mesh
	ShPerimeterPr CrossDMsh::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area();

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// get type
	std::string CrossDMsh::get_type(){
		return "mdl::crossdmsh";
	}

	// method for serialization into json
	void CrossDMsh::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// quad mesher enabled
		js["quad"] = quad_;

		// element size
		js["h0"] = h0_;

		// externally supplied fixed point list
		for(arma::uword i=0;i<pfix_ext_.n_rows;i++){
			js["pfix_x"].append(pfix_ext_(i,0));
			js["pfix_y"].append(pfix_ext_(i,1));
		}

		// serialize geometry and scaling distance functions
		js["geometryfun"] = cmn::Node::serialize_node(fd_,list);
		js["scalingfun"] = cmn::Node::serialize_node(fh_,list);
	}

	// method for deserialisation from json
	void CrossDMsh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// quad mesher enabled 
		quad_ = js["quad"].asBool();

		// element size
		h0_ = js["h0"].asDouble();

		// externally supplied fixed point list
		pfix_ext_.set_size(js["pfix_x"].size(),2);
		Json::Value x = js["pfix_x"]; Json::Value y = js["pfix_y"];
		for(arma::uword i=0;i<x.size();i++){
			pfix_ext_(i,0) = x.get(i,0).asDouble(); 
			pfix_ext_(i,1) = y.get(i,0).asDouble();
		}

		// deserialize distance and scaling functions
		fd_ = cmn::Node::deserialize_node<dm::DistFun>(js["geometryfun"], list, factory_list);
		fh_ = cmn::Node::deserialize_node<dm::DistFun>(js["scalingfun"], list, factory_list);
	}

}}