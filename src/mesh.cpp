/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "mesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Mesh::Mesh(){

	}

	// constructor with input
	Mesh::Mesh(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShMeshPr Mesh::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Mesh>();
	}

	// factory with input
	ShMeshPr Mesh::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<Mesh>(frame,area);
	}

	// set drive ID
	void Mesh::set_drive_id(const arma::uword drive_id){
		drive_id_ = drive_id;
	}

	// set drive ID
	arma::uword Mesh::get_drive_id() const{
		return drive_id_;
	}

	// setup function
	void Mesh::setup(ShFramePr &frame, ShAreaPr &area){
		// check input
		assert(frame!=NULL); assert(area!=NULL);
		
		// store in self
		area_ = area; frame_ = frame;

		// get nodes
		const arma::Mat<double> R2d = area->get_nodes();
		const arma::Mat<arma::uword> n2d = area->get_elements();

		// edge matrix construction
		arma::Mat<arma::uword> s2d,i2d;	
		area->create_edge_matrix(s2d,i2d);

		// call internal loft function
		frame->create_loft(R_,L_,N_,D_,n_,s_,R2d,n2d,s2d);

		// volume dimensionality
		if(n_.n_rows==1)n_dim_ = 0; // points
		if(n_.n_rows==2)n_dim_ = 1; // lines
		if(n_.n_rows==4)n_dim_ = 2; // quadrilaterals
		if(n_.n_rows==8)n_dim_ = 3; // hexahedrons

		// surface dimensionality
		if(s_.n_rows==1)s_dim_ = 0; // points
		if(s_.n_rows==2)s_dim_ = 1; // lines
		if(s_.n_rows==4)s_dim_ = 2; // quadrilaterals

		// check output
		assert(n_.n_rows==n2d.n_rows*2); assert(s_.n_rows==s2d.n_rows*2);
		assert(arma::max(arma::max(n_))<R_.n_cols); assert(arma::max(arma::max(s_))<R_.n_cols);
		assert(L_.n_cols==R_.n_cols); assert(N_.n_cols==R_.n_cols); assert(D_.n_cols==R_.n_cols);
	}

	// get number of dimensions
	arma::uword Mesh::get_n_dim() const{
		return n_dim_;
	}

	// get number of dimensions
	arma::uword Mesh::get_s_dim() const{
		return s_dim_;
	}

	// set operating temperature
	void Mesh::set_operating_temperature(const double operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// get operating temperature
	double Mesh::get_operating_temperature() const{
		return operating_temperature_;
	}

	// access area
	ShAreaPr Mesh::get_area(){
		return area_;
	}

	// access frame
	ShFramePr Mesh::get_frame(){
		return frame_;
	}

	// get coordinates for section
	const arma::Mat<double>& Mesh::get_coords() const{
		return R_;
	}

	// get coordinates for section
	const arma::Mat<double>& Mesh::get_direction() const{
		return L_;
	}

	// get coordinates for section
	const arma::Mat<double>& Mesh::get_normal() const{
		return N_;
	}

	// get coordinates for section
	const arma::Mat<double>& Mesh::get_transverse() const{
		return D_;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& Mesh::get_elements() const{
		return n_;
	}

	// get coordinates for section
	const arma::Mat<arma::uword>& Mesh::get_surface_elements() const{
		return s_;
	}

	// get number of nodes
	arma::uword Mesh::get_num_nodes() const{
		return R_.n_cols;
	}

	// get number of elements
	arma::uword Mesh::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of surface elements
	arma::uword Mesh::get_num_surface() const{
		return s_.n_cols;
	}

	// calculate volume for each element
	arma::Row<double> Mesh::calc_volume() const{
		// volume dimensionality
		arma::Row<double> V;
		if(n_dim_==0)V = arma::Row<double>(n_.n_cols,arma::fill::ones);
		if(n_dim_==1)V = cmn::Line::calc_length(R_,n_); // lines
		if(n_dim_==2)V = cmn::Quadrilateral::calc_area(R_,n_); // quadrilaterals
		if(n_dim_==3)V = cmn::Hexahedron::calc_volume(R_,n_); // hexahedrons

		// return volume
		return area_->get_hidden_dim()*V;
	}

	// calculate total volume
	double Mesh::calc_total_volume() const{
		return arma::accu(calc_volume());
	}

	// calculate length
	double Mesh::calc_ell() const{
		return frame_->calc_total_ell();
	}

	// calculate volume
	double Mesh::calc_total_surface_area() const{
		return arma::sum(cmn::Quadrilateral::calc_area(R_,s_));
	}

	// get curret density at nodes
	arma::Mat<double> Mesh::get_nodal_current_density() const{
		return arma::Mat<double>(3,get_num_nodes(),arma::fill::zeros);
	}

	// get temperature at nodes
	arma::Mat<double> Mesh::get_nodal_temperature() const{
		return arma::Row<double>(get_num_nodes(),arma::fill::ones)*operating_temperature_;
	}

	// calculate field angle
	arma::Row<double> Mesh::calc_alpha(const arma::Mat<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate engineering current density
	arma::Row<double> Mesh::calc_Je(const arma::Mat<double> &, const arma::Row<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate engineering current density
	arma::Row<double> Mesh::calc_Tc(const arma::Mat<double> &, const arma::Mat<double> &) const{
		return arma::Row<double>(get_num_nodes(),arma::fill::zeros);
	}

	// calculate electric field
	arma::Mat<double> Mesh::calc_E(const arma::Mat<double> &, const arma::Mat<double> &, const arma::Row<double> &) const{
		return arma::Mat<double>(3,get_num_nodes(),arma::fill::zeros);
	}

	// get number of turns
	double Mesh::get_number_turns() const{
		return 0;
	}

	// apply multiple transformations
	void Mesh::apply_transformations(const ShTransPrList &trans){
		// walk over transformations and apply
		for(arma::uword i=0;i<trans.n_elem;i++)
			apply_transformation(trans(i));
	}

	// apply transformation
	void Mesh::apply_transformation(const ShTransPr &trans){
		// transform node orientation vectors
		trans->apply_vectors(R_,L_,'L'); 
		trans->apply_vectors(R_,N_,'N');
		trans->apply_vectors(R_,D_,'D'); 
		
		// transform node coordinates
		trans->apply_coords(R_);

		// transform mesh
		trans->apply_mesh(n_,get_num_nodes()); 
		trans->apply_mesh(s_,get_num_nodes());
	}

	// setup surface mesh
	void Mesh::setup_surface(ShSurfacePr surf) const{
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// find indexes of unique nodes
		const arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s_,1,s_.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(1,num_nodes,arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		const arma::Mat<arma::uword> s = arma::reshape(
			node_indexing(arma::reshape(s_,1,s_.n_elem)),s_.n_rows,s_.n_cols);

		// get node coordinates
		//const arma::Mat<double> R = R_.cols(idx_node_surface);

		// surface
		surf->set_mesh(R_.cols(idx_node_surface),s);
		surf->set_longitudinal(L_.cols(idx_node_surface));
		surf->set_normal(N_.cols(idx_node_surface));
		surf->set_transverse(D_.cols(idx_node_surface));

		// transfer name
		surf->set_name(myname_);

		// set temperature
		surf->set_operating_temperature(operating_temperature_);

		// set frame and frame
		surf->set_frame(frame_); surf->set_area(area_);

		// set drive index
		surf->set_drive_id(drive_id_);
	}

	// extract surface mesh
	ShSurfacePr Mesh::create_surface() const{
		ShSurfacePr surf = Surface::create();
		setup_surface(surf);
		return surf;
	}

	// create target points at nodes
	fmm::ShMgnTargetsPr Mesh::create_node_targets() const{
		return fmm::MgnTargets::create(R_);
	}

	// cretae target points at element barycenters
	fmm::ShMgnTargetsPr Mesh::create_element_targets() const{
		// get counters
		arma::uword num_elements = n_.n_cols;

		// allocate
		arma::Mat<double> Re(3,num_elements);

		// walk over elements and calculate center point
		for(arma::uword i=0;i<num_elements;i++){
			Re.col(i) = cmn::Hexahedron::quad2cart(R_.cols(n_.col(i)),arma::Col<double>::fixed<3>{0,0,0});
		}

		// create target points and return
		return fmm::MgnTargets::create(Re);
	}

	// // create current sources
	// fmm::ShCurrentSourcesPr Mesh::create_current_sources() const{
	// 	return fmm::CurrentSources::create(
	// 		arma::Mat<double>(3,0),arma::Mat<double>(3,0),
	// 		arma::Mat<double>(1,0),arma::Mat<double>(1,0));
	// }

	// default create sources function to be overridden
	fmm::ShSourcesPrList Mesh::create_sources(const arma::sword, const arma::sword) const{
		return fmm::ShSourcesPrList(0);
	}

	// // create line elements
	// fmm::ShCurrentMeshPr Mesh::create_current_mesh() const{
	// 	// create source mesh
	// 	fmm::ShCurrentMeshPr mesh = fmm::CurrentMesh::create();

	// 	// set empty mesh
	// 	mesh->set_mesh(
	// 		arma::Mat<double>(3,0),
	// 		arma::Mat<arma::uword>(8,0),
	// 		arma::Mat<double>(3,0));

	// 	// return mesh
	// 	return mesh;
	// }

	// gmsh export
	void Mesh::export_gmsh(cmn::ShGmshFilePr gmsh, const bool incl_surface, const bool incl_vectors){
		// write nodes and elements
		gmsh->write_nodes(R_);
		if(incl_surface)gmsh->write_elements(
			n_,arma::Row<arma::uword>(n_.n_cols,arma::fill::ones),
			s_,arma::Row<arma::uword>(s_.n_cols,arma::fill::ones));
		else gmsh->write_elements(n_,arma::Row<arma::uword>(n_.n_cols,arma::fill::ones));
		
		// write vectors
		if(incl_vectors){
			gmsh->write_nodedata(L_,"dir. vector [m]");
			gmsh->write_nodedata(N_,"norm. vector [m]");
			gmsh->write_nodedata(D_,"trans. vector [m]");
		}
	}

	// display function
	void Mesh::display(cmn::ShLogPr &lg, ShMeshPrList &meshes){
		// header
		lg->msg(2,"%slist of coils%s\n",KBLU,KNRM);

		// count number of meshes
		const arma::uword num_coils = meshes.n_elem;

		// allocate counters
		arma::Row<arma::uword> num_nodes(num_coils);
		arma::Row<arma::uword> num_elements(num_coils);
		arma::Row<arma::uword> num_surface(num_coils);

		// create table with stats for each coil
		lg->msg("%s%3s %12s %8s %8s %8s%s\n",KBLD,"idx","name","#node","#elem","#surf",KNRM);
		for(arma::uword i=0;i<num_coils;i++){
			// get coil name
			const std::string name = meshes(i)->get_name();
			const char *c = name.c_str();

			// get counters
			num_nodes(i) = meshes(i)->get_num_nodes();
			num_elements(i) = meshes(i)->get_num_elements();
			num_surface(i) = meshes(i)->get_num_surface();

			// print entr
			lg->msg("%03llu %12s %08llu %08llu %08llu\n",i,c,num_nodes(i),num_elements(i),num_surface(i));
		}
		lg->msg("%03s %12s %08llu %08llu %08llu\n","+++","all",
			arma::sum(num_nodes),arma::sum(num_elements),arma::sum(num_surface));

		// footer
		lg->msg(-2);
	}


	// copy constructor
	ShMeshPr Mesh::copy() const{
		return std::make_shared<Mesh>(*this);
	}

	// get type
	std::string Mesh::get_type(){
		return "mdl::mesh";
	}

	// method for serialization into json
	void Mesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Nameable::serialize(js,list);

		// type
		js["type"] = get_type();
		
		// area
		js["area"] = cmn::Node::serialize_node(area_,list);
		js["frame"] = cmn::Node::serialize_node(frame_,list);

		// other settings
		js["drive_id"] = (int)drive_id_;
		js["operating_temperature"] = operating_temperature_;
	}

	// method for deserialisation from json
	void Mesh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Nameable::deserialize(js,list,factory_list);

		// area and generators
		ShAreaPr area = cmn::Node::deserialize_node<Area>(js["area"],list,factory_list);
		ShFramePr frame = cmn::Node::deserialize_node<Frame>(js["frame"],list,factory_list);

		// reconstruct mesh from the area and the generators 
		setup(frame,area);

		// other settings
		set_drive_id(js["drive_id"].asUInt64());
		set_operating_temperature(js["operating_temperature"].asDouble());
	}

}}