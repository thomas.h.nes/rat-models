/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "crossline.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossLine::CrossLine(){

	}

	// constructor with input
	CrossLine::CrossLine(const arma::Row<double> &u, const arma::Row<double> &v, const double thickness){
		// set to self
		set_coord(u,v); set_thickness(thickness);
	}

	// construct line with 2 points
	CrossLine::CrossLine(const double u1, const double v1, const double u2, const double v2, const double thickness, const double dl){
		set_coord(u1,v1,u2,v2,dl); set_thickness(thickness);
	}

	// factory
	ShCrossLinePr CrossLine::create(){
		return std::make_shared<CrossLine>();
	}

	// factory with dimension input
	ShCrossLinePr CrossLine::create(const arma::Row<double> &u, const arma::Row<double> &v, const double thickness){
		return std::make_shared<CrossLine>(u,v,thickness);
	}

	// factory with dimension input
	ShCrossLinePr CrossLine::create(const double u1, const double v1, const double u2, const double v2, const double thickness, const double dl){
		return std::make_shared<CrossLine>(u1,v1,u2,v2,thickness,dl);
	}

	// set dimensions in the normal direction
	void CrossLine::set_coord(const arma::Row<double> &u, const arma::Row<double> &v){
		if(u.n_elem!=v.n_elem)rat_throw_line("u and v must have same number of elements");
		u_ = u; v_ = v;
	}

	// set thickness
	void CrossLine::set_thickness(const double thickness){
		if(thickness<=0)rat_throw_line("thickness must be larger than zero");
		thickness_ = thickness;
	}

	// set coordinates
	void CrossLine::set_coord(const double u1, const double v1, const double u2, const double v2, const double dl){
		const double du = u2-u1; const double dv = v2-v1; const double ell = std::sqrt(du*du + dv*dv);
		arma::uword num_elem = std::max(2,(int)std::ceil(ell/dl)+1);
		set_coord(arma::linspace<arma::Row<double> >(u1,u2,num_elem),
			arma::linspace<arma::Row<double> >(v1,v2,num_elem));
	}

	// volume mesh
	ShAreaPr CrossLine::create_area() const{
		// check settings
		if(u_.is_empty() || v_.is_empty())rat_throw_line("line coordinates are not set");
		
		// number of coordinates in line
		const arma::uword num_coord = u_.n_elem;

		// coordinate matrix
		arma::Mat<double> Rl = arma::join_vert(u_,v_);

		// elements
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::regspace<arma::Row<arma::uword> >(0,num_coord-2),
			arma::regspace<arma::Row<arma::uword> >(1,num_coord-1));

		// create area
		ShAreaPr area = Area::create(Rl,n);
		area->set_hidden_thickness(thickness_);

		// return area
		return area;
	}

	// surface mesh
	ShPerimeterPr CrossLine::create_perimeter() const{
		// coordinate
		const arma::Col<double> Rn(2,0);
		const arma::Row<arma::uword> s(2,0);

		// extract periphery and return
		return Perimeter::create(Rn,s);
	}

	// get type
	std::string CrossLine::get_type(){
		return "mdl::crossline";
	}

	// method for serialization into json
	void CrossLine::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		js["type"] = get_type();
		js["u"] = rat::cmn::Node::serialize_arma(u_);
		js["v"] = rat::cmn::Node::serialize_arma(v_);
		js["thickness"] = thickness_;
	}

	// method for deserialisation from json
	void CrossLine::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/){
		set_coord(rat::cmn::Node::deserialize_arma(js["u"]),
			rat::cmn::Node::deserialize_arma(js["v"]));
		set_thickness(js["thickness"].asDouble());
	}

}}
