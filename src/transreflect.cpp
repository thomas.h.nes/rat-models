/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transreflect.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransReflect::TransReflect(){
		// default reflectionless matrix
		M_.zeros(); M_(0,0) = 1; M_(1,1) = 1; M_(2,2) = 1;
	}

	// constructor with vector input
	TransReflect::TransReflect(const double vx,const double vy,const double vz){
		set_reflection(vx,vy,vz);
	}

	// constructor with vector input
	TransReflect::TransReflect(const arma::Col<double>::fixed<3> vec){
		set_reflection(vec(0),vec(1),vec(2));
	}

	// factory
	ShTransReflectPr TransReflect::create(){
		//return ShTransReflectPr(new TransReflect);
		return std::make_shared<TransReflect>();
	}

	// factory with vector input
	ShTransReflectPr TransReflect::create(const double vx,const double vy,const double vz){
		//return ShTransReflectPr(new TransReflect(vx,vy,vz));
		return std::make_shared<TransReflect>(vx,vy,vz);
	}

	// factory with vector input
	ShTransReflectPr TransReflect::create(const arma::Col<double>::fixed<3> vec){
		//return ShTransReflectPr(new TransReflect(vec));
		return std::make_shared<TransReflect>(vec);
	}

	// set vector
	void TransReflect::set_reflection(const arma::Col<double>::fixed<3> vec){
		set_reflection(vec(0),vec(1),vec(2));
	}

	// set vector
	void TransReflect::set_reflection(
		const double vx,const double vy,const double vz){
		// normalize
		double ell = std::sqrt(vx*vx + vy*vy + vz*vz);
		double vxx = vx/ell, vyy = vy/ell, vzz = vz/ell;

		// setup matrix
		M_.row(0) = arma::Row<double>::fixed<3>{1-2*vxx*vxx,-2*vxx*vyy,-2*vxx*vzz};
		M_.row(1) = arma::Row<double>::fixed<3>{-2*vxx*vyy,1-2*vyy*vyy,-2*vyy*vzz};
		M_.row(2) = arma::Row<double>::fixed<3>{-2*vxx*vzz,-2*vyy*vzz,1-2*vzz*vzz};
	}

	// reflection in xy-plane
	void TransReflect::set_reflection_xy(){
		set_reflection(arma::Col<double>::fixed<3>{0,0,1});
	}

	// reflection in yz-plane
	void TransReflect::set_reflection_yz(){
		set_reflection(arma::Col<double>::fixed<3>{1,0,0});
	}

	// reflection in xz-plane
	void TransReflect::set_reflection_xz(){
		set_reflection(arma::Col<double>::fixed<3>{0,1,0});
	}
			
	// apply to coordinates only
	void TransReflect::apply_coords(arma::Mat<double> &R) const{
		R = M_*R;
	}

	// apply transformation to coordinates and vectors
	void TransReflect::apply_vectors(
		const arma::Mat<double> &, arma::Mat<double> &V, const char str) const{
		
		// apply to vectors (invert direction)
		V = M_*V;

		// inverse direction to keep righthandedness
		if(str=='D')
			V *= -1;
	}
		
	// apply transformation to mesh
	void TransReflect::apply_mesh(arma::Mat<arma::uword> &n, const arma::uword) const{
		// flip face 1
		n.rows(0,n.n_rows/2-1) = arma::flipud(n.rows(0,n.n_rows/2-1));

		// flip face 2
		n.rows(n.n_rows/2,n.n_rows-1) = arma::flipud(n.rows(n.n_rows/2,n.n_rows-1));
	}

	// get type
	std::string TransReflect::get_type(){
		return "mdl::transreflect";
	}

	// method for serialization into json
	void TransReflect::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		for(arma::uword i=0;i<9;i++)js["matrix"].append(M_(i));
	}

	// method for deserialisation from json
	void TransReflect::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		for(arma::uword i=0;i<9;i++)M_(i) = js["matrix"].get(i,0).asDouble();
	}

}}