/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "drivetrapz.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveTrapz::DriveTrapz(){

	}

	// constructor
	DriveTrapz::DriveTrapz(const double amplitude, const double tstart, const double ramprate, const double tpulse, const arma::uword id){
		set_amplitude(amplitude); set_tstart(tstart); set_ramprate(ramprate); set_tpulse(tpulse); set_id(id);
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(){
		return std::make_shared<DriveTrapz>();
	}

	// factory
	ShDriveTrapzPr DriveTrapz::create(const double amplitude, const double tstart, const double ramprate, const double tpulse, const arma::uword id){
		return std::make_shared<DriveTrapz>(amplitude,tstart,ramprate,tpulse,id);
	}

	// get scaling
	double DriveTrapz::get_scaling(const double time) const{
		return rat::cmn::Extra::sign(amplitude_)*
			std::max(0.0,std::min(std::abs(amplitude_),
			std::min(ramprate_*(time-tstart_),
			-ramprate_*(time-tstart_-tpulse_)+
			2*std::abs(amplitude_))));
	}

	// get current derivative
	double DriveTrapz::get_dscaling(const double time) const{
		double dsc = 0;
		const double tramp = std::abs(amplitude_)/ramprate_;
		if(time>tstart_ && time<(tstart_+tramp))
			dsc = rat::cmn::Extra::sign(amplitude_)*ramprate_;
		if(time>tstart_+tramp+tpulse_ && time<tstart_+2*tramp+tpulse_)
			dsc = -rat::cmn::Extra::sign(amplitude_)*ramprate_;
		return dsc;
	}

	// set amplitude of the pulse
	void DriveTrapz::set_amplitude(const double amplitude){
		amplitude_ = amplitude;
	}

	// set start time
	void DriveTrapz::set_tstart(const double tstart){
		tstart_ = tstart;
	}

	// set rate of change
	void DriveTrapz::set_ramprate(const double ramprate){
		ramprate_ = ramprate;
	}

	// set pulse duration
	void DriveTrapz::set_tpulse(const double tpulse){
		tpulse_ = tpulse;
	}
	

	// get type
	std::string DriveTrapz::get_type(){
		return "mdl::drivetrapz";
	}

	// method for serialization into json
	void DriveTrapz::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["tstart"] = tstart_;
		js["ramprate"] = ramprate_;
		js["tpulse"] = tpulse_;
	}

	// method for deserialisation from json
	void DriveTrapz::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		Drive::deserialize(js,list,factory_list);
		set_amplitude(js["amplitude"].asDouble());
		set_tstart(js["tstart"].asDouble());
		set_ramprate(js["ramprate"].asDouble());
		set_tpulse(js["tpulse"].asDouble());
	}

}}