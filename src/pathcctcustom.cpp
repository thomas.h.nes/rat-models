/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathcctcustom.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// function for acquiring number of poles
	arma::uword CCTHarmonic::get_num_poles() const{
		return num_poles_;
	}

	// constructor
	CCTHarmonicInterp::CCTHarmonicInterp(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<double> &turn, const arma::Row<double> &a){
		num_poles_ = num_poles; is_skew_ = is_skew;
		turn_ = turn; a_ = a;
	}

	// factory
	ShCCTHarmonicInterpPr CCTHarmonicInterp::create(
		const arma::uword num_poles, const bool is_skew, 
		const arma::Row<double> &turn, const arma::Row<double> &a){
		return std::make_shared<CCTHarmonicInterp>(num_poles,is_skew,turn,a);
	}

	// calculate offset in z
	arma::field<arma::Row<double> > CCTHarmonicInterp::calc_z(
		const arma::Row<double> &theta, const arma::Row<double> &rho) const{
		// calculate turn
		const arma::Row<double> turn = theta/(2*arma::datum::pi);

		// calculate angle
		arma::Col<double> a;
		cmn::Extra::interp1(turn_.t(),a_.t(),turn.t(),a);

		// convert amplitude to angle
		const arma::Row<double> alpha = arma::atan(rho/a.t());

		// allocate z and derivatives thereoff
		arma::field<arma::Row<double> > z(4);

		// skew harmonics
		if(is_skew_){
			z(0) = rho%arma::cos(num_poles_*theta)/(num_poles_*arma::tan(alpha));
			z(1) = -rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
			z(2) = -num_poles_*rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
			z(3) = num_poles_*num_poles_*rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
		}

		// normal harmonics
		else{
			z(0) = rho%arma::sin(num_poles_*theta)/(num_poles_*arma::tan(alpha));
			z(1) = rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
			z(2) = -num_poles_*rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
			z(3) = -num_poles_*num_poles_*rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
		}

		// return coordinates
		return z;
	}

	// get type
	std::string CCTHarmonicInterp::get_type(){
		return "mdl::cctharmonicinterp";
	}

	// method for serialization into json
	void CCTHarmonicInterp::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["num_poles"] = (unsigned int)num_poles_;
		js["turn"] = Node::serialize_arma(turn_);
		js["amplitude"] = Node::serialize_arma(a_);
	}

	// method for deserialisation from json
	void CCTHarmonicInterp::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		num_poles_ = js["num_poles"].asUInt64();
		turn_ = Node::deserialize_arma(js["turn"]);
		a_ = Node::deserialize_arma(js["amplitude"]);
	}


	// constructor
	CCTHarmonicEquation::CCTHarmonicEquation(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun){
		fun_ = fun; num_poles_ = num_poles; is_skew_ = is_skew;
	}

	// factory
	ShCCTHarmonicEquationPr CCTHarmonicEquation::create(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun){
		return std::make_shared<CCTHarmonicEquation>(num_poles,is_skew,fun);
	}

	// calculate offset in z
	arma::field<arma::Row<double> > CCTHarmonicEquation::calc_z(
		const arma::Row<double> &theta, const arma::Row<double> &rho) const{
		// calculate turn
		const arma::Row<double> turn = theta/(2*arma::datum::pi);
		
		// calculate angle
		const arma::Row<double> a = fun_(turn);

		// convert amplitude to angle
		const arma::Row<double> alpha = arma::atan(rho/a.t());
		
		// allocate z and derivatives thereoff
		arma::field<arma::Row<double> > z(4);

		// skew harmonics
		if(is_skew_){
			z(0) = rho%arma::cos(num_poles_*theta)/(num_poles_*arma::tan(alpha));
			z(1) = -rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
			z(2) = -num_poles_*rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
			z(3) = num_poles_*num_poles_*rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
		}

		// normal harmonics
		else{
			z(0) = rho%arma::sin(num_poles_*theta)/(num_poles_*arma::tan(alpha));
			z(1) = rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
			z(2) = -num_poles_*rho%arma::sin(num_poles_*theta)/(arma::tan(alpha));
			z(3) = -num_poles_*num_poles_*rho%arma::cos(num_poles_*theta)/(arma::tan(alpha));
		}

		// return coordinates
		return z;
	}



	// default constructor
	PathCCTCustom::PathCCTCustom(){

	}

	// factory
	ShPathCCTCustomPr PathCCTCustom::create(){
		return std::make_shared<PathCCTCustom>();
	}

	// set pitch
	void PathCCTCustom::set_geometry(
		const arma::Row<double> &turn, 
		const arma::Row<double> &omega,
		const arma::Row<double> &rho){
		turn_ = turn; omega_ = omega; rho_ = rho;
	}

	// set range
	void PathCCTCustom::set_range(const double nt1, const double nt2){
		assert(nt2>nt1);
		nt1_ = nt1; nt2_ = nt2;
	}

	// add harmonic
	void PathCCTCustom::add_harmonic(ShCCTHarmonicPr harm){
		// check input
		assert(harm!=NULL); 

		// get number of models
		const arma::uword num_harmonics = harmonics_.n_elem;

		// allocate new source list
		ShCCTHarmonicPrList new_harmonics(num_harmonics + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_harmonics;i++)new_harmonics(i) = harmonics_(i);
		new_harmonics(num_harmonics) = harm;
		
		// set new source list
		harmonics_ = new_harmonics;
	}

	// set number of nodes for each turn
	void PathCCTCustom::set_num_nodes_per_turn(const arma::uword num_nodes_per_turn){
		assert(num_nodes_per_turn>0);
		num_nodes_per_turn_ = num_nodes_per_turn;
	}

	// setup coordinates and orientation vectors
	ShFramePr PathCCTCustom::create_frame() const{
		// number of poles
		arma::uword num_poles_max = 0;
		for(arma::uword i=0;i<harmonics_.n_elem;i++)
			num_poles_max = std::max(num_poles_max,harmonics_(i)->get_num_poles());

		// calculate number of sections per half
		const arma::uword num_sect_per_turn = num_poles_max*4;
		const double sectsize = 2*arma::datum::pi/num_sect_per_turn;

		// calculate maximum theta
		const double thetamin = nt1_*2*arma::datum::pi;
		const double thetamax = nt2_*2*arma::datum::pi;
		
		arma::uword num_int = std::floor(thetamax/sectsize);
		if(std::abs(thetamax-num_int*sectsize)<1e-8)num_int--;

		// calculate number of sections
		const arma::uword num_sections = 2*(num_int+1);

		// allocate theta
		arma::Row<double> theta_start(num_sections);
		arma::Row<double> theta_end(num_sections);

		// set theta start positions
		theta_start(0) = thetamin;
		theta_start.cols(1,num_sections-1) = 
			arma::linspace<arma::Row<double> >
			(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		
		// set theta end positions
		theta_end.cols(0,num_sections-2) = 
			arma::linspace<arma::Row<double> >
			(-(num_int*sectsize),num_int*sectsize,num_sections-1);
		theta_end(num_sections-1) = thetamax;

		// allocate
		arma::field<arma::Mat<double> > R(1,num_sections);
		arma::field<arma::Mat<double> > L(1,num_sections);
		arma::field<arma::Mat<double> > N(1,num_sections); 
		arma::field<arma::Mat<double> > D(1,num_sections);

		// allocate section and turn indexes
		arma::Row<arma::uword> section_idx(num_sections);
		arma::Row<arma::uword> turn_idx(num_sections); 

		// integrate pitch
		const arma::Row<double> omega_average = 
			(omega_.head_cols(omega_.n_elem-1) + omega_.tail_cols(omega_.n_elem-1))/2;
		arma::Row<double> zpitch = arma::join_horiz(arma::Row<double>{0},
			arma::cumsum(arma::diff(turn_,1,1)%omega_average));

		// center around zero
		zpitch -= cmn::Extra::interp1(turn_,zpitch,0);

		// tracking vector
		arma::Col<double>::fixed<3> dtrack = {1,0,0};

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// calculate section
			const double theta1 = theta_start(i);
			const double theta2 = theta_end(i);
			assert(theta2>theta1);

			// calculate number of nodes
		 	const arma::uword num_nodes = 
		 		std::max(2,(int)std::ceil(num_nodes_per_turn_*
		 		(theta2-theta1)/(2*arma::datum::pi)));

			// create theta (taking into account last section
			const arma::Row<double> theta = 
				arma::linspace<arma::Row<double> >
				(theta1, theta2, num_nodes);

			// calculate turn number
			const arma::Row<double> turn = theta/(2*arma::datum::pi);
			
			// calculate local radius
			arma::Col<double> rho_c;
			cmn::Extra::interp1(turn_.t(),rho_.t(),turn.t(),rho_c);
			arma::Row<double> rho = rho_c.t();

			// create coordinates for circle
			const arma::Row<double> x = rho%arma::cos(theta);
			const arma::Row<double> y = rho%arma::sin(theta);
			const arma::Row<double> vx = -rho%arma::sin(theta);
			const arma::Row<double> vy = rho%arma::cos(theta);
			const arma::Row<double> ax = -rho%arma::cos(theta);
			const arma::Row<double> ay = -rho%arma::sin(theta);
			const arma::Row<double> jx = rho%arma::sin(theta);
			const arma::Row<double> jy = -rho%arma::cos(theta);
			
			// create offset in z
			arma::Row<double> z(num_nodes,arma::fill::zeros);
			arma::Row<double> vz(num_nodes,arma::fill::zeros);
			arma::Row<double> az(num_nodes,arma::fill::zeros);
			arma::Row<double> jz(num_nodes,arma::fill::zeros);

			// walk over harmonics
			for(arma::uword i=0;i<harmonics_.n_elem;i++){
				const arma::field<arma::Row<double> > zfld = 
					harmonics_(i)->calc_z(theta, rho);
				z += zfld(0); vz += zfld(1); 
				az += zfld(2); jz += zfld(3);
			}

			// calculate local offset
			arma::Col<double> dz_c;
			cmn::Extra::interp1(turn_.t(),zpitch.t(),turn.t(),dz_c);
			arma::Row<double> dz = dz_c.t();
			
			// calculate local pitch
			arma::Col<double> omega_c;
			cmn::Extra::interp1(turn_.t(),omega_.t(),turn.t(),omega_c);
			arma::Row<double> omega = omega_c.t();

			// offset z coordinates with the pitch
			z += dz; vz += omega/(2*arma::datum::pi); 

			// store position vectors
			R(i).set_size(3,num_nodes);
			R(i).row(0) = x; R(i).row(1) = y; R(i).row(2) = z;

			// convert to 3XN matrices
			arma::Mat<double> V(3,num_nodes);
			V.row(0) = vx; V.row(1) = vy; V.row(2) = vz;
			arma::Mat<double> A(3,num_nodes);
			A.row(0) = ax; A.row(1) = ay; A.row(2) = az;
			arma::Mat<double> J(3,num_nodes);
			J.row(0) = jx; J.row(1) = jy; J.row(2) = jz;

			// store longitudinal vectors
			L(i) = V.each_row()/cmn::Extra::vec_norm(V);

			// calculate radial vector
			arma::Mat<double> Vax(3,num_nodes,arma::fill::zeros);
			Vax.row(2).fill(1.0);
			D(i) = cmn::Extra::cross(L(i),Vax);		

			// create normal vectors
			N(i) = cmn::Extra::cross(L(i),D(i));

			// normalize
			D(i).each_row()/=cmn::Extra::vec_norm(D(i));
			N(i).each_row()/=cmn::Extra::vec_norm(N(i));

			// check size
			assert(R(i).n_rows==3);	assert(L(i).n_rows==3);
			assert(N(i).n_rows==3);	assert(D(i).n_rows==3);

			// set section and turn
			section_idx(i) = i%num_sect_per_turn;
			turn_idx(i) = i/num_sect_per_turn;
		}

		// create frame
		ShFramePr gen = Frame::create(R,L,N,D);

		// set section and turn indices
		gen->set_location(section_idx,turn_idx);

		// transformations
		gen->apply_transformations(trans_);

		// create frame
		return gen;
	}

	// get type
	std::string PathCCTCustom::get_type(){
		return "mdl::pathcctcustom";
	}

	// method for serialization into json
	void PathCCTCustom::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();

		// geometry
		js["nt1"] = nt1_;
		js["nt2"] = nt2_;
		js["num_nodes_per_turn"] = (unsigned int)num_nodes_per_turn_;

		// arrays
		js["turn"] = Node::serialize_arma(turn_);
		js["omega"] = Node::serialize_arma(omega_);
		js["rho"] = Node::serialize_arma(rho_);

		// harmonics
		for(arma::uword i=0;i<harmonics_.n_elem;i++)
			js["harmonics"].append(cmn::Node::serialize_node(harmonics_(i), list));
	}

	// method for deserialisation from json
	void PathCCTCustom::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Transformations::deserialize(js,list,factory_list);

		// geometry
		nt1_ = js["nt1"].asDouble();
		nt2_ = js["nt2"].asDouble();
		num_nodes_per_turn_ = js["num_nodes_per_turn"].asUInt64();

		// arrays
		turn_ = Node::deserialize_arma(js["turn"]);
		omega_ = Node::deserialize_arma(js["omega"]);
		rho_ = Node::deserialize_arma(js["rho"]);

		// subnodes
		const arma::uword num_harm = js["harmonics"].size();
		harmonics_.set_size(num_harm);
		for(arma::uword i=0;i<num_harm;i++)
			harmonics_(i) = cmn::Node::deserialize_node<CCTHarmonic>(js["harmonics"].get(i,0), list, factory_list);
	}

}}
