/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcgrid.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcGrid::CalcGrid(){
		set_name("grid");
	}

	// constructor
	CalcGrid::CalcGrid(
		const double x1, const double x2, const arma::uword num_x, 
		const double y1, const double y2, const arma::uword num_y, 
		const double z1, const double z2, const arma::uword num_z){
		set_dim_x(x1,x2,num_x);	set_dim_y(y1,y2,num_y);	set_dim_z(z1,z2,num_z); set_name("grid");
	}

	// factory
	ShCalcGridPr CalcGrid::create(){
		return std::make_shared<CalcGrid>();
	}

	// factory
	ShCalcGridPr CalcGrid::create(
		const double x1, const double x2, const arma::uword num_x, 
		const double y1, const double y2, const arma::uword num_y, 
		const double z1, const double z2, const arma::uword num_z){
		return std::make_shared<CalcGrid>(x1,x2,num_x,y1,y2,num_y,z1,z2,num_z);
	}

	// set dimensions for x
	void CalcGrid::set_dim_x(const double x1, const double x2, const arma::uword num_x){
		if(x1>=x2 && num_x!=1)rat_throw_line("x1 must be smaller than x2");
		if(num_x<=0)rat_throw_line("number of elements in x must be larger than zero");
		x1_ = x1; x2_ = x2; num_x_ = num_x;
	}

	// set dimensions for y
	void CalcGrid::set_dim_y(const double y1, const double y2, const arma::uword num_y){
		if(y1>=y2 && num_y!=1)rat_throw_line("y1 must be smaller than y2");
		if(num_y<=0)rat_throw_line("number of elements in y must be larger than zero");
		y1_ = y1; y2_ = y2; num_y_ = num_y;
	}

	// set dimensions for z
	void CalcGrid::set_dim_z(const double z1, const double z2, const arma::uword num_z){
		if(z1>=z2 && num_z!=1)rat_throw_line("z1 must be smaller than z2");
		if(num_z<=0)rat_throw_line("number of elements in z must be larger than zero");
		z1_ = z1; z2_ = z2; num_z_ = num_z;
	}

	// setup function
	void CalcGrid::setup(cmn::ShLogPr lg){
		// check input
		if(num_x_==0)rat_throw_line("number of elements in x is not set");
		if(num_y_==0)rat_throw_line("number of elements in y is not set");
		if(num_z_==0)rat_throw_line("number of elements in z is not set");
		if(x1_>=x2_ && num_x_!=1)rat_throw_line("dimensions in x not set");
		if(y1_>=y2_ && num_y_!=1)rat_throw_line("dimensions in y not set");
		if(z1_>=z2_ && num_z_!=1)rat_throw_line("dimensions in z not set");

		// header
		lg->msg(2,"%s%sVOLUME GRID%s\n",KBLD,KGRN,KNRM);
		
		// display settings
		lg->msg(2,"%sSettings%s\n",KBLU,KNRM);
		lg->msg("grid size in x: %s%llu%s\n",KYEL,num_x_,KNRM);
		lg->msg("grid size in y: %s%llu%s\n",KYEL,num_y_,KNRM);
		lg->msg("grid size in z: %s%llu%s\n",KYEL,num_z_,KNRM);
		lg->msg("range in x: %s%2.2f - %2.2f%s\n",KYEL,x1_,x2_,KNRM);
		lg->msg("range in y: %s%2.2f - %2.2f%s\n",KYEL,y1_,y2_,KNRM);
		lg->msg("range in z: %s%2.2f - %2.2f%s\n",KYEL,z1_,z2_,KNRM);
		lg->msg("number of target coordinates: %s%llu%s\n",KYEL,num_x_*num_y_*num_z_,KNRM);
		lg->msg(-2,"\n");

		// arrays defining coordinates
		const arma::Row<double> xa = arma::linspace<arma::Row<double> >(x1_,x2_,num_x_);
		const arma::Row<double> ya = arma::linspace<arma::Row<double> >(y1_,y2_,num_y_);
		const arma::Row<double> za = arma::linspace<arma::Row<double> >(z1_,z2_,num_z_);
		
		// allocate coordinates
		Rt_.set_size(3,num_x_*num_y_*num_z_);

		// walk over x
		for(arma::uword i=0;i<num_z_;i++){
			for(arma::uword j=0;j<num_y_;j++){
				// calculate indexes
				const arma::uword idx1 = i*num_x_*num_y_ + j*num_x_;
				const arma::uword idx2 = i*num_x_*num_y_ + (j+1)*num_x_ - 1;

				// set coordinates
				Rt_.submat(0,idx1,0,idx2) = xa;
				Rt_.submat(1,idx1,1,idx2).fill(ya(j));
				Rt_.submat(2,idx1,2,idx2).fill(za(i));
			}
		}

		// done
		lg->msg(-2);
	}


	// write surface to VTK file
	// void CalcGrid::export_vtk_rect(const std::string &fname, cmn::ShLogPr lg) const{
	// 	// check if calculation was done
	// 	if(!has_field())rat_throw_line("field is not calculated");

	// 	// header
	// 	lg->msg(2,"%s%sVTK RECTANGULAR GRID EXPORT%s\n",KBLD,KGRN,KNRM);
		
	// 	// header for data preparation
	// 	lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

	// 	// create coordinates for MLFMM
	// 	lg->msg("coordinate arrays\n");
	// 	arma::Row<double> xa = arma::linspace<arma::Row<double> >(x1_,x2_,num_x_);
	// 	arma::Row<double> ya = arma::linspace<arma::Row<double> >(y1_,y2_,num_y_);
	// 	arma::Row<double> za = arma::linspace<arma::Row<double> >(z1_,z2_,num_z_);
			
	// 	// wrap coordinate arrays
	// 	vtkSmartPointer<vtkDoubleArray> xvtk = vtkDoubleArray::New();
	// 	xvtk->SetNumberOfComponents(1); xvtk->SetArray(xa.memptr(), num_x_, 1);
	// 	vtkSmartPointer<vtkDoubleArray> yvtk = vtkDoubleArray::New();
	// 	yvtk->SetNumberOfComponents(1); yvtk->SetArray(ya.memptr(), num_y_, 1);
	// 	vtkSmartPointer<vtkDoubleArray> zvtk = vtkDoubleArray::New();
	// 	zvtk->SetNumberOfComponents(1); zvtk->SetArray(za.memptr(), num_z_, 1);

	// 	// setup grid
	// 	vtkSmartPointer<vtkRectilinearGrid> rgrid = vtkRectilinearGrid::New();
	// 	rgrid->SetDimensions(num_x_,num_y_,num_z_);
	// 	rgrid->SetXCoordinates(xvtk);
	// 	rgrid->SetYCoordinates(yvtk);
	// 	rgrid->SetZCoordinates(zvtk);

	// 	// wrap magnetic field
	// 	lg->msg("magnetic field\n");
	// 	arma::Mat<double> B = get_field("B");
	// 	assert(B.n_cols==num_x_*num_y_*num_z_);
	// 	vtkSmartPointer<vtkDoubleArray> Bvtk = vtkDoubleArray::New();
	// 	Bvtk->SetNumberOfComponents(3);
	// 	Bvtk->SetArray(B.memptr(), num_x_*num_y_*num_z_*3, 1);
	// 	Bvtk->SetName("Mgn. Flux Density [T]");
	// 	rgrid->GetPointData()->AddArray(Bvtk);

	// 	// wrap magnetic vector potential
	// 	lg->msg("magnetic vector potential\n");
	// 	arma::Mat<double> A = get_field("A");
	// 	assert(A.n_cols==num_x_*num_y_*num_z_);
	// 	vtkSmartPointer<vtkDoubleArray> Avtk = vtkDoubleArray::New();
	// 	Avtk->SetNumberOfComponents(A.n_rows);
	// 	Avtk->SetArray(A.memptr(), A.n_elem, 1);
	// 	Avtk->SetName("Vector Potential [Vs/m]");
	// 	rgrid->GetPointData()->AddArray(Avtk);

	// 	// done preparing data
	// 	lg->msg(-2,"\n");

	// 	// display file settings and type
	// 	std::string fname_ext = fname + ".vtr";
	// 	lg->msg(2,"%sWriting VTK File%s\n",KBLU,KNRM);
	// 	lg->msg("filename: %s%s%s\n",KYEL,fname_ext.c_str(),KNRM);
	// 	lg->msg("type: rectangular grid (vtr)\n");

	// 	// Write the file
	// 	vtkSmartPointer<vtkXMLRectilinearGridWriter> writer =  
	// 		vtkSmartPointer<vtkXMLRectilinearGridWriter>::New();
	// 	writer->SetFileName(fname_ext.c_str());
	// 	writer->SetInputData(rgrid);
	// 	writer->Write();

	// 	// done writing VTK table
	// 	lg->msg(-2,"\n");

	// 	// done exporting
	// 	lg->msg(-2);
	// }

	// write surface to VTK file
	ShVTKImgPr CalcGrid::export_vtk(cmn::ShLogPr lg) const{
		// check if calculation was done
		if(!has_field())rat_throw_line("field is not calculated");

		// header
		lg->msg(2,"%s%sVTK RECTANGULAR GRID EXPORT%s\n",KBLD,KGRN,KNRM);
		
		// header for data preparation
		lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

		// create coordinates for MLFMM
		lg->msg("coordinate arrays\n");
			
		// create VTK file
		ShVTKImgPr vtk_data = VTKImg::create();

		// calculate spacing
		vtk_data->set_dims(x1_,x2_,num_x_, y1_,y2_,num_y_, z1_,z2_,num_z_);

		// walk over list of output types
		for(std::list<CalcOutTypes>::const_iterator it = output_types_.begin(); it!=output_types_.end(); it++){

			// get type
			CalcOutTypes type = *it;

			// check the type tag
			switch(type){
				// magnetic vector potential
				case VECTOR_POTENTIAL:{
					lg->msg("magnetic vector potential\n");
					if(!has("A") || !has_field())rat_throw_line("vector potential not calculated");
					const arma::Mat<double> A = get_field("A");
					vtk_data->set_nodedata(A,"Vector Potential [Vs/m]");
				}break;

				// magnetic flux density
				case MAGNETIC_FLUX:{
					lg->msg("magnetic flux density\n");
					if(!has("H") || !has_field())rat_throw_line("magnetic field not calculated");
					const arma::Mat<double> B = get_field("B");
					vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");
				}break;	

				// magnetic field
				case MAGNETIC_FIELD:{
					lg->msg("magnetisation\n");
					const arma::Mat<double> H = get_field("H");
					vtk_data->set_nodedata(H,"Magnetic Field [A/m]");
				}break;

				// magnetisation
				case MAGNETISATION:{
					lg->msg("magnetisation\n");
					const arma::Mat<double> Mn = get_field("M");
					vtk_data->set_nodedata(Mn,"Magnetisation [A/m]");
				}break;

				// unrecognized output type
				default: break;
			}
		}

		// mesh done
		lg->msg(-2);
		
		// done exporting
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

	// export a vtk with times
	void CalcGrid::write(cmn::ShLogPr lg){
		// check if data directory set
		if(output_dir_.empty())return;

		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
		// check output directory
		if(output_times_.is_empty())rat_throw_line("output times are not set");

		// create output directory
		cmn::Extra::create_directory(output_dir_);

		// output filename
		std::string fname  = output_fname_;

		// report
		lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

		// settings report
		display_settings(lg);

		// report
		lg->msg(2, "%swriting griddata%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKImgPr vtk_img = export_vtk();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_img->write(output_dir_ + fname + "_grd");
		}

		// return
		lg->msg(-6,"\n"); 
	}


	// get type
	std::string CalcGrid::get_type(){
		return "mdl::calcgrid";
	}

	// method for serialization into json
	void CalcGrid::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		CalcFieldMap::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["x1"] = x1_; js["x2"] = x2_; js["num_x"] = (unsigned int)num_x_;
		js["y1"] = y1_; js["y2"] = y2_; js["num_y"] = (unsigned int)num_y_;
		js["z1"] = z1_; js["z2"] = z2_; js["num_z"] = (unsigned int)num_z_;
	}

	// method for deserialisation from json
	void CalcGrid::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// serialize fieldmap
		CalcFieldMap::deserialize(js,list,factory_list);

		// properties
		set_dim_x(js["x1"].asDouble(), js["x2"].asDouble(), js["num_x"].asUInt64());
		set_dim_y(js["y1"].asDouble(), js["y2"].asDouble(), js["num_y"].asUInt64());
		set_dim_z(js["z1"].asDouble(), js["z2"].asDouble(), js["num_z"].asUInt64());
	}

}}