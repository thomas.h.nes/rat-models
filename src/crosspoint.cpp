/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "crosspoint.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossPoint::CrossPoint(){

	}

	// constructor with input
	CrossPoint::CrossPoint(
		const double u, const double v, const double thickness, const double width){
		// set to self
		set_coord(u,v); set_thickness(thickness); set_width(width);
	}

	// factory
	ShCrossPointPr CrossPoint::create(){
		return std::make_shared<CrossPoint>();
	}

	// factory with dimension input
	ShCrossPointPr CrossPoint::create(
		const double u, const double v, const double thickness, const double width){
		return std::make_shared<CrossPoint>(u,v,thickness,width);
	}

	// set dimensions in the normal direction
	void CrossPoint::set_coord(
		const double u, const double v){
		u_ = u; v_ = v;
	}

	// set hidden area
	void CrossPoint::set_thickness(const double thickness){
		if(thickness<=0)rat_throw_line("thickness must be larger than zero");
		thickness_ = thickness;
	}

	// set hidden area
	void CrossPoint::set_width(const double width){
		if(width<=0)rat_throw_line("width must be larger than zero");
		width_ = width;
	}

	// volume mesh
	ShAreaPr CrossPoint::create_area() const{
		// coordinate
		arma::Col<double>::fixed<2> Rn = {u_,v_};
		arma::Row<arma::uword>::fixed<1> n = {0};

		// set area
		ShAreaPr area = Area::create(Rn,n);
		area->set_hidden_thickness(thickness_);
		area->set_hidden_width(width_);

		// return area
		return area;
	}

	// surface mesh
	ShPerimeterPr CrossPoint::create_perimeter() const{
		// coordinate
		arma::Col<double> Rn(2,0);
		arma::Row<arma::uword> s(2,0);

		// extract periphery and return
		return Perimeter::create(Rn,s);
	}

	// get type
	std::string CrossPoint::get_type(){
		return "mdl::crosspoint";
	}

	// method for serialization into json
	void CrossPoint::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		js["type"] = get_type();
		js["u"] = u_; js["v"] = v_;
	}

	// method for deserialisation from json
	void CrossPoint::deserialize(
		const Json::Value &js, cmn::DSList &/*list*/, 
		const cmn::NodeFactoryMap &/*factory_list*/){
		set_coord(js["u"].asDouble(),js["v"].asDouble());
	}

}}
