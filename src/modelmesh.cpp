/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelmesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelMesh::ModelMesh(){

	}

	// constructor immediately setting base and cross section
	ModelMesh::ModelMesh(ShPathPr base, ShCrossPr crss){
		// set base path and cross section
		set_base(base); set_cross(crss);
	}

	// factory
	ShModelMeshPr ModelMesh::create(){
		//return ShModelMeshPr(new ModelMesh);
		return std::make_shared<ModelMesh>();
	}

	// factory immediately setting base and cross section
	ShModelMeshPr ModelMesh::create(ShPathPr base, ShCrossPr crss){
		//return ShModelMeshPr(new ModelMesh(base,crss));
		return std::make_shared<ModelMesh>(base,crss);
	}

	// set baseline
	void ModelMesh::set_base(ShPathPr base){
		// check input
		if(base==NULL)rat_throw_line("base path points to NULL");

		// set base to self
		base_ = base;
	}

	// set cross section
	void ModelMesh::set_cross(ShCrossPr crss){
		// check input
		if(crss==NULL)rat_throw_line("cross section points to NULL");

		// set cross section to self
		crss_ = crss;
	}

	// set operating temperature 
	void ModelMesh::set_operating_temperature(const double operating_temperature){
		operating_temperature_ = operating_temperature;
	}
	
	// set circuit index
	void ModelMesh::set_drive_id(const arma::uword drive_id){
		drive_id_ = drive_id;
	}

	// get number of coils stored
	// this obviously is only one coil
	arma::uword ModelMesh::get_num_objects() const{
		return 1;
	}

	// get current mesh that represents this coil
	void ModelMesh::setup_mesh(ShMeshPr mesh) const{
		// check input
		if(base_==NULL)rat_throw_line("base path is not set");
		if(crss_==NULL)rat_throw_line("cross section is not set");

		// create frame
		ShFramePr frame = base_->create_frame();
		
		// combine frame into one single section 
		// this allows to form a single mesh
		frame->combine();
		// frame->split(2);

		// create 2d area mesh
		ShAreaPr area = crss_->create_area();
			
		// create mesh
		mesh->setup(frame,area);

		// set temperature
		mesh->set_operating_temperature(operating_temperature_);
		mesh->set_drive_id(drive_id_);

		// set name (is appended by models later)
		mesh->set_name(myname_);

		// apply transformations to mesh
		mesh->apply_transformations(trans_);
	}

	// create edges used for freeCAD and visual representation
	void ModelMesh::setup_edge(ShEdgesPr edge) const{
		// check input
		if(base_==NULL)rat_throw_line("base path is not set");
		if(crss_==NULL)rat_throw_line("cross section is not set");

		// create frame
		ShFramePr frame = base_->create_frame();
		
		// create 2d area mesh
		ShAreaPr area = crss_->create_area();

		// create edges
		edge->setup(frame,area);

		// set temperature
		edge->set_operating_temperature(operating_temperature_);
		edge->set_drive_id(drive_id_);

		// set name
		edge->set_name(myname_);

		// apply transformations
		edge->apply_transformations(trans_);
	}

	// factory for mesh objects
	ShMeshPrList ModelMesh::create_mesh() const{
		// allocate mesh data
		ShMeshPr mesh = Mesh::create();

		// fill mesh data
		setup_mesh(mesh);

		// combine into list
		ShMeshPrList meshes(1);
		meshes(0) = mesh;

		// return mesh data
		return meshes;
	}

	// factory for mesh objects
	ShEdgesPrList ModelMesh::create_edge() const{
		// allocate edge data
		ShEdgesPr edge = Edges::create();

		// fill edge data
		setup_edge(edge);

		// combine into list
		ShEdgesPrList edges(1);
		edges(0) = edge;

		// return edge data
		return edges;
	}

	// get type
	std::string ModelMesh::get_type(){
		return "mdl::modelmesh";
	}

	// method for serialization into json
	void ModelMesh::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Nameable::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();
		js["operating_temperature"] = operating_temperature_;
		js["drive_id"] = (unsigned int)drive_id_;

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
		js["crss"] = cmn::Node::serialize_node(crss_, list);
		//js["clnt"] = cmn::Node::serialize_node(clnt_, list);
	}

	// method for deserialisation from json
	void ModelMesh::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Nameable::deserialize(js,list,factory_list);
		Transformations::deserialize(js,list,factory_list);

		// properties
		set_operating_temperature(js["operating_temperature"].asDouble());
		set_drive_id(js["drive_id"].asUInt64());

		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list);
		crss_ = cmn::Node::deserialize_node<Cross>(js["crss"], list, factory_list);
		//clnt_ = cmn::Node::deserialize_node<Coolant>(js["clnt"], list, factory_list);
	}

}}