/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelGroup::ModelGroup(){

	}

	// constructor with model input
	ModelGroup::ModelGroup(ShModelPr model){
		models_.set_size(1);
		models_(0) = model;
	}

	// constructor with multiple input models
	ModelGroup::ModelGroup(ShModelPrList models){
		models_ = models;
	}

	// factory
	ShModelGroupPr ModelGroup::create(){
		return std::make_shared<ModelGroup>();
	}

	// factory
	ShModelGroupPr ModelGroup::create(ShModelPr model){
		return std::make_shared<ModelGroup>(model);
	}

	// factory
	ShModelGroupPr ModelGroup::create(ShModelPrList models){
		return std::make_shared<ModelGroup>(models);
	}

	// get number of coils stored
	// in this model and all child models/coils
	arma::uword ModelGroup::get_num_objects() const{
		// get number of models
		const arma::uword num_models = models_.n_elem;

		// create counter
		arma::uword cnt = 0;

		// walk over stored models and add
		for(arma::uword i=0;i<num_models;i++)
			cnt+=models_(i)->get_num_objects();

		// return counter
		return cnt;
	}

	// add coil
	void ModelGroup::add_model(ShModelPr model){
		// check input
		if(model==NULL)rat_throw_line("model points to zero");

		// get number of models
		const arma::uword num_models = models_.n_elem;

		// allocate new source list
		ShModelPrList new_models(num_models + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_models;i++)new_models(i) = models_(i);
		new_models(num_models) = model;
		
		// set new source list
		models_ = new_models;
	}

	// create a mesh of the stored coils
	ShMeshPrList ModelGroup::create_mesh() const{
		// check input model
		if(models_.is_empty())rat_throw_line("model is not set");

		// get number of models
		const arma::uword num_models = models_.n_elem;

		// check that models are set
		assert(num_models>0);

		// allocate temporary array of magnet signals
		arma::field<ShMeshPrList> temp(num_models);

		// allocate counter
		arma::Col<arma::uword> count(num_models);
		
		// walk over coils and create coilmesh for each
		//for(arma::uword i=0;i<models_.n_elem;i++){
		cmn::parfor(0,num_models,parallel_setup_,[&](int i, int){
			temp(i) = models_(i)->create_mesh();
			count(i) = temp(i).n_elem;
		//}
		});

		// total number of coils
		const arma::uword total_num_coils = arma::sum(count);

		// allocate output list of coil meshes
		ShMeshPrList coilmeshes(total_num_coils);

		// walk over coils again and collect magnet signals
		arma::uword idx = 0;
		for(arma::uword i=0;i<num_models;i++){
			for(arma::uword j=0;j<count(i);j++){
				coilmeshes(idx) = temp(i)(j); idx++;
			}
		}

		// sanity check
		assert(idx==total_num_coils);

		// apply transformations
		for(arma::uword j=0;j<total_num_coils;j++){
			coilmeshes(j)->apply_transformations(trans_);
			coilmeshes(j)->append_name(myname_);
		}

		// combine into single coilmesh and return
		return coilmeshes;
	}

	// create edges of the coil
	ShEdgesPrList ModelGroup::create_edge() const{
		// check input model
		if(models_.is_empty())rat_throw_line("model is not set");

		// get number of models
		const arma::uword num_models = models_.n_elem;

		// check that models are set
		assert(num_models>0);

		// create lists
		arma::field<ShEdgesPrList> temp(num_models);

		// allocate counter
		arma::Col<arma::uword> count(num_models);
		
		// walk over coils and create coilmesh for each
		//for(arma::uword i=0;i<models_.n_elem;i++){
		cmn::parfor(0,num_models,parallel_setup_,[&](int i, int){
			temp(i) = models_(i)->create_edge();
			count(i) = temp(i).n_elem;
		//}
		});

		// total number of coils
		const arma::uword total_num_coils = arma::sum(count);

		// allocate output list of coil meshes
		ShEdgesPrList coiledges(total_num_coils);

		// walk over coils again and collect magnet signals
		arma::uword idx = 0;
		for(arma::uword i=0;i<num_models;i++){
			for(arma::uword j=0;j<count(i);j++){
				coiledges(idx) = temp(i)(j); idx++;
			}
		}

		// sanity check
		assert(idx==total_num_coils);

		// apply transformations
		for(arma::uword j=0;j<total_num_coils;j++){
			coiledges(j)->apply_transformations(trans_);
		}

		// set tags
		//add_tags(coiledges);

		// combine into single coilmesh and return
		return coiledges;
	}

	// get type
	std::string ModelGroup::get_type(){
		return "mdl::modelgroup";
	}

	// method for serialization into json
	void ModelGroup::serialize(Json::Value &js, cmn::SList &list) const{
		// parent objects
		Nameable::serialize(js,list);
		Transformations::serialize(js,list);

		// type
		js["type"] = get_type();
		js["psetup"] = parallel_setup_;

		// subnodes
		for(arma::uword i=0;i<models_.n_elem;i++){
			js["models"].append(cmn::Node::serialize_node(models_(i), list));
		}
	}

	// method for deserialisation from json
	void ModelGroup::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Nameable::deserialize(js,list,factory_list);
		Transformations::deserialize(js,list,factory_list);

		// settings
		const arma::uword num_models = js["models"].size();
		models_.set_size(num_models);
		for(arma::uword i=0;i<num_models;i++)
			models_(i) = cmn::Node::deserialize_node<Model>(js["models"].get(i,0), list, factory_list);
		parallel_setup_ = js["psetup"].asBool();
	}

}}
