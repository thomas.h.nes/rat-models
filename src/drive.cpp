/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// set circuit identifier
	void Drive::set_id(const arma::uword id){
		id_ = id;
	}

	// get identifier
	arma::uword Drive::get_id() const{
		return id_;
	}

	// get type
	std::string Drive::get_type(){
		return "mdl::drive";
	}

	// method for serialization into json
	void Drive::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["drive_id"] = (unsigned int)id_;
	}

	// method for deserialisation from json
	void Drive::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		set_id(js["drive_id"].asUInt64());
	}


}}