/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// get type
	std::string Calc::get_type(){
		return "mdl::calc";
	}


	// set data directory
	void Calc::set_output_dir(const std::string &output_dir){
		output_dir_ = output_dir;
	}

	// set data filename
	void Calc::set_output_fname(const std::string &output_fname){
		output_fname_ = output_fname;
	}


	// set output times
	void Calc::set_output_times(const arma::Row<double> &output_times){
		if(output_times.is_empty())rat_throw_line("output times array is empty");
		output_times_ = output_times;
	}

	// get output times
	arma::Row<double> Calc::get_output_times() const{
		return output_times_;
	}

	// set output type
	void Calc::set_output_type(const CalcOutTypes type){
		output_types_.clear(); 
		add_output_type(type);
	}

	// add output type
	void Calc::add_output_type(const CalcOutTypes type){
		// check for all types
		if(type==ALL){
			output_types_.clear();
			for(int inttype=0;inttype<CalcOutTypes::ALL;inttype++)
				add_output_type((CalcOutTypes)inttype);
			return;
		}

		// check if the output type is already on the list
		std::list<CalcOutTypes>::const_iterator it = 
			std::find(output_types_.begin(), output_types_.end(), type);

		// add it to the list
		if(it == output_types_.end())
			output_types_.push_back(type);
	}

	// display calculation settings
	void Calc::display_settings(cmn::ShLogPr lg) const{
		lg->msg(2, "%soutput settings%s\n",KBLU,KNRM);
		lg->msg("directory: %s%s%s\n",KYEL,output_dir_.c_str(),KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,output_fname_.c_str(),KNRM);
		lg->msg("time span: %s%8.2e - %8.2e [s] (%llu steps)%s\n",KYEL,
			output_times_(0),output_times_(output_times_.n_elem-1),output_times_.n_elem,KNRM);
		lg->msg(-2,"\n");
	}

	// calculate and write
	void Calc::calculate_and_write(cmn::ShLogPr lg){
		calculate(lg); write(lg);
	}

	// method for serialization into json
	void Calc::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		Nameable::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["output_dir"] = output_dir_;
		js["output_fname"] = output_fname_;
		js["output_times"] = Node::serialize_arma(output_times_);
		
		// walk over output types
		for(std::list<CalcOutTypes>::const_iterator it = output_types_.begin(); it!=output_types_.end(); it++)
			js["output_types"].append((int)*it);

		// properties
		js["type"] = get_type();
	}

	// method for deserialisation from json
	void Calc::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// serialize fieldmap
		Nameable::deserialize(js,list,factory_list);

		// data directory and model name
		output_dir_ = js["output_dir"].asString();
		output_fname_ = js["output_fname"].asString();
		output_times_ = Node::deserialize_arma(js["output_times"]);
		arma::uword num_types = js["output_types"].size();
		for(arma::uword i=0;i<num_types;i++)
			add_output_type((CalcOutTypes)js["output_types"].get(i,0).asInt());
	}


}}