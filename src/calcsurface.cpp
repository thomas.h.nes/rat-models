/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcsurface.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcSurface::CalcSurface(){
		set_name("surf");
	}

	// constructor with model input
	CalcSurface::CalcSurface(ShModelPr source_model, ShModelPr target_model){
		set_source_model(source_model); set_target_model(target_model); set_name("surf");
	}

	// factory
	ShCalcSurfacePr CalcSurface::create(){
		return std::make_shared<CalcSurface>();
	}
	
	// factory
	ShCalcSurfacePr CalcSurface::create(ShModelPr source_model, ShModelPr target_model){
		return std::make_shared<CalcSurface>(source_model, target_model);
	}

	// set model
	void CalcSurface::set_target_model(ShModelPr target_model){
		if(target_model==NULL)rat_throw_line("target model points to NULL");
		target_model_ = target_model;
	}

	// setup function
	void CalcSurface::setup(cmn::ShLogPr lg){
		// header
		lg->msg(2,"%s%sSURFACE MESH%s\n",KBLD,KGRN,KNRM);
		
		// create meshes
		ShMeshPrList meshes = target_model_->create_mesh();

		// count number of meshes
		num_objects_ = meshes.n_elem;

		// display meshes
		Mesh::display(lg, meshes);

		// extract surfaces
		surfaces_.set_size(num_objects_);
		for(arma::uword i=0;i<num_objects_;i++)
			surfaces_(i) = meshes(i)->create_surface();

		// allocate indexes
		num_nodes_object_.set_size(num_objects_);
		num_elements_object_.set_size(num_objects_);

		// create indexes for each object 
		for(arma::uword i=0;i<num_objects_;i++){
			num_nodes_object_(i) = surfaces_(i)->get_num_nodes();
			num_elements_object_(i) = surfaces_(i)->get_num_elements();
		}
		
		// total number of nodes
		num_nodes_ = arma::sum(num_nodes_object_);
		num_elements_ = arma::sum(num_elements_object_);
		// num_targets_ = num_nodes_;

		// find sections
		idx_nodes_.set_size(num_objects_+1); idx_nodes_(0) = 0; 
		idx_nodes_.tail_cols(num_objects_) = arma::cumsum(num_nodes_object_);
		idx_elem_.set_size(num_objects_+1); idx_elem_(0) = 0; 
		idx_elem_.tail_cols(num_objects_) = arma::cumsum(num_elements_object_);

		// allocate nodes
		Rt_.set_size(3,num_nodes_);
		//s_.set_size(4,num_elements_);
		s_.set_size(1,num_objects_);

		// walk over objects
		for(arma::uword i=0;i<num_objects_;i++){
			// get indexes
			const arma::uword nid1 = idx_nodes_(i);
			const arma::uword nid2 = idx_nodes_(i+1)-1;
			// const arma::uword eid1 = idx_elem_(i);
			// const arma::uword eid2 = idx_elem_(i+1)-1;

			// copy target points which are the nodes of each mesh
			Rt_.cols(nid1,nid2) = surfaces_(i)->get_coords();
			//s_.cols(eid1,eid2) = surfaces_(i)->get_elements() + nid1;
			s_(i) = surfaces_(i)->get_elements() + nid1;
		}

		// done
		lg->msg(-2,"\n");
	}

	// function for calculating critical temperature at nodes
	arma::Row<double> CalcSurface::calc_Tc(
		const arma::Mat<double> &Bn, 
		const arma::Mat<double> &Jn, 
		const bool use_parallel) const{

		// allocate
		arma::Row<double> Tc(num_nodes_,arma::fill::zeros); 

		// walk over coils
		// for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,num_objects_,use_parallel,[&](int i, int){
			// get nodes for this coil
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			
			// calculate critical temperature
			Tc.cols(idx1,idx2) = surfaces_(i)->calc_Tc(
				Jn.cols(idx1,idx2), Bn.cols(idx1,idx2));
		});

		// return critical temp
		return Tc;
	}

	// function for calculating field angle at nodes
	// output in [radians]
	arma::Row<double> CalcSurface::calc_alpha(
		const arma::Mat<double> &Bn) const{
		// const arma::Mat<double> B = get_field("B");
		arma::Row<double> alpha(num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			alpha.cols(idx1,idx2) = surfaces_(i)->calc_alpha(Bn.cols(idx1,idx2));
		}
		return alpha;
	}

	// function for calculating critical current at nodes
	// output in [A/m^2]
	arma::Row<double> CalcSurface::calc_Je(
		const arma::Mat<double> &Bn, 
		const arma::Mat<double> &Tn, 
		const bool use_parallel) const{

		// allocate
		arma::Row<double> Je(num_nodes_,arma::fill::zeros);

		// walk over contained coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,num_objects_,use_parallel,[&](int i, int){
			// get nodes for this coil
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			
			// calculate critical current in [A/m^2]
			Je.cols(idx1,idx2) = surfaces_(i)->calc_Je(
				Bn.cols(idx1,idx2), Tn.cols(idx1,idx2));
		});

		// return critical current
		return Je;
	}

	// calculate electric field at nodes [V/m]
	// not that this is only the electric field
	// due to the resistance of the material.
	// The inductive component is not included.
	arma::Mat<double> CalcSurface::calc_E(
		const arma::Mat<double> &Bn, 
		const arma::Mat<double> &Jn, 
		const arma::Mat<double> &Tn, 
		const bool use_parallel) const{

		// allocate
		arma::Mat<double> E(3,num_nodes_,arma::fill::zeros);

		// walk over contained coils
		//for(arma::uword i=0;i<num_coils_;i++){
		cmn::parfor(0,num_objects_,use_parallel,[&](int i, int){
			// get nodes for this coil
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			
			// calculate electric field
			E.cols(idx1,idx2) = surfaces_(i)->calc_E(Jn.cols(idx1,idx2), 
				Bn.cols(idx1,idx2), Tn.cols(idx1,idx2));
		});

		// put electric field in same direction as current and return
		return E;
	}

	// gather current density at nodes of mesh
	arma::Mat<double> CalcSurface::get_nodal_current_density() const{
		arma::Mat<double> Jn(3,num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			const double scaling = drives_.at(surfaces_(i)->get_drive_id())->get_scaling(time_);
			Jn.cols(idx1,idx2) = scaling*surfaces_(i)->get_nodal_current_density();
		}
		return Jn;
	}

	// gather temperatures at nodes of mesh 
	arma::Row<double> CalcSurface::get_nodal_temperature() const{
		arma::Row<double> Tn(num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			Tn.cols(idx1,idx2) = surfaces_(i)->get_nodal_temperature();
		}
		return Tn;
	}

	// gather longitudinal vectors at nodes
	arma::Mat<double> CalcSurface::get_longitudinal() const{
		arma::Mat<double> L(3,num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			L.cols(idx1,idx2) = surfaces_(i)->get_direction();
		}
		return L;
	}

	// gather transverse vectors at nodes
	arma::Mat<double> CalcSurface::get_transverse() const{
		arma::Mat<double> D(3,num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			D.cols(idx1,idx2) = surfaces_(i)->get_transverse();
		}
		return D;
	}


	// gather normal vectors at nodes
	arma::Mat<double> CalcSurface::get_normal() const{
		arma::Mat<double> N(3,num_nodes_);
		for(arma::uword i=0;i<num_objects_;i++){
			const arma::uword idx1 = idx_nodes_(i);
			const arma::uword idx2 = idx_nodes_(i+1)-1;
			N.cols(idx1,idx2) = surfaces_(i)->get_normal();
		}
		return N;
	}

	// write surface to VTK file
	ShVTKUnstrPr CalcSurface::export_vtk(cmn::ShLogPr lg) const{
		// create VTK data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// header for export
		lg->msg(2,"%s%sVTK UNSTRUCTURED GRID EXPORT%s\n",KBLD,KGRN,KNRM);
		
		// header for data preparation
		lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

		// Create mesh and elements
		lg->msg("mesh coordinates and elements\n");
		// const arma::uword quad_type = 9;
		// vtk_data->set_mesh(Rt_,s_,quad_type); // 12 stands for hexahedrons
		arma::Row<arma::uword> element_types(num_objects_);
		for(arma::uword i=0;i<num_objects_;i++)
			element_types(i) = VTKUnstr::get_element_type(s_(i).n_rows);
		vtk_data->set_nodes(Rt_);
		vtk_data->set_elements(s_,element_types);

		// walk over list of output types
		for(std::list<CalcOutTypes>::const_iterator it = output_types_.begin(); it!=output_types_.end(); it++){

			// get type
			CalcOutTypes type = *it;

			// check the type tag
			switch(type){
				// conductor orientation
				case ORIENTATION:{
					lg->msg("longitudinal vectors\n");
					const arma::Mat<double> L = get_longitudinal();
					vtk_data->set_nodedata(L,"Longitudinal [m]");

					// transverse vector
					lg->msg("transverse vectors\n");
					const arma::Mat<double> D = get_transverse();
					vtk_data->set_nodedata(D,"Transverse [m]");

					// normal vector
					lg->msg("normal vectors\n");
					const arma::Mat<double> N = get_normal();
					vtk_data->set_nodedata(N,"Normal [m]");
				}break;
				
				// electric current density
				case CURRENT_DENSITY:{
					lg->msg("current density\n");
					const arma::Mat<double> Jn = get_nodal_current_density();
					vtk_data->set_nodedata(1e-6*Jn,"Current Density [A/mm^2]");
				}break;

				// nodal temperature
				case TEMPERATURE:{
					lg->msg("temperature\n");
					const arma::Row<double> Tn = get_nodal_temperature();
					vtk_data->set_nodedata(Tn,"Temperature [K]");
				}break;

				// magnetic vector potential
				case VECTOR_POTENTIAL:{
					lg->msg("magnetic vector potential\n");
					const arma::Mat<double> A = get_field("A");
					vtk_data->set_nodedata(A,"Vector Potential [Vs/m]");
				}break;

				// magnetic flux density
				case MAGNETIC_FLUX:{
					lg->msg("magnetic flux density\n");
					const arma::Mat<double> B = get_field("B");
					vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");
				}break;

				// magnetisation
				case MAGNETISATION:{
					lg->msg("magnetisation\n");
					const arma::Mat<double> Mn = get_field("M");
					vtk_data->set_nodedata(Mn,"Magnetisation [A/m]");
				}break;

				// magnetisation
				case MAGNETIC_FIELD:{
					lg->msg("magnetic field\n");
					const arma::Mat<double> Hn = get_field("H");
					vtk_data->set_nodedata(Hn,"Magnetic Field [A/m]");
				}break;

				// magnetic field angle
				case FIELD_ANGLE:{
					lg->msg("magnetic field angle\n");
					const arma::Mat<double> Bn = get_field("B");
					const arma::Row<double> alpha = 360.0*calc_alpha(Bn)/(arma::datum::pi*2);
					vtk_data->set_nodedata(alpha,"Mgn. Field Angle [deg]"); 
				}break;

				// engineering current density (critical current density over conductor area)
				// and percentage of critical current
				case ENG_CURRENT_DENSITY:{
					lg->msg("engineering current density\n");
					const arma::Row<double> Tn = get_nodal_temperature();
					const arma::Mat<double> Bn = get_field("B");
					const arma::Row<double> Jcn = 1e-6*calc_Je(Bn,Tn,true); // converted to [A/mm^2]
					vtk_data->set_nodedata(Jcn,"Eng. Cur. Dens. [A/mm^2]"); 

					const arma::Row<arma::uword> idx = arma::find(Jcn!=0).t();
					arma::Row<double> pctIc(num_nodes_,arma::fill::zeros);
					const arma::Mat<double> Jn = get_nodal_current_density();
					pctIc.cols(idx) = 100*cmn::Extra::vec_norm(1e-6*Jn.cols(idx))/Jcn.cols(idx);
					vtk_data->set_nodedata(pctIc,"Pct. Crit. Cur. Dens. [%]");
				}break;

				// critical temkperature and temperature margin
				case CRIT_TEMPERATURE:{
					lg->msg("critical temperature\n");
					const arma::Mat<double> Bn = get_field("B");
					const arma::Mat<double> Jn = get_nodal_current_density();
					const arma::Row<double> Tc = calc_Tc(Bn,Jn,true);
					vtk_data->set_nodedata(Tc,"Crit. Temperature [K]");
					
					lg->msg("temperature margin\n");
					const arma::Row<double> Tn = get_nodal_temperature();
					const arma::Row<double> Tm = Tc-Tn;
					vtk_data->set_nodedata(Tm,"Temperature Margin [K]");
				}break;

				// electric field and power density
				case ELECTRIC_FIELD:{
					lg->msg("electric field\n");
					const arma::Mat<double> Bn = get_field("B");
					const arma::Mat<double> Jn = get_nodal_current_density();
					const arma::Row<double> Tn = get_nodal_temperature();
					const arma::Mat<double> E = calc_E(Bn,Jn,Tn,true);
					vtk_data->set_nodedata(E,"Electric Field [V/m]");
										
					lg->msg("power density\n");
					const arma::Row<double> P = 1e-6*cmn::Extra::dot(E,Jn); // [W/cm^3] 
					vtk_data->set_nodedata(P,"Heating Power [W/cm^3]");
				}break;

				// Lorentz force density
				case FORCE_DENSITY:{
					lg->msg("force density\n");
					const arma::Mat<double> Bn = get_field("B");
					const arma::Mat<double> Jn = get_nodal_current_density();
					const arma::Mat<double> Fd = 1e-6*cmn::Extra::cross(Jn,Bn);
					vtk_data->set_nodedata(Fd,"Force Density [N/cm^3]");
				}break;

				// unrecognized output type
				default: break;
				// default: rat_throw_line("output type not recognized");
			}
		}

		// mesh done
		lg->msg(-2);

		// done exporting
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

	// // gmsh interface
	// // note that gmsh starts counting the nodes at one
	// void CalcSurface::export_gmsh(cmn::ShGmshFilePr gmsh) const{
	// 	// create element id
	// 	arma::Row<arma::uword> element_id(num_elements_);
	// 	for(arma::uword i=0;i<num_objects_;i++)
	// 		element_id.cols(idx_elem_(i),idx_elem_(i+1)-1).fill(i);

	// 	// mesh
	// 	gmsh->write_nodes(Rt_);
	// 	gmsh->write_elements(s_,element_id);

	// 	// calculated data
	// 	gmsh->write_nodedata(get_field("A"),"vector potential [Vs/m]");
	// 	gmsh->write_nodedata(get_field("H"),"mgn. field [A/m]");
	// 	gmsh->write_nodedata(get_field("B"),"mgn. fl. dens. [T]");

	// 	// field angle
	// 	//gmsh->write_nodedata(get_field_angle(),"mgn. fl. angle. [deg]");
	// }

	// export a vtk with times
	void CalcSurface::write(cmn::ShLogPr lg){
		// check if data directory set
		if(output_dir_.empty())return;

		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES:%s\n",KBLD,KGRN,KNRM);
		
		// check output directory
		if(output_times_.is_empty())rat_throw_line("output times are not set");

		// create output directory
		cmn::Extra::create_directory(output_dir_);

		// output filename
		std::string fname  = output_fname_;

		// report
		lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);
		
		// settings report
		display_settings(lg);

		// data report
		lg->msg(2, "%swriting meshdata%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);
		
		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKUnstrPr vtk_mesh = export_vtk();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_mesh->write(output_dir_ + fname + "_smsh");
		}

		// return
		lg->msg(-6,"\n"); 
	}

	// get type
	std::string CalcSurface::get_type(){
		return "mdl::calcsurface";
	}

	// method for serialization into json
	void CalcSurface::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		CalcFieldMap::serialize(js,list);

		// properties
		js["type"] = get_type();
		
		// subnodes
		js["target_model"] = cmn::Node::serialize_node(target_model_, list);
	}

	// method for deserialisation from json
	void CalcSurface::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// serialize fieldmap
		CalcFieldMap::deserialize(js,list,factory_list);

		// subnodes
		target_model_ = cmn::Node::deserialize_node<Model>(js["target_model"], list, factory_list);
	}

}}