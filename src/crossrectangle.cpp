/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "crossrectangle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CrossRectangle::CrossRectangle(){

	}

	// constructor with input
	CrossRectangle::CrossRectangle(
		const double n1, const double n2, 
		const double t1, const double t2, 
		const double dl){

		// set to self
		set_dim_normal(n1,n2,dl);
		set_dim_transverse(t1,t2,dl);
	}

	// constructor with input
	CrossRectangle::CrossRectangle(
		const double n1, const double n2, 
		const double t1, const double t2, 
		const arma::uword num_n, 
		const arma::uword num_t){

		// set to self
		set_dim_normal(n1,n2,num_n);
		set_dim_transverse(t1,t2,num_t);
	}

	// factory
	ShCrossRectanglePr CrossRectangle::create(){
		return std::make_shared<CrossRectangle>();
	}

	// factory with dimension input
	ShCrossRectanglePr CrossRectangle::create(
		const double n1, const double n2, 
		const double t1, const double t2, 
		const double dl){
		return std::make_shared<CrossRectangle>(n1,n2,t1,t2,dl);
	}

	// factory with dimension input
	ShCrossRectanglePr CrossRectangle::create(
		const double n1, const double n2, 
		const double t1, const double t2, 
		const arma::uword num_n, 
		const arma::uword num_t){
		return std::make_shared<CrossRectangle>(n1,n2,t1,t2,num_n,num_t);
	}

	// set dimensions in the normal direction
	void CrossRectangle::set_dim_normal(
		const double n1, const double n2, const arma::uword num_n){

		// check user input
		if(n1>=n2)rat_throw_line("inner must be smaller than outer dimension");
		if(num_n<=0)rat_throw_line("number of elements must be larger than zero");

		// set to self
		n1_ = n1; n2_ = n2; num_n_ = num_n;
	}

	// set dimensions in the normal direction
	void CrossRectangle::set_dim_normal(
		const double n1, const double n2, const double dl){

		// check element size
		if(n1>=n2)rat_throw_line("inner must be smaller than outer dimension");
		if(dl<=0)rat_throw_line("element size must be larger than zero");

		// calculate number of elements for each direction
		const arma::uword num_n = std::ceil((n2-n1)/dl);

		// set to self
		set_dim_normal(n1,n2,num_n);
	}

	// set dimensions in the transverse direction
	void CrossRectangle::set_dim_transverse(
		const double t1, const double t2, const arma::uword num_t){

		// check user input
		if(t1>=t2)rat_throw_line("inner must be smaller than outer dimension");
		if(num_t<=0)rat_throw_line("number of elements must be larger than zero");

		// set to self
		t1_ = t1; t2_ = t2; num_t_ = num_t;
	}

	// set dimensions in the transverse direction
	void CrossRectangle::set_dim_transverse(
		const double n1, const double n2, const double dl){

		// check element size
		if(n1>=n2)rat_throw_line("inner must be smaller than outer dimension");
		if(dl<=0)rat_throw_line("element size must be larger than zero");

		// calculate number of elements for each direction
		const arma::uword num_t = std::ceil((n2-n1)/dl);

		// set to self
		set_dim_transverse(n1,n2,num_t);
	}


	// volume mesh
	ShAreaPr CrossRectangle::create_area() const{
		// check input
		if(n1_>=n2_)rat_throw_line("inner must be smaller than outer dimension");
		if(t1_>=t2_)rat_throw_line("inner must be smaller than outer dimension");
		if(num_n_<=0)rat_throw_line("number of elements must be larger than zero");
		if(num_t_<=0)rat_throw_line("number of elements must be larger than zero");

		// allocate coordinates
		arma::Mat<double> u(num_t_+1, num_n_+1);
		arma::Mat<double> v(num_t_+1, num_n_+1);

		// set values
		u.each_row() = arma::linspace<arma::Row<double> >(n1_,n2_,num_n_+1);
		v.each_col() = arma::linspace<arma::Col<double> >(t1_,t2_,num_t_+1);

		// calculate number of nodes
		const arma::uword num_nodes = (num_t_+1)*(num_n_+1);

		// create node coordinates
		arma::Mat<double> Rn(2,num_nodes);
		Rn.row(0) = arma::reshape(u,1,num_nodes);
		Rn.row(1) = arma::reshape(v,1,num_nodes);

		// create matrix of node indices
		arma::Mat<arma::uword> node_idx = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes-1);
		node_idx.reshape(num_t_+1, num_n_+1);

		// number of elements
		const arma::uword num_elements = num_n_*num_t_;

		// get corner matrices
		const arma::Mat<arma::uword> M0 = node_idx.submat(0, 0, num_t_-1, num_n_-1);
		const arma::Mat<arma::uword> M1 = node_idx.submat(1, 0, num_t_, num_n_-1);
		const arma::Mat<arma::uword> M2 = node_idx.submat(1, 1, num_t_, num_n_);
		const arma::Mat<arma::uword> M3 = node_idx.submat(0, 1, num_t_-1, num_n_);

		// reshape into element matrix
		const arma::Mat<arma::uword> n = arma::join_vert(
			arma::reshape(M0, 1, num_elements),
			arma::reshape(M1, 1, num_elements),
			arma::reshape(M2, 1, num_elements),
			arma::reshape(M3, 1, num_elements));

		// check 
		assert(n.n_rows==4);

		// return positive
		return Area::create(Rn,n);
	}

	// surface mesh
	ShPerimeterPr CrossRectangle::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area();

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// get type
	std::string CrossRectangle::get_type(){
		return "mdl::crossrectangle";
	}

	// method for serialization into json
	void CrossRectangle::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["n1"] = n1_;	js["n2"] = n2_; 
		js["t1"] = t1_;	js["t2"] = t2_;
		js["num_n"] = (unsigned int)num_n_;	
		js["num_t"] = (unsigned int)num_t_;
	}

	// method for deserialisation from json
	void CrossRectangle::deserialize(
		const Json::Value &js, cmn::DSList &, 
		const cmn::NodeFactoryMap &){

		// decode the json object
		const double n1 = js["n1"].asDouble(); 
		const double n2 = js["n2"].asDouble();
		const double t1 = js["t1"].asDouble(); 
		const double t2 = js["t2"].asDouble();
		const arma::uword num_n = js["num_n"].asUInt64();
		const arma::uword num_t = js["num_t"].asUInt64();

		// use set functions
		set_dim_normal(n1, n2, num_n);
		set_dim_transverse(t1, t2, num_t);
	}

}}
