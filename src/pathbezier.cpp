/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathbezier.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathBezier::PathBezier(){

	}

	// constructor
	PathBezier::PathBezier(
		const arma::Col<double>::fixed<3> pstart, 
		const arma::Col<double>::fixed<3> lstart, 
		const arma::Col<double>::fixed<3> nstart,
		const arma::Col<double>::fixed<3> pend, 
		const arma::Col<double>::fixed<3> lend, 
		const arma::Col<double>::fixed<3> nend,
		const double str1, const double str2, 
		const double str3, const double str4,
		const double ell_trans, const double element_size, 
		const double path_offset){
		
		// start point
		pstart_ = pstart; lstart_ = lstart; nstart_ = nstart;

		// end point
		pend_ = pend; lend_ = lend; nend_ = nend;

		// set strengths
		str1_ = str1; str2_ = str2; 
		str3_ = str3; str4_ = str4;

		// transition length
		ell_trans_ = ell_trans;

		// element size
		element_size_ = element_size;
		path_offset_ = path_offset;
	}

	// factory
	ShPathBezierPr PathBezier::create(){
		return std::make_shared<PathBezier>();
	}

	// factory
	ShPathBezierPr PathBezier::create(
		const arma::Col<double>::fixed<3> pstart, 
		const arma::Col<double>::fixed<3> lstart, 
		const arma::Col<double>::fixed<3> nstart,
		const arma::Col<double>::fixed<3> pend, 
		const arma::Col<double>::fixed<3> lend, 
		const arma::Col<double>::fixed<3> nend,
		const double str1, const double str2, 
		const double str3, const double str4,
		const double ell_trans, const double element_size,
		const double path_offset){
		return std::make_shared<PathBezier>(pstart,lstart,nstart,
			pend,lend,nend,str1,str2,str3,str4,ell_trans,element_size,path_offset);
	}

	// set planar winding 
	void PathBezier::set_planar_winding(const bool planar_winding){
		planar_winding_ = planar_winding;
	}

	// set planar winding 
	void PathBezier::set_analytic_frame(const bool analytic_frame){
		analytic_frame_ = analytic_frame;
	}

	// set number of sections
	void PathBezier::set_num_sections(const arma::uword num_sections){
		if(num_sections<1)rat_throw_line("number of sections must be larger than one");
		num_sections_ = num_sections;
	}

	// get frame
	ShFramePr PathBezier::create_frame() const{
		// create control points for quintic spline
		// arma::Mat<double> P(3,6);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		// P.col(3) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - nend_*str4_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(5) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		arma::Mat<double> P(3,8);
		P.col(0) = pstart_ + lstart_*ell_trans_;
		P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - nstart_*str3_;
		P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - 2*nstart_*str3_;
		P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - 2*nend_*str4_;
		P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_) - nend_*str4_;
		P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		P.col(7) = pend_ - lend_*ell_trans_;

		// control points for septic spline
		// arma::Mat<double> P(3,8);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_);
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - nstart_*str3_;
		// P.col(4) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - nend_*str4_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+1*str2_);
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(7) = pend_ - lend_*ell_trans_;

		// control points for nonantic spline
		// arma::Mat<double> P(3,10);
		// P.col(0) = pstart_ + lstart_*ell_trans_;
		// P.col(1) = pstart_ + lstart_*ell_trans_ + lstart_*str1_;
		// P.col(2) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+str2_) - (1.0/5)*nstart_*str3_;
		// P.col(3) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+2*str2_) - (3.0/5)*nstart_*str3_;
		// P.col(4) = pstart_ + lstart_*ell_trans_ + lstart_*(str1_+3*str2_) - 1.0*nstart_*str3_;
		// P.col(5) = pend_ - lend_*ell_trans_ - lend_*(str1_+3*str2_) - 1.0*nend_*str4_;
		// P.col(6) = pend_ - lend_*ell_trans_ - lend_*(str1_+2*str2_) - (3.0/5)*nend_*str4_;
		// P.col(7) = pend_ - lend_*ell_trans_ - lend_*(str1_+str2_) - (1.0/5)*nend_*str4_;
		// P.col(8) = pend_ - lend_*ell_trans_ - lend_*str1_;
		// P.col(9) = pend_ - lend_*ell_trans_ ;

		// calculate start and end frame
		const arma::Col<double>::fixed<3> dstart = cmn::Extra::cross(nstart_,lstart_);
		const arma::Col<double>::fixed<3> dend = cmn::Extra::cross(nend_,lend_);

		// create times
		const arma::Row<double> t = regular_times(P);

		// number of times
		const arma::uword num_times = t.n_elem;

		// create spline and its derivatives
		//arma::Mat<double> R1,V1,A1; create_bezier(R1,V1,A1,t,P);
		const arma::field<arma::Mat<double> > C = create_bezier_tn(t,P,4);
		
		// get coordinates
		arma::Mat<double> R = C(0); 

		// get derivatives
		const arma::Mat<double> V = C(1); // velocity
		const arma::Mat<double> A = C(2); // acceleration
		const arma::Mat<double> J = C(3); // jerk

		// create darboux frame
		Darboux db(V,A,J);
		db.setup(analytic_frame_); db.correct_sign(dstart);
		arma::Mat<double> D = db.get_transverse();
		arma::Mat<double> L = db.get_longitudinal();
		// arma::Mat<double> N = db.get_normal();

		// check if transition region needed
		if(ell_trans_>0){
			// number of transition elements
			const arma::uword num_trans = std::max(2,(int)std::ceil(1.5*ell_trans_/element_size_));

			// allocate transition
			arma::Mat<double> R1trans(3,num_trans);
			arma::Mat<double> R2trans(3,num_trans);
			arma::Mat<double> D1trans(3,num_trans);
			arma::Mat<double> D2trans(3,num_trans);
			arma::Mat<double> L1trans(3,num_trans);
			arma::Mat<double> L2trans(3,num_trans);

			// times for transition
			arma::Row<double> ttrans = arma::linspace<arma::Row<double> >(0,1.0,num_trans);

			// calculate transition
			for(arma::uword i=0;i<num_trans;i++){
				R1trans.col(i) = (1.0-ttrans(i))*pstart_ + ttrans(i)*R.col(0);
				R2trans.col(i) = (1.0-ttrans(i))*R.col(num_times-1) + ttrans(i)*pend_;
				D1trans.col(i) = (1.0-ttrans(i))*dstart + ttrans(i)*D.col(0);
				D2trans.col(i) = (1.0-ttrans(i))*D.col(num_times-1) + ttrans(i)*dend;
			}
			L1trans.each_col() = lstart_;
			L2trans.each_col() = lend_;

			// atach transition
			R = arma::join_horiz(arma::join_horiz(
				R1trans.head_cols(num_trans-1),R),
				R2trans.tail_cols(num_trans-1));
			D = arma::join_horiz(arma::join_horiz(
				D1trans.head_cols(num_trans-1),D),
				D2trans.tail_cols(num_trans-1));
			L = arma::join_horiz(arma::join_horiz(
				L1trans.head_cols(num_trans-1),L),
				L2trans.tail_cols(num_trans-1));
		}

		// Calculate normal vector
		arma::Mat<double> N = cmn::Extra::cross(L,D);
		N.each_row()/=cmn::Extra::vec_norm(N);

		// allocate block direction vectors
		arma::Mat<double> B;

		// in case of planar windings
		if(planar_winding_){
			// get out of plane vector
			arma::Col<double>::fixed<3> plane_vec = cmn::Extra::cross(lstart_,nstart_);
			assert(std::abs(arma::as_scalar(cmn::Extra::dot(plane_vec,lend_))-1.0)>1e-6);

			// calculate block vector
			B = cmn::Extra::cross(arma::Mat<double>(3,L.n_cols,arma::fill::ones).each_col()%plane_vec,L);

			B = (B+N)/2;
		}

		// non-planar winding
		else{
			B = N;
		}

		// check sizes
		assert(R.n_cols==L.n_cols); assert(R.n_cols==N.n_cols);
		assert(R.n_cols==D.n_cols); assert(R.n_cols==B.n_cols);

		// create field arrays
		// arma::field<arma::Mat<double> > 
		// 	Rfld(1,num_sections_), Lfld(1,num_sections_), 
		// 	Nfld(1,num_sections_), Dfld(1,num_sections_), 
		// 	Bfld(1,num_sections_);

		// // create indexes at which we want to split the sections
		// arma::Row<arma::uword> idx_divide = 
		// 	arma::regspace<arma::Row<arma::uword> >(0,num_sections_)*
		// 	std::floor(num_times/num_sections_);
		// idx_divide(num_sections_) = num_times-1;

		// // split sections
		// for(arma::uword i=0;i<num_sections_;i++){
		// 	const arma::uword idx1 = idx_divide(i);
		// 	const arma::uword idx2 = idx_divide(i+1);
		// 	Rfld(i) = R.cols(idx1,idx2); Lfld(i) = L.cols(idx1,idx2);
		// 	Nfld(i) = N.cols(idx1,idx2); Dfld(i) = D.cols(idx1,idx2);
		// 	Bfld(i) = B.cols(idx1,idx2);
		// }

		// subdivide
		//ShFramePr frame = Frame::create(Rfld,Lfld,Nfld,Dfld,Bfld);

		// create frame and return 
		ShFramePr frame = Frame::create(R,L,N,D,B,num_sections_);

		// return the frame
		return frame;
	}

	// regular time function
	arma::Row<double> PathBezier::regular_times(
		const arma::Mat<double> &P) const{

		// calculate line with regular spaced times
		const arma::Row<double> treg = arma::linspace<arma::Row<double> >(0,1.0,num_reg_);

		// create spline
		//arma::Mat<double> R,V,A;
		//create_bezier(R,V,A,treg,P);
		const arma::field<arma::Mat<double> > C = create_bezier_tn(treg,P,3);
		const arma::Mat<double> R = C(0);
		const arma::Mat<double> V = C(1);
		arma::Mat<double> A = C(2);

		// acceleration fix/cheat (zero acceleration gives weird results)
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<1e-9).t();
		if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);

		// calculate radius of curvature from velocity and acceleration
		const arma::Row<double> radius = cmn::Extra::vec_norm(V)%cmn::Extra::vec_norm(V)/cmn::Extra::vec_norm(A);

		// calculate scaling factor based on radius
		// the clamp here can be debated
		const arma::Row<double> factor = (radius.head_cols(num_reg_-1) + path_offset_)/radius.head_cols(num_reg_-1);

		// calculate length
		const arma::Row<double> ell = factor%arma::sqrt(arma::sum(arma::pow(arma::diff(R,1,1),2),0));

		// calculate number of times required
		const arma::uword num_times = std::max(2,(int)(std::ceil(arma::sum(ell)/element_size_)));

		// calculate line with regular spaced times
		const arma::Row<double> treq = arma::linspace<arma::Row<double> >(0,1.0,num_times);

		// calculate accumalation of length
		arma::Row<double> rp(num_reg_,arma::fill::zeros);
		rp.tail_cols(num_reg_-1) = arma::cumsum(ell)/arma::sum(ell);

		// allocate output
		arma::Col<double> tset;

		// use interpolation
		arma::interp1(rp.t(),treg.t(),treq.t(),tset,"linear");

		// fix first and last point
		tset(0) = 1e-9; tset.tail(1) = 1.0-1e-9;

		// check if sorted
		assert(tset.is_sorted("ascend"));

		// return time
		return tset.t();
	}


	// bezier function
	// general version
	void PathBezier::create_bezier(
		arma::Mat<double> &R,
		arma::Mat<double> &V,
		arma::Mat<double> &A,
		const arma::Row<double> &t, 
		const arma::Mat<double> &P){

		// get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword nmax = P.n_cols-1; // the order of the spline

		// allocate to zeros
		R.zeros(num_dim, num_times);
		V.zeros(num_dim, num_times);
		A.zeros(num_dim, num_times);

		// create factorial list
		const arma::Col<double> facs = fmm::Extra::factorial(nmax);

		// walk over source points
		for(arma::sword i=0;i<=(arma::sword)nmax;i++){
			// calculate binomial coefficient
			double nk = facs(nmax)/(facs(i)*facs(nmax-i));

			// walk over dimensions
			for(arma::uword j=0;j<num_dim;j++){
				// calculate coordinates
				R.row(j) += nk*P(j,i)*arma::pow(1.0-t,nmax-i)%arma::pow(t,i);

				// calculate velocity (first derivative)
				V.row(j) += nk*P(j,i)*(nmax*t-i)%arma::pow(1.0-t,nmax-i)%arma::pow(t,i-1)/(t-1.0);

				// calculate acceleration (second derivative)
				A.row(j) += nk*P(j,i)*((-1+i)*i*arma::pow(1.0-t,-i+nmax)%arma::pow(t,-2+i) - 
					2*i*(-i+nmax)*arma::pow(1.0-t,-1-i+nmax)%arma::pow(t,-1+i) + 
					(-1-i+nmax)*(-i+nmax)*arma::pow(1.0-t,-2-i+nmax)%arma::pow(t,i));
			}
		}

		// acceleration fix/cheat (zero acceleration gives weird results)
		const arma::Row<arma::uword> idx = arma::find(cmn::Extra::vec_norm(A)<1e-9).t();
		if(!idx.is_empty())A.cols(idx) = A.cols(idx-1);
	}

	// bezier function with casteljau's algorithm
	// https://pages.mtu.edu/~shene/COURSES/cs3621/NOTES/spline/Bezier/bezier-der.html
	arma::field<arma::Mat<double> > PathBezier::create_bezier_tn(
		const arma::Row<double> &t, const arma::Mat<double> &P, const arma::uword depth){

		 // get dimensions
		const arma::uword num_dim = P.n_rows;
		const arma::uword num_times = t.n_elem;
		const arma::uword n = P.n_cols-1; // the order of the spline

	    // create factorial list
		const arma::Col<double> facs = fmm::Extra::factorial(n);

		// allocate
		arma::field<arma::Mat<double> > C(depth);

		// store p for recursive calculation
		arma::Mat<double> D = P;

		// keep track of prefac
		double prefac = 1.0;

		// calculate coordinates
		for(arma::uword k=0;k<depth;k++){
			// allocate spline
			C(k).zeros(3,num_times);

			// walk over n
			for(arma::uword i=0;i<=n-k;i++){
				// calculate binomial coefficient
				const double nk = facs(n-k)/(facs(i)*facs(n-k-i));

				// calculate bezier function
				const arma::Row<double> B = nk*arma::pow(1.0-t,n-k-i)%arma::pow(t,i);

				// walk over dimensions
				for(arma::uword j=0;j<num_dim;j++){
					// calculate coordinate or derivatives thereof
					C(k).row(j) += prefac*D(j,i)*B;
				}
			}
			
			// casteljau's algorithm	
			D = arma::diff(D,1,1);

			// update prefactor
			prefac *= (n-k);
		}

		// return the spline and its derivatives
		return C;
	}

}}