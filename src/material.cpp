/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default critical current is zero (not superconducting)
	arma::Row<double> Material::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &/*T*/, const arma::Row<double> &/*alpha*/) const{

		// create zero matrix and return
		return arma::Row<double>(Bm.n_elem,arma::fill::zeros);
	}

	// default resistivity
	arma::Row<double> Material::calc_conductivity(const arma::Row<double> &/*Bm*/, const arma::Row<double> &T) const{
		arma::Row<double> sigma(T.n_elem); sigma.fill(0);
		return sigma;
	}

	// current density without pre-calculating sigma and jc
	arma::Row<double> Material::calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		// perform pre-calculation
		const arma::Mat<double> props = calc_properties(Bm,T,alpha);

		// call normally
		return arma::sign(E)%calc_current_density_fast(arma::abs(E),props);
	}

	// electric field without pre-calculating sigma and jc
	arma::Row<double> Material::calc_electric_field(
		const arma::Row<double> &J, const arma::Row<double> &Bm, 
		const arma::Row<double> &T, const arma::Row<double> &alpha) const{

		// perform pre-calculation
		const arma::Mat<double> props = calc_properties(Bm,T,alpha);
		
		// call normally
		return arma::sign(J)%calc_electric_field_fast(arma::abs(J),props);
	}

	// get factions
	arma::Col<double> Material::get_fraction()const{
		return arma::Col<double>::fixed<1>{1.0};
	}

	// default critical current is zero (not superconducting)
	arma::Row<double> Material::calc_critical_temperature(
		const arma::Row<double> &J, const arma::Row<double> &Bm, 
		const arma::Row<double> &alpha, const ShAreaPr area) const{
		
		// settings
		const double Tinc = 10;
		const double tol = 0.1;

		// allocate critical temperature
		arma::Row<double> Tc_upper = arma::Row<double>(Bm.n_rows,Bm.n_cols,arma::fill::ones)*Tinc;

		// iterations
		for(;;){
			// calculate critical current in [A/m^2]
			const arma::Row<double> Jc = calc_critical_current_density(Bm,Tc_upper,alpha,area);

			// find indexes of elements below Jc
			const arma::Row<arma::uword> idx = arma::find(Jc>J).t();
			
			// check if any left
			if(idx.is_empty())break;

			// increase critical temperature
			Tc_upper(idx)+=Tinc;
		}

		// remove last increment
		arma::Row<double> Tc_lower = Tc_upper - Tinc;

		// bisection algorithm
		for(;;){
			// calculate average
			const arma::Row<double> Tc_mid = (Tc_upper + Tc_lower)/2;

			// calculate critical current in [A/m^2]
			const arma::Row<double> Jc = calc_critical_current_density(Bm,Tc_mid,alpha,area);

			// update temperatures
			const arma::Row<arma::uword> idx1 = arma::find(Jc<J).t();
			Tc_upper(idx1) = Tc_mid(idx1);
			const arma::Row<arma::uword> idx2 = arma::find(Jc>=J).t();
			Tc_lower(idx2) = Tc_mid(idx2);

			// stop condiction
			if(arma::all(arma::abs(Tc_lower - Tc_upper)<tol))break;
		}

		// check output
		assert(arma::all(Tc_upper>0));

		// return critical temp
		return Tc_upper;
	}


	// calculate critical current averaged over the cross-section of the cable
	arma::Row<double> Material::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, 
		const arma::Row<double> &alpha, const ShAreaPr area) const{

		if(area==NULL)return calc_critical_current_density(Bm,T,alpha);

		// calculate critical current normally
		const arma::Row<double> Jen = calc_critical_current_density(Bm,T,alpha);

		// get counters
		const arma::uword num_nodes_total = Jen.n_elem;
		const arma::uword num_nodes = area->get_num_nodes();
		const arma::uword num_elements = area->get_num_elements();
		assert(num_nodes_total%num_nodes==0);
		const arma::uword num_sections = num_nodes_total/num_nodes; // number of cross sections

		// calculate areas
		const arma::Row<double> A = area->calculate_areas();
		const double Atot = arma::accu(A);
		assert(std::abs(arma::accu(A)-Atot)<1e-9);

		// get elements
		const arma::Mat<arma::uword> n = area->get_elements();
		assert(n.n_rows==4 || n.n_rows==2); assert(n.n_cols==num_elements);

		// allocate
		arma::Row<double> Jeav(num_nodes_total);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// extract section J at nodes
			const arma::Row<double> Jnsection = Jen.cols(i*num_nodes, (i+1)*num_nodes-1);

			// calculate J at elements
			arma::Row<double> Jee(num_elements);
			for(arma::uword j=0;j<num_elements;j++)
				Jee.col(j) = arma::mean(Jnsection.cols(n.col(j)),1);

			// calculate average current density
			const double Jav = arma::accu(A%Jee)/Atot;

			// set to output
			Jeav.cols(i*num_nodes, (i+1)*num_nodes-1).fill(Jav);
		}

		// return averaged engineering current density
		return Jeav;
	}

	// electric field with area
	arma::Row<double> Material::calc_electric_field(
		const arma::Row<double> &J, const arma::Row<double> &Bm, 
		const arma::Row<double> &T, const arma::Row<double> &alpha, 
		const ShAreaPr area) const{

		// get counters
		const arma::uword num_nodes_total = J.n_elem;
		const arma::uword num_nodes = area->get_num_nodes();
		const arma::uword num_elements = area->get_num_elements();
		assert(num_nodes_total%num_nodes==0);
		const arma::uword num_sections = num_nodes_total/num_nodes; // number of cross sections

		// get elements
		const arma::Mat<arma::uword> n = area->get_elements();

		// calculate areas
		const arma::Row<double> A = area->calculate_areas();
		const double Atot = arma::accu(A);

		// calculate needed properties at nodes
		const arma::Mat<double> props = calc_properties(Bm,T,alpha);

		// allocate for cross sections
		arma::Mat<double> props_section(props.n_rows,num_sections,arma::fill::zeros);
		arma::Row<double> current_density_section(num_sections,arma::fill::zeros);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// extract section J at nodes
			const arma::Mat<double> props_section_nodes = props.cols(i*num_nodes, (i+1)*num_nodes-1);
			const arma::Row<double> current_density_section_nodes = J.cols(i*num_nodes, (i+1)*num_nodes-1);

			// walk over elements in section
			for(arma::uword j=0;j<num_elements;j++){
				props_section.col(i) += (A(j)/Atot)*arma::mean(props_section_nodes.cols(n.col(j)),1);
				current_density_section.col(i) += (A(j)/Atot)*arma::mean(current_density_section_nodes.cols(n.col(j)),1);
			}
		}

		// calculate electric field in each section
		const arma::Row<double> electric_field_section = calc_electric_field_fast(current_density_section, props_section);

		// send to nodes
		arma::Row<double> En(num_nodes_total);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++)
			En.cols(i*num_nodes, (i+1)*num_nodes-1).fill(electric_field_section(i));

		// return electric field at nodes
		return En;
	}

	// electric field with area
	arma::Row<double> Material::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props, const ShAreaPr area) const{

		// get counters
		const arma::uword num_nodes_total = J.n_elem;
		const arma::uword num_nodes = area->get_num_nodes();
		const arma::uword num_elements = area->get_num_elements();
		const arma::uword num_sections = num_nodes_total/num_nodes; // number of cross sections

		// get elements
		const arma::Mat<arma::uword> n = area->get_elements();

		// calculate areas
		const arma::Row<double> A = area->calculate_areas();
		const double Atot = arma::accu(A);

		// allocate for cross sections
		arma::Mat<double> props_section(props.n_rows,num_sections,arma::fill::zeros);
		arma::Row<double> current_density_section(num_sections,arma::fill::zeros);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// extract section J at nodes
			const arma::Mat<double> props_section_nodes = props.cols(i*num_nodes, (i+1)*num_nodes-1);
			const arma::Row<double> current_density_section_nodes = J.cols(i*num_nodes, (i+1)*num_nodes-1);

			// walk over elements in section
			for(arma::uword j=0;j<num_elements;j++){
				// props_section.col(i) += arma::sum(A(j)*props_section_nodes.cols(n.col(j)),1)/Atot;
				props_section.col(i) += A(j)*arma::mean(props_section_nodes.cols(n.col(j)),1)/Atot;
				// current_density_section(i) += arma::accu(A(j)*J(n.col(j)))/Atot;
				current_density_section(i) += A(j)*arma::as_scalar(arma::mean(current_density_section_nodes.cols(n.col(j)),1))/Atot;
			}
		}

		// calculate electric field in each section
		const arma::Row<double> electric_field_section = calc_electric_field_fast(current_density_section, props_section);

		// send to nodes
		arma::Row<double> En(num_nodes_total);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++)
			En.cols(i*num_nodes, (i+1)*num_nodes-1).fill(electric_field_section(i));

		// return electric field at nodes
		return En;
	}

	// calculate heat capacity with doubles
	double Material::calc_specific_heat(const double T) const{
		arma::Row<double>::fixed<1> Tmat = {T};
		const arma::Row<double>::fixed<1> Cp = calc_specific_heat(Tmat);
		return arma::as_scalar(Cp);
	}

	// electric field calculation with scalar input
	double Material::calc_electric_field(const double J, const double Bm, const double T, const double alpha) const{
		// convert to matrices
		arma::Row<double>::fixed<1> Jmat = {J};
		arma::Row<double>::fixed<1> Bmat = {Bm};
		arma::Row<double>::fixed<1> Tmat = {T};
		arma::Row<double>::fixed<1> amat = {alpha};

		// calculate and return value
		return arma::as_scalar(calc_electric_field(Jmat,Bmat,Tmat,amat));
	}

	// export vtk table
	ShVTKTablePr Material::export_vtk(const double Tmin, const double Tmax, const double Jfix) const{
		// create table
		ShVTKTablePr vtk = VTKTable::create();

		// create temperature and field arrays
		arma::Row<double> T = arma::regspace<arma::Row<double> >(Tmin,Tmax);
		arma::Row<double> Bm = arma::Row<double>(T.n_elem,arma::fill::ones)*5;
		arma::Row<double> alpha = arma::Row<double>(T.n_elem,arma::fill::zeros);
		arma::Row<double> J = arma::Row<double>(T.n_elem,arma::fill::ones)*Jfix;

		// set table size
		vtk->set_num_rows(T.n_elem);

		// add data
		vtk->set_data(T.t(),"temperature [K]");
		vtk->set_data(1.0/calc_conductivity(Bm,T).t(),"resistivity [Ohm m]");
		vtk->set_data(calc_thermal_conductivity(Bm,T).t(),"thermal cond. [W m^-1 K^-1]");
		vtk->set_data(calc_specific_heat(T).t(),"specific heat [J m^-3 K^-1]");
		vtk->set_data(calc_critical_current_density(Bm,T,alpha).t(),"eng. current density [A/m^2]");
		vtk->set_data(calc_electric_field(J,Bm,T,alpha).t(),"electric field [V/m]");

		// return table
		return vtk;
	}
}}