/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcFieldMap::CalcFieldMap(){
		// create the default drive
		add_drive(DriveDC::create(1.0,0));
	}

	// set number of gauss points
	void CalcFieldMap::set_num_gauss(const arma::sword num_gauss){
		num_gauss_ = num_gauss;
	}

	// add drive
	void CalcFieldMap::add_drive(ShDrivePr drive){
		drives_[drive->get_id()] = drive;
	}

	// set model
	void CalcFieldMap::set_source_model(ShModelPr source_model){
		if(source_model==NULL)rat_throw_line("source model points to NULL");
		source_model_ = source_model;
	}

	// set field type
	void CalcFieldMap::set_field_type(
		const std::string &field_type, const arma::uword num_dim){
		set_field_type(field_type,arma::Row<arma::uword>{num_dim});
	}

	// set field type and number of dimensions
	void CalcFieldMap::set_field_type(
		const std::string &field_type, 
		const arma::Row<arma::uword> &num_dim){

		// check if all values set
		if(field_type.length()!=num_dim.n_elem)rat_throw_line(
			"field type string must equal length of dimensions vector");

		// set field type
		field_type_ = field_type;
		num_dim_ = num_dim;
	}

	// setting the volume elements
	// void CalcFieldMap::set_use_volume_elements(const bool use_volume_elements){
	// 	use_volume_elements_ = use_volume_elements;
	// }

	// add background
	void CalcFieldMap::add_background(ShBackgroundPr bg){
		// check input
		if(bg==NULL)rat_throw_line("model points to zero");

		// get number of models
		const arma::uword num_bg = bg_.n_elem;

		// allocate new source list
		ShBackgroundPrList new_bg(num_bg + 1);

		// set old and new sources
		for(arma::uword i=0;i<num_bg;i++)new_bg(i) = bg_(i);
		new_bg(num_bg) = bg;
		
		// set new source list
		bg_ = new_bg;
	}

	// set settings for the MLFMM
	void CalcFieldMap::set_fmm_settings(fmm::ShSettingsPr stngs){
		if(stngs==NULL)rat_throw_line("fmm settings point to NULL");
		fmm_stngs_ = stngs;
	}

	// access the settings for the multipole method
	fmm::ShSettingsPr CalcFieldMap::get_fmm_settings(){
		return fmm_stngs_;
	}

	// calculate the fieldmap
	void CalcFieldMap::calculate(cmn::ShLogPr lg){
		// header
		lg->msg("%s%s--- CALCULATING FIELD ---%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sGEOMETRY SETUP:%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%s%sSOURCE MESHES%s\n",KBLD,KGRN,KNRM);
		
		// check input
		if(source_model_==NULL)rat_throw_line("Source model is not set");

		// create meshes
		ShMeshPrList meshes = source_model_->create_mesh();

		// display meshes
		Mesh::display(lg, meshes);

		// number of meshes
		const arma::uword num_meshes = meshes.n_elem;

		// geometry setup done
		lg->msg(-2,"\n");

		// setup targets
		setup(lg);

		// geometry setup done
		lg->msg(-2);

		// walk over meshes and extract drive
		arma::Row<arma::uword> drive_id_list(num_meshes);
		for(arma::uword i=0;i<num_meshes;i++)
			drive_id_list(i) = meshes(i)->get_drive_id();

		// get unique drives
		drive_id_ = arma::unique(drive_id_list);

		// get number of drives
		const arma::uword num_drives = drive_id_.n_elem;

		// allocate targets
		tar_.set_size(num_drives);

		// walk over drive groups
		for(arma::uword i=0;i<num_drives;i++){
			// find coils with the given drive index
			const arma::Row<arma::uword> mesh_indexes = 
				arma::find(drive_id_list==drive_id_(i)).t();
			assert(!mesh_indexes.is_empty());

			// number of meshes
			const arma::uword num_meshes_drive = mesh_indexes.n_elem;

			// // allocate sources
			// fmm::ShSourcesPr src;

			// // line elements
			// if(!use_volume_elements_){
			// 	// extract sources from the relevant meshes
			// 	fmm::ShCurrentSourcesPrList srcs(num_meshes_drive);
			// 	for(arma::uword j=0;j<num_meshes_drive;j++)
			// 		srcs(j) = meshes(mesh_indexes(j))->create_current_sources();

			// 	// combine sources
			// 	src = fmm::CurrentSources::create(srcs);
			// }

			// else{
			// 	// extract sources from the relevant meshes
			// 	fmm::ShCurrentMeshPrList srcs(num_meshes_drive);
			// 	for(arma::uword j=0;j<num_meshes_drive;j++)
			// 		srcs(j) = meshes(mesh_indexes(j))->create_current_mesh();

			// 	// combine sources
			// 	src = fmm::CurrentMesh::create(srcs);
			// }

			// create source objects
			arma::field<fmm::ShSourcesPrList> srcs_fld(num_meshes_drive);
			for(arma::uword j=0;j<num_meshes_drive;j++)
				srcs_fld(j) = meshes(mesh_indexes(j))->create_sources(num_gauss_, num_gauss_surface_);

			// merge sources
			fmm::ShSourcesPrList srcs = Extra::field2mat(srcs_fld);

			// walk over source objects and extract and 
			// combine sources into a multisource object
			fmm::ShMultiSourcesPr multi_sources = fmm::MultiSources::create();

			// merge line elements
			fmm::ShCurrentSourcesPrList csrcs = Extra::filter<fmm::CurrentSources>(srcs);
			if(csrcs.n_elem==1)multi_sources->add_sources(csrcs(0));
			else if(csrcs.n_elem>1)multi_sources->add_sources(fmm::CurrentSources::create(csrcs));

			// merge current meshes
			fmm::ShCurrentMeshPrList cmsrcs = Extra::filter<fmm::CurrentMesh>(srcs);
			if(cmsrcs.n_elem==1)multi_sources->add_sources(cmsrcs(0));
			else if(cmsrcs.n_elem>1)multi_sources->add_sources(fmm::CurrentMesh::create(cmsrcs));

			// merge current meshes
			fmm::ShCurrentSurfacePrList ssrcs = Extra::filter<fmm::CurrentSurface>(srcs);
			if(ssrcs.n_elem==1)multi_sources->add_sources(ssrcs(0));
			else if(ssrcs.n_elem>1)multi_sources->add_sources(fmm::CurrentSurface::create(ssrcs));

			// merge interp meshes
			fmm::ShInterpPrList ipsrcs = Extra::filter<fmm::Interp>(srcs);
			if(ipsrcs.n_elem==1)multi_sources->add_sources(ipsrcs(0));
			else if(ipsrcs.n_elem>1)multi_sources->add_sources(fmm::Interp::create(ipsrcs));

			// setup the multisources
			multi_sources->setup();

			// create targets
			tar_(i) = fmm::MgnTargets::create(Rt_);
			tar_(i)->set_field_type(field_type_, num_dim_);

			// calculate field
			fmm::ShMlfmmPr mlfmm = fmm::Mlfmm::create(fmm_stngs_);

			// add sources and targets
			mlfmm->set_sources(multi_sources);
			mlfmm->set_targets(tar_(i));

			// run mlfmmm
			lg->msg(2,"%s%sSOURCE DRIVE %llu (with %llu source type(s)):%s\n",
				KBLD,KGRN,drive_id_(i),multi_sources->get_num_source_objects(),KNRM);
			// lg->msg(2,"%s%sSOURCE DRIVE %llu (with %llu source type(s)):%s\n",
			// 	KBLD,KGRN,drive_id_(i),1,KNRM);
			mlfmm->setup(lg);
			mlfmm->calculate(lg);
			lg->msg(-2);
		}
	}

	// set the time for field calculation
	void CalcFieldMap::set_time(const double time){
		time_ = time;
	}

	// get present time
	double CalcFieldMap::get_time() const{
		return time_;
	}

	// getting field
	bool CalcFieldMap::has_field() const{
		return !tar_.is_empty();
	}

	// check what field types requested
	bool CalcFieldMap::has(const std::string &type) const{
		// find location of this field
		const std::size_t n = field_type_.find(type);
		
		// check and return
		return n!=std::string::npos;
	}

	// get field from map at specified time
	arma::Mat<double> CalcFieldMap::get_field(const std::string &type, const double time) const{
		// allocate
		arma::Mat<double> M(3,Rt_.n_cols,arma::fill::zeros);

		// walk over drives
		for(arma::uword i=0;i<drive_id_.n_elem;i++){
			// find drive with corresponding index
			std::map<arma::uword,ShDrivePr>::const_iterator drive_it = drives_.find(drive_id_(i));
			
			// check if drive found
			if(drive_it==drives_.end())rat_throw_line("drive with index " 
				+ std::to_string(drive_id_(i)) + " does not exist");
			
			// add field for this drive
			M += drive_it->second->get_scaling(time)*tar_(i)->get_field(type);
		}

		// walk over background fields and add there contributions
		for(arma::uword i=0;i<bg_.n_elem;i++){
			// calculate field using background
			fmm::ShTargetsPr bg_tar = fmm::MgnTargets::create(Rt_); 
			bg_tar->set_field_type(field_type_, num_dim_);
			bg_tar->setup_targets(); bg_(i)->calc_field(bg_tar);

			// add to field
			M += drives_.at(bg_(i)->get_drive_id())->get_scaling(time)*bg_tar->get_field(type);
		}

		// return data
		return M;
	}

	// get field without time
	arma::Mat<double> CalcFieldMap::get_field(const std::string &type) const{
		return get_field(type,time_);
	}

	// indexed output filename
	std::string CalcFieldMap::indexed_output_fname(const std::string &fname, const arma::uword idx){
		std::ostringstream out;
		out << fname << "_" << std::internal << 
			std::setfill('0') << std::setw(4) << idx;
		return out.str();
	}

	// get type
	std::string CalcFieldMap::get_type(){
		return "mdl::fieldmap";
	}

	// method for serialization into json
	void CalcFieldMap::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Calc::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["field_type"] = field_type_;
		for(arma::uword i=0;i<num_dim_.n_elem;i++)
			js["num_dim"].append((unsigned int)num_dim_(i));
		// js["use_volume_elements"] = use_volume_elements_;
		
		// settings etc
		js["fmm_stngs"] = cmn::Node::serialize_node(fmm_stngs_, list);
		// js["mdl_stngs"] = cmn::Node::serialize_node(mdl_stngs_, list);
		js["source_model"] = cmn::Node::serialize_node(source_model_, list);

		// list of background contributions
		for(arma::uword i=0;i<bg_.n_elem;i++)
			js["bg"].append(cmn::Node::serialize_node(bg_(i), list));

		// list of drives
		for(std::map<arma::uword, ShDrivePr>::const_iterator it = drives_.begin();it!=drives_.end();it++)
			js["drives"].append(cmn::Node::serialize_node(it->second, list));
	}

	// method for deserialisation from json
	void CalcFieldMap::deserialize(
		const Json::Value &js, cmn::DSList &list, 
		const cmn::NodeFactoryMap &factory_list){
		// parent
		Calc::deserialize(js,list,factory_list);

		// properties
		field_type_ = js["field_type"].asString();
		arma::uword num_fields = js["num_dim"].size();
		num_dim_.set_size(num_fields);
		for(arma::uword i=0;i<num_fields;i++)
			num_dim_(i) = js["num_dim"].get(i,0).asUInt64();
		// use_volume_elements_ = js["use_volume_elements"].asBool();

		// settings etc
		fmm_stngs_ = cmn::Node::deserialize_node<fmm::Settings>(js["fmm_stngs"], list, factory_list);
		// mdl_stngs_ = cmn::Node::deserialize_node<mdl::Settings>(js["mdl_stngs"], list, factory_list);
		source_model_ = cmn::Node::deserialize_node<Model>(js["source_model"], list, factory_list);

		// list of background contributions
		const arma::uword num_bg = js["bg"].size();	bg_.set_size(num_bg);
		for(arma::uword i=0;i<num_bg;i++)
			bg_(i) = cmn::Node::deserialize_node<Background>(js["bg"].get(i,0), list, factory_list);

		// list of drives
		const arma::uword num_drives = js["drives"].size();
		for(arma::uword i=0;i<num_drives;i++)
			add_drive(cmn::Node::deserialize_node<Drive>(js["drives"].get(i,0), list, factory_list));
	}

}}
