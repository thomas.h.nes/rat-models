/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transrotate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransRotate::TransRotate(){
		// default rotationless matrix
		M_.zeros(); M_(0,0) = 1; M_(1,1) = 1; M_(2,2) = 1;
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const double phi, const double theta, const double psi){
		set_rotation(phi,theta,psi);
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const double ux, const double uy, const double uz, const double alpha){
		set_rotation(ux,uy,uz,alpha);
	}

	// constructor with rotation input
	TransRotate::TransRotate(
		const arma::Col<double>::fixed<3> &V, const double alpha){
		set_rotation(V,alpha);
	}


	// factory
	ShTransRotatePr TransRotate::create(){
		//return ShTransRotatePr(new TransRotate);
		return std::make_shared<TransRotate>();
	}

	// factory with rotation input
	ShTransRotatePr TransRotate::create(const double phi, const double theta, const double psi){
		//return ShTransRotatePr(new TransRotate(phi,theta,psi));
		return std::make_shared<TransRotate>(phi,theta,psi);
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(const double ux, const double uy, const double uz, const double alpha){
		//return ShTransRotatePr(new TransRotate(ux,uy,uz,alpha));
		return std::make_shared<TransRotate>(ux,uy,uz,alpha);
	}

	// factory with rotation around vector
	ShTransRotatePr TransRotate::create(const arma::Col<double>::fixed<3> &V, const double alpha){
		//return ShTransRotatePr(new TransRotate(V,alpha));
		return std::make_shared<TransRotate>(V,alpha);
	}


	// euler rotation with specified order
	void TransRotate::set_rotation(
		const double phi, const double theta, const double psi){
		// combined rotation matrix
		M_ = cmn::Extra::create_rotation_matrix(phi,theta,psi);
	}

	// rotation around unit vector
	void TransRotate::set_rotation(
		const double ux, const double uy, 
		const double uz, const double alpha){
		// use other set function
		set_rotation(arma::Col<double>::fixed<3>{ux,uy,uz},alpha);
	}

	// rotation around unit vector
	void TransRotate::set_rotation(
		const arma::Col<double>::fixed<3> &V, const double alpha){

		// normalize
		const arma::Col<double>::fixed<3> Vnrm = V/arma::as_scalar(cmn::Extra::vec_norm(V));

		// setup matrix
		M_ = cmn::Extra::create_rotation_matrix(Vnrm,alpha);
	}

	// apply to coordinates only
	void TransRotate::apply_coords(arma::Mat<double> &R) const{
		R = M_*R;
	}

	// apply transformation to coordinates and vectors
	void TransRotate::apply_vectors(
		const arma::Mat<double> &, arma::Mat<double> &V, const char) const{
		V = M_*V;
	}

	// get type
	std::string TransRotate::get_type(){
		return "mdl::transrotate";
	}

	// method for serialization into json
	void TransRotate::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		for(arma::uword i=0;i<9;i++)js["matrix"].append(M_(i));
	}

	// method for deserialisation from json
	void TransRotate::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		for(arma::uword i=0;i<9;i++)M_(i) = js["matrix"].get(i,0).asDouble();
	}

}}
