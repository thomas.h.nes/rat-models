/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "crosscircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	CrossCircle::CrossCircle(){

	}

	// constructors
	CrossCircle::CrossCircle(const double radius, const double dl){
		set_radius(radius); set_element_size(dl);
		set_center(0,0);
	}

	// constructors
	CrossCircle::CrossCircle(
		const double uc, const double vc,
		const double radius, const double dl){
		set_radius(radius); set_element_size(dl);
		set_center(uc,vc);
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(){
		return std::make_shared<CrossCircle>();
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(const double radius, const double dl){
		return std::make_shared<CrossCircle>(radius,dl);
	}

	// factory methods
	ShCrossCirclePr CrossCircle::create(
		const double uc, const double vc,
		const double radius, const double dl){
		return std::make_shared<CrossCircle>(uc,vc,radius,dl);
	}

	// set radius
	void CrossCircle::set_radius(const double radius){
		if(radius<=0)rat_throw_line("radius must be larger than zero");
		radius_ = radius;
	}

	// set element size
	void CrossCircle::set_element_size(const double dl){
		if(dl<=0)rat_throw_line("element size must be larger than zero");
		dl_ = dl;
	}

	// set center position u and v
	void CrossCircle::set_center(const double nc, const double tc){
		nc_ = nc; tc_ = tc;
	}

	// setup
	ShAreaPr CrossCircle::create_area() const{
		// check input
		if(radius_==0)rat_throw_line("radius is not set");
		if(dl_==0)rat_throw_line("element size is not set");

		// calculate size of central rectangle
		const double drect = 2*radius_/3;

		// calculate number of elements
		const arma::uword nrect = std::max(2,(int)std::ceil(2*arma::datum::pi*radius_/(4*dl_)));
		const arma::uword nrad = std::max(2,(int)std::ceil((radius_-std::sqrt(2.0)*drect/2)/dl_));

		// create rectangle
		// allocate coordinates
		arma::Mat<double> ur(nrect+1,nrect+1);
		arma::Mat<double> vr(nrect+1,nrect+1);

		// set values
		ur.each_row() = arma::linspace<arma::Row<double> >(-drect/2,drect/2,nrect+1);
		vr.each_col() = arma::linspace<arma::Col<double> >(-drect/2,drect/2,nrect+1);

		// draw quarter circle
		const arma::Col<double> theta = arma::linspace<arma::Col<double> >(-arma::datum::pi/4,arma::datum::pi/4,nrect+1);
		const arma::Col<double> rho = arma::linspace<arma::Col<double> >(radius_,radius_,nrect+1);
		const arma::Col<double> uc = rho%arma::cos(theta); 
		const arma::Col<double> vc = rho%arma::sin(theta);

		// build right quadrant
		arma::Mat<double> u1(nrad+1,nrect+1);
		arma::Mat<double> v1(nrad+1,nrect+1);

		// fill in
		arma::Row<double> t = arma::linspace<arma::Row<double> >(0,1,nrad+1);
		for(arma::uword i=0;i<nrad+1;i++){
			u1.row(i) = ((1-t(i))*ur.col(nrect) + t(i)*uc).t();
			v1.row(i) = ((1-t(i))*vr.col(nrect) + t(i)*vc).t();
		}
		
		// build other quadrants by rotation
		arma::Mat<double> ucc(nrad+1,nrect*4);
		arma::Mat<double> vcc(nrad+1,nrect*4);
		for(arma::uword i=0;i<4;i++){
			// get matrix
			arma::Mat<double> u = u1.cols(0,nrect-1);
			arma::Mat<double> v = v1.cols(0,nrect-1);

			// convert to polar and rotate
			arma::Mat<double> thetac = arma::atan2(v,u) + i*arma::datum::pi/2;
			arma::Mat<double> rhoc = arma::sqrt(v%v + u%u);

			// convert to carthesian and store in matrix
			ucc.cols(i*nrect,(i+1)*nrect-1) = rhoc%arma::cos(thetac);
			vcc.cols(i*nrect,(i+1)*nrect-1) = rhoc%arma::sin(thetac);
		}

		// count number of nodes
		const arma::uword num_nodes_circle = (nrad+1)*nrect*4;

		// create matrix of node indices for the circular area
		arma::Mat<arma::uword> node_idx_circle = 
			arma::regspace<arma::Mat<arma::uword> >(0,num_nodes_circle-1);
		node_idx_circle.reshape(nrad+1,4*nrect);

		// connect start to end
		node_idx_circle = arma::join_horiz(node_idx_circle,node_idx_circle.col(0));

		// number of elements
		const arma::uword num_elements_circle = nrad*nrect*4;

		// create coordinates
		arma::Mat<double> Rc(2,num_nodes_circle);
		Rc.row(0) = arma::reshape(ucc,1,num_nodes_circle);
		Rc.row(1) = arma::reshape(vcc,1,num_nodes_circle);

		// generate internal element node indexes
		arma::Mat<arma::uword> nc(4,num_elements_circle);
		nc.row(0) = arma::reshape(node_idx_circle.submat(0, 0, nrad-1, 4*nrect-1), 1, num_elements_circle);
		nc.row(1) = arma::reshape(node_idx_circle.submat(1, 0, nrad, 4*nrect-1), 1, num_elements_circle);
		nc.row(2) = arma::reshape(node_idx_circle.submat(1, 1, nrad, 4*nrect), 1, num_elements_circle);
		nc.row(3) = arma::reshape(node_idx_circle.submat(0, 1, nrad-1, 4*nrect), 1, num_elements_circle);

		// count number of nodes
		const arma::uword num_nodes_rectangle = (nrect-1)*(nrect-1);

		// now create rectangle (only select central nodes as edges are part of circle)
		arma::Mat<double> Rr(2,num_nodes_rectangle);
		Rr.row(0) = arma::reshape(ur.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);
		Rr.row(1) = arma::reshape(vr.submat(1,1,nrect-1,nrect-1),1,num_nodes_rectangle);

		// create matrix of node indices for the rectangular area
		arma::Mat<arma::uword> node_idx_rect = num_nodes_circle + 
			arma::regspace<arma::Mat<arma::uword> >(0,(nrect-1)*(nrect-1)-1);
		node_idx_rect.reshape(nrect-1,nrect-1);

		// expand rectangle
		arma::Mat<arma::uword> node_idx_rect_ext(nrect+1,nrect+1,arma::fill::zeros);
		node_idx_rect_ext.submat(1,1,nrect-1,nrect-1) = node_idx_rect;
		
		// connect edges
		node_idx_rect_ext.col(nrect) = node_idx_circle.submat(0,0,0,nrect).t();
		node_idx_rect_ext.row(nrect) = arma::fliplr(node_idx_circle.submat(0,1*nrect,0,2*nrect));
		node_idx_rect_ext.col(0) = arma::flipud(node_idx_circle.submat(0,2*nrect,0,3*nrect).t());
		node_idx_rect_ext.row(0) = node_idx_circle.submat(0,3*nrect,0,4*nrect);
		
		// calculate number of elements in rectangle
		arma::uword num_elements_rectangle = nrect*nrect;

		// generate internal element node indexes
		arma::Mat<arma::uword> nr(4,num_elements_rectangle);
		nr.row(0) = arma::reshape(node_idx_rect_ext.submat(0, 0, nrect-1, nrect-1), 1, num_elements_rectangle);
		nr.row(1) = arma::reshape(node_idx_rect_ext.submat(1, 0, nrect, nrect-1), 1, num_elements_rectangle);
		nr.row(2) = arma::reshape(node_idx_rect_ext.submat(1, 1, nrect, nrect), 1, num_elements_rectangle);
		nr.row(3) = arma::reshape(node_idx_rect_ext.submat(0, 1, nrect-1, nrect), 1, num_elements_rectangle);

		// connect meshes
		arma::Mat<arma::uword> n = arma::join_horiz(nc,nr);
		arma::Mat<double> Rn = arma::join_horiz(Rc,Rr);
		
		// apply center position offset
		Rn.row(0) += nc_; Rn.row(1) += tc_;
		
		// count total number of nodes and elements
		// const arma::uword num_nodes = num_nodes_rectangle + num_nodes_circle;
		// const arma::uword num_elements = num_elements_rectangle + num_elements_circle;

		// flip mesh
		n = arma::flipud(n);

		// circle area
		ShAreaPr carea = Area::create(Rn,n);

		// mesh smoothing
		carea->smoothen(num_smooth_iter_,smooth_dampen_);

		// check areas and periphery within percent of perfect circle
		// assert(std::abs(carea->calculate_area()-arma::datum::pi*radius_*radius_)/(arma::datum::pi*radius_*radius_)<5e-2);
		// assert(std::abs(carea->calculate_periphery()-2*arma::datum::pi*radius_)/(2*arma::datum::pi*radius_)<5e-2);

		// return positive
		return carea;
	}

	// surface mesh
	ShPerimeterPr CrossCircle::create_perimeter() const{
		// create area mesh
		ShAreaPr area_mesh = create_area();

		// extract periphery and return
		return area_mesh->extract_perimeter();
	}

	// get type
	std::string CrossCircle::get_type(){
		return "mdl::crosscircle";
	}

	// method for serialization into json
	void CrossCircle::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["radius"] = radius_;
		js["nc"] = nc_; js["tc"] = tc_; js["dl"] = dl_;
	}

	// method for deserialisation from json
	void CrossCircle::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		set_radius(js["radius"].asDouble());
		set_center(js["nc"].asDouble(),js["tc"].asDouble());
		set_element_size(js["dl"].asDouble());
	}

}}