/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "matnb3sn.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatNb3Sn::MatNb3Sn(){
		// interpolation arrays
		Ti_ = arma::regspace<arma::Col<double> >(1,300);
		ki_ = arma::Col<double>{0.00562,0.01,0.0162,0.031,0.0793,0.157,0.257,0.378,0.517,0.671,0.839,1.02,1.2,1.39,1.59,1.78,1.97,2.15,2.32,2.48,2.56,2.63,2.69,2.73,2.77,2.8,2.81,2.82,2.83,2.83,2.84,2.85,2.85,2.86,2.86,2.85,2.85,2.84,2.84,2.83,2.82,2.82,2.81,2.81,2.8,2.8,2.79,2.79,2.78,2.78,2.76,2.74,2.72,2.7,2.68,2.66,2.64,2.62,2.6,2.58,2.56,2.55,2.54,2.52,2.51,2.5,2.49,2.47,2.46,2.45,2.44,2.42,2.41,2.4,2.39,2.38,2.36,2.35,2.34,2.33,2.32,2.31,2.3,2.29,2.28,2.27,2.26,2.26,2.25,2.24,2.23,2.22,2.22,2.21,2.2,2.2,2.19,2.18,2.18,2.17,2.16,2.16,2.15,2.14,2.13,2.13,2.12,2.11,2.11,2.1,2.09,2.08,2.08,2.07,2.06,2.06,2.05,2.04,2.03,2.03,2.02,2.01,2.01,2,1.99,1.98,1.98,1.97,1.96,1.96,1.95,1.94,1.93,1.93,1.92,1.91,1.91,1.9,1.89,1.89,1.88,1.87,1.87,1.86,1.85,1.85,1.84,1.84,1.83,1.82,1.82,1.81,1.81,1.8,1.8,1.79,1.79,1.78,1.78,1.77,1.77,1.76,1.76,1.76,1.75,1.75,1.74,1.74,1.73,1.73,1.72,1.72,1.72,1.71,1.71,1.7,1.7,1.69,1.69,1.69,1.68,1.68,1.67,1.67,1.67,1.66,1.66,1.65,1.65,1.65,1.64,1.64,1.64,1.63,1.63,1.62,1.62,1.62,1.61,1.61,1.6,1.6,1.59,1.59,1.58,1.58,1.58,1.57,1.57,1.56,1.56,1.55,1.55,1.54,1.54,1.53,1.53,1.53,1.52,1.52,1.51,1.51,1.5,1.5,1.49,1.49,1.49,1.48,1.48,1.47,1.47,1.46,1.46,1.46,1.45,1.45,1.44,1.44,1.43,1.43,1.43,1.42,1.42,1.41,1.41,1.41,1.4,1.4,1.4,1.39,1.29,1.2,1.11,1.02,0.947,0.878,0.817,0.764,0.72,0.685,0.66,0.645,0.642,0.649,0.669,0.702,0.747,0.805,0.878,0.966,1.07,1.19,1.32,1.47,1.64,1.82,2.03,2.25,2.49,2.75,3.03,3.34,3.66,4.01,4.37,4.76,5.18,5.62,6.08,6.56,7.08,7.62,8.18,8.77,9.39,10,10.7,11.4,12.2,12.9};
		Cpi_ = arma::Col<double>{0.00269,0.0215,0.0725,0.172,0.376,0.676,1.06,1.53,2.07,2.69,3.3,3.96,4.7,5.51,6.39,7.36,8.4,9.54,10.8,12.1,13.7,15.3,17.1,18.9,20.9,22.9,25,27.2,29.4,31.7,34.1,36.6,39.1,41.7,44.3,46.9,49.5,52.2,54.8,57.5,60,62.6,65.1,67.6,70,72.5,75,77.4,79.8,82.2,84.6,86.9,89.3,91.6,93.9,96.2,98.4,101,103,105,107,109,111,113,115,117,119,121,123,125,127,129,131,132,134,136,137,139,140,142,143,145,146,147,148,150,151,152,153,154,155,156,157,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,181,182,183,184,185,186,187,187,188,189,190,191,191,192,193,194,195,195,196,197,197,198,199,200,200,201,202,202,203,204,204,205,205,206,207,207,208,208,209,209,210,210,211,212,212,213,213,214,214,215,215,215,216,216,217,217,218,218,219,219,219,220,220,220,221,221,222,222,222,223,223,223,224,224,224,225,225,225,226,226,226,226,227,227,227,228,228,228,228,229,229,229,229,229,230,230,230,230,230,231,231,231,231,231,232,232,232,232,232,233,233,233,233,233,233,233,234,234,234,234,234,234,235,235,235,235,235,235,235,235,236,236,236,236,236,236,236,237,237,237,237,237,237,237,237,238,238,238,238,238,238,238,238,239,239,239,239,239,239,239,240,240,240,240,240,240,240,241,241,241,241,241,241,241,242,242,242,242,242,242,242,243,243,243};
	}

	// factory
	ShMatNb3SnPr MatNb3Sn::create(){
		return std::make_shared<MatNb3Sn>();
	}

	// iter type
	void MatNb3Sn::set_ITER(){
		// Deformation related parameters
		Ca1_ = 47.6;
		Ca2_ = 6.4;
		eps_0a_ = 0.00136; // normalized
		C1_ = 180e3; // [kAT/mm^2]

		// Superconducting parameters
		Bc2m_ = 29.7; // [T]
		Tcm_ = 17.9; // [K]

		// magnetic field dependence parameters
		p_ = 0.5;
		q_ = 2;
	}

	// set powder in tube
	void MatNb3Sn::set_PIT(){
		// Deformation related parameters
		Ca1_ = 47.6;
		Ca2_ = 6.4;
		eps_0a_ = 0.00136; // normalized
		C1_ = 200e3; // [kAT/mm^2]

		// Superconducting parameters
		Bc2m_ = 29.7; // [T]
		Tcm_ = 17.9; // [K]

		// magnetic field dependence parameters
		p_ = 0.5;
		q_ = 2;
	}

	// parameters for an rod restack process wire
	void MatNb3Sn::set_RRP(){
		// Deformation related parameters
		Ca1_ = 47.6;
		Ca2_ = 6.4;
		eps_0a_ = 0.00136; // normalized
		C1_ = 213e3; // [kAT/mm^2]

		// Superconducting parameters
		Bc2m_ = 29.7; // [T]
		Tcm_ = 17.9; // [K]

		// magnetic field dependence parameters
		p_ = 0.5;
		q_ = 2;
	}

	// precalculate material properties for fast electric field calculations
	arma::Mat<double> MatNb3Sn::calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		const arma::Mat<double> props = calc_critical_current_density(Bm,T,alpha);
		return props;
	}

	// calculate critical current
	arma::Row<double> MatNb3Sn::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &) const{
		// strain dependence
		const double eps_shift = Ca2_*eps_0a_/std::sqrt(Ca1_*Ca1_ - Ca2_*Ca2_);
		const double S = (Ca1_*(std::sqrt(eps_shift*eps_shift + eps_0a_*eps_0a_) - 
			std::sqrt(std::pow(eps_ax_ - eps_shift,2) + eps_0a_*eps_0a_)) - 
			Ca2_*eps_ax_)/(1.0 - Ca1_*eps_0a_) + 1.0;

		// scaled values
		const double Tc = Tcm_*std::pow(S,(1.0/3));
		const arma::Row<double> t = T/Tc;
		const arma::Row<double> Bc2 = Bc2m_*S*(1.0-arma::pow(t,1.52));
		const arma::Row<double> h = Bm/Bc2;

		// Godeke Equation
		arma::Row<double> Jc = C1_*S*(1.0-arma::pow(t,1.52))%(1.0-arma::pow(t,2))%arma::pow(h,p_)%arma::pow(1.0-h,q_)/Bm;

		// prevent formula from rising again after reaching Bc
		Jc(arma::find(Bm>Bc2)).fill(0);
		Jc(arma::find(Bm==0)).fill(arma::datum::inf);

		// return critical current
		return 1e6*Jc;
	}

	// calculate electric field
	arma::Row<double> MatNb3Sn::calc_current_density_fast(
		const arma::Row<double> &E, const arma::Mat<double> &props) const{
		// calculate critical current
		// const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law reversed
		return Jc%arma::pow(E/E0_,1.0/N_); // A/m^2
	}

	arma::Row<double> MatNb3Sn::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props) const{
		// calculate critical current
		// const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law
		return E0_*arma::pow(J/Jc,N_); // V/m
	}

	// calculate thermal conductivity in [W m^-1 K^-1]
	arma::Row<double> MatNb3Sn::calc_thermal_conductivity(
		const arma::Row<double> &/*Bm*/, const arma::Row<double> &T) const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!ki_.is_empty());

		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,ki_,T.t(),V,true);

		// reshape and return
		return V.t();
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Row<double> MatNb3Sn::calc_specific_heat(const arma::Row<double> &T)const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!Cpi_.is_empty());

		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,Cpi_,T.t(),V,true);

		// reshape and return
		return density_*V.t(); //arma::reshape(V,T.n_rows,T.n_cols);
	}

	// copy constructor
	ShMaterialPr MatNb3Sn::copy() const{
		return std::make_shared<MatNb3Sn>(*this);
	}

	// get type
	std::string MatNb3Sn::get_type(){
		return "mdl::matnb3sn";
	}

	// method for serialization into json
	void MatNb3Sn::serialize(Json::Value &js, cmn::SList &) const{
		// general
		js["type"] = get_type();
		js["ca1"] = Ca1_;
		js["ca2"] = Ca2_;
		js["eps_0a"] = eps_0a_;
		js["C1"] = C1_;
		js["Bc2m"] = Bc2m_;
		js["Tcm"] = Tcm_;
		js["p"] = p_;
		js["q"] = q_;
		js["eps_ax"] = eps_ax_;
		js["E0"] = E0_;
		js["N"] = N_;
	}

	// method for deserialisation from json
	void MatNb3Sn::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// general
		Ca1_ = js["ca1"].asDouble();
		Ca2_ = js["ca2"].asDouble();
		eps_0a_ = js["eps_0a"].asDouble();
		C1_ = js["C1"].asDouble();
		Bc2m_ = js["Bc2m"].asDouble();
		Tcm_ = js["Tcm"].asDouble();
		p_ = js["p"].asDouble();
		q_ = js["q"].asDouble();
		eps_ax_ = js["eps_ax"].asDouble();
		E0_ = js["E0"].asDouble();
		N_ = js["N"].asDouble();
	}


}}
