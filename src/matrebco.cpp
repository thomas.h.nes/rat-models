/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "matrebco.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatReBCO::MatReBCO(){
		setup_interpolation();
	}

	// factory
	ShMatReBCOPr MatReBCO::create(){
		return std::make_shared<MatReBCO>();
	}

	// set interpolation arrays
	void MatReBCO::setup_interpolation(){
		Ti_ = arma::regspace<arma::Col<double> >(1,300);
		ki_ = arma::Col<double>{0.103,0.47,0.714,0.874,1.07,1.29,1.53,1.77,2.03,2.3,2.62,2.95,3.29,3.64,3.99,4.34,4.69,5.04,5.37,5.7,5.95,6.19,6.41,6.62,6.82,7,7.17,7.33,7.47,7.6,7.72,7.83,7.93,8.01,8.09,8.15,8.2,8.25,8.28,8.3,8.31,8.3,8.29,8.27,8.24,8.2,8.16,8.11,8.06,8,7.94,7.89,7.82,7.76,7.69,7.62,7.54,7.46,7.38,7.3,7.22,7.13,7.04,6.95,6.86,6.77,6.68,6.59,6.49,6.4,6.3,6.21,6.11,6.02,5.93,5.83,5.75,5.66,5.58,5.5,5.43,5.36,5.3,5.24,5.19,5.14,5.1,5.06,5.03,5,4.99,4.98,4.98,4.98,4.99,5.01,5.02,5.05,5.07,5.1,5.1,5.11,5.11,5.12,5.12,5.12,5.12,5.12,5.12,5.11,5.11,5.11,5.11,5.1,5.1,5.09,5.08,5.08,5.07,5.06,5.05,5.05,5.04,5.03,5.02,5.01,5,4.99,4.98,4.96,4.95,4.94,4.93,4.92,4.9,4.89,4.88,4.86,4.85,4.84,4.82,4.81,4.8,4.78,4.77,4.75,4.74,4.73,4.71,4.7,4.69,4.69,4.68,4.68,4.68,4.67,4.67,4.66,4.66,4.65,4.65,4.64,4.64,4.64,4.63,4.63,4.63,4.62,4.62,4.62,4.61,4.61,4.61,4.6,4.6,4.6,4.59,4.59,4.59,4.59,4.58,4.58,4.58,4.58,4.57,4.57,4.57,4.57,4.57,4.56,4.56,4.56,4.56,4.56,4.56,4.56,4.55,4.55,4.55,4.55,4.55,4.55,4.54,4.54,4.54,4.54,4.54,4.54,4.54,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.52,4.52,4.52,4.52,4.52,4.52,4.52,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.53,4.54,4.54,4.54,4.54,4.54,4.54,4.55,4.55,4.55,4.55,4.56,4.56,4.56,4.57,4.57,4.57,4.58,4.58,4.58,4.59,4.59,4.6,4.6,4.6,4.61,4.61,4.61,4.62,4.62,4.62,4.63,4.63,4.63,4.64,4.64,4.64,4.64,4.65,4.65,4.65,4.65,4.66,4.66,4.66,4.66,4.67,4.67,4.67,4.67,4.68,4.68,4.68,4.68,4.69,4.69,4.69,4.69,4.7,4.7};
		Cpi_ = arma::Col<double>{0.00899,0.0118,0.0234,0.045,0.0654,0.0933,0.138,0.208,0.309,0.45,0.693,0.993,1.35,1.77,2.25,2.79,3.39,4.07,4.81,5.62,6.61,7.67,8.79,9.96,11.2,12.5,13.8,15.2,16.6,18,19.4,20.9,22.4,23.8,25.3,26.9,28.4,29.9,31.5,33,34.5,36,37.5,38.9,40.4,41.9,43.4,44.9,46.3,47.8,49.2,50.7,52.1,53.6,55.1,56.5,58,59.5,61,62.5,64.1,65.7,67.4,69,70.6,72.3,73.9,75.5,77.1,78.7,80.2,81.7,83.1,84.6,86,87.4,88.8,90.1,91.5,92.8,94,95.3,96.5,97.7,98.9,100,101,102,104,105,106,108,109,110,112,113,114,116,117,118,120,121,122,123,125,126,127,128,130,131,132,133,134,135,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,167,168,169,170,171,172,173,174,174,175,176,177,178,179,179,180,181,182,183,184,184,185,186,187,187,188,189,190,190,191,192,193,193,194,195,196,196,197,198,198,199,200,200,201,202,202,203,204,204,205,206,206,207,207,208,209,209,210,210,211,212,212,213,214,214,215,215,216,216,217,217,218,219,219,220,220,221,221,222,222,223,223,224,224,225,225,226,226,227,227,227,228,228,229,229,230,230,230,231,231,231,232,232,233,233,233,234,234,234,234,235,235,235,236,236,236,236,237,237,237,237,238,238,238,238,239,239,239,239,240,240,240,240,240,241,241,241,241,241,242,242,242,242,242,243,243,243,243,243,243,244,244,244,244,244};
	}

	// set Fujikura values
	// "Y-based high temperature superconductor", Fujikura, leaflet at ICEC
	void MatReBCO::set_fujikura_cern(){
		// general parameters
		Tc0_ = 93; // [K]
		n_ = 1;
		n1_ = 1.4;
		n2_ = 4.45;

		// Parameters for ab-plane
		pab_ = 1;
		qab_ = 5;
		Bi0ab_ = 250; // [T]
		a_ = 0.1;
		yab_ = 1.63;
		alpha_ab_ = 68.3e12; // [AT/m^2]

		// Parameters for c-plane
		pc_ = 0.5;
		qc_ = 2.5;
		Bi0c_ = 140; // [T]
		yc_ = 2.44;
		alphac_ = 1.86e12; // [AT/m^2]

		// Parameters for anisotropy
		g0_ = 0.03;
		g1_ = 0.25;
		g2_ = 0.06;
		g3_ = 0.058;
		v_ = 1.85;

		// power law settings
		N_ = 24;
		E0_ = 100e-6;

		// offset of the peak in deg
		pkoff_ = 0;

		// calculate mean of flipped pair
		type0_flip_ = false;
	}

	// precalculate material properties for fast electric field calculations
	arma::Mat<double> MatReBCO::calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		const arma::Mat<double> props = calc_critical_current_density(Bm,T,alpha);
		return props;
	}
	
	// scaling relation by Jerome Fleiter
	// J. Fleiter and A. Ballarino, "Parameterization of the critical surface of
	// ReBCO conductors from Fujikura", October 2014, technical report, CERN, Geneva 23
	arma::Row<double> MatReBCO::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{

		// prevent zero field at which a anomalous behaviour occurs
		arma::Row<double> B = Bm;
		B = arma::clamp(B,0.1,arma::datum::inf);

		// calculate unitless temperature
		const arma::Row<double> t = T/Tc0_;

		// irreversibility field for ab-plane
		const arma::Row<double> Bi_ab = Bi0ab_*(
			arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)));

		// calculate unitless magnetic field for ab-plane
		const arma::Row<double> bab = B/Bi_ab;

		// irreversibility field for c-plane
		const arma::Row<double> Bi_c = Bi0c_*(1.0-arma::pow(t,n_));

		// unitless field for c-plane
		const arma::Row<double> bc = B/Bi_c;

		// critical current density c-plane
		arma::Row<double> Jc_c = (alphac_/B)%arma::pow(bc,pc_)%
			arma::pow(1.0-bc,qc_)%arma::pow(1.0-arma::pow(t,n_),yc_);

		// critical current density ab-plane
		arma::Row<double> Jc_ab = (alpha_ab_/B)%arma::pow(bab,pab_)%arma::pow(1.0-bab,qab_)%
			arma::pow(arma::pow(1.0-arma::pow(t,n1_),n2_)+a_*(1.0-arma::pow(t,n_)),yab_);

		// temperature over Tcs
		Jc_c(arma::find(t>=1.0)).fill(0.0); 
		Jc_ab(arma::find(t>=1.0)).fill(0.0);

		// field over Bi
		Jc_c(arma::find(bc>=1.0)).fill(0.0); 
		Jc_ab(arma::find(bab>=1.0)).fill(0.0);

		// anisotropy factor
		const arma::Row<double> g = g0_ + g1_*arma::exp(-(g2_*arma::exp(g3_*T))%B);

		// Sort out angular dependence
		const double pkoff_rad = 2*arma::datum::pi*pkoff_/360.0;

		// critical current density calculation
		arma::Mat<double> Jc = arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(alpha + pkoff_rad - arma::datum::pi/2.0))/g,v_)));

		// flipped type-0 pair
		if(type0_flip_){
			Jc += arma::min(Jc_c,Jc_ab)+
			(arma::clamp(Jc_ab-Jc_c,0.0,arma::datum::inf)/
			(1.0+arma::pow((arma::abs(alpha - pkoff_rad - arma::datum::pi/2.0))/g,v_)));
			Jc/=2;
		}

		// return critical current density
		return Jc;
	}

	// calculate current density from electric field
	arma::Row<double> MatReBCO::calc_current_density_fast(
		const arma::Row<double> &E, const arma::Mat<double> &props) const{
		// calculate critical current
		//const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law reversed
		return Jc%arma::pow(E/E0_,1.0/N_); // A/m^2
	}

	// calculate electric field from current density
	arma::Row<double> MatReBCO::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props) const{
		// calculate critical current
		// const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law
		return E0_*arma::pow(J/Jc,N_); // V/m
	}

	// calculate thermal conductivity in [W m^-1 K^-1]
	arma::Row<double> MatReBCO::calc_thermal_conductivity(
		const arma::Row<double> &/*Bm*/, const arma::Row<double> &T) const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!ki_.is_empty());
		assert(Ti_.n_elem==ki_.n_elem);

		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,ki_,T.t(),V,true);
		//rat::cmn::Extra::interp1(Ti_,ki_,arma::vectorise(T),V,"linear",true);

		// reshape and return
		return V.t();
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Row<double> MatReBCO::calc_specific_heat(const arma::Row<double> &T)const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!Cpi_.is_empty());

		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,Cpi_,T.t(),V,true);
		// rat::cmn::Extra::interp1(Ti_,Cpi_,arma::vectorise(T),V,"linear",true);

		// reshape and return
		return density_*V.t();
	}

	// copy constructor
	ShMaterialPr MatReBCO::copy() const{
		return std::make_shared<MatReBCO>(*this);
	}

	// get type
	std::string MatReBCO::get_type(){
		return "mdl::matrebco";
	}

	// method for serialization into json
	void MatReBCO::serialize(Json::Value &js, cmn::SList &) const{
		// general
		js["type"] = get_type();
		js["Tc0"] = Tc0_;
		js["n"] = n_;
		js["n1"] = n1_;
		js["n2"] = n2_;

		// AB-plane
		js["pab"] = pab_;
		js["qab"] = qab_;
		js["Bi0ab"] = Bi0ab_;
		js["a"] = a_;
		js["yab"] = yab_;
		js["alpha_ab"] = alpha_ab_;

		// C-axis
		js["pc"] = pc_;
		js["qc"] = qc_;
		js["Bi0c"] = Bi0c_;
		js["yc"] = yc_;
		js["alphac"] = alphac_;

		// anisotropy
		js["g0"] = g0_;
		js["g1"] = g1_;
		js["g2"] = g2_;
		js["g3"] = g3_;
		js["v"] = v_;
		js["pkoff"] = pkoff_;

		// power law parameters
		js["N"] = N_;
		js["E0"] = E0_;

		// type-0 pair flipping
		js["type0_flip"] = type0_flip_;
	}

	// method for deserialisation from json
	void MatReBCO::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// general
		Tc0_ = js["Tc0"].asDouble();
		n_ = js["n"].asDouble();
		n1_ = js["n1"].asDouble();
		n2_ = js["n2"].asDouble();

		// AB-plane
		pab_ = js["pab"].asDouble();
		qab_ = js["qab"].asDouble();
		Bi0ab_ = js["Bi0ab"].asDouble();
		a_ = js["a"].asDouble();
		yab_ = js["yab"].asDouble();
		alpha_ab_ = js["alpha_ab"].asDouble();

		// C-axis
		pc_ = js["pc"].asDouble();
		qc_ = js["qc"].asDouble();
		Bi0c_ = js["Bi0c"].asDouble();
		yc_ = js["yc"].asDouble();
		alphac_ = js["alphac"].asDouble();

		// anisotropy
		g0_ = js["g0"].asDouble();
		g1_ = js["g1"].asDouble();
		g2_ = js["g2"].asDouble();
		g3_ = js["g3"].asDouble();
		v_ = js["v"].asDouble();
		pkoff_ = js["pkoff"].asDouble();

		// power law parameters
		N_ = js["N"].asDouble();
		E0_ = js["E0"].asDouble();

		// type-0 pair flipping
		type0_flip_ = js["type0_flip"].asBool();
	}



}}

