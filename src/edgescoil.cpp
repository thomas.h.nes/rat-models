/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "edgescoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	EdgesCoil::EdgesCoil(){

	}

	// constructor with input
	EdgesCoil::EdgesCoil(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShEdgesCoilPr EdgesCoil::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<EdgesCoil>();
	}

	// factory with input
	ShEdgesCoilPr EdgesCoil::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<EdgesCoil>(frame, area);
	}

	// set operating current
	void EdgesCoil::set_operating_current(const double operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	double EdgesCoil::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void EdgesCoil::set_number_turns(const double number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	double EdgesCoil::get_number_turns() const{
		return number_turns_;
	}

	// set material
	void EdgesCoil::set_material(ShMaterialPr material){
		assert(material!=NULL);
		material_ = material;
	}

	// get material
	ShMaterialPr EdgesCoil::get_material() const{
		assert(material_!=NULL);
		return material_;
	}


	// export edges to freecad
	void EdgesCoil::export_freecad(cmn::ShFreeCADPr freecad) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("freecad currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<double> > x,y,z; create_xyz(x,y,z);

		// write to freecad
		freecad->write_edges(x,y,z,section_,turn_,myname_);
	}

	// export edges to freecad
	void EdgesCoil::export_opera(cmn::ShOperaPr opera) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("opera currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<double> > x,y,z; create_xyz(x,y,z);

		// calculate current density
		const double J = number_turns_*operating_current_/area_->calculate_area();

		// write edges with current density
		opera->write_edges(x,y,z,J);
	}

	// copy constructor
	ShEdgesPr EdgesCoil::copy() const{
		return std::make_shared<EdgesCoil>(*this);
	}

}}