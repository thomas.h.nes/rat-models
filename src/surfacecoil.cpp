/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "surfacecoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	SurfaceCoil::SurfaceCoil(){

	}

	// factory
	ShSurfaceCoilPr SurfaceCoil::create(){
		return std::make_shared<SurfaceCoil>();
	}

	// set operating current
	void SurfaceCoil::set_operating_current(const double operating_current){
		operating_current_ = operating_current;
	}

	// get operating current
	double SurfaceCoil::get_operating_current() const{
		return operating_current_;
	}

	// set number of turns
	void SurfaceCoil::set_number_turns(const double number_turns){
		number_turns_ = number_turns;
	}

	// get number of turns
	double SurfaceCoil::get_number_turns() const{
		return number_turns_;
	}

	// set material
	void SurfaceCoil::set_material(ShMaterialPr material){
		assert(material!=NULL);
		material_ = material;
	}

	// get material
	ShMaterialPr SurfaceCoil::get_material() const{
		assert(material_!=NULL);
		return material_;
	}

	// current density
	double SurfaceCoil::calc_current_density() const{
		// calculate cross section areas
		const double cross_area = area_->calculate_area();

		// calculate current distribution in cross section
		const double Jop = number_turns_*operating_current_/cross_area;

		// return current density
		return Jop;
	}

	// get curret density at nodes
	arma::Mat<double> SurfaceCoil::get_nodal_current_density() const{
		return L_*calc_current_density();
	}

	// calculate field angle
	arma::Row<double> SurfaceCoil::calc_alpha(const arma::Mat<double> &B) const{
		// check number of columns
		assert(B.n_cols==get_num_nodes());

		// calculate components along L, N and D
		const arma::Row<double> Bl = cmn::Extra::dot(B,L_);
		const arma::Row<double> Bn = cmn::Extra::dot(B,N_);
		const arma::Row<double> Bd = cmn::Extra::dot(B,D_);

		// calculate angle
		const arma::Row<double> alpha_rad = arma::atan2(arma::sqrt(Bl%Bl + Bd%Bd),Bn);

		// return angle
		return alpha_rad;
	}

	// calculate engineering current density
	arma::Row<double> SurfaceCoil::calc_Je(
		const arma::Mat<double> &B, 
		const arma::Row<double> &T) const{
		// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);

		// calculate engineering current density and return
		return material_->calc_critical_current_density(Bm, T, alpha);
	}

	// calculate engineering current density
	arma::Row<double> SurfaceCoil::calc_Tc(
		const arma::Mat<double> &J, 
		const arma::Mat<double> &B) const{
		// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);
		arma::Row<double> Jm = cmn::Extra::vec_norm(J);

		// calculate engineering current density and return
		return material_->calc_critical_temperature(Jm, Bm, alpha);
	}

	// calculate electric field
	arma::Mat<double> SurfaceCoil::calc_E(
		const arma::Mat<double> &J, 
		const arma::Mat<double> &B, 
		const arma::Row<double> &T) const{
			// calculate alpha
		arma::Row<double> alpha = calc_alpha(B);

		// calculate magnitude of field
		arma::Row<double> Bm = cmn::Extra::vec_norm(B);
		arma::Row<double> Jm = cmn::Extra::vec_norm(J);

		// calculate electric field
		return L_.each_row()%material_->calc_electric_field(Jm,Bm,T,alpha);	
	}


}}
