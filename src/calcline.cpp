/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcline.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcLine::CalcLine(){
		set_name("line");
	}

	// constructor
	CalcLine::CalcLine(ShModelPr source_model, ShPathPr base){
		set_source_model(source_model); set_base(base);	set_name("line");
	}

	// factory
	ShCalcLinePr CalcLine::create(){
		return std::make_shared<CalcLine>();
	}

	// factory
	ShCalcLinePr CalcLine::create(ShModelPr source_model, ShPathPr base){
		return std::make_shared<CalcLine>(source_model, base);
	}


	// set path
	void CalcLine::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("supplied target path points to NULL");
		base_ = base;
	}

	// calculate field on line
	void CalcLine::setup(cmn::ShLogPr lg){
		// header
		lg->msg(2,"%s%sSETTING UP LINE CALCULATION%s\n",KBLD,KGRN,KNRM);

		// create frame for base line
		ShFramePr gen = base_->create_frame();

		// combine sections
		gen->combine();

		// get line properties
		Rt_ = gen->get_coords(0);
		L_ = gen->get_direction(0);
		N_ = gen->get_normal(0);
		D_ = gen->get_transverse(0);

		// calculate length
		const arma::Row<double> dl = arma::sqrt(arma::sum(arma::pow(arma::diff(Rt_,1,1),2),0));
		ell_ = arma::join_horiz(arma::Row<double>{0},arma::cumsum(dl));

		// display input path
		lg->msg(2,"%sInput Path%s\n",KBLU,KNRM);
		lg->msg("number of points: %s%llu%s\n",KYEL,ell_.n_elem,KNRM);
		lg->msg("total path length: %s%2.2f%s\n",KYEL,arma::sum(dl),KNRM);
		lg->msg(-2);

		// line calculation setup complete
		lg->msg(-2,"\n");
	}

	// get coordinates for section
	arma::Mat<double> CalcLine::get_coords() const{
		return Rt_;
	}

	// get coordinates for section
	arma::Mat<double> CalcLine::get_direction() const{
		return L_;
	}

	// get coordinates for section
	arma::Mat<double> CalcLine::get_normal() const{
		return N_;
	}

	// get coordinates for section
	arma::Mat<double> CalcLine::get_transverse() const{
		return D_;
	}

	// export points/lines to vtk file
	ShVTKUnstrPr CalcLine::export_vtk_coord(cmn::ShLogPr lg) const{
		// header
		lg->msg(2,"%s%sVTK LINE EXPORT%s\n",KBLD,KGRN,KNRM);
		
		// header for data preparation
		lg->msg(2,"%sPreparing Data%s\n",KBLU,KNRM);

		// create unstructured grid
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// check if calculation was done
		if(!has_field())rat_throw_line("field is not calculated");

		// counters
		const arma::uword num_ell = Rt_.n_cols;

		// write node coordinates
		lg->msg("coordinates\n");
		vtk_data->set_nodes(Rt_);

		// create line elements
		lg->msg("line elements\n");
		arma::Mat<arma::uword> n(2,num_ell-1);
		for(arma::uword i=0;i<num_ell-1;i++)
			n.col(i) = arma::Col<arma::uword>::fixed<2>{i,i+1};
		const arma::uword line_type = 3;
		vtk_data->set_elements(n,line_type);

		// walk over list of output types
		for(std::list<CalcOutTypes>::const_iterator it = output_types_.begin(); it!=output_types_.end(); it++){

			// get type
			CalcOutTypes type = *it;

			// check the type tag
			switch(type){
				// magnetic vector potential
				case VECTOR_POTENTIAL:{
					lg->msg("magnetic vector potential\n");
					const arma::Mat<double> A = get_field("A");
					vtk_data->set_nodedata(A,"Vector Potential [Vs/m]");
				}break;

				// magnetic flux density
				case MAGNETIC_FLUX:{
					lg->msg("magnetic flux density\n");
					const arma::Mat<double> B = get_field("B");
					vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");
				}break;

				// magnetisation
				case MAGNETISATION:{
					lg->msg("magnetisation\n");
					const arma::Mat<double> Mn = get_field("M");
					vtk_data->set_nodedata(Mn,"Magnetisation [A/m]");
				}break;

				// magnetisation
				case MAGNETIC_FIELD:{
					lg->msg("magnetic field\n");
					const arma::Mat<double> Hn = get_field("H");
					vtk_data->set_nodedata(Hn,"Magnetic Field [A/m]");
				}break;

				// unrecognized output type
				default: break;
			}
		}

		// mesh done
		lg->msg(-2);

		// done exporting
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

	// export a vtk with times
	void CalcLine::write(cmn::ShLogPr lg){
		// check if data directory set
		if(output_dir_.empty())return;

		// header
		lg->msg(2,"%s%sWRITING OUTPUT FILES%s\n",KBLD,KGRN,KNRM);
		
		// check output directory
		if(output_times_.is_empty())rat_throw_line("output times are not set");

		// create output directory
		cmn::Extra::create_directory(output_dir_);

		// output filename
		std::string fname  = output_fname_;

		// report
		lg->msg(2,"%s%sVISUALISATION TOOLKIT%s\n",KBLD,KGRN,KNRM);

		// settings report
		display_settings(lg);

		// report
		lg->msg(2, "%swriting linedata%s\n",KBLU,KNRM);
		lg->msg("%s%4s %8s %s%s\n",KBLD,"id","time","filename",KNRM);

		// walk over timesteps
		for(arma::uword i=0;i<output_times_.n_elem;i++){
			// set time
			set_time(output_times_(i));

			// get data at this time
			ShVTKUnstrPr vtk_line = export_vtk_coord();

			// extend filename with index
			if(output_times_.n_elem!=1)fname = indexed_output_fname(output_fname_,i);

			// show in log
			lg->msg("%04llu %8.2e %s\n",i,get_time(),fname.c_str());

			// write data to file
			vtk_line->write(output_dir_ + fname + "_ln");
		}

		// return
		lg->msg(-6,"\n"); 
	}

	// get type
	std::string CalcLine::get_type(){
		return "mdl::calcline";
	}

	// method for serialization into json
	void CalcLine::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize fieldmap
		CalcFieldMap::serialize(js,list);

		// properties
		js["type"] = get_type();

		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
		
	}

	// method for deserialisation from json
	void CalcLine::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// serialize fieldmap
		CalcFieldMap::deserialize(js,list,factory_list);

		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list);
	}


}}
