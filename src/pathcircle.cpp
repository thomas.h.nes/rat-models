/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathcircle.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathCircle::PathCircle(){

	}

	// constructor with dimension input
	PathCircle::PathCircle(
		const double radius, const arma::uword num_sections, 
		const double element_size, const double offset){

		// set values with respective functions
		set_radius(radius);
		set_num_sections(num_sections);
		set_element_size(element_size);
		set_offset(offset);
	}

	// factory
	ShPathCirclePr PathCircle::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathCircle>();
	}

	// factory with dimension input
	ShPathCirclePr PathCircle::create(
		const double radius, const arma::uword num_sections, 
		const double element_size, const double offset){
		return std::make_shared<PathCircle>(radius,num_sections,element_size,offset);
	}

	// set element size 
	void PathCircle::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// set divisor
	void PathCircle::set_element_divisor(const arma::uword element_divisor){
		if(element_divisor<=0)rat_throw_line("divisor must be larger than zero");
		element_divisor_ = element_divisor;
	}

	// path offset
	void PathCircle::set_offset(const double offset){
		offset_ = offset;
	}

	// set radius
	void PathCircle::set_radius(const double radius){
		if(radius<=0)rat_throw_line("radius must be larger than zero");
		radius_ = radius;
	}

	// set radius
	void PathCircle::set_num_sections(const arma::uword num_sections){
		if(num_sections==0)rat_throw_line("number of sections must be larger than zero");
		num_sections_ = num_sections;
	}

	// get frame
	ShFramePr PathCircle::create_frame() const{
		// check input
		if(radius_==0)rat_throw_line("radius is not set");
		if(element_size_==0)rat_throw_line("element size is not set");
		if(num_sections_==0)rat_throw_line("number of sections is not set");

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();

		// add sections
		for(arma::uword i=0;i<num_sections_;i++){
			ShPathArcPr arc = PathArc::create(radius_,
				2*arma::datum::pi/num_sections_,element_size_);
			arc->set_offset(offset_);
			arc->set_element_divisor(element_divisor_);
			pathgroup->add_path(arc);
		}

		// move
		pathgroup->add_translation(radius_,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame();

		// apply transformations
		frame->apply_transformations(trans_);
		
		// return frame
		return frame;
	}

	// get type
	std::string PathCircle::get_type(){
		return "mdl::pathcircle";
	}

	// method for serialization into json
	void PathCircle::serialize(Json::Value &js, cmn::SList &list) const{
		// parent nodes
		Transformations::serialize(js,list);

		// settings
		js["type"] = get_type();
		js["num_sections"] = (unsigned int)num_sections_;
		js["radius"] = radius_;
		js["element_size"] = element_size_; 
		js["offset"] = offset_; 
	}

	// method for deserialisation from json
	void PathCircle::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent nodes
		Transformations::deserialize(js,list,factory_list);

		// settings
		set_num_sections(js["num_sections"].asUInt64());
		set_radius(js["radius"].asDouble());
		set_element_size(js["element_size"].asDouble()); 
		set_offset(offset_ = js["offset"].asDouble()); 
	}

}}