/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathclover.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathClover::PathClover(){

	}

	// constructor with dimension input
	PathClover::PathClover(
		const double ellstr1, const double ellstr2, 
		const double height, const double dpack, const double ell_trans, 
		const double str12, const double str34, const double element_size){
		set_ellstr1(ellstr1); set_ellstr2(ellstr2);
		set_height(height); set_dpack(dpack); set_ell_trans(ell_trans);
		set_str12(str12); set_str34(str34); set_element_size(element_size);
	}

	// factory
	ShPathCloverPr PathClover::create(){
		//return ShPathCloverPr(new PathClover);
		return std::make_shared<PathClover>();
	}

	// factory with dimension input
	ShPathCloverPr PathClover::create(
		const double ellstr1, const double ellstr2, 
		const double height, const double dpack, const double ell_trans, 
		const double str12, const double str34, const double element_size){
		return std::make_shared<PathClover>(ellstr1,ellstr2,height,dpack,ell_trans,str12,str34,element_size);
	}

	// set first length
	void PathClover::set_ellstr1(const double ellstr1){
		if(ellstr1<=0)rat_throw_line("first length must be larger than zero");
		ellstr1_ = ellstr1;
	}

	// set second length
	void PathClover::set_ellstr2(const double ellstr2){
		if(ellstr2<=0)rat_throw_line("second length must be larger than zero");
		ellstr2_ = ellstr2;
	}

	// set bridge height
	void PathClover::set_height(const double height){
		height_ = height;
	}

	// set spacing needed for winding pack
	void PathClover::set_dpack(const double dpack){
		if(dpack<0)rat_throw_line("pack thickness must be zero or larger");
		dpack_ = dpack;
	}

	// set spacing needed for winding pack
	void PathClover::set_ell_trans(const double ell_trans){
		if(ell_trans<0)rat_throw_line("transition length must be zero or larger");
		ell_trans_ = ell_trans;
	}

	// set first and second strength
	void PathClover::set_str12(const double str12){
		if(str12<=0)rat_throw_line("strength must larger than zero: " + std::to_string(str12));
		str12_ = str12;
	}

	void PathClover::set_str34(const double str34){
		if(str34<=0)rat_throw_line("strength must larger than zero: " + std::to_string(str34));
		str34_ = str34;
	}

	// set element size 
	void PathClover::set_element_size(const double element_size){
		element_size_ = element_size;
	}

	// set bending radius
	void PathClover::set_bending_radius(const double bending_radius){
		bending_radius_ = bending_radius;
	}

	// set bridge angle
	void PathClover::set_bridge_angle(const double bridge_angle){
		bridge_angle_ = bridge_angle;
	}

	// get first straight section length
	double PathClover::get_ellstr1() const{
		return ellstr1_;
	}

	// get second straight section length
	double PathClover::get_ellstr2() const{
		return ellstr2_;
	}

	// get frame
	ShFramePr PathClover::create_frame() const{
		// check input
		if(ellstr1_==0)rat_throw_line("first length is not set");
		if(ellstr2_==0)rat_throw_line("second length is not set");
		if(str12_==0)rat_throw_line("strength is not set");
		if(str34_==0)rat_throw_line("strength is not set");

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create(
			arma::Col<double>::fixed<3>{ellstr2_/2,0,0},
			arma::Col<double>::fixed<3>{0,1,0},
			arma::Col<double>::fixed<3>{1,0,0},
			arma::Col<double>::fixed<3>{0,0,1});

		// start and end vectors for cloverleaf end
		const arma::Col<double>::fixed<3> R0 = {0,-ell_trans_+dpack_,0};
		const arma::Col<double>::fixed<3> L0 = {0,1,0};
		const arma::Col<double>::fixed<3> N0 = {1,0,0};
		const arma::Col<double>::fixed<3> R1 = {-ell_trans_+dpack_,0,height_};
		const arma::Col<double>::fixed<3> L1 = {-std::cos(bridge_angle_),-std::sin(bridge_angle_),0};
		const arma::Col<double>::fixed<3> N1 = {std::sin(bridge_angle_),std::cos(bridge_angle_),0};

		// strengths
		const double str1 = str12_;
		const double str2 = str12_/2;
		const double str3 = -str34_;
		const double str4 = -str34_;

		// create quintic bezier spline with constant perimeter
		ShPathBezierPr path_clover1 = PathBezier::create(
			R0,L0,N0, R1,L1,N1, str1,str2,str3,str4, ell_trans_,element_size_,dpack_);
		path_clover1->set_planar_winding(true);
		path_clover1->set_num_sections(4);

		// create quintic bezier spline with constant perimeter
		// ShPathBezierPr path_clover2 = PathBezier::create(
		// 	R1,-L1,-N1, -R0,L0,N0, str1,str2,str3,str4, ell_trans_,element_size_,0);
		// path_clover2->set_planar_winding(true);
		ShPathGroupPr path_clover2 = PathGroup::create();
		path_clover2->add_path(path_clover1);
		path_clover2->add_reflect_yz();
		path_clover2->add_reverse();
		//path_clover2->add_flip();

		// calculate clover straight section length
		const double ellstr1 = ellstr1_/2 + dpack_ - ell_trans_;
		const double ellstr2 = ellstr2_/2 + dpack_ - ell_trans_;

		// create straight section
		ShPathPr path_straight1A, path_straight1B;
		if(bending_radius_==0){
			path_straight1A = PathStraight::create(ellstr1, element_size_);
			path_straight1B = path_straight1A;
		}

		// when straight section is curved
		else{
			const double arc_length = ellstr1/bending_radius_;
			path_straight1A = PathArc::create(bending_radius_-ellstr2_/2, -arc_length, element_size_);
			path_straight1B = PathArc::create(bending_radius_+ellstr2_/2, arc_length, element_size_);
		}

		// create bridged section
		ShPathPr path_straight2;
		if(bridge_angle_==0){
			path_straight2 = PathStraight::create(ellstr2, element_size_);
		}

		// 
		else{
			path_straight2 = PathArc::create(ellstr2/std::sin(bridge_angle_), -bridge_angle_, element_size_);
		}

		// add sections to the path group
		pathgroup->add_path(path_straight1A); 
		pathgroup->add_path(path_clover1); 
		pathgroup->add_path(path_straight2);
		pathgroup->add_path(path_straight2); 
		pathgroup->add_path(path_clover2); 
		pathgroup->add_path(path_straight1B);
		pathgroup->add_path(path_straight1B); 
		pathgroup->add_path(path_clover1); 
		pathgroup->add_path(path_straight2);
		pathgroup->add_path(path_straight2); 
		pathgroup->add_path(path_clover2); 
		pathgroup->add_path(path_straight1A);

		// move path
		//pathgroup->add_translation(ellstr2_/2,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame();

		// apply transformations
		frame->apply_transformations(trans_);
		
		// return frame
		return frame;
	}

	// get type
	std::string PathClover::get_type(){
		return "mdl::pathclover";
	}

	// method for serialization into json
	void PathClover::serialize(Json::Value &js, cmn::SList &list) const{
		Transformations::serialize(js,list);
		js["type"] = get_type();
		js["ell_trans"] = ell_trans_;
		js["dpack"] = dpack_;
		js["height"] = height_; 
		js["str12"] = str12_; 
		js["str34"] = str34_; 
		js["element_size"] = element_size_; 
		js["ellstr1"] = ellstr1_; 
		js["ellstr2"] = ellstr2_; 
		js["bending_radius"] = bending_radius_; 
		js["bridge_angle"] = bridge_angle_;
	}

	// method for deserialisation from json
	void PathClover::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		Transformations::deserialize(js,list,factory_list);
		set_ell_trans(js["ell_trans"].asDouble());
		set_dpack(js["dpack"].asDouble());
		set_height(js["height"].asDouble()); 
		set_str12(js["str12"].asDouble()); 
		set_str34(js["str34"].asDouble()); 
		set_element_size(js["element_size"].asDouble()); 
		set_ellstr1(js["ellstr1"].asDouble());
		set_ellstr2(js["ellstr2"].asDouble()); 
		set_bending_radius(js["bending_radius"].asDouble()); 
		set_bridge_angle(js["bridge_angle"].asDouble());
	}

}}
