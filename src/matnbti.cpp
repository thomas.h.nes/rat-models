/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "matnbti.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatNbTi::MatNbTi(){
		Ti_ = arma::regspace<arma::Col<double> >(1,300);
		ki_ = arma::Col<double>{0.026,0.06,0.112,0.176,0.24,0.304,0.37,0.436,0.503,0.57,0.63,0.688,0.746,0.803,0.858,0.913,0.968,1.02,1.08,1.13,1.19,1.24,1.3,1.36,1.42,1.47,1.53,1.59,1.64,1.7,1.76,1.81,1.87,1.93,1.98,2.04,2.09,2.15,2.2,2.26,2.32,2.37,2.43,2.48,2.54,2.59,2.64,2.7,2.75,2.81,2.87,2.92,2.98,3.03,3.09,3.15,3.2,3.26,3.31,3.37,3.43,3.48,3.54,3.59,3.65,3.71,3.76,3.82,3.87,3.93,3.99,4.04,4.1,4.15,4.21,4.27,4.32,4.38,4.43,4.49,4.55,4.6,4.66,4.72,4.77,4.83,4.88,4.93,4.99,5.04,5.09,5.14,5.19,5.23,5.28,5.33,5.37,5.41,5.46,5.5,5.54,5.59,5.63,5.67,5.71,5.75,5.79,5.83,5.87,5.91,5.95,5.99,6.03,6.06,6.1,6.14,6.17,6.21,6.24,6.28,6.31,6.35,6.38,6.41,6.45,6.48,6.51,6.54,6.57,6.6,6.63,6.66,6.69,6.72,6.75,6.78,6.81,6.84,6.87,6.89,6.92,6.95,6.97,7,7.03,7.05,7.08,7.1,7.13,7.15,7.17,7.2,7.22,7.24,7.27,7.29,7.31,7.33,7.35,7.38,7.4,7.42,7.44,7.46,7.48,7.5,7.52,7.54,7.56,7.58,7.6,7.62,7.63,7.65,7.67,7.69,7.71,7.73,7.74,7.76,7.78,7.8,7.82,7.83,7.85,7.87,7.88,7.9,7.92,7.93,7.95,7.97,7.98,8,8.02,8.03,8.05,8.07,8.08,8.1,8.12,8.14,8.16,8.18,8.2,8.22,8.24,8.26,8.28,8.3,8.32,8.34,8.36,8.38,8.4,8.42,8.44,8.46,8.48,8.5,8.51,8.53,8.55,8.57,8.59,8.61,8.63,8.64,8.66,8.68,8.7,8.72,8.73,8.75,8.77,8.78,8.8,8.82,8.83,8.85,8.87,8.88,8.9,8.91,8.93,8.94,8.96,8.97,8.99,9,9.01,9.02,9.04,9.05,9.06,9.07,9.08,9.09,9.11,9.12,9.13,9.14,9.15,9.16,9.17,9.18,9.19,9.2,9.21,9.22,9.23,9.24,9.25,9.26,9.27,9.28,9.29,9.3,9.31,9.32,9.33,9.34,9.34,9.35,9.36,9.37,9.38,9.39,9.4,9.41,9.42,9.43,9.44,9.44,9.45,9.46,9.47,9.48,9.49,9.5};
		Cpi_ = arma::Col<double>{0.00164,0.027,0.209,0.523,0.871,1.27,1.75,2.32,3,3.8,4.8,5.94,7.23,8.68,10.3,12.1,14,16.2,18.5,21,23.9,27,30.2,33.7,37.3,41.1,45.1,49.2,53.5,58,63,68.1,73.3,78.5,83.8,89.1,94.4,99.7,105,110,114,118,122,126,130,134,138,142,146,150,155,160,165,170,175,180,185,190,195,200,204,209,213,217,221,225,229,233,236,240,243,246,250,253,256,259,261,264,267,270,273,277,280,283,286,289,292,295,298,300,302,303,304,305,306,306,307,307,307,307,308,309,310,311,312,313,314,315,316,317,318,319,320,322,323,324,325,327,328,329,331,332,333,334,336,337,339,340,341,343,344,345,347,348,350,351,352,354,355,357,358,359,361,362,363,365,366,367,369,370,371,372,373,373,374,375,376,376,377,378,379,379,380,381,381,382,383,383,384,385,385,386,387,387,388,388,389,389,390,390,391,392,392,393,393,394,394,395,395,396,396,396,397,397,398,398,399,399,400,400,401,401,402,402,403,403,404,404,405,405,405,406,406,407,407,408,408,409,409,410,410,410,411,411,412,412,412,413,413,414,414,414,415,415,415,416,416,416,417,417,417,418,418,418,419,419,419,419,420,420,420,420,421,421,421,421,421,422,422,422,422,422,423,423,423,423,423,423,423,424,424,424,424,424,424,424,424,425,425,425,425,425,425,425,425,425,425,425,425,426,426,426,426,426,426,426,426,426,426,426};
	}

	// factory
	ShMatNbTiPr MatNbTi::create(){
		return std::make_shared<MatNbTi>();
	}

	// set LHC values
	void MatNbTi::set_nbti_LHC(){
		// electric field criterion
		E0_ = 1e-5; // [V/m]
		N_ = 50; // N-value

		// fitting parameters
		C0_ = 31.4; // [T]
		alpha_ = 0.63;
		beta_ = 1.0;
		gamma_ = 2.3;
		n_ = 1.7;

		// materialistic parameters
		Bc20_ = 14.5; // [T] upper critical magnetic flux at 0K
		Tc0_ = 9.2; // [K] critical temperature at zero magnetic flux
		Jref_ = 3000; // [A/mm^2]critical current density at 4.2K and 5T
	}

	// precalculate material properties for fast electric field calculations
	arma::Mat<double> MatNbTi::calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const{
		const arma::Mat<double> props = calc_critical_current_density(Bm,T,alpha);
		return props;
	}

	// calculate critical current
	arma::Row<double> MatNbTi::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &) const{
		// check input
		assert(Bm.n_cols==T.n_cols);
		assert(Bm.n_rows==T.n_rows);

		// copy B
		arma::Row<double> Blim = Bm;
		Blim(arma::find(Bm<0.1)).fill(0.1);

		// calculate scaled values
		const arma::Row<double> t = T/Tc0_;
		const arma::Row<double> Bc2 = Bc20_*(1.0-arma::pow(t,n_)); // Lubell's equation
		const arma::Row<double> b = Blim/Bc2;

		// bottura's equation
		arma::Mat<double> Jc = 1e6*Jref_*(C0_/Blim)%arma::pow(b,alpha_)%
			arma::pow(1.0-b,beta_)%arma::pow(1.0-arma::pow(t,n_),gamma_);

		// do not allow critical currents lower than zero
		Jc(arma::find(Bm>Bc2)).fill(0);
		Jc(arma::find(Bm==0)).fill(arma::datum::inf);

		// return critical current
		return Jc; // A/m^2
	}

	// calculate electric field
	arma::Row<double> MatNbTi::calc_current_density_fast(
		const arma::Row<double> &E, const arma::Mat<double> &props) const{
		// calculate critical current
		// const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law reversed
		return Jc%arma::pow(E/E0_,1.0/N_); // A/m^2
	}

	arma::Row<double> MatNbTi::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props) const{
		// calculate critical current
		// const arma::Row<double> Jc = calc_critical_current_density(Bm,T,alpha);
		assert(props.n_rows==1);

		// get critical current 
		const arma::Row<double> Jc = props;

		// power law
		return E0_*arma::pow(J/Jc,N_); // V/m
	}

	// copy
	// ShMaterialPr MatNbTi::copy() const{
	// 	// create new conductor 
	// 	ShMatNbTiPr con = MatNbTi::create();
		
	// 	// electric field criterion
	// 	con->E0_ = E0_; 
	// 	con->N_ = N_; 

	// 	// fitting parameters
	// 	con->C0_ = C0_; // [T]
	// 	con->alpha_ = alpha_;
	// 	con->beta_ = beta_;
	// 	con->gamma_ = gamma_;
	// 	con->n_ = n_;

	// 	// materialistic parameters
	// 	con->Bc20_ = Bc20_; 
	// 	con->Tc0_ = Tc0_; 
	// 	con->Jref_ = Jref_;

	// 	// return copy
	// 	return con;
	// }

	// calculate thermal conductivity in [W m^-1 K^-1]
	arma::Row<double> MatNbTi::calc_thermal_conductivity(
		const arma::Row<double> &/*Bm*/, const arma::Row<double> &T) const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!ki_.is_empty());

		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,ki_,T.t(),V,true);

		// reshape and return
		return V.t();
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Row<double> MatNbTi::calc_specific_heat(const arma::Row<double> &T)const{
		// check if arrays set
		assert(!Ti_.is_empty()); assert(!Cpi_.is_empty());
		
		// interpolate
		arma::Col<double> V;
		rat::cmn::Extra::lininterp1f(Ti_,Cpi_,T.t(),V,true);

		// reshape and return
		return density_*V.t();
	}

	// copy constructor
	ShMaterialPr MatNbTi::copy() const{
		return std::make_shared<MatNbTi>(*this);
	}

	// get type
	std::string MatNbTi::get_type(){
		return "mdl::matnbti";
	}

	// method for serialization into json
	void MatNbTi::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		js["E0"] = E0_;
		js["N"] = N_;
		js["C0"] = C0_;
		js["alpha"] = alpha_;
		js["beta"] = beta_;
		js["gamma"] = gamma_;
		js["n"] = n_;
		js["Bc20"] = Bc20_;
		js["Tc0"] = Tc0_;
		js["Jref"] = Jref_;
	}

	// method for deserialisation from json
	void MatNbTi::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		E0_ = js["E0"].asDouble();
		N_ = js["N"].asDouble();
		C0_ = js["C0"].asDouble();
		alpha_ = js["alpha"].asDouble();
		beta_ = js["beta"].asDouble();
		gamma_ = js["gamma"].asDouble();
		n_ = js["n"].asDouble();
		Bc20_ = js["Bc20"].asDouble();
		Tc0_ = js["Tc0"].asDouble();
		Jref_ = js["Jref"].asDouble();
	}


}}