/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "edges.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Edges::Edges(){

	}

	// constructor with input
	Edges::Edges(ShFramePr &frame, ShAreaPr &area){
		setup(frame, area);
	}

	// factory
	ShEdgesPr Edges::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Edges>();
	}

	// factory with input
	ShEdgesPr Edges::create(ShFramePr &frame, ShAreaPr &area){
		return std::make_shared<Edges>(frame, area);
	}

	// set drive ID
	void Edges::set_drive_id(const arma::uword drive_id){
		drive_id_ = drive_id;
	}

	// set drive ID
	arma::uword Edges::get_drive_id() const{
		return drive_id_;
	}

	// setup function
	void Edges::setup(ShFramePr &frame, ShAreaPr &area){
		// check input
		assert(frame!=NULL); assert(area!=NULL);

		// set generator and area
		gen_ = frame; area_ = area;

		// get number of sections
		const arma::uword num_sections = frame->get_num_sections();

		// get edges
		const arma::Col<arma::uword> c = area->get_corner_nodes();

		// get nodes
		const arma::Mat<double> Rn = area->get_nodes();

		// edge matrices
		Re_.set_size(c.n_elem, num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// walk over edges
			for(arma::uword j=0;j<c.n_elem;j++){
				// perform shift and store
				Re_(j,i) = frame->perform_shift(i,Rn(0,c(j)),Rn(1,c(j)));
			}
		}

		// get section and turn indices
		section_ = frame->get_section();
		turn_ = frame->get_turn();
	}

	// set operating temperature
	void Edges::set_operating_temperature(const double operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// get operating temperature
	double Edges::get_operating_temperature() const{
		return operating_temperature_;
	}

	// transformations
	void Edges::apply_transformations(const ShTransPrList &trans){
		for(arma::uword i=0;i<trans.n_elem;i++)
			apply_transformation(trans(i));
	}
		
	// apply a transformation
	void Edges::apply_transformation(const ShTransPr &trans){
		// apply transformations
		for(arma::uword j=0;j<Re_.n_rows;j++)
			for(arma::uword i=0;i<Re_.n_cols;i++)
				trans->apply_coords(Re_(j,i));
	}

	// calculate edge length
	arma::Mat<double> Edges::calc_ell() const{
		// allocate lengths
		arma::Mat<double> ell(Re_.n_rows,Re_.n_cols);

		// walk over edges
		for(arma::uword i=0;i<Re_.n_rows;i++){
			// walk over sections
			for(arma::uword j=0;j<Re_.n_cols;j++){
				ell(i,j) = arma::sum(cmn::Extra::vec_norm(Re_(i,j)));
			}
		}

		// return length matrix
		return ell;
	}


	// get xyz matrices
	void Edges::create_xyz(
		arma::field<arma::Mat<double> > &x, 
		arma::field<arma::Mat<double> > &y, 
		arma::field<arma::Mat<double> > &z) const{ 

		// count number sectoins and edges
		const arma::uword num_sections = Re_.n_cols;
		const arma::uword num_edges = Re_.n_rows;

		// allocate
		x.set_size(1,num_sections); 
		y.set_size(1,num_sections); 
		z.set_size(1,num_sections);

		// walk over sections
		for(arma::uword i=0;i<num_sections;i++){
			// allocate edge matrix for this section
			x(i).set_size(num_edges,Re_(0,i).n_cols);
			y(i).set_size(num_edges,Re_(0,i).n_cols);
			z(i).set_size(num_edges,Re_(0,i).n_cols);

			// walk over edges
			for(arma::uword j=0;j<num_edges;j++){
				// copy edge
				x(i).row(j) = Re_(j,i).row(0);
				y(i).row(j) = Re_(j,i).row(1);
				z(i).row(j) = Re_(j,i).row(2);
			}
		}
	}


	// export edges to freecad
	void Edges::export_freecad(cmn::ShFreeCADPr freecad) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("freecad currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<double> > x,y,z; create_xyz(x,y,z);

		// write to freecad
		freecad->write_edges(x,y,z,section_,turn_,myname_);
	}

	// export edges to freecad
	void Edges::export_opera(cmn::ShOperaPr opera) const{
		// get counters
		const arma::uword num_edges = Re_.n_rows;

		// make sure it is four edges
		if(num_edges!=4)rat_throw_line("opera currently only works with four edges");

		// allocate and get edge matrices
		arma::field<arma::Mat<double> > x,y,z; create_xyz(x,y,z);

		// calculate current density
		const double J = 0; //number_turns_*circuit_->get_operating_current()/area_crss_;

		// write edges with current density
		opera->write_edges(x,y,z,J);
	}

	// copy constructor
	ShEdgesPr Edges::copy() const{
		return std::make_shared<Edges>(*this);
	}

}}