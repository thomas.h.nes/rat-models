/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "driveac.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveAC::DriveAC(){

	}

	// constructor
	DriveAC::DriveAC(const double amplitude, const double frequency, const double phase, const arma::uword id){
		set_amplitude(amplitude); set_frequency(frequency); set_phase(phase); set_id(id);
	}

	// factory
	ShDriveACPr DriveAC::create(){
		return std::make_shared<DriveAC>();
	}

	// factory
	ShDriveACPr DriveAC::create(const double amplitude, const double frequency, const double phase, const arma::uword id){
		return std::make_shared<DriveAC>(amplitude,frequency,phase,id);
	}

	// set frequency
	void DriveAC::set_frequency(const double frequency){
		frequency_ = frequency;
	}

	// set frequency
	void DriveAC::set_phase(const double phase){
		phase_ = phase;
	}

	// set scaling
	void DriveAC::set_amplitude(const double amplitude){
		amplitude_ = amplitude;
	}

	// get current
	double DriveAC::get_scaling(const double time) const{
		return amplitude_*std::sin(2*arma::datum::pi*frequency_*time + phase_);
	}

	// get current derivative
	double DriveAC::get_dscaling(const double time) const{
		return 2*arma::datum::pi*frequency_*amplitude_*std::cos(2*arma::datum::pi*frequency_*time + phase_);
	}

	// get type
	std::string DriveAC::get_type(){
		return "mdl::driveac";
	}

	// method for serialization into json
	void DriveAC::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		js["amplitude"] = amplitude_;
		js["frequency"] = frequency_;
		js["phase"] = phase_;
	}

	// method for deserialisation from json
	void DriveAC::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		Drive::deserialize(js,list,factory_list);
		set_amplitude(js["amplitude"].asDouble());
		set_frequency(js["frequency"].asDouble());
		set_phase(js["phase"].asDouble());
	}

}}