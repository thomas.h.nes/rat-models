/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transbend.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransBend::TransBend(){

	}

	// constructor with vector input
	TransBend::TransBend(
		const double vx1, const double vy1, const double vz1, 
		const double vx2, const double vy2, const double vz2, 
		const double r){

		set_bending(vx1,vy1,vz1,vx2,vy2,vz2,r);
	}

	// constructor with vector input
	TransBend::TransBend(const arma::Col<double>::fixed<3> &V1, 
		const arma::Col<double>::fixed<3> &V2, const double r){
		set_bending(V1,V2,r);
	}

	// factory
	ShTransBendPr TransBend::create(){
		//return ShTransBendPr(new TransBend);
		return std::make_shared<TransBend>();
	}

	// factory with vector input
	ShTransBendPr TransBend::create(const double vx1, const double vy1, const double vz1, const double vx2, const double vy2, const double vz2, const double r){
		//return ShTransBendPr(new TransBend(vx1,vy1,vz1,vx2,vy2,vz2,r));
		return std::make_shared<TransBend>(vx1,vy1,vz1,vx2,vy2,vz2,r);
	}

	// factory with vector input
	ShTransBendPr TransBend::create(const arma::Col<double>::fixed<3> &V1, const arma::Col<double>::fixed<3> &V2, const double r){
		//return ShTransBendPr(new TransBend(V1,V2,r));
		return std::make_shared<TransBend>(V1,V2,r);
	}

	// set bending vectors and radius
	void TransBend::set_bending(
		const double vx1, const double vy1, const double vz1, 
		const double vx2, const double vy2, const double vz2, 
		const double r){

		// combine into vector
		const arma::Col<double>::fixed<3> V1 = {vx1,vy1,vz1};
		const arma::Col<double>::fixed<3> V2 = {vx2,vy2,vz2};

		// set vector and radius
		set_bending(V1,V2,r);
	}

	// set bending vectors and radius
	void TransBend::set_bending(
		const arma::Col<double>::fixed<3> &V1, 
		const arma::Col<double>::fixed<3> &V2, 
		const double r){

		// set vectors and radius
		V1_ = V1; V2_ = V2; r_ = r;

		// normalize vectors
		V1_.each_row() /= cmn::Extra::vec_norm(V1_);
		V2_.each_row() /= cmn::Extra::vec_norm(V2_);

		// check orthogonality of vectors
		assert(arma::as_scalar(cmn::Extra::dot(V1_,V2_))<1e-6);
	}

	// apply bending to coordinates only
	void TransBend::apply_coords(arma::Mat<double> &R) const{
		// skip if radius is set to zero
		if(r_==0)return;

		// calcultate third vector
		arma::Col<double>::fixed<3> V3 = cmn::Extra::cross(V1_,V2_);

		// calculate distance along vectors
		arma::Mat<double> V1ext(3,R.n_cols); V1ext.each_col() = V1_; 
		arma::Row<double> u = cmn::Extra::dot(R,V1ext);

		arma::Mat<double> V2ext(3,R.n_cols); V2ext.each_col() = V2_; 
		arma::Row<double> v = cmn::Extra::dot(R,V2ext);

		arma::Mat<double> V3ext(3,R.n_cols); V3ext.each_col() = V3; 
		arma::Row<double> w = cmn::Extra::dot(R,V3ext);

		// calculate angle
		arma::Row<double> angle = arma::datum::pi/2 - v/r_;

		// perform bending
		v = (r_ + w)%arma::cos(angle);
		w = (r_ + w)%arma::sin(angle) - r_;

		// transform back to coordinates
		R = V1ext.each_row()%u + V2ext.each_row()%v + V3ext.each_row()%w;
	}

	// apply transformation to coordinates and vectors
	void TransBend::apply_vectors(
		const arma::Mat<double> &R, arma::Mat<double> &V, const char) const{
		// skip if radius is set to zero
		if(r_==0)return;
		
		// calcultate third vector
		arma::Col<double>::fixed<3> V3 = cmn::Extra::cross(V1_,V2_);

		// calculate distance along vectors
		arma::Mat<double> V1ext(3,R.n_cols); V1ext.each_col() = V1_; 
		arma::Row<double> u = cmn::Extra::dot(R,V1ext);

		arma::Mat<double> V2ext(3,R.n_cols); V2ext.each_col() = V2_; 
		arma::Row<double> v = cmn::Extra::dot(R,V2ext);

		arma::Mat<double> V3ext(3,R.n_cols); V3ext.each_col() = V3; 
		arma::Row<double> w = cmn::Extra::dot(R,V3ext);

		// calculate angle
		arma::Row<double> angle = arma::datum::pi/2 - v/r_;

		// perform bending
		v = (r_ + w)%arma::cos(angle);
		w = (r_ + w)%arma::sin(angle) - r_;

		// rotate angles
		for(arma::uword i=0;i<V.n_cols;i++){
			// get local angle
			double alpha = angle(i) - arma::datum::pi/2;

			// setup local rotation matrix
			arma::Mat<double>::fixed<3,3> M = 
				cmn::Extra::create_rotation_matrix(V1_(0),V1_(1),V1_(2),alpha);

			// rotate
			V.col(i) = M*V.col(i);
		}
	}

	// get type
	std::string TransBend::get_type(){
		return "mdl::transbend";
	}

	// method for serialization into json
	void TransBend::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		for(arma::uword i=0;i<3;i++)js["vector1"].append(V1_(i));
		for(arma::uword i=0;i<3;i++)js["vector2"].append(V2_(i));
		js["radius"] = r_;
	}

	// method for deserialisation from json
	void TransBend::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		for(arma::uword i=0;i<3;i++)V1_(i) = js["vector1"].get(i,0).asDouble();
		for(arma::uword i=0;i<3;i++)V2_(i) = js["vector2"].get(i,0).asDouble();
		r_ = js["radius"].asDouble();
	}

}}