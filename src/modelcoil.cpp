/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "modelcoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	ModelCoil::ModelCoil(){

	}

	// constructor
	ModelCoil::ModelCoil(ShPathPr base, ShCrossPr crss, ShMaterialPr material){
		set_base(base); set_cross(crss); set_material(material);
	}


	// factory
	ShModelCoilPr ModelCoil::create(){
		return std::make_shared<ModelCoil>();
	}

	// factory immediately setting base and cross section
	ShModelCoilPr ModelCoil::create(ShPathPr base, ShCrossPr crss, ShMaterialPr material){
		return std::make_shared<ModelCoil>(base,crss,material);
	}

	// current sharing 
	void ModelCoil::set_enable_current_sharing(const bool enable_current_sharing){
		enable_current_sharing_ = enable_current_sharing;
	}

	// set operating current 
	void ModelCoil::set_operating_current(const double operating_current){
		operating_current_ = operating_current;
	}

	// set material
	void ModelCoil::set_material(ShMaterialPr material){
		assert(material!=NULL);
		material_ = material;
	}

	// get material
	ShMaterialPr ModelCoil::get_material() const{
		assert(material_!=NULL);
		return material_;
	}

	// set number of turns
	void ModelCoil::set_number_turns(const double number_turns){
		number_turns_ = number_turns;
	}

	// set number of turns
	void ModelCoil::set_softening(const double softening){
		if(softening_<0)rat_throw_line("softening must be zero or larger");
		softening_ = softening;
	}


	// getting of coil number of turns
	double ModelCoil::get_number_turns() const{
		return number_turns_;
	}

	void ModelCoil::set_use_volume_elements(const bool use_volume_elements){
		use_volume_elements_ = use_volume_elements;
	}

	// getting of coil number of turns
	// void ModelCoil::set_num_gauss(const arma::sword num_gauss){
	// 	if(num_gauss==0)rat_throw_line("number of gauss points can not be zero.");
	// 	num_gauss_ = num_gauss;
	// }



	// factory for mesh objects
	ShMeshPrList ModelCoil::create_mesh() const{
		// allocate mesh data
		ShMeshCoilPr mesh = MeshCoil::create();

		// fill mesh data
		setup_mesh(mesh);

		// copy properties
		mesh->set_material(get_material()->copy());
		mesh->set_operating_current(operating_current_);
		mesh->set_number_turns(number_turns_);
		mesh->set_enable_current_sharing(enable_current_sharing_);
		mesh->set_softening(softening_);
		mesh->set_use_volume_elements(use_volume_elements_);
		
		// combine into list
		ShMeshPrList meshes(1);
		meshes(0) = mesh;

		// return mesh data
		return meshes;
	}

	// factory for mesh objects
	ShEdgesPrList ModelCoil::create_edge() const{
		// allocate edge data
		ShEdgesCoilPr edge = EdgesCoil::create();

		// fill edge data
		setup_edge(edge);

		// copy properties
		edge->set_material(get_material()->copy());
		edge->set_operating_current(operating_current_);
		edge->set_number_turns(number_turns_);

		// combine into list
		ShEdgesPrList edges(1);
		edges(0) = edge;

		// return edge data
		return edges;
	}


	// get type
	std::string ModelCoil::get_type(){
		return "mdl::modelcoil";
	}

	// method for serialization into json
	void ModelCoil::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		ModelMesh::serialize(js,list);

		// properties
		js["type"] = get_type();
		// js["num_gauss"] = (int)num_gauss_;
		js["enable_current_sharing"] = enable_current_sharing_;
		js["softening"] = softening_;
		js["number_turns"] = number_turns_;
		js["operating_current"] = operating_current_;

		// subnodes
		//js["circ"] = cmn::Node::serialize_node(circ_, list);
		js["material"] = cmn::Node::serialize_node(material_, list);
	}

	// method for deserialisation from json
	void ModelCoil::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		ModelMesh::deserialize(js,list,factory_list);

		// properties
		// set_num_gauss(js["num_gauss"].asInt64());
		set_enable_current_sharing(js["enable_current_sharing"].asBool());
		set_softening(js["softening"].asDouble());
		set_number_turns(js["number_turns"].asDouble());
		set_operating_current(js["operating_current"].asDouble());

		// subnodes
		//circ_ = cmn::Node::deserialize_node<Circuit>(js["circ"], list, factory_list);
		material_ = cmn::Node::deserialize_node<Material>(js["material"], list, factory_list);
	}

	// write current elements to VTK for checking
	void ModelCoil::write_elements(const std::string &fname) const{
		// get coil meshes
		rat::mdl::ShMeshPrList meshes = create_mesh();
		rat::mdl::ShMeshCoilPrList cmshes = rat::mdl::Extra::filter<rat::mdl::MeshCoil>(meshes);
		
		// get source elements
		rat::fmm::ShCurrentSourcesPr src = cmshes(0)->create_current_sources(2);
		arma::Mat<double> Rs = src->get_source_coords();
		arma::Mat<double> dRs = src->get_direction();
		arma::Row<double> Is = src->get_source_currents();

		// create vtk file
		rat::mdl::ShVTKUnstrPr vtk = rat::mdl::VTKUnstr::create();
		vtk->set_nodes(Rs);
		vtk->set_elements(arma::regspace<arma::Row<arma::uword> >(0,Rs.n_cols-1),1);
		vtk->set_nodedata(dRs,"direction [m]");
		vtk->set_nodedata(dRs.each_row()%Is,"current [Am]");

		// write
		vtk->write(fname);
	}


}}