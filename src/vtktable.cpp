/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	VTKTable::VTKTable(){
		vtk_table_ = vtkSmartPointer<vtkTable>::New();

	}

	// factory
	ShVTKTablePr VTKTable::create(){
		return std::make_shared<VTKTable>();
	}

	// write mesh
	void VTKTable::set_num_rows(const arma::uword num_rows){
		vtk_table_->SetNumberOfRows(num_rows);
	}

	// write data at nodes
	void VTKTable::set_data(const arma::Col<double> &v, const std::string &name){
		// create VTK array
		vtkSmartPointer<vtkDoubleArray> vvtk = vtkDoubleArray::New();
		
		// set data name	
		vvtk->SetName(name.c_str());
		
		// allocate
		vvtk->SetNumberOfComponents(1); 
		vvtk->SetNumberOfValues(v.n_elem);

		// copy data into array
		for(arma::uword i=0;i<v.n_elem;i++)
			vvtk->SetValue(i,v(i));

		// add to VTK
		vtk_table_->AddColumn(vvtk);
	}

	// write output file
	void VTKTable::write(const std::string fname, cmn::ShLogPr lg){
		// display file settings and type
		std::string fname_ext = fname + ".vtt";
		lg->msg(2,"%s%sWriting VTK File%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sFile Settings%s\n",KBLU,KNRM);
		lg->msg("filename: %s%s%s\n",KYEL,fname_ext.c_str(),KNRM);
		lg->msg("type: table (vtt)\n");

		// create writer
		vtkSmartPointer<vtkXMLTableWriter> writer =  
			vtkSmartPointer<vtkXMLTableWriter>::New();
		writer->SetFileName(fname_ext.c_str());
		writer->SetInputData(vtk_table_);
		
		// write data
		writer->Write();

		// done writing VTK table
		lg->msg(-2);
		lg->msg(-2,"\n");
	}

}}