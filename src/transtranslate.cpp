/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transtranslate.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	TransTranslate::TransTranslate(){
		set_vector(0.0,0.0,0.0);
	}

	TransTranslate::TransTranslate(const arma::Col<double>::fixed<3> &dR){
		set_vector(dR);
	}

	TransTranslate::TransTranslate(const double dx, const double dy, const double dz){
		set_vector(dx,dy,dz);
	}

	// factory methods
	ShTransTranslatePr TransTranslate::create(){
		//return ShTransTranslatePr(new TransTranslate);
		return std::make_shared<TransTranslate>();
	}

	ShTransTranslatePr TransTranslate::create(const arma::Col<double>::fixed<3> &dR){
		//return ShTransTranslatePr(new TransTranslate(dR));
		return std::make_shared<TransTranslate>(dR);
	}

	ShTransTranslatePr TransTranslate::create(const double dx, const double dy, const double dz){
		//return ShTransTranslatePr(new TransTranslate(dx,dy,dz));
		return std::make_shared<TransTranslate>(dx,dy,dz);
	}

	// set translation vector
	void TransTranslate::set_vector(const arma::Col<double>::fixed<3> &dR){
		dR_ = dR;
	}

	// set translation vector from components
	void TransTranslate::set_vector(const double dx, const double dy, const double dz){
		// create vector
		const arma::Col<double>::fixed<3> dR = {dx,dy,dz};

		// call overloaded function
		set_vector(dR);
	}

	// apply to coordinates only
	void TransTranslate::apply_coords(arma::Mat<double> &R) const{
		// apply to coordinate
		R.each_col() += dR_;
	}

	// get type
	std::string TransTranslate::get_type(){
		return "mdl::transtranslate";
	}

	// method for serialization into json
	void TransTranslate::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
		for(arma::uword i=0;i<3;i++)js["vector"].append(dR_(i));
	}

	// method for deserialisation from json
	void TransTranslate::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		for(arma::uword i=0;i<3;i++)dR_(i) = js["vector"].get(i,0).asDouble();
	}

}}