/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathrutherford.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	PathRutherford::PathRutherford(){

	}

	// constructor with input of base path
	PathRutherford::PathRutherford(ShPathPr base){
		set_base(base);
	}

	// factory methods
	ShPathRutherfordPr PathRutherford::create(){
		return std::make_shared<PathRutherford>();
	}

	// factory with input of base path
	ShPathRutherfordPr PathRutherford::create(ShPathPr base){
		return std::make_shared<PathRutherford>(base);
	}

	// set base path
	void PathRutherford::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base points to zero");
		base_ = base;
	}

	// update function
	ShFramePr PathRutherford::create_frame() const{
		// check input
		if(base_==NULL)rat_throw_line("base is not set");
		
		// create frame
		ShFramePr frame = base_->create_frame();
		arma::field<arma::Mat<double> > Rf = frame->get_coords();
		const arma::uword num_section = Rf.n_elem;

		// calculate length array
		double ell_accu = 0;
		arma::field<arma::Row<double> > ell(Rf.n_elem);
		for(arma::uword i=0;i<num_section;i++){
			ell(i) = arma::cumsum(rat::cmn::Extra::vec_norm(
				Rf(i).tail_cols(Rf(i).n_cols-1)-Rf(i).head_cols(Rf(i).n_cols-1))) + ell_accu;
			ell_accu = ell(i)(ell(i).n_elem-1);
		}

		// calculate properties
		double width = u2_-u1_, thickness = v2_-v1_;
		double omega = 2*thickness*(thickness+width)/
			std::sqrt(-num_strand_*num_strand_*thickness*
			thickness + 4*thickness*thickness + 
			8*thickness*width + 4*width*width);
		double pitch = num_strand_*omega;
		double alpha = std::atan((2*width + 2*thickness)/pitch);
		double dw = width/std::tan(alpha);
		double dt = thickness/std::tan(alpha);

		// draw single pitch
		arma::Row<double> w = {0,dw,dw+dt,2*dw+dt,2*dw+2*dt};
		arma::Row<double> u = {0,dw,dw,0,0};
		arma::Row<double> v = {0,0,dt,dt,0};
		
		// create strands
		arma::field<arma::Mat<double> > Rs(num_strand_,num_section);

		// walk over sections
		for(arma::uword k=0;k<num_section;k++){
			// calculate local position within pitch
			arma::Row<double> local_ell(ell(k)); local_ell.zeros();
			for(arma::uword j=0;j<ell(k).n_elem;j++)
				local_ell(j) = std::fmod(ell(k)(j),pitch);

			// walk over strands
			for(arma::uword i=0;i<num_strand_;i++){
				// allocate
				arma::Col<double> us,vs;

				// interpolate
				arma::interp1(w.t(),u.t(),local_ell.t(),us);
				arma::interp1(w.t(),u.t(),local_ell.t(),vs);

				// combine
				Rs(k,i) = arma::join_vert(us.t(),vs.t(),ell(i));
			}
		}

		// num_turns = 1.0/num_strand_;
		return frame;
	}

	// get type
	std::string PathRutherford::get_type(){
		return "mdl::pathrutherford";
	}

	// method for serialization into json
	void PathRutherford::serialize(Json::Value &js, cmn::SList &list) const{
		// serialize parents first 
		// this allows the type to be overridden
		Transformations::serialize(js,list);

		// properties
		
		// subnodes
		js["base"] = cmn::Node::serialize_node(base_, list);
	}

	// method for deserialisation from json
	void PathRutherford::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent objects
		Transformations::deserialize(js,list,factory_list);

		// properties
		
		// subnodes
		base_ = cmn::Node::deserialize_node<Path>(js["base"], list, factory_list);
	}

}}
