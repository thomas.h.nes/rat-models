/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "transreverse.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructors
	TransReverse::TransReverse(){

	}

	// factory methods
	ShTransReversePr TransReverse::create(){
		return std::make_shared<TransReverse>();
	}

	// apply to coordinates only
	void TransReverse::apply_coords(arma::Mat<double> &R) const{
		R = arma::fliplr(R);
	}

	// apply reversion
	void TransReverse::apply_vectors(const arma::Mat<double> &, arma::Mat<double> &V, const char str) const{
		// invert 
		V = arma::fliplr(V);

		// inverse direction
		if(str=='L' || str=='D')V *= -1;
	}

	// apply reversion to mesh elements
	void TransReverse::apply_mesh(arma::Mat<arma::uword> &n, const arma::uword num_nodes) const{
		// swap first face with second face
		// for(arma::uword i=0;i<n.n_rows/2;i++)
		// 	n.swap_rows(i,n.n_rows-i-1);
		for(arma::uword i=0;i<n.n_rows/2;i++)
			n.swap_rows(i,n.n_rows/2+i);

		// opposite indexing
		n = num_nodes - n - 1;
	}

	// apply to vectors for field arrays
	void TransReverse::apply_coords(
		arma::field<arma::Mat<double> > &R) const{
		// reverse everything
		cmn::Extra::reverse_field(R);
	}


	// apply to vectors for field arrays
	void TransReverse::apply_vectors(
		const arma::field<arma::Mat<double> > &, 
		arma::field<arma::Mat<double> > &V, const char str) const{
		// reverse everything
		cmn::Extra::reverse_field(V);

		// inverse direction
		if(str=='L' || str=='D')
			for(arma::uword i=0;i<V.n_elem;i++)
				V(i) *= -1;
	}

	// get type
	std::string TransReverse::get_type(){
		return "mdl::transreverse";
	}

	// method for serialization into json
	void TransReverse::serialize(Json::Value &js, cmn::SList &) const{
		js["type"] = get_type();
	}

}}