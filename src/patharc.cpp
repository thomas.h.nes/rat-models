/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "patharc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathArc::PathArc(){

	}

	// constructor with dimension input
	PathArc::PathArc(
		const double radius, const double arc_length, 
		const double element_size, const double offset){
		// set parameters
		set_radius(radius); set_element_size(element_size);
		set_arc_length(arc_length);	set_offset(offset);
	}

	// factory
	ShPathArcPr PathArc::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<PathArc>();
	}

	// factory with dimension input
	ShPathArcPr PathArc::create(
		const double radius, const double arc_length, 
		const double element_size, const double offset){
		return std::make_shared<PathArc>(radius,arc_length,element_size,offset);
	}

	// set transverse
	void PathArc::set_transverse(const bool transverse){
		transverse_ = transverse;
	}

	// set path offset
	void PathArc::set_offset(const double offset){
		offset_ = offset;
	}

	// set element size
	void PathArc::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// set divisor
	void PathArc::set_element_divisor(const arma::uword element_divisor){
		if(element_divisor<=0)rat_throw_line("divisor must be larger than zero");
		element_divisor_ = element_divisor;
	}

	// set element size
	void PathArc::set_arc_length(const double arc_length){
		if(arc_length==0)rat_throw_line("arc length can not be zero");
		arc_length_ = arc_length;
	}

	// set element size
	void PathArc::set_radius(const double radius){
		if(radius<=0)rat_throw_line("radius must be larger than zero");
		radius_ = radius;
	}

	// method for creating the frame
	ShFramePr PathArc::create_frame() const{
		// check input again
		if(arc_length_==0)rat_throw_line("arc length is not set");
		if(radius_==0)rat_throw_line("radius is not set");
		if(element_size_==0)rat_throw_line("element size is not set");

		// decide which part of the circle to use
		double uoff,poff,rhos;
		if(arc_length_<0){
			rhos = -radius_;
			uoff = radius_; 
			poff = -std::min(0.0,offset_);
		}else{
			rhos = radius_; 
			uoff = -radius_; 
			poff = std::max(0.0,offset_);
		}

		// calculate number of nodes
		arma::uword num_nodes = std::max(2,(int)std::ceil(std::abs(arc_length_)*(radius_+poff)/element_size_)+1);
		while((num_nodes-1)%element_divisor_!=0)num_nodes++;

		// create cylindrical coordinates for circle
		const arma::Row<double> rhovec = arma::linspace<arma::Row<double> >(rhos,rhos,num_nodes);
		const arma::Row<double> thetavec = arma::linspace<arma::Row<double> >(0,arc_length_,num_nodes);

		// allocate
		arma::Mat<double> R(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> L(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> D(3,num_nodes,arma::fill::zeros);
		arma::Mat<double> N(3,num_nodes,arma::fill::zeros);

		// bend in transverse direction
		if(transverse_){
			// coordinates
			R.row(1) = rhovec%arma::sin(thetavec);
			R.row(2) = rhovec%arma::cos(thetavec)+uoff;

			// direction
			L.row(1) = arma::cos(thetavec);
			L.row(2) = -arma::sin(thetavec);

			// normal vector
			N.row(0).fill(1.0);

			// set remaining orientation vector
			D = cmn::Extra::cross(N,L);
		}

		// bend in normal direction
		else{
			// coordinates
			R.row(0) = rhovec%arma::cos(thetavec)+uoff;
			R.row(1) = rhovec%arma::sin(thetavec);

			// direction
			L.row(0) = -arma::sin(thetavec);
			L.row(1) = arma::cos(thetavec);
				
			// direction vector
			D.row(2).fill(1.0);

			// set remaining orientation vector
			N = cmn::Extra::cross(L,D);
		}

		// create frame and return
		return Frame::create(R,L,N,D);
	}

	// get type
	std::string PathArc::get_type(){
		return "mdl::patharc";
	}

	// method for serialization into json
	void PathArc::serialize(Json::Value &js, cmn::SList &) const{
		// properties
		js["type"] = get_type();
		js["transverse"] = transverse_;
		js["offset"] = offset_;
		js["element_size"] = element_size_;
		js["radius"] = radius_;
		js["arc_length"] = arc_length_;
	}

	// method for deserialisation from json
	void PathArc::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// properties
		set_transverse(js["transverse"].asBool());
		set_offset(js["offset"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_radius(js["radius"].asDouble());
		set_arc_length(js["arc_length"].asDouble());
	}

}}

