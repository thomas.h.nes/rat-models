/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathoffset.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathOffset::PathOffset(){

	}

	// constructor with specification
	PathOffset::PathOffset(ShPathPr base, const double noff, const double doff){
		set_base(base);	set_noff(noff); set_doff(doff);
	}

	// factory
	ShPathOffsetPr PathOffset::create(){
		return std::make_shared<PathOffset>();
	}

	// constructor with dimensions and element size
	ShPathOffsetPr PathOffset::create(ShPathPr base, const double noff, const double doff){
		return std::make_shared<PathOffset>(base,noff,doff);
	}

	// set base 
	void PathOffset::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("base path points to zero");
		base_ = base;
	}

	// set offset in normal direction
	void PathOffset::set_noff(const double noff){
		noff_ = noff;
	}

	// set offset in transverse direction
	void PathOffset::set_doff(const double doff){
		doff_ = doff;
	}

	// get frame
	ShFramePr PathOffset::create_frame() const{
		// check if base path was set
		if(base_==NULL)rat_throw_line("base path is not set");

		// create frame
		ShFramePr frame = base_->create_frame();

		// copy coordinates
		arma::field<arma::Mat<double> > R = frame->get_coords();
		const arma::field<arma::Mat<double> > L = frame->get_direction();
		const arma::field<arma::Mat<double> > D = frame->get_transverse();
		const arma::field<arma::Mat<double> > N = frame->get_normal();
		const arma::field<arma::Mat<double> > B = frame->get_block();

		// move frame along N and D
		for(arma::uword i=0;i<R.n_cols;i++)
			R(i) += noff_*N(i) + doff_*D(i);

		// create offset frame
		ShFramePr offset_frame = Frame::create(R,L,N,D,B);

		// conserve location
		offset_frame->set_location(frame->get_section(), frame->get_turn());

		// create new frame
		return offset_frame;
	}

	// get type
	std::string PathOffset::get_type(){
		return "mdl::pathoffset";
	}

	// method for serialization into json
	void PathOffset::serialize(Json::Value &js, cmn::SList &list) const{
		// type
		js["type"] = get_type();

		// properties
		js["base"] = cmn::Node::serialize_node(base_,list);
		js["noff"] = noff_;
		js["doff"] = doff_;
	}

	// method for deserialisation from json
	void PathOffset::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		set_base(cmn::Node::deserialize_node<Path>(js["base"],list,factory_list));
		set_noff(js["noff"].asDouble()); set_doff(js["doff"].asDouble());
	}


}}