/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "matcopper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatCopper::MatCopper(){
		
	}

	// default constructor
	MatCopper::MatCopper(const double RRR){
		set_RRR(RRR);
	}

	// factory
	ShMatCopperPr MatCopper::create(){
		return std::make_shared<MatCopper>();
	}

	// factory
	ShMatCopperPr MatCopper::create(const double RRR){
		return std::make_shared<MatCopper>(RRR);
	}

	// default settings
	void MatCopper::set_copper_OFHC_RRR50(){
		// tc_params_ = arma::Row<double>{1.8743, -0.41538, -0.6018, 0.13294, 0.26426, -0.0219, -0.051276, 0.0014871, 0.003723};
		RRR_ = 50;
	}

	// default settings
	void MatCopper::set_copper_OFHC_RRR100(){
		// tc_params_ = arma::Row<double>{2.2154, -0.47461, -0.88068, 0.13871, 0.29505, -0.02043, -0.04831, 0.001281, 0.003207};
		RRR_ = 100;
	}

	// default settings
	void MatCopper::set_copper_OFHC_RRR150(){
		// tc_params_ = arma::Row<double>{2.3797,-0.4918, -0.98615, 0.13942, 0.30475, -0.019713, -0.046897, 0.0011969, 0.0029988};
		RRR_ = 150;
	}

	// default settings
	void MatCopper::set_copper_OFHC_RRR300(){
		// tc_params_ = arma::Row<double>{1.357, 0.3981, 2.669, -0.1346, -0.6683, 0.01342, 0.05773, 0.0002147, 0};
		RRR_ = 300;
	}

	// default settings
	void MatCopper::set_copper_OFHC_RRR500(){
		// tc_params_ = arma::Row<double>{2.8075, -0.54074, -1.2777, 0.15362, 0.36444, -0.02105, -0.051727, 0.0012226, 0.0030964};
		RRR_ = 500;
	}

	// set triple-R
	void MatCopper::set_RRR(const double RRR){
		RRR_ = RRR;
	}

	// calculate thermal conductivity in [W m^-1 K^-1]
	arma::Row<double> MatCopper::calc_thermal_conductivity(
		const arma::Row<double> &Bm, const arma::Row<double> &T) const{
		// lorenz number (for Wiedemann Franz Law)
		// https://en.wikipedia.org/wiki/Wiedemann%E2%80%93Franz_law
		const double L_ = 2.44e-8; // W Ohm K^-2

		// calculate electrical resistivity conductivity
		const arma::Row<double> sigma = calc_conductivity(Bm,T);

		// relate to electrical resistivity (using Wiedemann Franz Law)
		const arma::Row<double> k = (L_*T)%sigma; 

		// return calculate conductivity
		return k; // W/(m-K)
	}

	// calculate electrical conductivity output in [1/(Ohm m)]
	arma::Row<double> MatCopper::calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const{
		// coefficients
		const double C0 = 1.7;
		const double C1 = 2.33e9; 
		const double C2 = 9.57e5; 
		const double C3 = 163;

		// Equation [Ohm.m]
		const arma::Row<double> TTT = T%T%T; 
		const arma::Row<double> TTTTT = TTT%T%T;
		const arma::Row<double> rho = (C0/RRR_ + 1.0/(C1/TTTTT + C2/TTT + C3/T))*1e-8 + Bm*(0.37 + 0.0005*RRR_)*1e-10;

		// return calculated conductivity
		return 1.0/rho; // 1/(Ohm m)
	}

	// precalculate material properties for fast electric field calculations
	arma::Mat<double> MatCopper::calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &/*alpha*/) const{
		const arma::Mat<double> props = calc_conductivity(Bm,T);
		return props;
	}


	// calculate current density from electric field
	arma::Row<double> MatCopper::calc_current_density_fast(
		const arma::Row<double> &E, const arma::Mat<double> &props) const{

		// calculate resistivity in W/(mK)
		// const arma::Row<double> rho = calc_resistivity(Bm,T);
		assert(props.n_rows==1);

		// get sigma
		const arma::Row<double> sigma = props;

		// calculate current density
		const arma::Row<double> J = E%sigma; // A/m^2

		// return current density
		return J;
	}

	// calculate electric field from current density
	arma::Row<double> MatCopper::calc_electric_field_fast(
		const arma::Row<double> &J, const arma::Mat<double> &props) const{
		
		// calculate resistivity in W/(mK)
		assert(props.n_rows==1);

		// get sigma
		const arma::Row<double> sigma = props;

		// return electric field
		const arma::Row<double> E = J/sigma; // V/m

		// return electric field
		return E;
	}

	// copy constructor
	ShMaterialPr MatCopper::copy() const{
		return std::make_shared<MatCopper>(*this);
	}

	// specific heat output in [J m^-3 K^-1]
	// source NIST: https://trc.nist.gov/cryogenics/materials/OFHC%20Copper/OFHC_Copper_rev1.htm
	arma::Row<double> MatCopper::calc_specific_heat(const arma::Row<double> &T)const{
		// check range
		// if(arma::any(T<4))rat_throw_line("temperature below range");
		// if(arma::any(T>300))rat_throw_line("temperature beyond range");

		// coefficients of fit
		arma::Row<double> ft = {-1.91844, -0.15973, 8.61013, -18.996, 21.9661, -12.7328, 3.54322, -0.3797};
		
		// calculate and return
		arma::Row<double> A(T); A.zeros();
		arma::Row<double> log10T = arma::log10(T);
		arma::Row<double> plt(T); plt.fill(1.0);
		for(arma::uword i=0;i<ft.n_elem;i++){
			A += ft(i)*plt; plt%=log10T;
		}

		// 10^A and multiply with density
		arma::Row<double> sh = density_*arma::exp10(A);

		// extrapolation beyond end
		const arma::Row<arma::uword> extrap = arma::find(T>300).t();
		if(!extrap.is_empty()){
			const arma::Row<double> Te = {298.0,300};
			const arma::Row<double> Cpe = calc_specific_heat(Te);
			sh.cols(extrap) = Cpe(1) + (T.cols(extrap)-Te(1))*((Cpe(1)-Cpe(0))/(Te(1)-Te(0)));
		}

		// fix value when below range
		const arma::Row<arma::uword> idx = arma::find(T<4.0).t();
		if(!idx.is_empty()){
			sh.cols(idx) = calc_specific_heat(arma::Row<double>(idx.n_elem,arma::fill::ones)*(4.0+1e-10));
		}

		// return answer
		return sh;
	}

	// get type
	std::string MatCopper::get_type(){
		return "mdl::matcopper";
	}

	// method for serialization into json
	void MatCopper::serialize(Json::Value &js, cmn::SList &) const{
		// settings
		js["type"] = get_type();
		// for(arma::uword i=0;i<tc_params_.n_elem;i++)
		// 	js["tc_params"].append(tc_params_(i));
		js["RRR"] = RRR_;
	}

	// method for deserialisation from json
	void MatCopper::deserialize(const Json::Value &js, cmn::DSList &, const cmn::NodeFactoryMap &){
		// const arma::uword num_tc_params = js["tc_params"].size();
		// tc_params_.set_size(num_tc_params);
		// for(arma::uword i=0;i<num_tc_params;i++)
		// 	tc_params_(i) = js["tc_params"].get(i,0).asDouble();
		RRR_ = js["RRR"].asDouble();
	}

}}