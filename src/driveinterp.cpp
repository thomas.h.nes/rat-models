/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "driveinterp.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	DriveInterp::DriveInterp(){

	}

	// constructor
	DriveInterp::DriveInterp(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id){
		set_id(id); set_interpolation_table(ti,vi);
	}

	// factory
	ShDriveInterpPr DriveInterp::create(){
		return std::make_shared<DriveInterp>();
	}

	// factory
	ShDriveInterpPr DriveInterp::create(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id){
		return std::make_shared<DriveInterp>(ti,vi,id);
	}


	// set and get current
	void DriveInterp::set_interpolation_table(const arma::Col<double> &ti, const arma::Col<double> &vi){
		if(ti.n_elem!=vi_.n_elem)rat_throw_line("number of elements must be the same");
		ti_ = ti; vi_ = vi;
	}

	// get current
	double DriveInterp::get_scaling(const double time) const{
		return cmn::Extra::interp1(ti_,vi_,time,"linear",true);
	}

	// get current derivative
	double DriveInterp::get_dscaling(const double time) const{
		// find index
		assert(!ti_.is_empty()); assert(!vi_.is_empty());
		assert(time<=ti_.back()); assert(time>ti_.front());
		const arma::uword idx = arma::as_scalar(arma::find(ti_.head_cols(ti_.n_elem-1)<time && ti_.tail_cols(ti_.n_elem-1)>=time));
		return (vi_(idx+1)-vi_(idx))/(ti_(idx+1)-ti_(idx));
	}

	// get type
	std::string DriveInterp::get_type(){
		return "mdl::driveinterp";
	}

	// method for serialization into json
	void DriveInterp::serialize(Json::Value &js, cmn::SList &list) const{
		Drive::serialize(js,list);
		js["type"] = get_type();
		
		// walk over timesteps and export interpolation table
		for(arma::uword i=0;i<ti_.n_elem;i++){
			js["ti"].append(ti_(i)); js["vi"].append(vi_(i));
		}
	}

	// method for deserialisation from json
	void DriveInterp::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		Drive::deserialize(js,list,factory_list);
		const arma::uword num_times = js["ti"].size();
		ti_.set_size(num_times); vi_.set_size(num_times);
		for(arma::uword i=0;i<num_times;i++){
			ti_(i) = js["ti"].get(i,0).asDouble(); 
			vi_(i) = js["vi"].get(i,0).asDouble();
		}
	}

}}