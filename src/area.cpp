/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	Area::Area(){

	}

	// constructor with input
	Area::Area(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){
		
		// make sure it is a quad mesh
		//assert(n.n_rows==4);
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set mesh
		set_mesh(Rn,n);	
	}

	// default constructor
	Area::Area(const ShAreaPr &area){
		assert(area!=NULL);
		Rn_ = area->Rn_;
		n_ = area->n_;
	}

	// factory
	ShAreaPr Area::create(){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Area>();
	}

	// factory with dimension input
	ShAreaPr Area::create(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){
		return std::make_shared<Area>(Rn,n);
	}

	// copy factory
	ShAreaPr Area::create(const ShAreaPr &area){
		//return ShPathCirclePr(new PathCircle);
		return std::make_shared<Area>(area);
	}

	// method for setting the mesh
	void Area::set_mesh(
		const arma::Mat<double> &Rn, 
		const arma::Mat<arma::uword> &n){

		// check mesh
		assert(arma::max(arma::max(n))<Rn.n_cols);

		// set to self
		Rn_ = Rn; n_ = n;
	}

	// set hidden dimension
	void Area::set_hidden_thickness(const double hidden_thickness){
		assert(hidden_thickness>0);
		hidden_thickness_ = hidden_thickness;
	}

	// set hidden dimension
	void Area::set_hidden_width(const double hidden_width){
		assert(hidden_width>0);
		hidden_width_ = hidden_width;
	}

	// get hidden dimension
	double Area::get_hidden_thickness() const{
		return hidden_thickness_;
	}

	// get hidden dimension
	double Area::get_hidden_width() const{
		return hidden_width_;
	}

	// get hidden dimension
	double Area::get_hidden_dim() const{
		return hidden_thickness_*hidden_width_;
	}

	// get number of elements
	arma::uword Area::get_num_elements() const{
		return n_.n_cols;
	}

	// get number of nodes
	arma::uword Area::get_num_nodes() const{
		return Rn_.n_cols;
	}

	// get node coordinates
	const arma::Mat<double>& Area::get_nodes() const{
		return Rn_;
	}

	// get elements
	const arma::Mat<arma::uword>& Area::get_elements() const{
		return n_;
	}

	// smoothen mesh using simple node averaging algorithm
	void Area::smoothen(const arma::uword N, const double dmp){
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);

		// find indexes of unique nodes
		arma::Row<arma::uword> idx_node_surface = 
			arma::unique<arma::Row<arma::uword> >(arma::reshape(s,1,s.n_elem));

		// allocate internal node indexes
		arma::Row<arma::uword> idx_node_internal = 
			arma::regspace<arma::Row<arma::uword> >(0,num_nodes-1);

		// check which nodes are internal
		arma::Row<arma::uword> keep(num_nodes,arma::fill::ones);
		keep.cols(idx_node_surface).fill(0);

		// remove non-internal indexes from the list
		idx_node_internal = idx_node_internal.cols(arma::find(keep==1));

		// x and y coordinates of nodes
		arma::Row<double> x = Rn_.row(0); 
		arma::Row<double> y = Rn_.row(1); 

		// get indexes
		arma::Row<arma::uword> a = i.row(0);
		arma::Row<arma::uword> b = i.row(1);

		// iterations
		for(arma::uword j=0;j<N;j++){
			// get from store
			arma::Row<double> xnew = x; 
			arma::Row<double> ynew = y;

			// walk over internal nodes
			for(arma::uword i=0;i<idx_node_internal.n_cols;i++){
				// get this node's index
				arma::uword idx = idx_node_internal(i);

				// get neighbour indexes
				arma::Row<arma::uword> idxnb = arma::join_horiz(a.cols(arma::find(b==idx)),b.cols(arma::find(a==idx)));

				// calculate new position through
				// averaging with neighbours
				arma::Row<double> xn = x.cols(idxnb); arma::Row<double> yn = y.cols(idxnb);
				xnew(idx) = arma::sum(xn)/xn.n_elem; ynew(idx) = arma::sum(yn)/yn.n_elem;
			}

			// update
			x += dmp*(xnew-x); 
			y += dmp*(ynew-y);
		}

		// give p
		Rn_ = arma::join_vert(x,y);
	}

	// analyze function
	void Area::create_edge_matrix(
		arma::Mat<arma::uword> &s, 
		arma::Mat<arma::uword> &in) const{

		// point mesh (2D) will become linemesh in 3D
		if(n_.n_rows==1){
			s = n_; in.set_size(1,0); return;
		}

		// line mesh (2D) will become quadmesh in 3D
		else if(n_.n_rows==2){
			s = n_; in.set_size(1,0); return;
		}

		// must be quad mesh (2D) 
		else if(n_.n_rows!=4){
			rat_throw_line("area mesh not recognised");
		}

		// get counters
		const arma::uword num_elements = get_num_elements();

		// get list of faces for each element
		const arma::Mat<arma::uword>::fixed<4,2> M = cmn::Quadrilateral::get_edges();

		// create list of all faces present in mesh
		arma::Mat<arma::uword> S(2,num_elements*4);
		for(arma::uword i=0;i<4;i++){
			S.cols(i*num_elements,(i+1)*num_elements-1) = n_.rows(M.row(i));
		}

		// sort each column in S
		arma::Mat<arma::uword> Ss(2,num_elements*4);
		for(arma::uword i=0;i<num_elements*4;i++){
			Ss.col(i) = arma::sort(S.col(i));
		}

		// sort S by rows
		for(arma::uword i=0;i<2;i++){
			arma::Col<arma::uword> idx = arma::stable_sort_index(Ss.row(1-i));
			Ss = Ss.cols(idx); S = S.cols(idx);
		}	
		
		// find duplicates and mark
		arma::Row<arma::uword> duplicates = 
			arma::all(Ss.cols(0,num_elements*4-2)==Ss.cols(1,num_elements*4-1),0);

		// extend duplicate list to contain first and second
		arma::Row<arma::uword> extended(num_elements*4,arma::fill::zeros);
		extended.head(num_elements*4-1) += duplicates;
		extended.tail(num_elements*4-1) += duplicates;

		// remove marked indices
		s = S.cols(arma::find(extended==0));
		in = S.cols(arma::find(duplicates!=0));

		// sort edge matrix
		arma::Row<arma::uword> 
		sidx = arma::sort_index(s.row(1)).t();
		s = s.cols(sidx); 
		sidx = arma::stable_sort_index(s.row(0)).t(); 
		s = s.cols(sidx); 

		// start of closed loop
		arma::uword idx0 = 0;

		// walk over s and put edges in the correct order
		for(arma::uword i=0;i<s.n_cols-1;i++){
			// find next section
			const arma::Row<arma::uword> idx = arma::find(s.submat(0,i+1,0,s.n_cols-1)==s(1,i),1,"first").t();

			// check if exists
			if(!idx.is_empty()){
				// move
				s.swap_cols(i+1+arma::as_scalar(idx),i+1);
			}else{
				// check for full circle
				if(s(0,idx0)!=s(1,i))rat_throw_line("surface inconsistent");

				// move idx0
				idx0 = i;
			}
		}

	}

	// get corners
	arma::Col<arma::uword> Area::get_corner_nodes() const{
		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);
		
		// calculate angle between successive elements
		arma::Row<double> alpha(s.n_cols);
		for(arma::uword i=0;i<s.n_cols;i++){
			// prevent overlow using modulus
			const arma::uword idx2 = (i+1)%s.n_cols;

			// calculate direction vectors
			const arma::Col<double>::fixed<2> dR1 = Rn_.col(s(1,i)) - Rn_.col(s(0,i));
			const arma::Col<double>::fixed<2> dR2 = Rn_.col(s(1,idx2)) - Rn_.col(s(0,idx2));

			// calculate angle
			alpha(i) = std::acos(arma::as_scalar(cmn::Extra::dot(dR1,dR2)/
				(cmn::Extra::vec_norm(dR1)%cmn::Extra::vec_norm(dR2))));
		}

		// find corner node indexes
		const double corner_tolerance = arma::datum::pi/4;
		arma::Col<arma::uword> c = s.submat(arma::Col<arma::uword>{1}, arma::find(
			arma::abs(alpha)>corner_tolerance)).t();

		// return corner node indexes
		return c;
	}

	// get conductor area
	// calculated by splitting elements into triangles
	double Area::calculate_area() const{
		// check if surface
		if(n_.is_empty())return 0;

		// sum values and return
		return arma::accu(calculate_areas());
	}

	// get conductor area
	// calculated by splitting elements into triangles
	arma::Row<double> Area::calculate_areas() const{
		// check if surface
		if(n_.is_empty())return 0;

		// allocate area in [m^2]
		arma::Row<double> A;

		// for quadrilaterals area 
		if(n_.n_rows==4){
			// get vectors spanning face
			const arma::Mat<double> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));
			const arma::Mat<double> V2 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(0));
			const arma::Mat<double> V3 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(2));
			const arma::Mat<double> V4 = Rn_.cols(n_.row(3)) - Rn_.cols(n_.row(2));

			// calculate cross products (only z component)
			const arma::Row<double> A1 = arma::abs(V1.row(0)%V2.row(1) - V1.row(1)%V2.row(0))/2;
			const arma::Row<double> A2 = arma::abs(V3.row(0)%V4.row(1) - V3.row(1)%V4.row(0))/2;
			
			// sum values and return
			A = get_hidden_dim()*(A1 + A2);
		}

		// for line elements area
		else if(n_.n_rows==2){
			// get edge vector
			const arma::Mat<double> V1 = Rn_.cols(n_.row(1)) - Rn_.cols(n_.row(0));

			// sum values and return
			A = get_hidden_dim()*arma::sqrt(arma::sum(V1%V1,0));
		}

		// for point sources area is just 1
		// this ensures that the current is 
		// set fully to this element
		else if(n_.n_rows==1){
			A = get_hidden_dim();
		}

		// do not recognise this element type
		else{
			rat_throw_line("unknown element type");
		}

		// return areas
		return A;
	}

	// extract surface from internal mesh
	ShPerimeterPr Area::extract_perimeter() const{
		// get counters
		const arma::uword num_nodes = get_num_nodes();

		// create edges
		arma::Mat<arma::uword> s,i; 
		create_edge_matrix(s,i);

		// find indexes of unique nodes
		arma::Row<arma::uword> idx_node_surface = arma::unique<arma::Row<arma::uword> >(arma::reshape(s,1,s.n_elem));

		// figure out which nodes to drop as 
		// they are no longer contained in the mesh
		arma::Row<arma::uword> node_indexing(1,num_nodes,arma::fill::zeros);
		node_indexing.cols(idx_node_surface) = 
			arma::regspace<arma::Row<arma::uword> >(0,idx_node_surface.n_elem-1);

		// re-index connectivity
		s = arma::reshape(node_indexing(arma::reshape(s,1,s.n_elem)),2,s.n_cols);

		// get node coordinates
		arma::Mat<double> Rs = Rn_.cols(idx_node_surface);

		// return peripheral mesh
		return Perimeter::create(Rs,s);
	}

	// get type
	std::string Area::get_type(){
		return "mdl::area";
	}

	// method for serialization into json
	void Area::serialize(Json::Value &js, cmn::SList &/*list*/) const{
		// type
		js["type"] = get_type();

		// settings
		js["hidden_thickness"] = hidden_thickness_;
		js["hidden_width"] = hidden_width_;

		// node coordinates
		for(arma::uword i=0;i<Rn_.n_cols;i++){
			Json::Value jsr;
			for(arma::uword j=0;j<Rn_.n_rows;j++){
				jsr.append(Rn_(j,i));
			}
			js["Rn"].append(jsr);
		}

		// elements
		js["nn"] = (int)n_.n_rows;
		for(arma::uword i=0;i<n_.n_cols;i++){
			Json::Value jsn;
			for(arma::uword j=0;j<n_.n_rows;j++){
				jsn.append((int)n_(j,i));
			}
			js["n"].append(jsn);
		}

	}

	// method for deserialisation from json
	void Area::deserialize(const Json::Value &js, cmn::DSList &/*list*/, const cmn::NodeFactoryMap &/*factory_list*/){
		// node coordinates
		const arma::uword num_nodes = js["Rn"].size();
		Rn_.set_size(2,num_nodes);
		for(arma::uword i=0;i<num_nodes;i++){
			Json::Value jsr = js["Rn"].get(i,0);
			for(arma::uword j=0;j<2;j++){
				Rn_(j,i) = jsr.get(j,0).asDouble();
			}
		}

		// node coordinates
		const arma::uword num_elements = js["n"].size();
		const arma::uword num_nn = js["nn"].asUInt64();
		n_.set_size(num_nn,num_elements);
		for(arma::uword i=0;i<num_elements;i++){
			Json::Value jsn = js["n"].get(i,0);
			for(arma::uword j=0;j<num_nn;j++){
				n_(j,i) = jsn.get(j,0).asUInt64();
			}
		}

		// settings
		set_hidden_thickness(js["hidden_thickness"].asDouble());
		set_hidden_width(js["hidden_width"].asDouble());
	}


}}