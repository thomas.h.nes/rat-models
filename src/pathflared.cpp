/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathflared.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathFlared::PathFlared(){

	}

	// constructor with dimension input
	PathFlared::PathFlared(
		const double ell1, const double ell2, 
		const double arcl, const double radius1, 
		const double radius2, const double element_size, 
		const double offset, const double offset_arc){

		// set to self
		set_ell1(ell1);	set_ell2(ell2);
		set_arcl(arcl);	set_radius1(radius1); set_radius2(radius2);
		set_element_size(element_size);	set_offset(offset);
		set_offset_arc(offset_arc);
	}

	// factory
	ShPathFlaredPr PathFlared::create(){
		//return ShPathFlaredPr(new PathFlared);
		return std::make_shared<PathFlared>();
	}

	// factory with dimension input
	ShPathFlaredPr PathFlared::create(
		const double ell1, const double ell2, 
		const double arcl, const double radius1, 
		const double radius2, const double element_size, 
		const double offset, const double offset_arc){
		return std::make_shared<PathFlared>(ell1,ell2,arcl,
			radius1,radius2,element_size,offset,offset_arc);
	}

	// set element size 
	void PathFlared::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// path offset
	void PathFlared::set_offset(const double offset){
		offset_ = offset;
	}

	// path offset for hardway bend
	void PathFlared::set_offset_arc(const double offset_arc){
		offset_arc_ = offset_arc;
	}

	// set first length
	void PathFlared::set_ell1(const double ell1){
		if(ell1<=0)rat_throw_line("first length must be larger than zero");
		ell1_ = ell1;
	}

	// set second length
	void PathFlared::set_ell2(const double ell2){
		if(ell2<=0)rat_throw_line("second length must be larger than zero");
		ell2_ = ell2;
	}

	// set arc length
	void PathFlared::set_arcl(const double arcl){
		if(arcl==0)rat_throw_line("arc length can not be zero");
		arcl_ = arcl;
	}

	// set first radius
	void PathFlared::set_radius1(const double radius1){
		if(radius1<=0)rat_throw_line("first radius must be larger than zero");
		radius1_ = radius1;
	}

	// set second radius
	void PathFlared::set_radius2(const double radius2){
		if(radius2<=0)rat_throw_line("second radius must be larger than zero");
		radius2_ = radius2;
	}

	// get frame
	ShFramePr PathFlared::create_frame() const{
		// check input
		if(arcl_==0)rat_throw_line("arclength is not set");
		if(element_size_==0)rat_throw_line("element size is not set");
		if(ell1_==0)rat_throw_line("first length is not set");
		if(ell2_==0)rat_throw_line("second length is not set");
		if(radius1_==0)rat_throw_line("second radius is not set");
		if(radius2_==0)rat_throw_line("second radius is not set");

		// create arcs
		ShPathArcPr arc1 = PathArc::create(radius1_,arcl_,element_size_,offset_arc_); arc1->set_transverse(true);
		ShPathArcPr arc2 = PathArc::create(radius2_,arma::datum::pi/2,element_size_,offset_);
		ShPathStraightPr str1 = PathStraight::create(ell1_/2,element_size_);
		ShPathStraightPr str2 = PathStraight::create(ell2_/2,element_size_);

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();
		pathgroup->add_path(str1); pathgroup->add_path(arc1); pathgroup->add_path(str2); pathgroup->add_path(arc2);
		pathgroup->add_path(arc2); pathgroup->add_path(str2); pathgroup->add_path(arc1); pathgroup->add_path(str1);
		pathgroup->add_path(str1); pathgroup->add_path(arc1); pathgroup->add_path(str2); pathgroup->add_path(arc2);
		pathgroup->add_path(arc2); pathgroup->add_path(str2); pathgroup->add_path(arc1); pathgroup->add_path(str1);
		pathgroup->add_translation(radius2_,0,0);
		
		// create frame
		ShFramePr frame = pathgroup->create_frame();

		// apply transformations
		frame->apply_transformations(trans_);

		// return frame
		return frame;
	}

	// get type
	std::string PathFlared::get_type(){
		return "mdl::pathflared";
	}

	// method for serialization into json
	void PathFlared::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["arcl"] = arcl_;
		js["radius1"] = radius1_;
		js["radius2"] = radius2_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
		js["offset_arc"] = offset_arc_;
	}

	// method for deserialisation from json
	void PathFlared::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Transformations::deserialize(js,list,factory_list);
		
		// properties
		set_ell1(js["ell1"].asDouble());
		set_ell2(js["ell2"].asDouble());
		set_arcl(js["arcl"].asDouble());
		set_radius1(js["radius1"].asDouble());
		set_radius2(js["radius2"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_offset(js["offset"].asDouble());
		set_offset_arc(js["offset_arc"].asDouble());
	}

}}
