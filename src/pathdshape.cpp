/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "pathdshape.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	PathDShape::PathDShape(){

	}

	// constructor
	PathDShape::PathDShape(
		const double ell1, const double ell2, 
		const double element_size, const double offset){
		// set parameters
		set_ell1(ell1);	set_ell2(ell2);
		set_element_size(element_size); 
		set_offset(offset);
	}

	// factory
	ShPathDShapePr PathDShape::create(){
		return std::make_shared<PathDShape>();
	}

	// factory with dimensional input
	ShPathDShapePr PathDShape::create(
		const double ell1, const double ell2, 
		const double element_size, const double offset){
		return std::make_shared<PathDShape>(ell1,ell2,element_size,offset);
	}

	// set ell1
	void PathDShape::set_ell1(const double ell1){
		if(ell1<=0)rat_throw_line("first length must be larger than zero");
		ell1_ = ell1;
	}

	// set ell2
	void PathDShape::set_ell2(const double ell2){
		if(ell2<=0)rat_throw_line("second length must be larger than zero");
		ell2_ = ell2;
	}

	// set element size 
	void PathDShape::set_element_size(const double element_size){
		if(element_size<=0)rat_throw_line("element size must be larger than zero");
		element_size_ = element_size;
	}

	// path offset
	void PathDShape::set_offset(const double offset){
		offset_ = offset;
	}

	// get frame
	ShFramePr PathDShape::create_frame() const{
		// check input
		if(ell1_==0)rat_throw_line("first length is not set");
		if(ell2_==0)rat_throw_line("second length is not set");
		if(element_size_==0)rat_throw_line("element size is not set");

		// create unified path
		ShPathGroupPr pathgroup = PathGroup::create();

		// add sections
		pathgroup->add_path(PathBSpline::create(
			0,0,0, 0,1,0, 1,0,0, -0.7*ell2_,ell1_/2,0, -1,0,0, 0,1,0, 0.6*ell1_, ell2_/1.6, element_size_, offset_));
		pathgroup->add_path(PathBSpline::create(
			0,0,0, 0,1,0, 1,0,0, -0.3*ell1_/2,0.3*ell2_,0, -1,0,0, 0,1,0, ell2_/3, ell2_/3, element_size_, offset_));
		pathgroup->add_path(PathStraight::create(0.7*ell1_/2,element_size_));
		pathgroup->add_path(PathStraight::create(0.7*ell1_/2,element_size_));
		pathgroup->add_path(PathBSpline::create(
		 	0,0,0, 0,1,0, 1,0,0, -0.3*ell2_,0.3*ell1_/2,0, -1,0,0, 0,1,0, ell2_/3, ell2_/3, element_size_, offset_));
		pathgroup->add_path(PathBSpline::create(
		 	0,0,0, 0,1,0, 1,0,0, -ell1_/2,0.7*ell2_,0, -1,0,0, 0,1,0, ell2_/1.6, 0.6*ell1_, element_size_, offset_));

		// set offset
		pathgroup->add_translation(ell2_,0,0);

		// create frame
		ShFramePr frame = pathgroup->create_frame();

		// apply transformations
		frame->apply_transformations(trans_);
		
		// return frame
		return frame;
	}

	// get type
	std::string PathDShape::get_type(){
		return "mdl::pathdshape";
	}

	// method for serialization into json
	void PathDShape::serialize(Json::Value &js, cmn::SList &list) const{
		// parent
		Transformations::serialize(js,list);

		// properties
		js["type"] = get_type();
		js["ell1"] = ell1_;
		js["ell2"] = ell2_;
		js["element_size"] = element_size_;
		js["offset"] = offset_;
	}

	// method for deserialisation from json
	void PathDShape::deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list){
		// parent
		Transformations::deserialize(js,list,factory_list);
		
		// properties
		set_ell1(js["ell1"].asDouble());
		set_ell2(js["ell2"].asDouble());
		set_element_size(js["element_size"].asDouble());
		set_offset(js["offset"].asDouble());
	}


}}