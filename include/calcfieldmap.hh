/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_FIELDMAP_HH
#define MDL_FIELDMAP_HH

#include <armadillo> 
#include <memory>
#include <map>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"

#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/sources.hh"
#include "rat/mlfmm/multisources.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/interp.hh"

#include "modelgroup.hh"
#include "drivedc.hh"
#include "vtkobj.hh"
#include "background.hh"
#include "calc.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcFieldMap> ShCalcFieldMapPr;

	// a collection of coils
	class CalcFieldMap: public Calc{
		// settings
		protected:
			// model that generates the field
			ShModelPr source_model_;

			// calculated field type and its dimensions
			std::string field_type_ = "AHM";
			arma::Row<arma::uword> num_dim_ = {3,3,3};

			// settings for the MLFMM
			fmm::ShSettingsPr fmm_stngs_ = fmm::Settings::create();

			// background field (only added once)
			arma::field<ShBackgroundPr> bg_;

			// list of drives sorted by index
			std::map<arma::uword, ShDrivePr> drives_;

			// use line elements or volume elements
			//bool use_volume_elements_ = false;

			// number of gauss points
			arma::sword num_gauss_ = 1;
			arma::sword num_gauss_surface_ = 5;

		// storage
		protected:
			// time used for calculating field
			double time_ = 0;

			// target coordinates
			arma::Mat<double> Rt_;

			// unique drive ids in this simulation
			arma::Row<arma::uword> drive_id_;

			// stored targets for each drive
			arma::field<fmm::ShMgnTargetsPr> tar_;

		// methods
		public:
			// constructor
			CalcFieldMap();

			// destructor
			~CalcFieldMap(){};

			// set directory name
			//void set_output_dir(const std::string &output_dir);

			// set number of gauss points
			void set_num_gauss(const arma::sword num_gauss);

			// set the field type
			void set_field_type(const std::string &field_type, const arma::Row<arma::uword> &num_dim);
			void set_field_type(const std::string &field_type, const arma::uword num_dim);

			// set source model
			void set_source_model(ShModelPr model);

			// add drive
			void add_drive(ShDrivePr drive);

			// add a background field
			void add_background(ShBackgroundPr bg);

			// set the settings
			void set_fmm_settings(fmm::ShSettingsPr settings);
			// void set_mdl_settings(mdl::ShSettingsPr settings);

			// set volume elements
			// void set_use_volume_elements(const bool use_volume_elements);

			// access the settings
			fmm::ShSettingsPr get_fmm_settings();
			// mdl::ShSettingsPr get_mdl_settings();

			// calculate the fieldmap
			virtual void calculate(cmn::ShLogPr lg = cmn::NullLog::create());

			// set time
			virtual void set_time(const double time);
			double get_time() const;

			// checking which fields are available
			bool has_field() const;
			bool has(const std::string &type) const;

			// getting field
			arma::Mat<double> get_field(const std::string &type, const double time) const;
			arma::Mat<double> get_field(const std::string &type) const;

			// extended filename with time index
			static std::string indexed_output_fname(const std::string &fname, const arma::uword idx);

			// setup function
			virtual void setup(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
