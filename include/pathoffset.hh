/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_OFFSET_HH
#define MDL_PATH_OFFSET_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathOffset> ShPathOffsetPr;

	// cross section of coil
	class PathOffset: public Path{
		// properties
		protected:		
			// the base path to derive the offset path from
			ShPathPr base_ = NULL;

			// settings
			double noff_ = 0; // offset in normal direction [m]
			double doff_ = 0; // offset in transverse direction [m]

		// methods
		public:
			// constructor
			PathOffset();
			PathOffset(ShPathPr base, const double noff = 0, const double doff = 0);

			// factory
			static ShPathOffsetPr create();
			static ShPathOffsetPr create(ShPathPr base, const double noff = 0, const double doff = 0);

			// set properties
			void set_base(ShPathPr base);
			void set_noff(const double noff);
			void set_doff(const double doff);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
