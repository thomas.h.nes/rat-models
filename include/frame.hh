/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// frame are defined as
// D L
// |/
// o--N
// and are in principle orthogonal 
// although this is not enforced.

#ifndef MDL_FRAME_HH
#define MDL_FRAME_HH

#include <memory>
#include <cassert>
#include <armadillo>

#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "rat/common/node.hh"

#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Frame> ShFramePr;
	typedef arma::field<ShFramePr> ShFramePrList;

	// type of the frame
	enum DbType {block,cable};

	// frame for describing three dimensional
	// ribbon like shapes.
	class Frame: public cmn::Node{
		private:
			// type for the frame
			// determines how the coordinate 
			// transformations are performed
			DbType conductor_type_ = block;

			// properties in (num_sections)[3XN] matrices
			arma::field<arma::Mat<double> > R_; // coordinates
			arma::field<arma::Mat<double> > L_; // longitudinal vectors
			arma::field<arma::Mat<double> > N_; // normal vectors
			arma::field<arma::Mat<double> > D_; // transverse vectors
			arma::field<arma::Mat<double> > B_; // block winding direction

			// section and turn
			arma::Row<arma::uword> section_;
			arma::Row<arma::uword> turn_;

			// tolerance for checking if closed
			const double tol_closed_ = 1e-6;

		public:
			// constructors
			Frame();
			Frame(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<double> &B); 
			Frame(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<double> &B, const arma::uword num_sections);
			Frame(const arma::field<arma::Mat<double> > &R, const arma::field<arma::Mat<double> > &L, const arma::field<arma::Mat<double> > &N, const arma::field<arma::Mat<double> > &D, const arma::field<arma::Mat<double> > &B);
			Frame(const ShFramePrList &frame);

			// factory
			static ShFramePr create();
			static ShFramePr create(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D);
			static ShFramePr create(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::uword num_sections);
			static ShFramePr create(const arma::field<arma::Mat<double> > &R, const arma::field<arma::Mat<double> > &L, const arma::field<arma::Mat<double> > &N, const arma::field<arma::Mat<double> > &D);
			static ShFramePr create(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<double> &B);
			static ShFramePr create(const arma::Mat<double> &R, const arma::Mat<double> &L, const arma::Mat<double> &N, const arma::Mat<double> &D, const arma::Mat<double> &B, const arma::uword num_sections);
			static ShFramePr create(const arma::field<arma::Mat<double> > &R, const arma::field<arma::Mat<double> > &L, const arma::field<arma::Mat<double> > &N, const arma::field<arma::Mat<double> > &D, const arma::field<arma::Mat<double> > &B);
			static ShFramePr create(const ShFramePrList &frame);

			// access to vectors
			arma::uword get_num_sections() const;
			arma::Row<arma::uword> get_num_nodes() const;
			arma::uword get_num_nodes(const arma::uword index) const;
			const arma::field<arma::Mat<double> >& get_coords() const;
			const arma::field<arma::Mat<double> >& get_direction() const;
			const arma::field<arma::Mat<double> >& get_normal() const;
			const arma::field<arma::Mat<double> >& get_transverse() const;
			const arma::field<arma::Mat<double> >& get_block() const;

			// access to vectors per section
			const arma::Mat<double>& get_coords(const arma::uword section) const;
			const arma::Mat<double>& get_direction(const arma::uword section) const;
			const arma::Mat<double>& get_normal(const arma::uword section) const;
			const arma::Mat<double>& get_transverse(const arma::uword section) const;
			const arma::Mat<double>& get_block(const arma::uword section) const;

			// function for calculating shearing angle
			arma::Row<double> calc_ashear(const arma::uword section) const;

			// perform coordinate transformations
			arma::Mat<double> map_coords(const double d, const double n) const;

			// check if the sections are closed in on themselves
			arma::Row<arma::uword> is_closed() const;	
			bool is_loop() const;

			// access flag for keeping track of type
			void set_conductor_type(const DbType type);
			DbType get_conductor_type() const;

			// set section and turn indices to understand where we are located
			void set_location(const arma::Row<arma::uword> &section, const arma::Row<arma::uword> &turn);

			// calculate length
			arma::Row<double> calc_ell() const;
			double calc_total_ell() const;

			// get indexes to section or turn
			arma::Row<arma::uword> get_section() const;
			arma::Row<arma::uword> get_turn() const;

			// method for aligning the sections
			void align_sections();
			void align_sections(const arma::Col<double>::fixed<3> &R0, const arma::Col<double>::fixed<3> &L0, const arma::Col<double>::fixed<3> &N0, const arma::Col<double>::fixed<3> &D0);
			
			// combine sections into single set of frame
			void combine();
			void split(const arma::uword num_sub);

			// reverse sections and their contents
			void reverse();

			// apply a transformation to these frame
			void apply_transformations(const ShTransPrList &trans);
			void apply_transformation(const ShTransPr &trans);

			// extrusions
			//ShCoilMeshPr create_loft(const ShAreaPr &area) const;
			void create_loft(arma::Mat<double> &R, arma::Mat<double> &L, arma::Mat<double> &N, arma::Mat<double> &D, arma::Mat<arma::uword> &n, arma::Mat<arma::uword> &s, const arma::Mat<double> &R2d, const arma::Mat<arma::uword> &n2d, const arma::Mat<arma::uword> &s2d) const;

			// shifting of the basebath
			arma::Mat<double> perform_normal_shift(const arma::uword section, const arma::Row<double> &n) const; 
			arma::Mat<double> perform_shift(const arma::uword section, const arma::Row<double> &n, const arma::Row<double> &d) const;
			arma::Mat<double> perform_shift(const arma::uword section, const double n, const double d) const;

			// get sub frame
			ShFramePr get_sub(const arma::uword section, const arma::uword idx1, const arma::uword idx2) const;
			ShFramePr get_sub(const arma::uword section) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif

