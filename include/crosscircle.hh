/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CROSS_CIRCLE_HH
#define MDL_CROSS_CIRCLE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "cross.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossCircle> ShCrossCirclePr;

	// cross section of coil
	class CrossCircle: public Cross{
		// properties
		private:
			// dimensions
			double radius_ = 0;	

			// center position
			double nc_ = 0;
			double tc_ = 0;

			// element size
			double dl_ = 0;
					
			// mesh smoothening
			arma::uword num_smooth_iter_ = 10;
			double smooth_dampen_ = 0.7;

		// methods
		public:
			// constructors
			CrossCircle();
			CrossCircle(const double radius,const double dl);
			CrossCircle(const double nc, const double tc, const double radius, const double dl);
			
			// factory methods
			static ShCrossCirclePr create();
			static ShCrossCirclePr create(const double radius,const double dl);
			static ShCrossCirclePr create(const double nc, const double tc, const double radius, const double dl);

			// set properties
			void set_radius(const double radius);
			void set_element_size(const double dl);
			void set_center(const double uc, const double vc);

			// volume mesh
			virtual ShAreaPr create_area() const;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
