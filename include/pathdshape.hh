/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_DSHAPE_HH
#define MDL_PATH_DSHAPE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "rat/common/defines.hh"
#include "path.hh"
#include "pathbspline.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathDShape> ShPathDShapePr;

	// cross section of coil
	class PathDShape: public Path, public Transformations{
		// properties
		private:
			// settings
			double ell1_ = 0; // height of the D [m]
			double ell2_ = 0; // width of the D [m]
			
			// element size
			double element_size_ = 0; // size of the elements [m]

			// offset
			double offset_ = 0; // offset [m]

		// methods
		public:
			// constructor
			PathDShape();
			PathDShape(const double ell1, const double ell2, const double element_size, const double offset = 0);

			// factory methods
			static ShPathDShapePr create();
			static ShPathDShapePr create(const double ell1, const double ell2, const double element_size, const double offset = 0);

			// set properties
			void set_ell1(const double ell1);
			void set_ell2(const double ell2);
			void set_element_size(const double element_size);
			void set_offset(const double offset);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif