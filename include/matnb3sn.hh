/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// Description:
// Calculates the critical current density of Nb3Sn using the A. God
// formula. The initial parameters can be set using two standard
// variants: Powder in Tube (PIT) and Rod-Restack Process (RRP). It must
// be noted that the formulas are not accurate in the low field region.

// references:
// A. Godeke, Msc. Thesis: Performance Boundaries in Nb3Sn Superconductors,
// Twente University 2005, p168

#ifndef MDL_MATERIAL_NB3SN_HH
#define MDL_MATERIAL_NB3SN_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatNb3Sn> ShMatNb3SnPr;

	// Lubell/Kramer scaling relation
	class MatNb3Sn: public Material{
		private:
			// density
			double density_ = 8950; // [Kg m^-3]

			// deformation related parameters
			double Ca1_ = 0;
			double Ca2_ = 0;
			double eps_0a_ = 0; // normalized
			double C1_ = 0; // [kAT mm^-2]

			// superconducting parameters
			double Bc2m_ = 0; // [T]
			double Tcm_ = 0; // [K]

			// magnetic field dependence parameters
			double p_ = 0;
			double q_ = 0;

			// environmental parameters
			double eps_ax_ = 0;

			// electric field criterion
			double E0_ = 1e-5; // [V/m]
			double N_ = 40; 

			// interpolation arrays
			arma::Col<double> Ti_;
			arma::Col<double> ki_;
			arma::Col<double> Cpi_; 

		// methods
		public:
			// conductor
			MatNb3Sn();

			// factory
			static ShMatNb3SnPr create();

			// default conductor settings
			void set_ITER();
			void set_PIT();
			void set_RRP();
			
			// precalculate material properties for fast electric field calculations
			arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate critical current
			arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate electric field
			arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const;
			arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const;

			// thermal properties
			// arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif