/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CCT_HH
#define MDL_PATH_CCT_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>

#include "path.hh"
#include "rat/common/extra.hh"
#include "transformations.hh"
#include "frame.hh"
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCCT> ShPathCCTPr;

	// cross section of coil
	class PathCCT: public Path, public Transformations{
		// properties
		private:
			bool is_reverse_ = false;
			arma::uword num_poles_; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
			double radius_; // radius
			double a_; // skew amplitude
			double omega_; // spacing between turns
			double num_turns_; // number of turns (can be non-integer)
			arma::uword num_nodes_per_turn_;
			double twist_ = 0; // additional twisting along length of cable

			// squeeze transformation
			double scale_x_ = 1.0; // 1.0 is not elliptic at all
			double scale_y_ = 1.0; // 1.0 is not elliptic at all

		// methods
		public:
			// constructor
			PathCCT();
			PathCCT(const arma::uword num_poles, const double radius, const double a, const double omega, const double num_turns, const arma::uword num_nodes_per_turn);

			// factory methods
			static ShPathCCTPr create();
			static ShPathCCTPr create(const arma::uword num_poles, const double radius, const double a, const double omega, const double num_turns, const arma::uword num_nodes_per_turn);

			// setting and getting
			void set_is_reverse(const bool is_reverse);
			void set_twist(const double twist);
			void set_amplitude(const double a);
			void set_pitch(const double omega);
			void set_num_turns(const double num_turns);
			void set_scale_x(const double scale_x);
			void set_scale_y(const double scale_y);
			void set_radius(const double radius);
			void set_num_poles(const arma::uword num_poles);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);

			// omega calculation
			double calc_alpha() const;

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
