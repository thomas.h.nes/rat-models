/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_BAR_HH
#define MDL_MODEL_BAR_HH

#include <armadillo> 
#include <memory>

#include "rat/common/node.hh"
#include "rat/common/defines.hh"

#include "modelmesh.hh"
#include "meshbar.hh"
#include "edges.hh"
#include "material.hh"
#include "matcopper.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelBar> ShModelBarPr;

	// current carying coil extruded with
	// a base path and a cross section
	class ModelBar: public ModelMesh{
		// properties
		protected:
			// softening factor
			double softening_ = 1.0;

			// magnetisation in frame coordinates
			arma::Col<double>::fixed<3> Mf_ = {0,0,0}; // M_L,M_N,M_D

		// methods
		public:
			// constructor
			ModelBar();
			ModelBar(ShPathPr pth, ShCrossPr crss);

			// factory methods
			static ShModelBarPr create();
			static ShModelBarPr create(ShPathPr pth, ShCrossPr crss);

			// calculation settings
			void set_softening(const double softening); // softening factor
			void set_magnetisation(const arma::Col<double>::fixed<3> &Mf);

			// creation of calculation data objects
			virtual ShMeshPrList create_mesh() const; // create a mesh
			virtual ShEdgesPrList create_edge() const; // create edge matrices

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
