/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_RUTHERFORD_HH
#define MDL_PATH_RUTHERFORD_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathRutherford> ShPathRutherfordPr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathRutherford: public Path, public Transformations{
		// properties
		protected:
			// basepath
			ShPathPr base_ = NULL;

			// geometry
			arma::uword num_strand_ = 20;
			double strand_diameter_ = 1.0e-3;
			
			// cable envelope
			double u1_ = 0;
			double u2_ = 0;
			double v1_ = 0;
			double v2_ = 0;

		// methods
		public:
			// constructor
			PathRutherford();
			PathRutherford(ShPathPr base);
			
			// factory methods
			static ShPathRutherfordPr create();
			static ShPathRutherfordPr create(ShPathPr base);

			// set base path
			void set_base(ShPathPr base);
			void set_reverse_base(const bool reverse_base);

			// get frame
			virtual ShFramePr create_frame() const;
			
			// indexing arrays
			void get_indexing(arma::Row<arma::uword> &section_idx, arma::Row<arma::uword> &turn_idx) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
