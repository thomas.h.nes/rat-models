/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CALCULATE_TRACKS_HH
#define MDL_CALCULATE_TRACKS_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "emitter.hh"
#include "particle.hh"
#include "vtkunstr.hh"
#include "calcmesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcTracks> ShCalcTracksPr;

	// template for coil
	class CalcTracks: public CalcMesh{
		// properties
		protected:
			// time stepping
			arma::uword num_particles_ = 1000;
			double step_size_ = 5e-3;
			
			// particle emitter
			ShEmitterPr emitter_ = NULL;

			// track data
			arma::field<Particle> particles_;

		// methods
		public:
			// constructor
			CalcTracks();
			CalcTracks(ShModelPr source_model, ShModelPr target_model, ShEmitterPr emitter);

			// factory methods
			static ShCalcTracksPr create();
			static ShCalcTracksPr create(ShModelPr source_model, ShModelPr target_model, ShEmitterPr emitter);
			
			// setting of emitter
			void set_emitter(ShEmitterPr emitter);

			// set properties
			void set_stepsize(const double step_size);

			// set mesh for interpolation
			void set_fieldmap(ShCalcMeshPr fieldmap);

			// tracking functions
			static arma::Mat<double> interpolate_field(const ShVTKUnstrPr &vtk_mesh, const arma::Mat<double> &Rp);
			static arma::Mat<double> system_function(const double t, const arma::Mat<double> &Xp, const arma::Row<double> &q, const arma::Row<double> &m, const ShVTKUnstrPr &vtk_mesh);
			static arma::Mat<double> runge_kutta(const double t, const double dt, const arma::Mat<double> &Xp, const arma::Row<double> &q, const arma::Row<double> &m, const ShVTKUnstrPr &vtk_mesh);

			// calculate tracks
			void calculate(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// export to vtk
			ShVTKUnstrPr export_vtk_tracks(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// get particle
			Particle get_particle(const arma::uword idx) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};
}}

#endif
