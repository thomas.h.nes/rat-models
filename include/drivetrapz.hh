/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_TRAPZ_HH
#define MDL_DRIVE_TRAPZ_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveTrapz> ShDriveTrapzPr;
	typedef arma::field<ShDriveTrapzPr> ShDriveTrapzPrList;

	// circuit class
	class DriveTrapz: public Drive{
		// properties
		protected:
			// amplitude
			double amplitude_ = 1.0;

			// scaling amplitude
			double tstart_ = 1.0;

			// frequency
			double ramprate_ = 0.1;

			// phase
			double tpulse_ = 2.0;

		// methods
		public:
			// constructor
			DriveTrapz();
			DriveTrapz(const double amplitude, const double tstart, const double ramprate, const double tpulse, const arma::uword id = 0);

			// factory
			static ShDriveTrapzPr create();
			static ShDriveTrapzPr create(const double amplitude, const double tstart, const double ramprate, const double tpulse, const arma::uword id = 0);

			// set and get definition
			void set_amplitude(const double amplitude);
			void set_tstart(const double tstart);
			void set_ramprate(const double ramprate);
			void set_tpulse(const double tpulse);
			
			// get scaling at given time
			double get_scaling(const double time) const;
			double get_dscaling(const double time) const;
			
			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
