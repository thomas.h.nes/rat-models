/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef MDL_VTK_IMG_HH
#define MDL_VTK_IMG_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// magrat headers
#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "vtkobj.hh"

// vtk headers
#include <vtkSmartPointer.h>
#include <vtkRectilinearGrid.h>
#include <vtkDoubleArray.h>
#include <vtkXMLRectilinearGridWriter.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkPointData.h>
#include <vtkImageData.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKImg> ShVTKImgPr;

	// VTK Unstructured grid
	class VTKImg: public VTKObj{
		protected:
			// pointer to dataset
			vtkSmartPointer<vtkImageData> vtk_igrid_;

		public:
			// constructor
			VTKImg();

			// factory
			static ShVTKImgPr create();

			// write mesh
			void set_dims(const double x1, const double x2, const arma::uword num_x, const double y1, const double y2, const arma::uword num_y, const double z1, const double z2, const arma::uword num_z);

			// write data at nodes
			void set_nodedata(const arma::Mat<double> &v, const std::string &name);

			// write output file
			void write(const std::string fname, cmn::ShLogPr lg = cmn::NullLog::create());
	};

}}

#endif
