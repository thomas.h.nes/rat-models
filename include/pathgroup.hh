/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_UNIFIED_HH
#define MDL_PATH_UNIFIED_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathGroup> ShPathGroupPr;

	// cross section of coil
	class PathGroup: public Path, public Transformations{
		// properties
		protected:
			// coordinate
			arma::Col<double>::fixed<3> R0_; // start point
			
			// orientation
			arma::Col<double>::fixed<3> L0_; // start direction vector
			arma::Col<double>::fixed<3> N0_; // start normal vector
			arma::Col<double>::fixed<3> D0_; // start transverse vector

			// align frame
			// when enabled (default) the sections making up the
			// pathgroup are automatically lined up such that 
			// a smooth transition is made between them
			bool align_frame_ = true;

			// list of pointers to the path sections	
			ShPathPrList paths_;

		// methods
		public:
			// constructor
			PathGroup();
			PathGroup(const arma::Col<double>::fixed<3> R0, const arma::Col<double>::fixed<3> L0, const arma::Col<double>::fixed<3> N0, const arma::Col<double>::fixed<3> D0);

			// factory
			static ShPathGroupPr create();
			static ShPathGroupPr create(const arma::Col<double>::fixed<3> R0, const arma::Col<double>::fixed<3> L0, const arma::Col<double>::fixed<3> N0, const arma::Col<double>::fixed<3> D0);

			// set properties
			void set_start_coord(const arma::Col<double>::fixed<3> &R0);
			void set_start_longitudinal(const arma::Col<double>::fixed<3> &L0);
			void set_start_normal(const arma::Col<double>::fixed<3> &N0);
			void set_start_transverse(const arma::Col<double>::fixed<3> &D0);
			void set_align_frame(const bool align_frame = true);

			// function for adding a path to the path list
			void add_path(ShPathPr path);
				
			// get number of paths stored inside
			arma::uword get_num_paths() const;

			// get frame
			virtual ShFramePr create_frame() const;
	};

}}

#endif
