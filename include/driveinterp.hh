/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_INTERP_HH
#define MDL_DRIVE_INTERP_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/extra.hh"
#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveInterp> ShDriveInterpPr;
	typedef arma::field<ShDriveInterpPr> ShDriveInterpPrList;

	// circuit class
	class DriveInterp: public Drive{
		// properties
		protected:
			// interpolation table
			arma::Col<double> ti_;
			arma::Col<double> vi_;

		// methods
		public:
			// constructor
			DriveInterp();
			DriveInterp(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id = 0);

			// factory
			static ShDriveInterpPr create();
			static ShDriveInterpPr create(const arma::Col<double> &ti, const arma::Col<double> &vi, const arma::uword id = 0);

			// set and get current
			void set_interpolation_table(const arma::Col<double> &ti, const arma::Col<double> &vi);
			
			// get data
			double get_scaling(const double time) const;
			double get_dscaling(const double time) const;
			
			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
