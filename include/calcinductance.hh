/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_INDUCTANCE_HH
#define MDL_MODEL_INDUCTANCE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"

#include "rat/mlfmm/mlfmm.hh"

#include "model.hh"
#include "mesh.hh"
#include "meshcoil.hh"
#include "modelgroup.hh"
#include "calc.hh"
#include "vtktable.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcInductance> ShCalcInductancePr;

	// enum array of calculation types
	// coil -> matrix includes inductance between each coil
	// drive -> matrix includes inductance between each drive
	// total -> matrix contains inductance of all coils combined
	enum IndType {COIL, DRIVE, TOTAL};

	// template for coil
	class CalcInductance: public Calc{
		// properties
		protected:
			// input model
			ShModelPr model_;

			// parallel calculation of all combinations
			// uses more memory but can be a little faster
			bool use_parallel_ = false;

			// used dIdt
			const double dIdt_ = 1.0; // [A/s]

			// inductance matrix type
			IndType type_ = DRIVE;

			// matrix composition
			arma::field<arma::Col<arma::uword> > coil_groups_;

			// operating currents for each coil
			arma::Row<double> operating_currents_;

			// inductance matrix
			arma::Mat<double> M_;
			
			// number of gauss points for calculation
			arma::sword num_gauss_ = 1;

		// methods
		public:
			// constructor
			CalcInductance();
			CalcInductance(ShModelPr model);

			// factory methods
			static ShCalcInductancePr create();
			static ShCalcInductancePr create(ShModelPr model);

			// set model
			void set_model(ShModelPr model);

			// set inductance type
			void set_type(const IndType type);

			// calculation function
			void calculate(cmn::ShLogPr lg = cmn::NullLog::create()) override;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// access matrix
			arma::Mat<double> get_inductance_matrix() const;
			double get_inductance() const;

			// export to vtk
			ShVTKTablePr export_vtk_table(cmn::ShLogPr lg = cmn::NullLog::create()) const;

			// get stored energy
			double calc_stored_energy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;

	};

}}

#endif
