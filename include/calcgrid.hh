/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CALCULATE_GRID_HH
#define MDL_CALCULATE_GRID_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"

#include "rat/mlfmm/mgntargets.hh"

#include "model.hh"
#include "path.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "vtkimg.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcGrid> ShCalcGridPr;

	// template for coil
	class CalcGrid: public CalcFieldMap{		
		// properties
		protected:
			// grid extends
			double x1_ = 0;
			double x2_ = 0;
			double y1_ = 0;
			double y2_ = 0;
			double z1_ = 0;
			double z2_ = 0;

			// number of points
			arma::uword num_x_ = 0;
			arma::uword num_y_ = 0;
			arma::uword num_z_ = 0;
			
		// methods
		public:
			// constructor
			CalcGrid();
			CalcGrid(const double x1, const double x2, const arma::uword num_x, const double y1, const double y2, const arma::uword num_y, const double z1, const double z2, const arma::uword num_z);

			// factory methods
			static ShCalcGridPr create();
			static ShCalcGridPr create(const double x1, const double x2, const arma::uword num_x, const double y1, const double y2, const arma::uword num_y, const double z1, const double z2, const arma::uword num_z);

			// set properties
			void set_dim_x(const double x1, const double x2, const arma::uword num_x);
			void set_dim_y(const double y1, const double y2, const arma::uword num_y);
			void set_dim_z(const double z1, const double z2, const arma::uword num_z);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// VTK export
			ShVTKImgPr export_vtk(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif
