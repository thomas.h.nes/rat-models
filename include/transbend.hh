/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_TRANS_BEND_HH
#define MDL_TRANS_BEND_HH

#include <armadillo>
#include <memory>

#include "trans.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransBend> ShTransBendPr;

	// cross section of coil
	class TransBend: public Trans{
		// properties
		private:
			// bending over three axes
			arma::Col<double>::fixed<3> V1_ = {0.0,0.0,1.0}; // axis over which the bending takes place
			arma::Col<double>::fixed<3> V2_ = {0.0,1.0,0.0}; // bending direction
			double r_ = 0; // bending radius

		// methods
		public:
			// constructors
			TransBend();
			TransBend(const double vx1, const double vy1, const double vz1, const double vx2, const double vy2, const double vz2, const double r);
			TransBend(const arma::Col<double>::fixed<3> &V1, const arma::Col<double>::fixed<3> &V2, const double r);
			
			// factory methods
			static ShTransBendPr create();
			static ShTransBendPr create(const double vx1, const double vy1, const double vz1, const double vx2, const double vy2, const double vz2, const double r);
			static ShTransBendPr create(const arma::Col<double>::fixed<3> &V1, const arma::Col<double>::fixed<3> &V2, const double r);
			
			// setting
			void set_bending(const double vx1, const double vy1, const double vz1, const double vx2, const double vy2, const double vz2, const double r);
			void set_bending(const arma::Col<double>::fixed<3> &V1, const arma::Col<double>::fixed<3> &V2, const double r);
				
			// applying
			// void apply(arma::Mat<double> &R, arma::Mat<double> &L, arma::Mat<double> &N, arma::Mat<double> &D) const;
			void apply_coords(arma::Mat<double> &R) const;
			void apply_vectors(const arma::Mat<double> &R, arma::Mat<double> &V, const char str) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
