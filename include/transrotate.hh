/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_TRANS_ROTATE_HH
#define MDL_TRANS_ROTATE_HH

#include <armadillo> 
#include <memory>

#include "trans.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransRotate> ShTransRotatePr;

	// cross section of coil
	class TransRotate: public Trans{
		// properties
		private:
			// rotation matrix
			arma::Mat<double>::fixed<3,3> M_;
			
		// methods
		public:
			// constructors
			TransRotate();
			TransRotate(const double phi, const double theta, const double psi);
			TransRotate(const double ux, const double uy, const double uz, const double alpha);
			TransRotate(const arma::Col<double>::fixed<3> &V, const double alpha);

			// factory methods
			static ShTransRotatePr create();
			static ShTransRotatePr create(const double phi, const double theta, const double psi);
			static ShTransRotatePr create(const double ux, const double uy, const double uz, const double alpha);
			static ShTransRotatePr create(const arma::Col<double>::fixed<3> &V, const double alpha);

			// set rotation
			void set_rotation(const double phi, const double theta, const double psi);
			void set_rotation(const double ux, const double uy, const double uz, const double alpha);
			void set_rotation(const arma::Col<double>::fixed<3> &V, const double alpha);

			// apply rotation
			// void apply(arma::Mat<double> &R, arma::Mat<double> &L, arma::Mat<double> &N, arma::Mat<double> &D) const;
			virtual void apply_coords(arma::Mat<double> &R) const;
			virtual void apply_vectors(const arma::Mat<double> &R, arma::Mat<double> &V, const char str) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
