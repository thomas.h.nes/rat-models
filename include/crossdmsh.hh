/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CROSS_DMSH_HH
#define MDL_CROSS_DMSH_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/dmsh/distmesh2d.hh"
#include "rat/dmsh/distfun.hh"

#include "area.hh"
#include "perimeter.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossDMsh> ShCrossDMshPr;

	// cross section of coil
	class CrossDMsh: public Cross{
		// properties
		private:
			// quad mesh
			bool quad_ = true;

			// element size
			double h0_;

			// mesh input
			dm::ShDistFunPr fd_;
			dm::ShDistFunPr fh_;

			// additional fixed points
			arma::Mat<double> pfix_ext_ = arma::Mat<double>(0,2);

		// methods
		public:
			// constructors
			CrossDMsh(){};
			CrossDMsh(const double h0, dm::ShDistFunPr df, dm::ShDistFunPr hf);
			
			// factory methods
			static ShCrossDMshPr create(const double h0, dm::ShDistFunPr df, dm::ShDistFunPr hf);
			
			// set fixed points
			void set_fixed(const arma::Mat<double> &pfix_ext);

			// volume mesh
			virtual ShAreaPr create_area() const;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
