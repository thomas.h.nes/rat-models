/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_MESH_HH
#define MDL_MODEL_MESH_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "model.hh"
#include "path.hh"
#include "cross.hh"
#include "mesh.hh"
#include "edges.hh"
#include "transformations.hh"
#include "area.hh"
#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelMesh> ShModelMeshPr;

	// model object that is capable of creating meshes
	// using a base path and a cross section
	class ModelMesh: public Model, public Transformations, public Nameable{
		// properties
		protected:
			// assign drive
			arma::uword drive_id_ = 0;

			// operating conditions
			double operating_temperature_ = 4.5;

			// pointers to basepath and cross section
			ShPathPr base_ = NULL;
			ShCrossPr crss_ = NULL;

		// methods
		public:
			// constructor
			ModelMesh();
			ModelMesh(ShPathPr pth, ShCrossPr crss);

			// factory methods
			static ShModelMeshPr create();
			static ShModelMeshPr create(ShPathPr pth, ShCrossPr crss);

			// set base path and cross section
			void set_base(ShPathPr pth);
			void set_cross(ShCrossPr crss);	

			// set operating conditions
			void set_operating_temperature(const double operating_temperature);
			void set_drive_id(const arma::uword drive_id);

			// setup calculation objects
			void setup_mesh(ShMeshPr mesh) const;
			void setup_edge(ShEdgesPr edge) const;

			// get number of calculation objects (obviously only one)
			virtual arma::uword get_num_objects() const;

			// create calculation data objects
			virtual ShMeshPrList create_mesh() const; // create a mesh
			virtual ShEdgesPrList create_edge() const; // create edge matrices

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
