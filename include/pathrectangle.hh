/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_RECTANGLE_HH
#define MDL_PATH_RECTANGLE_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathRectangle> ShPathRectanglePr;

	// cross section of coil
	class PathRectangle: public Path, public Transformations{
		// properties
		protected:
			// shape parameters
			double width_ = 0; // width of rectangle [m]
			double height_ = 0; // height of rectangle [m]
			double radius_ = 0; // radius at corners [m]

			// element size 
			double element_size_ = 0; // size of the elements [m]

			// offset
			double offset_ = 0; // ofset [m]

			// bend for small synchrotrons
			// this is different from bending 
			// transformation as it only bends 
			// the straight section. When
			// set to zero the feature is 
			// disabled.
			double bending_radius_ = 0; // [m]

		// methods
		public:
			// constructor
			PathRectangle();
			PathRectangle(const double width, const double height, const double radius, const double element_size);

			// factory
			static ShPathRectanglePr create();
			static ShPathRectanglePr create(const double width, const double height, const double radius, const double element_size);

			// set properties
			void set_radius(const double radius);
			void set_num_sections(const arma::uword num_sections);

			// set properties
			void set_width(const double width);
			void set_height(const double height);
			void set_element_size(const double element_size);
			void set_offset(const double offset);
			void set_bending_radius(const double bending_radius);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
