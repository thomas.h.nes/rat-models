/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DARBOUX_HH
#define MDL_DARBOUX_HH

#include <armadillo> 
#include <memory>
#include <cmath>

#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Darboux> ShDarbouxPr;

	// template class for cross section
	class Darboux{
		// properties
		protected:
			// derivatives
			arma::Mat<double> V_; 
			arma::Mat<double> A_;
			arma::Mat<double> J_;

			// twist and bending
			arma::Row<double> kappa_;
			arma::Row<double> tau_;

			// orientation
			arma::Mat<double> B_;
			arma::Mat<double> T_;

			// darboux vectors
			arma::Mat<double> D_;

			// darboux vector angle
			arma::Row<double> alpha_;

		// methods
		public:
			// constructors
			Darboux();
			Darboux(const arma::Mat<double> &V, const arma::Mat<double> &A, const arma::Mat<double> &J);

			// factory
			static ShDarbouxPr create();
			static ShDarbouxPr create(const arma::Mat<double> &V, const arma::Mat<double> &A, const arma::Mat<double> &J);

			// calculate
			void setup(const bool analytic_frame);
			void correct_sign(const arma::Col<double>::fixed<3> &dstart);

			// get orientation vectors
			arma::Mat<double> get_longitudinal() const;
			arma::Mat<double> get_transverse() const;
			arma::Mat<double> get_normal() const;
	};

}}

#endif
