/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_AXIS_HH
#define MDL_PATH_AXIS_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "rat/common/extra.hh"
#include "pathgroup.hh"
#include "pathstraight.hh"
#include "transformations.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathAxis> ShPathAxisPr;

	// circular softway bend
	class PathAxis: public Path, public Transformations{
		// properties
		protected:
			// orientation
			char axis_;
			char dir_;

			// length
			double ell_;

			// position
			arma::Col<double>::fixed<3> Rc_;

			// element size
			double dl_;

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;

		// methods
		public:
			// constructor
			PathAxis(){};
			PathAxis(const char axis, const char dir, const double ell, const arma::Col<double>::fixed<3> &Rc, const double dl);
			PathAxis(const char axis, const char dir, const double ell, const double x, const double y, const double z, const double dl);
			
			// factory
			static ShPathAxisPr create(const char axis, const char dir, const double ell, const arma::Col<double>::fixed<3> &Rc, const double dl);
			static ShPathAxisPr create(const char axis, const char dir, const double ell, const double x, const double y, const double z, const double dl);

			// elements
			void set_element_divisor(const arma::uword element_divisor);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
