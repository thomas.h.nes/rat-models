/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_HH
#define MDL_MODEL_HH

#include <armadillo> 
#include <memory>

#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/node.hh"
#include "material.hh"
#include "mesh.hh"
#include "edges.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Model> ShModelPr;
	typedef arma::field<ShModelPr> ShModelPrList;

	// template for any modeling object
	class Model: virtual public cmn::Node{
		// methods
		public:
			// virtual destructor
			virtual ~Model(){};

			// exporting of stored geometry
			void export_freecad(cmn::ShFreeCADPr freecad) const; // export edges to freecad file
			void export_opera(cmn::ShOperaPr opera) const; // export edges to opera file

			// volume and surface area calculation (mainly for testing)
			double calc_total_volume() const;
			double calc_total_surface_area() const;

			// get number of calculation data objects
			virtual arma::uword get_num_objects() const = 0;

			// creation of calculation data objects
			virtual ShMeshPrList create_mesh() const = 0; // create volume current elements
			virtual ShEdgesPrList create_edge() const = 0; // create edges of the coil
	};

}}

#endif