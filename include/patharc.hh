/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_ARC_HH
#define MDL_PATH_ARC_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathArc> ShPathArcPr;

	// circular softway bend
	class PathArc: public Path{
		// properties
		protected:
			// bend in normal or transverse direction
			// where transverse implies hard-way bending
			bool transverse_ = false;

			// offset in [m]
			double offset_ = 0;

			// target element length in [m]
			double element_size_ = 0;

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;

			// radius of circle section in [m]
			double radius_ = 0;

			// angle of circle section in [rad]
			double arc_length_ = 0;


		// methods
		public:
			// constructor
			PathArc();
			PathArc(const double radius, const double arc_length, const double element_size, const double offset = 0);

			// factory
			static ShPathArcPr create();
			static ShPathArcPr create(const double radius, const double arc_length, const double element_size, const double offset = 0);

			// set geometry
			void set_transverse(const bool transverse);
			void set_arc_length(const double arc_length);
			void set_radius(const double radius);

			// path offset
			void set_offset(const double offset);

			// set element size 
			void set_element_size(const double element_size);
			void set_element_divisor(const arma::uword element_divisor);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
