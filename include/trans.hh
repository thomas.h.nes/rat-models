/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_TRANS_HH
#define MDL_TRANS_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Trans> ShTransPr;
	typedef arma::field<ShTransPr> ShTransPrList;

	// cross section of coil
	class Trans: virtual public cmn::Node{
		// methods
		public:
			// destructor
			virtual ~Trans(){};

			// transformation functions (have empty implementations)
			virtual void apply_coords(arma::Mat<double> &R) const;
			virtual void apply_vectors(const arma::Mat<double> &R, arma::Mat<double> &V, const char str) const;
			virtual void apply_mesh(arma::Mat<arma::uword> &n, const arma::uword num_nodes) const;

			// for field arrays
			virtual void apply_vectors(const arma::field<arma::Mat<double> > &R, arma::field<arma::Mat<double> > &V, const char str) const;
			virtual void apply_coords(arma::field<arma::Mat<double> > &R) const;
	};

}}

#endif
