/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_TRANS_TRANSLATE_HH
#define MDL_TRANS_TRANSLATE_HH

#include <armadillo>
#include <memory>

#include "trans.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class TransTranslate> ShTransTranslatePr;

	// cross section of coil
	class TransTranslate: public Trans{
		// properties
		private:
			// translation vector
			arma::Col<double>::fixed<3> dR_ = {0,0,0};

		// methods
		public:
			// constructors
			TransTranslate();
			TransTranslate(const arma::Col<double>::fixed<3> &dR);
			TransTranslate(const double dx, const double dy, const double dz);

			// factory methods
			static ShTransTranslatePr create();
			static ShTransTranslatePr create(const arma::Col<double>::fixed<3> &dR);
			static ShTransTranslatePr create(const double dx, const double dy, const double dz);

			// setting
			void set_vector(const arma::Col<double>::fixed<3> &dR);
			void set_vector(const double dx, const double dy, const double dz);

			// applying
			virtual void apply_coords(arma::Mat<double> &R) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
