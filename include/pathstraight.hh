/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_STRAIGHT_HH
#define MDL_PATH_STRAIGHT_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathStraight> ShPathStraightPr;

	// cross section of coil
	class PathStraight: public Path{
		// properties
		protected:		
			// geometry 
			double offset_ = 0; // offset in [m]
			double element_size_ = 0; // target element length in [m]	
			double length_ = 0; // length of the path in [m]

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;

		// methods
		public:
			// constructor
			PathStraight();
			PathStraight(const double length, const double element_size);

			// factory
			static ShPathStraightPr create();
			static ShPathStraightPr create(const double length, const double element_size);

			// elements
			void set_element_divisor(const arma::uword element_divisor);

			// set properties
			void set_length(const double length);
			virtual void set_element_size(const double element_size);
			virtual void set_offset(const double offset);
			
			// get frame
			virtual ShFramePr create_frame() const;
	};

}}

#endif
