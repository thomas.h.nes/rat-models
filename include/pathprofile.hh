/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// Class Description:
// connects two paths with a connector
// this then becomes a new combined path
// assumed is that the top profile resides 
// in the xy plane, while the side profile 
// is located in the yz plane. The axis of 
// the magnet is then oriented along the 
// y-axis.

#ifndef MDL_PATH_PROFILE_HH
#define MDL_PATH_PROFILE_HH

#include <armadillo> 
#include <cassert>
#include <memory>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "rat/common/extra.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathProfile> ShPathProfilePr;

	// profile class
	class PathProfile: public Path, public Transformations{
		// properties
		private:
			// top profile in xy plane
			ShPathPr top_ = NULL;

			// side profile in yz plane
			ShPathPr side_ = NULL;

			// introduce a twist along the path
			// this is used in Feather-M2
			double alpha_ = 0; // shear angle [rad]
			double pshear_ = 1; // shear parameter
			double ellshear_ = 1; // shear length [m]

		// methods
		public:
			// constructor
			PathProfile();
			PathProfile(ShPathPr top, ShPathPr side);

			// factory methods
			static ShPathProfilePr create();
			static ShPathProfilePr create(ShPathPr top, ShPathPr side);

			// set properties
			void set_top(ShPathPr top);
			void set_side(ShPathPr side);
			void set_shearing(const double alpha, const double ptwist, const double ellshear);

			// get frame
			virtual ShFramePr create_frame() const;
	};

}}

#endif
