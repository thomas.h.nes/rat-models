/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MESH_BAR_HH
#define MDL_MESH_BAR_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// common header files
#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

// mlfmm header files
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/interp.hh"

//#include "coilsurface.hh"
#include "mesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MeshBar> ShMeshBarPr;
	typedef arma::field<ShMeshBarPr> ShMeshBarPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class MeshBar: public Mesh{
		// properties
		private:
			// use line elements or volume elements
			bool use_volume_elements_ = false;

			// number of gauss points to use for current sources
			double softening_ = 0.7;

			// magnetisation at the nodes
			arma::Col<double>::fixed<3> Mf_;

		// methods
		public:
			// default constructor
			MeshBar();
			MeshBar(ShFramePr &frame, ShAreaPr &area);

			// virtual destructor
			virtual ~MeshBar(){};

			// factory
			static ShMeshBarPr create();
			static ShMeshBarPr create(ShFramePr &frame, ShAreaPr &area);

			// set magnetisation
			void set_magnetisation(const arma::Col<double>::fixed<3> &Mf);

			// set softening
			void set_softening(const double softening);
			void set_use_volume_elements(const bool use_volume_elements);

			// create a copy
			virtual ShMeshPr copy() const;

			// method for creating MLFMM sources
			virtual fmm::ShSourcesPrList create_sources(const arma::sword num_gauss, const arma::sword num_gauss_surface) const;

	};

}}

#endif
