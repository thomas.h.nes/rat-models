/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_EDGES_HH
#define MDL_EDGES_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"

#include "trans.hh"
#include "nameable.hh"
#include "frame.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Edges> ShEdgesPr;
	typedef arma::field<ShEdgesPr> ShEdgesPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class Edges: public Nameable{
		// properties
		protected:
			// store frame
			ShFramePr gen_;

			// store area
			ShAreaPr area_;

			// edge matrix
			arma::field<arma::Mat<double> > Re_;

			// section and turn
			arma::Row<arma::uword> section_;
			arma::Row<arma::uword> turn_;

			// drive
			arma::uword drive_id_;

			// temperature
			double operating_temperature_;

		// methods
		public:
			// constructor
			Edges();
			Edges(ShFramePr &frame, ShAreaPr &area);

			// destructor
			virtual ~Edges(){};

			// factory
			static ShEdgesPr create();
			static ShEdgesPr create(ShFramePr &frame, ShAreaPr &area);

			// setup function
			void setup(ShFramePr &frame, ShAreaPr &area);

			// get stored edge matrix
			arma::field<arma::Mat<double> > get_edges() const;

			// operating temperature
			void set_operating_temperature(const double operating_temperature);
			double get_operating_temperature() const;

			// setting coil properties
			void set_area_crss(const double area_crss);

			// calculate edge length
			arma::Mat<double> calc_ell() const;

			// assign a drive
			void set_drive_id(const arma::uword drive_id);
			arma::uword get_drive_id() const;

			// transformations
			void apply_transformations(const ShTransPrList &trans);
			void apply_transformation(const ShTransPr &trans);

			// construct edge matrices
			void create_xyz(arma::field<arma::Mat<double> > &x, arma::field<arma::Mat<double> > &y, arma::field<arma::Mat<double> > &z) const;

			// export edges to freecad file
			virtual void export_freecad(cmn::ShFreeCADPr freecad) const;
			virtual void export_opera(cmn::ShOperaPr opera) const;

			// create a copy
			virtual ShEdgesPr copy() const;
	};

}}

#endif
