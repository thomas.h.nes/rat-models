/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_FLARED_HH
#define MDL_PATH_FLARED_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathFlared> ShPathFlaredPr;

	// cross section of coil
	class PathFlared: public Path, public Transformations{
		// properties
		protected:
			// coil geometry settings
			double ell1_ = 0; // straight section length [m]
			double ell2_ = 0; // flared section length [m]

			// length of the arc
			double arcl_ = 0; // hardway bending angle [rad]
			
			// radii
			double radius1_ = 0; // hardway bending radius [m]
			double radius2_ = 0; // softway bending radius [m]
			
			// element size
			double element_size_ = 0; // size of the elements [m]

			// radial offset
			double offset_ = 0; // softway bend offset [m]
			double offset_arc_ = 0; // hardway bend offset [m]

		// methods
		public:
			// constructor
			PathFlared();
			PathFlared(const double ell1, const double ell2, const double arcl, const double radius1, const double radius2, const double element_size, const double offset = 0, const double offset_arc = 0);

			// factory
			static ShPathFlaredPr create();
			static ShPathFlaredPr create(const double ell1, const double ell2, const double arcl, const double radius1, const double radius2, const double element_size, const double offset = 0, const double offset_arc = 0);

			// set properties
			void set_element_size(const double element_size);
			void set_offset(const double offset);
			void set_offset_arc(const double offset_arc);
			void set_arcl(const double arcl);
			void set_ell1(const double ell1);
			void set_ell2(const double ell2);
			void set_radius1(const double radius1);
			void set_radius2(const double radius2);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
