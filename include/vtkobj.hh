/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef MDL_VTK_HH
#define MDL_VTK_HH

// general headers
#include <armadillo> 
#include <memory>

#include "rat/common/log.hh"
#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKObj> ShVTKObjPr;

	// VTK Unstructured grid
	class VTKObj: public Nameable{
		public:
			// destructor
			virtual ~VTKObj(){};

			// write output file
			virtual void write(const std::string fname, cmn::ShLogPr lg = cmn::NullLog::create()) = 0;
	};

}}

#endif
