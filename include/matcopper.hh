/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MATERIAL_COPPER_HH
#define MDL_MATERIAL_COPPER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatCopper> ShMatCopperPr;

	// material properties based on NIST
	class MatCopper: public Material{
		// parameters
		private:
			// density
			double density_ = 8940; // [kg/m3]

			// tripple-R
			double RRR_ = 50;

		// methods
		public:
			// conductor
			MatCopper();
			MatCopper(const double RRR);

			// factory
			static ShMatCopperPr create();
			static ShMatCopperPr create(const double RRR);

			// default conductors
			void set_copper_OFHC_RRR50();
			void set_copper_OFHC_RRR100();
			void set_copper_OFHC_RRR150();
			void set_copper_OFHC_RRR300();
			void set_copper_OFHC_RRR500();
			void set_RRR(const double RRR);

			// calculate electrical resistivity
			arma::Row<double> calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;

			// calculate electric field
			arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const;
			arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const;

			// copy
			ShMaterialPr copy() const;

			// precalculate material properties for fast electric field calculations
			virtual arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// thermal properties
			virtual arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			virtual arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif