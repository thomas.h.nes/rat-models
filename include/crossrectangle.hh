/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CROSS_RECTANGLE_HH
#define MDL_CROSS_RECTANGLE_HH

#include <armadillo> 
#include <memory>
#include <cmath>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "area.hh"
#include "perimeter.hh"
#include "cross.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossRectangle> ShCrossRectanglePr;

	// template class for cross section
	class CrossRectangle: public Cross{
		// properties
		protected:
			// dimensions in [m]
			double n1_;
			double n2_;
			double t1_;
			double t2_;

			// element size in [m]
			arma::uword num_n_;
			arma::uword num_t_;

		// methods
		public:
			// constructor
			CrossRectangle();

			// constructor with input
			CrossRectangle(const double n1, const double n2, const double t1, const double t2, const double dl);
			CrossRectangle(const double n1, const double n2, const double t1, const double t2, const arma::uword num_n, const arma::uword num_t);

			// factory
			static ShCrossRectanglePr create();

			// factory with input
			static ShCrossRectanglePr create(const double n1, const double n2, const double t1, const double t2, const double dl);
			static ShCrossRectanglePr create(const double n1, const double n2, const double t1, const double t2, const arma::uword num_n, const arma::uword num_t);

			// set properties
			void set_dim_transverse(const double t1, const double t2, const arma::uword num_t);
			void set_dim_transverse(const double t1, const double t2, const double dl);
			void set_dim_normal(const double n1, const double n2, const arma::uword num_n);
			void set_dim_normal(const double n1, const double n2, const double dl);

			// volume mesh
			virtual ShAreaPr create_area() const;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
