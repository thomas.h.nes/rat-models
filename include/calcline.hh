/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CALCULATE_LINE_HH
#define MDL_CALCULATE_LINE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "model.hh"
#include "path.hh"
#include "vtkunstr.hh"
#include "calcfieldmap.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcLine> ShCalcLinePr;

	// template for coil
	class CalcLine: public CalcFieldMap{		
		// properties
		protected:
			// line definition
			ShPathPr base_ = NULL;
			
			// coordinates
			//arma::Mat<double> R_;
			arma::Mat<double> L_;
			arma::Mat<double> D_;
			arma::Mat<double> N_;

			// position along line
			arma::Row<double> ell_;
		
		// methods
		public:
			// constructor
			CalcLine();
			CalcLine(ShModelPr model, ShPathPr base);

			// factory methods
			static ShCalcLinePr create();
			static ShCalcLinePr create(ShModelPr model, ShPathPr base);

			// setting of the path
			void set_base(ShPathPr base);

			// calculation
			void setup(cmn::ShLogPr lg = cmn::NullLog::create()) override;
			
			// access to vectors per section
			virtual arma::Mat<double> get_coords() const;

			// orientation vectors
			arma::Mat<double> get_direction() const;
			arma::Mat<double> get_normal() const;
			arma::Mat<double> get_transverse() const;

			// export to vtk
			ShVTKUnstrPr export_vtk_coord(cmn::ShLogPr lg = cmn::NullLog::create()) const;
			void write(cmn::ShLogPr lg = cmn::NullLog::create()) override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};
}}

#endif
