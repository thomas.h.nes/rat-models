/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CALCULATE_LENGTH_HH
#define MDL_CALCULATE_LENGTH_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "model.hh"
#include "calc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CalcLength> ShCalcLengthPr;

	// template for coil
	class CalcLength: public Calc{		
		// properties
		protected:
			// input model
			ShModelPr model_;

			// stored properties
			arma::field<std::string> name_; // mesh names
			arma::Row<double> ell_; // length of conductor calculated from volume
			arma::Row<double> volume_; // volume of mesh
			arma::Row<double> area_; // extruded cross sectional area of mesh
			arma::Row<double> ell_gen_; // length of the generator frame
			arma::Row<double> num_turns_; // number of turns

		// methods
		public:
			// constructor
			CalcLength();
			CalcLength(ShModelPr model);

			// factory
			static ShCalcLengthPr create();
			static ShCalcLengthPr create(ShModelPr model);

			// set input model
			void set_model(ShModelPr model);

			// calculation
			void calculate(cmn::ShLogPr lg = cmn::NullLog::create());

			// write output files
			void write(cmn::ShLogPr lg = cmn::NullLog::create());

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};
}}

#endif
