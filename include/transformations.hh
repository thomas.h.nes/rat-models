/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_TRANSFORMATIONS_HH
#define MDL_TRANSFORMATIONS_HH

#include <armadillo> 
#include <cassert>

#include "rat/common/node.hh"
#include "trans.hh"
#include "transtranslate.hh"
#include "transrotate.hh"
#include "transreflect.hh"
#include "transreverse.hh"
#include "transflip.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Transformations> ShTransformationsPr;

	// superclass to add transformation options
	class Transformations: virtual public cmn::Node{
		// transformations
		protected:
			ShTransPrList trans_;

		// methods
		public:
			// constructor
			Transformations();

			// virtual destructor
			virtual ~Transformations(){};

			// factory (in case of standalone use)
			static ShTransformationsPr create();

			// transformations
			void add_transformation(ShTransPr trans);
			void add_transformation(arma::field<ShTransPr> trans);

			// adding of standard transformations
			void add_translation(const double dx, const double dy, const double dz);
			void add_translation(const arma::Col<double>::fixed<3> &dR);
			void add_rotation(const double phi, const double theta, const double psi);
			void add_rotation(const double ux, const double uy, const double uz, const double alpha);
			void add_rotation(const arma::Col<double>::fixed<3> &V, const double alpha);
			void add_reflect_xy();
			void add_reflect_yz();
			void add_reflect_xz();
			void add_reverse();
			void add_flip();

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif




