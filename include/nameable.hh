/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_NAMEABLE_HH
#define MDL_NAMEABLE_HH

#include <string>
#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// template for coil
	class Nameable: virtual public cmn::Node{
		// properties
		protected:
			// update lock
			std::string myname_ = "";

		// methods
		public:
			// get name
			std::string get_name()const;

			// get name
			void set_name(const std::string &name);

			// append name
			void append_name(const std::string &append);

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
