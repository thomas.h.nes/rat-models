/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CROSS_LINE_HH
#define MDL_CROSS_LINE_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "cross.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossLine> ShCrossLinePr;

	// cross section of coil
	class CrossLine: public Cross{
		// properties
		private:
			// coordinate
			arma::Row<double> u_; // in n direction
			arma::Row<double> v_; // in d direction

			// thickness
			double thickness_ = 0.1e-3;

		// methods
		public:
			// constructors
			CrossLine();
			CrossLine(const arma::Row<double> &u, const arma::Row<double> &v, const double thickness);
			CrossLine(const double u1, const double v1, const double u2, const double v2, const double thickness, const double dl);

			// factory methods
			static ShCrossLinePr create();
			static ShCrossLinePr create(const arma::Row<double> &u, const arma::Row<double> &v, const double thickness);
			static ShCrossLinePr create(const double u1, const double v1, const double u2, const double v2, const double thickness, const double dl);

			// set properties
			void set_coord(const arma::Row<double> &u, const arma::Row<double> &v);
			void set_coord(const double u1, const double v1, const double u2, const double v2, const double dl);
			void set_thickness(const double thickness);

			// volume mesh
			virtual ShAreaPr create_area() const;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
