/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include guard
#ifndef MDL_VTK_UNSTR_HH
#define MDL_VTK_UNSTR_HH

// general headers
#include <armadillo> 
#include <memory>
#include <iomanip>

// magrat headers
#include "rat/common/defines.hh"
#include "rat/common/log.hh"

#include "vtkobj.hh"

// VTK headers
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkXMLUnstructuredGridWriter.h> 
#include <vtkDoubleArray.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkPointData.h>
#include <vtkDataSet.h>
#include <vtkPointData.h>
#include <vtkProbeFilter.h>
#include <vtkPolyData.h>
#include "vtkCellArray.h"
//#include <vtkPProbeFilter.h>
//#include <vtkMultiProcessController.h>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class VTKUnstr> ShVTKUnstrPr;

	// VTK Unstructured grid
	class VTKUnstr: public VTKObj{
		protected:
			// pointer to dataset
			vtkSmartPointer<vtkUnstructuredGrid> vtk_ugrid_;

			// probe for data interpolation
			vtkSmartPointer<vtkProbeFilter> vtk_probe_;

			// number of nodes and elements
			arma::uword num_nodes_;
			arma::uword num_elements_;

		public:
			// constructor
			VTKUnstr();

			// factory
			static ShVTKUnstrPr create();

			// write mesh
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_nodes(const arma::Mat<double> &Rn);
			void set_elements(const arma::Mat<arma::uword> &n, const arma::uword element_type);
			void set_elements(const arma::field<arma::Mat<arma::uword> > &n, const arma::Row<arma::uword> &element_types);

			// add time stamp to data
			void set_time(const double time);

			// write data at nodes
			void set_nodedata(const arma::Mat<double> &v, const std::string &name);

			// write output file
			void write(const std::string fname, cmn::ShLogPr lg = cmn::NullLog::create());

			// interpolation methods
			void setup_interpolation();
			arma::Mat<double> interpolate_field(const arma::Mat<double> &Rp, const std::string &field_name);

			// element type list
			static int get_element_type(const arma::uword num_rows);
	};

}}

#endif
