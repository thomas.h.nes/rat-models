/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CALC_HH
#define MDL_CALC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"
#include "rat/common/node.hh"

#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Calc> ShCalcPr;
	typedef arma::field<ShCalcPr> ShCalcPrList;

	// output types
	enum CalcOutTypes{
		ORIENTATION, MAGNETIC_FLUX, MAGNETISATION, VECTOR_POTENTIAL, 
		MAGNETIC_FIELD, CURRENT_DENSITY, TEMPERATURE, FIELD_ANGLE,
		ENG_CURRENT_DENSITY, CRIT_TEMPERATURE, FORCE_DENSITY, 
		ELECTRIC_FIELD, ALL
	};

	// template for coil
	class Calc: public Nameable{
		// output settings
		protected:
			// data directory
			std::string output_dir_;
			std::string output_fname_ = "meshdata";

			// list of output times (by default only zero)
			arma::Row<double> output_times_ = {0};

			// list of output data types (better implemented with std::set)
			std::list<CalcOutTypes> output_types_ = {VECTOR_POTENTIAL, MAGNETIC_FLUX};

		// methods
		public:
			// virtual destructor
			virtual ~Calc(){};

			// set data directory
			void set_output_dir(const std::string &output_dir);
			void set_output_fname(const std::string &output_fname);

			// set output times
			void set_output_times(const arma::Row<double> &output_times);
			arma::Row<double> get_output_times() const;

			// set type for writing to output
			void set_output_type(const CalcOutTypes type);
			void add_output_type(const CalcOutTypes type);

			// settings display function
			void display_settings(cmn::ShLogPr lg) const;

			// virtual functions
			virtual void calculate_and_write(cmn::ShLogPr lg = cmn::NullLog::create());
			virtual void calculate(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;
			virtual void write(cmn::ShLogPr lg = cmn::NullLog::create()) = 0;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
