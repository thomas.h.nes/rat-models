/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MATERIAL_HH
#define MDL_MATERIAL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/extra.hh"
#include "rat/common/node.hh"
#include "area.hh"
#include "vtktable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Material> ShMaterialPr;
	typedef arma::field<ShMaterialPr> ShMaterialPrList;

	// template for materials
	class Material: virtual public cmn::Node{
		// methods
		public:
			// virtual destructor
			virtual ~Material(){};
			
			// get fraction list
			arma::Col<double> get_fraction() const;

			// precalculate material properties for fast electric field calculations
			virtual arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const = 0;

			// calculate electric field
			virtual arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const = 0;
			virtual arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const = 0;
			virtual arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props, const ShAreaPr area) const;

			// without pre-calculation
			virtual arma::Row<double> calc_current_density(const arma::Row<double> &E, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_electric_field(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const ShAreaPr area) const;

			// calculate electric field with scalar input
			virtual double calc_specific_heat(const double T) const;
			virtual double calc_electric_field(const double J, const double Bm, const double T, const double alpha) const;

			// critical current function
			// if critical current returns 0 it is either 
			// a superconductor in normal conducting state
			// or a normal conductor
			virtual arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			virtual arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const ShAreaPr area) const;

			// critical temperature calculation
			virtual arma::Row<double> calc_critical_temperature(const arma::Row<double> &J, const arma::Row<double> &Bm, const arma::Row<double> &alpha, const ShAreaPr area = NULL) const;

			// thermal properties
			// virtual arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const = 0;
			virtual arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const = 0;
			virtual arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const = 0;

			// resistivity function returns only reasonable
			// values when above critical temperature
			virtual arma::Row<double> calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;

			// export vtk table
			ShVTKTablePr export_vtk(const double Tmin = 1.0, const double Tmax = 300.0, const double Jfix = 100e6) const;

			// copy
			virtual ShMaterialPr copy() const = 0;
	};

}}

#endif