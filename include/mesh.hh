/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MESH_HH
#define MDL_MESH_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentmesh.hh"
#include "rat/mlfmm/sources.hh"

#include "rat/common/elements.hh"
#include "rat/common/extra.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/node.hh"

#include "trans.hh"
#include "area.hh"
#include "frame.hh"
#include "nameable.hh"
#include "surface.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Mesh> ShMeshPr;
	typedef arma::field<ShMeshPr> ShMeshPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class Mesh: public Nameable{
		// properties
		protected:
			// frame
			ShFramePr frame_;

			// cross sectional area
			ShAreaPr area_;

			// mesh node coordinates
			arma::Mat<double> R_;
			
			// node orientation vectors
			arma::Mat<double> L_;
			arma::Mat<double> N_;
			arma::Mat<double> D_;

			// mesh element indexes
			arma::Mat<arma::uword> n_; // internal (hex)
			arma::Mat<arma::uword> s_; // surface (quad)

			// dimensionality of meshes
			arma::uword n_dim_ = 0;
			arma::uword s_dim_ = 0;

			// operating drive
			arma::uword drive_id_ = 0;

			// operating conditions
			double operating_temperature_;

		// methods
		public:
			// constructors
			Mesh(); // default
			Mesh(ShFramePr &frame, ShAreaPr &area);
		
			// virtual destructor
			virtual ~Mesh(){};

			// factory
			static ShMeshPr create();
			static ShMeshPr create(ShFramePr &frame, ShAreaPr &area);

			// setup mesh using frame and area
			void setup(ShFramePr &frame, ShAreaPr &area);

			// get pointers to area and frame
			ShFramePr get_frame();
			ShAreaPr get_area();

			// get coordinates and elements 
			const arma::Mat<double>& get_coords() const;
			const arma::Mat<arma::uword>& get_elements() const;
			const arma::Mat<arma::uword>& get_surface_elements() const;

			// get orientation vectors
			const arma::Mat<double>& get_direction() const;
			const arma::Mat<double>& get_normal() const;
			const arma::Mat<double>& get_transverse() const;

			// get element dimensions
			arma::uword get_n_dim() const;
			arma::uword get_s_dim() const;

			// apply a transformation to the mesh and orientation vectors
			void apply_transformations(const ShTransPrList &trans);
			void apply_transformation(const ShTransPr &trans);

			// setting operating conditions
			void set_drive_id(const arma::uword drive_id);
			void set_operating_temperature(const double operating_temperature);

			// getting operating conditions
			double get_operating_temperature() const;
			arma::uword get_drive_id() const;

			// getting counters
			arma::uword get_num_nodes() const;
			arma::uword get_num_elements() const;
			arma::uword get_num_surface() const;

			// calculation of volume, length and surface area
			double calc_total_volume() const;
			double calc_total_surface_area() const;
			arma::Row<double> calc_volume() const;
			double calc_ell() const;

			// setup surface
			void setup_surface(ShSurfacePr surf) const;

			// get the current density and temperature at the mesh nodes
			// for a non-coil mesh these output zero matrices
			virtual arma::Mat<double> get_nodal_current_density() const;
			virtual arma::Mat<double> get_nodal_temperature() const;
			
			// calculate field angle, critical current, critical temperature and electric field
			// the input for this is the magnetic field, temperature and current density at the nodes
			virtual arma::Row<double> calc_alpha(const arma::Mat<double> &B) const;
			virtual arma::Row<double> calc_Je(const arma::Mat<double> &B, const arma::Row<double> &T) const;
			virtual arma::Row<double> calc_Tc(const arma::Mat<double> &J, const arma::Mat<double> &B) const;
			virtual arma::Mat<double> calc_E(const arma::Mat<double> &J, const arma::Mat<double> &B, const arma::Row<double> &T) const;

			// get number of turns
			virtual double get_number_turns() const;

			// create target coordinates for mlfmm
			virtual fmm::ShMgnTargetsPr create_node_targets() const; // at the nodes
			virtual fmm::ShMgnTargetsPr create_element_targets() const; // at the barycenters of the elements

			// create sources for the mlfmm
			// virtual fmm::ShCurrentSourcesPr create_current_sources() const; 
			// virtual fmm::ShCurrentMeshPr create_current_mesh() const;
			virtual ShSurfacePr create_surface() const;
			virtual fmm::ShSourcesPrList create_sources(const arma::sword num_gauss, const arma::sword num_gauss_surface) const;

			// deep copy the entire mesh
			virtual ShMeshPr copy() const;

			// write mesh to gmsh file
			virtual void export_gmsh(cmn::ShGmshFilePr gmsh, const bool incl_surface = true, const bool incl_vectors = true);

			// display mesh data
			static void display(cmn::ShLogPr &lg, ShMeshPrList &meshes);

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif
