/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CIRCLE_HH
#define MDL_PATH_CIRCLE_HH

#include <armadillo> 
#include <memory>
#include <cassert>
#include <json/json.h>

#include "rat/common/defines.hh"

#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "patharc.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCircle> ShPathCirclePr;

	// cross section of coil
	class PathCircle: public Path, public Transformations{
		// properties
		protected:
			// number of sections
			arma::uword num_sections_ = 0;

			// circle radius		
			double radius_ = 0;

			// element size
			double element_size_ = 0;

			// offset
			double offset_ = 0;

			// number of elements must be divisible by this number
			arma::uword element_divisor_ = 1;

		// methods
		public:
			// constructor
			PathCircle();
			PathCircle(const double radius, const arma::uword num_sections, const double element_size, const double offset = 0);

			// factory
			static ShPathCirclePr create();
			static ShPathCirclePr create(const double radius, const arma::uword num_sections, const double element_size, const double offset = 0);

			// set properties
			void set_radius(const double radius);
			void set_num_sections(const arma::uword num_sections);
			void set_element_size(const double element_size);
			void set_element_divisor(const arma::uword element_divisor);
			void set_offset(const double offset);

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
