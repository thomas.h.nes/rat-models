/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CLOVER_HH
#define MDL_PATH_CLOVER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "path.hh"
#include "frame.hh"
#include "pathgroup.hh"
#include "pathbezier.hh"
#include "patharc.hh"
#include "pathstraight.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathClover> ShPathCloverPr;

	// cross section of coil
	class PathClover: public Path, public Transformations{
		// properties
		protected:
			// transition length
			double ell_trans_ = 0;

			// thickness of the coil pack
			double dpack_ = 0;

			// height of the bridge
			double height_ = 0;

			// strengths for bezier curves
			double str12_ = 0;
			double str34_ = 0;

			// element sizes
			double element_size_ = 0;

			// straight section lengths
			double ellstr1_ = 0;
			double ellstr2_ = 0;

			// bend for small synchrotrons
			// this is different from bending 
			// transformation as it only bends 
			// the straight section. When
			// set to zero the feature is 
			// disabled.
			double bending_radius_ = 0;

			// angle of the bridge section [rad]
			double bridge_angle_ = 0;

		// methods
		public:
			// constructor
			PathClover();
			PathClover(const double ellstr1, const double ellstr2, const double height, const double dpack, const double ell_trans, const double str12, const double str34, const double element_size);

			// factory
			static ShPathCloverPr create();
			static ShPathCloverPr create(const double ellstr1, const double ellstr2, const double height, const double dpack, const double ell_trans, const double str12, const double str34, const double element_size);

			// set properties
			void set_ell_trans(const double ell_trans);
			void set_dpack(const double dpack);
			void set_height(const double height);
			void set_ellstr1(const double ellstr1);
			void set_ellstr2(const double ellstr2);
			void set_str12(const double str12);
			void set_str34(const double str34);
			void set_element_size(const double element_size);
			void set_bending_radius(const double bending_radius);
			void set_bridge_angle(const double bridge_angle);

			// get properties
			double get_ellstr1() const;
			double get_ellstr2() const;

			// get frame
			virtual ShFramePr create_frame() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
