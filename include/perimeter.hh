/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MESH_PERIMETER_HH
#define MDL_MESH_PERIMETER_HH

#include <armadillo> 
#include <memory>
#include <cassert>

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Perimeter> ShPerimeterPr;

	// path virtual class description
	class Perimeter{
		// properties
		protected:
			// nodes [2xN] matrix
			arma::Mat<double> Rn_;

			// elements
			arma::Mat<arma::uword> n_;
			
		// methods
		public:
			// default constructor
			Perimeter();

			// constructor with input
			Perimeter(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// factory
			static ShPerimeterPr create();

			// factory with input
			static ShPerimeterPr create(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// set mesh
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// calculate length of periphery
			double calculate_length() const;

			// get number of elements
			arma::uword get_num_elements() const;

			// get number of nodes
			arma::uword get_num_nodes() const;

			// access nodes and elements
			arma::Mat<double> get_nodes() const;
			arma::Mat<arma::uword> get_elements() const;
	};

}}

#endif
