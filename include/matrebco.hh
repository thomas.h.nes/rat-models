/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MATERIAL_REBCO_HH
#define MDL_MATERIAL_REBCO_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatReBCO> ShMatReBCOPr;

	// Lubell/Kramer scaling relation
	class MatReBCO: public Material{
		protected:
			// density
			double density_ = 6380;

			// general parameters
			double Tc0_; // [K]
			double n_;
			double n1_;
			double n2_;

			// Parameters for ab-plane
			double pab_;
			double qab_;
			double Bi0ab_; // [T]
			double a_;
			double yab_;
			double alpha_ab_; // [AT/m^2]

			// Parameters for c-plane
			double pc_;
			double qc_;
			double Bi0c_; // [T]
			double yc_;
			double alphac_; // [AT/m^2]

			// Parameters for anisotropy
			double g0_;
			double g1_;
			double g2_;
			double g3_;
			double v_;
			double pkoff_;

			// power law settings
			double N_; // N value 
			double E0_; // electric field criterion [V/m]

			// type-0 pair flipping
			bool type0_flip_;

			// interpolation arrays
			arma::Col<double> Ti_;
			arma::Col<double> ki_;
			arma::Col<double> Cpi_; 
			arma::Col<double> rhoi_; 

		// methods
		public:
			// conductor
			MatReBCO();

			// factory
			static ShMatReBCOPr create();

			// setup interpolation
			void setup_interpolation();

			// default conductor settings
			void set_fujikura_cern();
			// void set_superpower_te();
			// void set_shanghai_te();
			// void set_fujikura_te();

			// precalculate material properties for fast electric field calculations
			arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate critical current
			arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate electric field
			arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const;
			arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const;

			// thermal properties
			// arma::Row<double> calc_resistivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif