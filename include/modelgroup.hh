/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_GROUP_HH
#define MDL_MODEL_GROUP_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "rat/common/parfor.hh"

#include "transformations.hh"
#include "model.hh"
#include "mesh.hh"
#include "edges.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelGroup> ShModelGroupPr;

	// a collection of coils
	class ModelGroup: public Model, public Transformations, public Nameable{
		// set coils
		protected:
			// input coils
			ShModelPrList models_;

			// parallel build
			bool parallel_setup_ = true;

		// methods
		public:
			// constructor
			ModelGroup();
			ModelGroup(ShModelPr model);
			ModelGroup(ShModelPrList models);

			// factory methods
			static ShModelGroupPr create();
			static ShModelGroupPr create(ShModelPr model);
			static ShModelGroupPr create(ShModelPrList models);

			// add model to this group
			void add_model(ShModelPr model);

			// get number of calculation objects in all submodels
			virtual arma::uword get_num_objects() const;

			// create calculation data objects
			virtual ShMeshPrList create_mesh() const; // create a mesh of the stored coils
			virtual ShEdgesPrList create_edge() const; // create edges of the coil

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
