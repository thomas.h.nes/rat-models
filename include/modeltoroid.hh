/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_TOROID_HH
#define MDL_MODEL_TOROID_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "model.hh"
#include "rat/common/parfor.hh"

#include "transformations.hh"
#include "mesh.hh"
#include "edges.hh"
#include "nameable.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelToroid> ShModelToroidPr;

	// applies toroid symmetry to the input coil
	class ModelToroid: public Model, public Transformations, public Nameable{
		// set coils
		protected:
			// base coil
			ShModelPr base_model_;

			// settings
			arma::uword num_coils_ = 0;
			double radius_ = 0;

			// setup sourcemeshes in parallel
			bool parallel_setup_ = true;

		// methods
		public:
			// constructor
			ModelToroid();
			ModelToroid(ShModelPr base_model);

			// factory method
			static ShModelToroidPr create();
			static ShModelToroidPr create(ShModelPr base_model);

			// setting properties
			void set_num_coils(const arma::uword num_coils);
			void set_radius(const double radius);
			void set_parallel_setup(const bool parallel_setup);

			// set basemodel
			void set_base_model(ShModelPr base_model);

			// get number of calculation objects
			virtual arma::uword get_num_objects() const;

			// create calculation data objects
			virtual ShMeshPrList create_mesh() const; // create volume current elements
			virtual ShEdgesPrList create_edge() const; // create edges of the coil

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
