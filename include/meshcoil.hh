/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MESH_COIL_HH
#define MDL_MESH_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/currentsurface.hh"
#include "rat/mlfmm/currentmesh.hh"

//#include "coilsurface.hh"
#include "mesh.hh"
#include "material.hh"
#include "surfacecoil.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MeshCoil> ShMeshCoilPr;
	typedef arma::field<ShMeshCoilPr> ShMeshCoilPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class MeshCoil: public Mesh{
		// properties
		protected:
			// material of this coil
			ShMaterialPr material_;

			// homogenized properties throughout cross section
			bool enable_current_sharing_ = false;

			// operating current
			double operating_current_;
			
			// number of turns
			double number_turns_;

			// use line elements or volume elements
			bool use_volume_elements_ = false;

			// number of gauss points to use for current sources
			double softening_ = 1.0;


		// methods
		public:
			// default constructor
			MeshCoil();
			MeshCoil(ShFramePr &frame, ShAreaPr &area);

			// destructor
			virtual ~MeshCoil(){};

			// factory
			static ShMeshCoilPr create();
			static ShMeshCoilPr create(ShFramePr &frame, ShAreaPr &area);

			// set/get operating current
			double calc_current_density() const;

			// get line current elements
			void get_current_elements(arma::Mat<double> &Rl, arma::Mat<double> &dRl, arma::Row<double> &Acrss, const arma::sword num_gauss) const;
			void get_current_elements(arma::Mat<double> &Rl, arma::Mat<double> &dRl, arma::Row<double> &Acrss, const arma::sword num_gauss, const arma::uword idx1, const arma::uword idx2) const;

			// set softening
			void set_softening(const double softening);
			void set_use_volume_elements(const bool use_volume_elements);

			// operating current
			void set_operating_current(const double operating_current);
			double get_operating_current() const;

			// number of turns
			void set_number_turns(const double number_turns);

			// material
			void set_material(ShMaterialPr material);
			ShMaterialPr get_material() const;

			// current sharing
			void set_enable_current_sharing(const bool enable_current_sharing);

			// create a copy
			virtual ShMeshPr copy() const override;

			// get the current density at mesh nodes
			virtual arma::Mat<double> get_nodal_current_density() const override;
			
			// calculate field angle, critical current, critical temperature and electric field
			// the input for this is the magnetic field, temperature and current density at the nodes
			virtual arma::Row<double> calc_alpha(const arma::Mat<double> &B) const override;
			virtual arma::Row<double> calc_Je(const arma::Mat<double> &B, const arma::Row<double> &T) const override;
			virtual arma::Row<double> calc_Tc(const arma::Mat<double> &J, const arma::Mat<double> &B) const override;
			virtual arma::Mat<double> calc_E(const arma::Mat<double> &J, const arma::Mat<double> &B, const arma::Row<double> &T) const override;
			virtual double get_number_turns() const override;

			// get line current elements
			virtual fmm::ShCurrentSourcesPr create_current_sources(const arma::sword num_gauss) const;
			virtual fmm::ShSourcesPr create_current_mesh(const arma::sword num_gauss) const;
			virtual ShSurfacePr create_surface() const override;
			virtual fmm::ShSourcesPrList create_sources(const arma::sword num_gauss, const arma::sword num_gauss_surface) const override;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const override;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list) override;
	};

}}

#endif
