/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_HH
#define MDL_DRIVE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "rat/common/node.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Drive> ShDrivePr;
	typedef arma::field<ShDrivePr> ShDrivePrList;

	// circuit class
	class Drive: public cmn::Node{
		// properties
		protected:
			// circuit id
			arma::uword id_ = 0;

		// methods
		public:
			// virtual destructor
			virtual ~Drive(){};

			// get identifier
			void set_id(const arma::uword id);
			arma::uword get_id() const;

			// set and get current
			virtual double get_scaling(const double time) const = 0;
			virtual double get_dscaling(const double time) const = 0;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
