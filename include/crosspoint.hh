/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_CROSS_POINT_HH
#define MDL_CROSS_POINT_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <json/json.h>

#include "rat/common/defines.hh"
#include "cross.hh"
#include "area.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CrossPoint> ShCrossPointPr;

	// cross section of coil
	class CrossPoint: public Cross{
		// properties
		private:
			// coordinate
			double u_;
			double v_;

			// hidden area
			double thickness_;
			double width_;

		// methods
		public:
			// constructors
			CrossPoint();
			CrossPoint(const double u,const double v, const double thickness, const double width);

			// factory methods
			static ShCrossPointPr create();
			static ShCrossPointPr create(const double u, const double v, const double thickness, const double width);

			// set properties
			void set_coord(const double u, const double v);
			void set_thickness(const double thickness);
			void set_width(const double width);
			
			// volume mesh
			virtual ShAreaPr create_area() const;

			// surface mesh
			virtual ShPerimeterPr create_perimeter() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
