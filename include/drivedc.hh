/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_DRIVE_DC_HH
#define MDL_DRIVE_DC_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "drive.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class DriveDC> ShDriveDCPr;
	typedef arma::field<ShDriveDCPr> ShDriveDCPrList;

	// circuit class
	class DriveDC: public Drive{
		// properties
		protected:
			// fixed scaling
			double scaling_ = 1.0;

		// methods
		public:
			// constructor
			DriveDC();
			DriveDC(const double scaling, const arma::uword id = 0);

			// factory
			static ShDriveDCPr create();
			static ShDriveDCPr create(const double scaling, const arma::uword id = 0);

			// set and get current
			void set_scaling(const double scaling);
			double get_scaling(const double time) const;
			double get_dscaling(const double time) const;
			
			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
