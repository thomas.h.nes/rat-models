/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_EDGES_COIL_HH
#define MDL_EDGES_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "trans.hh"
#include "rat/common/freecad.hh"
#include "rat/common/opera.hh"
#include "nameable.hh"
#include "edges.hh"
#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EdgesCoil> ShEdgesCoilPr;
	typedef arma::field<ShEdgesCoilPr> ShEdgesCoilPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class EdgesCoil: public Edges{
		protected:
			// operating current
			double operating_current_;
			
			// number of turns
			double number_turns_;
			
			// material of this coil
			ShMaterialPr material_;

		// methods
		public:
			// constructor
			EdgesCoil();
			EdgesCoil(ShFramePr &frame, ShAreaPr &area);

			// destructor
			virtual ~EdgesCoil(){};

			// factory
			static ShEdgesCoilPr create();
			static ShEdgesCoilPr create(ShFramePr &frame, ShAreaPr &area);

			// setting and getting
			// operating current
			void set_operating_current(const double operating_current);
			double get_operating_current() const;

			// number of turns
			void set_number_turns(const double number_turns);
			double get_number_turns() const;

			// material
			void set_material(ShMaterialPr material);
			ShMaterialPr get_material() const;

			// export edges to freecad file
			virtual void export_freecad(cmn::ShFreeCADPr freecad) const;
			virtual void export_opera(cmn::ShOperaPr opera) const;

			// create a copy
			virtual ShEdgesPr copy() const;
	};

}}

#endif
