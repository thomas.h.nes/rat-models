/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CABLE_HH
#define MDL_PATH_CABLE_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "rat/common/extra.hh"
#include "path.hh"
#include "transformations.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class PathCable> ShPathCablePr;

	// path defining the shape of the cable
	// uses the coil path as input
	class PathCable: public Path, public Transformations{
		// properties
		protected:
			// basepath
			ShPathPr base_ = NULL;

			// settings
			arma::uword num_turns_ = 1; // number of turns
			double turn_step_ = 0.0; // stepsize of each turn: usually dcable + 2*dinsu

			// start position and increment position
			arma::uword idx_start_ = 0; // start section
			arma::uword idx_incr_ = 0; // section of increment
			arma::sword num_add_ = 0; // add extra sections
			//arma::uword num_rem_ = 0; // remove sections
			
			// offsetting the coil block
			arma::sword num_offset_ = 0; // ofset in n-direction (in cable thicnesses)
			double offset_ = 0; // absolute offset in n-direction [m]

			// disable increment
			bool disable_increment_ = false;
			bool reverse_base_ = false;

		// methods
		public:
			// constructor
			PathCable();
			PathCable(ShPathPr base);
			
			// factory methods
			static ShPathCablePr create();
			static ShPathCablePr create(ShPathPr base);

			// set base path
			void set_base(ShPathPr base);
			void set_reverse_base(const bool reverse_base);

			// get indexing arrays
			// virtual void get_indexing(arma::Row<arma::uword> &section_idx, arma::Row<arma::uword> &turn_idx) const;

			// set cable properties
			void set_num_turns(const arma::uword num_turns);
			void set_turn_step(const double turn_step);
			void set_disable_increment(const bool disable_increment);

			// set counters
			void set_num_add(const arma::sword num_add);
			void set_idx_incr(const arma::uword idx_incr);
			void set_num_offset(const arma::sword num_offset);
			void set_offset(const double offset);
			
			// set start position and increment position
			void set_idx_start(const arma::uword idx_start);

			// get frame
			virtual ShFramePr create_frame() const;
			
			// indexing arrays
			void get_indexing(arma::Row<arma::uword> &section_idx, arma::Row<arma::uword> &turn_idx) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
