/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_COIL_HH
#define MDL_MODEL_COIL_HH

#include <armadillo> 
#include <memory>

#include "rat/common/node.hh"
#include "rat/common/defines.hh"

#include "modelmesh.hh"
#include "meshcoil.hh"
#include "edgescoil.hh"
#include "material.hh"
#include "matcopper.hh"
#include "vtkunstr.hh"
#include "extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelCoil> ShModelCoilPr;

	// current carying coil extruded with
	// a base path and a cross section
	class ModelCoil: public ModelMesh{
		// properties
		protected:
			// winding geometry
			double number_turns_ = 1;

			// material of this coil
			ShMaterialPr material_ = MatCopper::create(100);

			// current sharing feature
			// when enabled the current can flow freely over
			// the cross section of the conductor (use for cables)
			bool enable_current_sharing_ = false;

			// use line elements or volume elements
			bool use_volume_elements_ = false;

			// softening factor
			double softening_ = 0.7;

			// design operation current
			double operating_current_ = 0;

		// methods
		public:
			// constructor
			ModelCoil();
			ModelCoil(ShPathPr pth, ShCrossPr crss, ShMaterialPr material = MatCopper::create());

			// factory methods
			static ShModelCoilPr create();
			static ShModelCoilPr create(ShPathPr pth, ShCrossPr crss, ShMaterialPr material = MatCopper::create());

			// calculation settings
			void set_enable_current_sharing(const bool enable_current_sharing);
			void set_softening(const double softening); // softening factor
			void set_use_volume_elements(const bool use_volume_elements);

			// getting of number of turns
			void set_number_turns(const double number_turns);
			double get_number_turns() const;

			// operating conditions
			void set_operating_current(const double operating_current);
			void set_material(ShMaterialPr material);
			ShMaterialPr get_material() const;

			// creation of calculation data objects
			virtual ShMeshPrList create_mesh() const; // create a mesh
			virtual ShEdgesPrList create_edge() const; // create edge matrices

			// write elements to VTK file
			void write_elements(const std::string &fname) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
