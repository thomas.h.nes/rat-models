/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MATERIAL_GROUP_HH
#define MDL_MATERIAL_GROUP_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class MatGroup> ShMatGroupPr;

	// (superconducting) material using current sharing model
	class MatGroup: public Material{
		protected: 
			// list of conductors
			ShMaterialPrList con_; 

			// fraction of cross section taken by each conductor
			arma::Col<double> fraction_;

			// materials are in parallel
			bool is_parallel_ = true;

			// parallel bi-section
			bool use_parallel_ = true;

			// tolerance for bisection
			double tolerance_ = 1e-7;

		public:
			// constructor
			MatGroup();

			// factory
			static ShMatGroupPr create();

			// get fraction list
			arma::Col<double> get_fraction() const;

			// set tolerance
			void set_tolerance(const double tolerance);

			// add new conductors
			void add_material(const double fraction, ShMaterialPr con);
			void add_material(const arma::Col<double> fractions, ShMaterialPrList con);

			// precalculate material properties for fast electric field calculations
			arma::Mat<double> calc_properties(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;

			// calculate electric field using bi-section solver
			arma::Row<double> calc_current_density_fast(const arma::Row<double> &E, const arma::Mat<double> &props) const;
			arma::Row<double> calc_electric_field_fast(const arma::Row<double> &J, const arma::Mat<double> &props) const;

			// calculation of critical current
			arma::Row<double> calc_critical_current_density(const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha) const;
			arma::Row<double> calc_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T) const;

			// thermal properties
			arma::Row<double> calc_specific_heat(const arma::Row<double> &T)const;
			arma::Row<double> calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const;

			// copy this group
			ShMaterialPr copy() const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};
}}

#endif