/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_SURFACE_COIL_HH
#define MDL_SURFACE_COIL_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/elements.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/extra.hh"
#include "rat/common/gauss.hh"

#include "rat/mlfmm/mgntargets.hh"
#include "rat/mlfmm/currentsources.hh"
#include "rat/mlfmm/currentmesh.hh"

//#include "coilsurface.hh"
#include "mesh.hh"
#include "material.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class SurfaceCoil> ShSurfaceCoilPr;
	typedef arma::field<ShSurfaceCoilPr> ShSurfaceCoilPrList;

	// mesh of a coil
	// this objects carries all properties 
	// required for the calculations
	class SurfaceCoil: public Surface{
		// properties
		protected:
			// operating current
			double operating_current_;
			
			// number of turns
			double number_turns_;
			
			// material of this coil
			ShMaterialPr material_;

		// methods
		public:
			// default constructor
			SurfaceCoil();

			// virtual destructor
			virtual ~SurfaceCoil(){};

			// factory
			static ShSurfaceCoilPr create();

			// operating current
			void set_operating_current(const double operating_current);
			double get_operating_current() const;

			// number of turns
			void set_number_turns(const double number_turns);
			double get_number_turns() const;

			// material
			void set_material(ShMaterialPr material);
			ShMaterialPr get_material() const;

			// get the current density at mesh nodes
			double calc_current_density() const;
			virtual arma::Mat<double> get_nodal_current_density() const override;
			virtual arma::Row<double> calc_alpha(const arma::Mat<double> &B) const override;
			virtual arma::Row<double> calc_Je(const arma::Mat<double> &B, const arma::Row<double> &T) const override;
			virtual arma::Row<double> calc_Tc(const arma::Mat<double> &J, const arma::Mat<double> &B) const override;
			virtual arma::Mat<double> calc_E(const arma::Mat<double> &J, const arma::Mat<double> &B, const arma::Row<double> &T) const override;
			
	};

}}

#endif
