/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_PATH_CCT_CUSTOM_HH
#define MDL_PATH_CCT_CUSTOM_HH

#include <armadillo> 
#include <cassert>
#include <memory>
#include <cmath>
#include <functional>

#include "path.hh"
#include "rat/common/extra.hh"
#include "transformations.hh"
#include "frame.hh"
#include "darboux.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonic> ShCCTHarmonicPr;
	typedef arma::field<ShCCTHarmonicPr> ShCCTHarmonicPrList;

	// cross section of coil
	class CCTHarmonic: public cmn::Node{
		// properties
		protected:
			// which harmonic
			arma::uword num_poles_;
			bool is_skew_ = false;
			
		public:
			// virtual destructor (obligatory)
			virtual ~CCTHarmonic(){};
				
			// function for getting number of poles
			virtual arma::uword get_num_poles() const;

			// geometry functions
			virtual arma::field<arma::Row<double> > calc_z(const arma::Row<double> &theta, const arma::Row<double> &rho) const = 0;
	};
		
	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicInterp> ShCCTHarmonicInterpPr;

	// harmonics by interpolation
	class CCTHarmonicInterp: public CCTHarmonic{
		protected:
			// interpolation arrays
			arma::Row<double> turn_;
			arma::Row<double> a_;

		public:
			// constructor
			CCTHarmonicInterp(){};
			CCTHarmonicInterp(const arma::uword num_poles, const bool is_skew, const arma::Row<double> &turn, const arma::Row<double> &a);
			
			// factory
			static ShCCTHarmonicInterpPr create(const arma::uword num_poles, const bool is_skew, const arma::Row<double> &turn, const arma::Row<double> &a);

			// geometry functions
			arma::field<arma::Row<double> > calc_z(const arma::Row<double> &theta, const arma::Row<double> &rho) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

	// shared pointer definition
	typedef std::shared_ptr<class CCTHarmonicEquation> ShCCTHarmonicEquationPr;

	// define the coordinate function
	typedef std::function<arma::Row<double>(const arma::Row<double> &turn)> CCTCustomFun;


	// harmonics by interpolation
	class CCTHarmonicEquation: public CCTHarmonic{
		protected:
			// interpolation arrays
			CCTCustomFun fun_;

		public:
			// constructor
			CCTHarmonicEquation(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun);
			
			// factory
			static ShCCTHarmonicEquationPr create(const arma::uword num_poles, const bool is_skew, CCTCustomFun fun);

			// geometry functions
			arma::field<arma::Row<double> > calc_z(const arma::Row<double> &theta, const arma::Row<double> &rho) const;
	};


	// shared pointer definition
	typedef std::shared_ptr<class PathCCTCustom> ShPathCCTCustomPr;

	// cross section of coil
	class PathCCTCustom: public Path, public Transformations{
		// properties
		private:
			// geometry
			double nt1_;
			double nt2_;
			arma::uword num_nodes_per_turn_;

			// stored harmonics
			ShCCTHarmonicPrList harmonics_;
		
			// interpolation arrays
			arma::Row<double> turn_;
			arma::Row<double> omega_;
			arma::Row<double> rho_;

		// methods
		public:
			// constructor
			PathCCTCustom();

			// factory methods
			static ShPathCCTCustomPr create();

			// get frame
			virtual ShFramePr create_frame() const;

			// set geometry
			void add_harmonic(ShCCTHarmonicPr harm);
			void set_num_nodes_per_turn(const arma::uword num_nodes_per_turn);
			void set_geometry(const arma::Row<double> &turn, const arma::Row<double> &omega, const arma::Row<double> &rho);
			void set_range(const double nt1, const double nt2);

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
