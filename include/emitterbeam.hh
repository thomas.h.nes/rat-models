/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_EMITTER_BEAM_HH
#define MDL_EMITTER_BEAM_HH

#include <armadillo> 
#include <memory>

#include "rat/common/defines.hh"
#include "particle.hh"
#include "emitter.hh"
#include "rat/common/extra.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class EmitterBeam> ShEmitterBeamPr;

	// emitter for accelerator beam
	class EmitterBeam: public Emitter{
		protected:
			// following unit system from CERN MAD
			// particle settings
			double beam_energy_ = 0; // [GeV] including rest mass
			double rest_mass_ = 0; // [GeV/C^2]
			double charge_ = 0; // elementary charge

			// emitter location
			arma::Col<double>::fixed<3> R_ = {0,0,0};
			
			// emitter orientation
			arma::Col<double>::fixed<3> L_ = {0,1,0};
			arma::Col<double>::fixed<3> N_ = {1,0,0};
			arma::Col<double>::fixed<3> D_ = {0,0,1};

			// emitter properties
			double sigma_ = 1e-2;

			// normal xx' relationship
			double corr_xx_ = 0.9;
			double sigma_x_ = 0.004;
			double sigma_xa_ = 0.5*2*arma::datum::pi/360;
			
			// transverse yy' relationship
			double corr_yy_ = 0.9;
			double sigma_y_ = 0.004;
			double sigma_ya_ = 0.5*2*arma::datum::pi/360;

			// track settings
			arma::uword lifetime_ = 501;
			arma::uword start_idx_ = 250;


		public:
			// constructor
			EmitterBeam();
				
			// factory
			static ShEmitterBeamPr create();

			// default particles
			void set_proton();
			void set_electron();

			// setting of properties
			void set_beam_energy(const double beam_energy);
			void set_rest_mass(const double rest_mass);
			void set_charge(const double charge);
			void set_lifetime(const arma::uword lifetime);
			void set_start_idx(const arma::uword start_idx);
			void set_spawn_coord(const arma::Col<double>::fixed<3> &R, const arma::Col<double>::fixed<3> &L, const arma::Col<double>::fixed<3> &N, const arma::Col<double>::fixed<3> &D);
	 
			// getting properties
			double get_rest_mass() const;

			// set relation ship for normal direction
			void set_xx(const double corr_xx, const double sigma_x, const double sigma_xa);
			void set_yy(const double corr_yy, const double sigma_y, const double sigma_ya);

			// normal distribution but correlated
			static arma::Mat<double> correlated_normdist(const double correlation, const arma::uword num_cols);

			// particle
			arma::field<Particle> spawn_particles(const arma::uword num_particles) const;

			// serialization
			static std::string get_type();
			virtual void serialize(Json::Value &js, cmn::SList &list) const;
			virtual void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif