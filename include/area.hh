/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_AREA_HH
#define MDL_AREA_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "rat/common/defines.hh"
#include "rat/common/elements.hh"
#include "rat/common/node.hh"

#include "perimeter.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Area> ShAreaPr;
	typedef arma::field<ShAreaPr> ShAreaPrList;

	// area carries the mesh of the 
	// cross-sectional area of the coil
	class Area: public cmn::Node{
		// properties
		protected:
			// hidden dimensions
			double hidden_thickness_ = 1.0;
			double hidden_width_ = 1.0;

			// nodes [2xN] matrix
			arma::Mat<double> Rn_;

			// elements
			arma::Mat<arma::uword> n_;
			
		// methods
		public:
			// default constructor
			Area();
			
			// constructor with input
			Area(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// copy constructor
			Area(const ShAreaPr &area);

			// factory
			static ShAreaPr create();

			// factory with input
			static ShAreaPr create(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// copy factory
			static ShAreaPr create(const ShAreaPr &area);

			// set mesh
			void set_mesh(const arma::Mat<double> &Rn, const arma::Mat<arma::uword> &n);

			// set hidden dimensions
			void set_hidden_thickness(const double hidden_thickness);
			void set_hidden_width(const double hidden_width);

			// get hidden dimensions
			double get_hidden_thickness() const;
			double get_hidden_width() const;
			double get_hidden_dim() const;
			
			// edge matrix construction
			void create_edge_matrix(arma::Mat<arma::uword> &s, arma::Mat<arma::uword> &in) const;
			arma::Col<arma::uword> get_corner_nodes() const;

			// get number of elements
			arma::uword get_num_elements() const;

			// get number of nodes
			arma::uword get_num_nodes() const;

			// access nodes and elements
			const arma::Mat<double>& get_nodes() const;
			const arma::Mat<arma::uword>& get_elements() const;

			// apply smoothing to mesh
			void smoothen(const arma::uword N, const double dmp);

			// get conductor area
			// calculated by splitting elements into triangles
			double calculate_area() const;
			arma::Row<double> calculate_areas() const;

			// extract peripheral mesh
			ShPerimeterPr extract_perimeter() const;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, cmn::SList &list) const;
			void deserialize(const Json::Value &js, cmn::DSList &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
