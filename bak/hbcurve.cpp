/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header file
#include "hbcurve.hh"

// constructor
HBCurve::HBCurve(){
	// set default roxie bh-curve
	set_roxie();
}

// factory
ShHBCurvePr HBCurve::create(){
	return std::make_shared<HBCurve>();
}

// set BH curve based on Roxie BHiron1 (1.0 filling)
void HBCurve::set_roxie(){
	// HB curve from Roxie
	Hinterp_ = {
		0000000.0, 0000159.2, 0000294.4, 0000501.3, 0000795.8,
		0001154.0, 0001795.0, 0002862.0, 0004383.0, 0006044.0,
		0008122.0, 0010590.0, 0013160.0, 0021170.0, 0026750.0,
		0033760.0, 0043800.0, 0066000.0, 0099740.0, 0120960.0,
		0141210.0, 0169600.0, 0212170.0, 0283130.0, 0339890.0,
		0425040.0, 0566950.0, 0850760.0, 1702300.0, 2128000.0,
		2837700.0, 3405100.0, 4256700.0, 4256700.0+1.0/arma::datum::mu_0
	};

	Binterp_ = {
		0.0000, 0.8000, 1.2000, 1.4000, 1.5000,
		1.5500, 1.6000, 1.6500, 1.7000, 1.7500,
		1.8000, 1.8500, 1.9000, 2.0000, 2.0500,
		2.1000, 2.1500, 2.2000, 2.2490, 2.2750,
		2.3000, 2.3443, 2.3996, 2.4905, 2.5627,
		2.6706, 2.8498, 3.2074, 4.2782, 4.8134,
		5.7052, 6.4186, 7.4887, 7.4887+1.0
	};

	// check lengths
	assert(Hinterp_.n_elem==Binterp_.n_elem);

	// filling fraction
	ff_ = 1.0;

	// tell class if the interpolation is linear
	mono_interp_ = false;
}

// set BH curve based on COMSOL soft iron (1.0 filling)
void HBCurve::set_comsol(){
	// HB curve from COMSOL
	Hinterp_ = {
		000000.000,000663.146,0001067.500,001705.230,002463.110,
		003841.670,005425.740,0007957.750,012298.300,020462.800,
		032169.600,061213.400,0111408.000,175070.000,261469.000,
		318310.000,318310.000+1.0/arma::datum::mu_0
	};
	
	Binterp_ = {
		0.0,1.0,1.1,1.2,1.3,
		1.4,1.5,1.6,1.7,1.8,
		1.9,2.0,2.1,2.2,2.3,
		2.4,2.4+1.0
	};

	// check lengths
	assert(Hinterp_.n_elem==Binterp_.n_elem);

	// filling fraction
	ff_ = 1.0;

	// tell class if the interpolation is linear
	mono_interp_ = false;
}

// set BH curve based on opera
void HBCurve::set_opera(){
	// default HB curve from opera (SI units)
	Hinterp_ = {
		0,79.57747,135.2817,159.1549,190.9859,238.7324, 
		318.3099,493.3803,644.5775,875.3522,1273.24,1591.549,
		2148.592,3342.254,4774.648,6525.353,9151.409,11936.62,
		15119.72,18541.55,22281.69,27454.23,35809.86,47746.48,
		63661.98,93901.42,127324
		};

	Binterp_ = {
		0.000,0.640,0.920,1.010,1.100,1.200,1.300,
		1.400,1.450,1.500,1.550,1.570,1.600,1.650,
		1.700,1.750,1.800,1.850,1.900,1.950,2.000,
		2.050,2.100,2.150,2.200,2.250,2.300
		};
      
	// check lengths
	assert(Hinterp_.n_elem==Binterp_.n_elem);

	// filling fraction
	ff_ = 1.0;

	// tell class if the interpolation is linear
	mono_interp_ = false;
}

// set BH curve externally
void HBCurve::set_extern(
	const arma::Row<double> &Hinterp, 
	const arma::Row<double> &Binterp,
	const double ff){

	// check lengths
	assert(Hinterp_.n_elem==Binterp_.n_elem);
	assert(ff>0); assert(ff<1.0);

	// set interpolation arrays
	Hinterp_ = Hinterp; Binterp_ = Binterp;	ff_ = ff;
}

// calculate magnetic flux density from HB curve
arma::Row<double> HBCurve::calc_magnetic_flux_density(
	const arma::Row<double> &Hmag) const{

	// ensure that Hmag has no nans
	assert(!Hmag.has_nan()); assert(arma::all(Hmag>=0));
	assert(!Hinterp_.is_empty()); assert(!Binterp_.is_empty());

	// allocate output
	arma::Col<double> Bmag;

	// interpolate HB curve to acquire field magnitude
	arma::interp1(Hinterp_,Binterp_,Hmag.t(),Bmag,"linear",0.0);

	// extrapolation: beyond the last interpolation point
	// it is assumed that the material no longer magnetises
	double Hend = arma::as_scalar(Hinterp_.tail(1));
	double Bend = arma::as_scalar(Binterp_.tail(1));
	arma::Col<arma::uword> idx = arma::find(Hmag>Hend);
	Bmag.rows(idx) = Bend + arma::datum::mu_0*(Hmag.cols(idx).t()-Hend);
	
	// return magnetic field
	return Bmag.t();
}


// calculate magnetistion from HB curve
arma::Row<double> HBCurve::calc_magnetisation(
	const arma::Row<double> &Hmag) const{
	// get magnetic flux density
	arma::Row<double> Bmag = calc_magnetic_flux_density(Hmag);
		
	// calculate magnetisation
	arma::Row<double> M = Bmag/arma::datum::mu_0 - Hmag;

	// return
	return M; 
}

// calculate magnetic susceptibility from HB curve
arma::Row<double> HBCurve::calc_susceptibility(
	const arma::Row<double> &Hmag) const{

	// get magnetic flux density
	arma::Row<double> Bmag = calc_magnetic_flux_density(Hmag);

	// calculate susceptibility
	arma::Row<double> Chi = Bmag/(arma::datum::mu_0*Hmag)-1.0;

	// return susceptibility
	return Chi;
}

// calculate magnetic susceptibility from HB curve
arma::Row<double> HBCurve::calc_susceptibility_diff(
	const arma::Row<double> &Hmag) const{
	
	// get magnetic flux density
	arma::Row<double> Bmag1 = calc_magnetic_flux_density(Hmag);
	arma::Row<double> Bmag2 = calc_magnetic_flux_density(Hmag+dH_);
	
	// calculate susceptibility
	arma::Row<double> Chi1 = Bmag1/(arma::datum::mu_0*Hmag)-1.0;
	arma::Row<double> Chi2 = Bmag2/(arma::datum::mu_0*(Hmag+dH_))-1.0;

	// difference
	arma::Row<double> dChi = (Chi2-Chi1)/dH_;

	// return derivative of susceptibility
	return dChi;
}
