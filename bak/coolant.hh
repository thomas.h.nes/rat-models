/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_COOLANT_HH
#define MDL_COOLANT_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "common/node.hh"
//#include "common/linknode.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class Coolant> ShCoolantPr;
	typedef arma::field<ShCoolantPr> ShCoolantPrList;

	// circuit class
	class Coolant: public cmn::Node{
		// properties
		protected:
			// all coils (at least) start at this temperature
			double operating_temperature_ = 4.5;

		// methods
		public:
			// constructor
			Coolant();
			Coolant(const double operating_temperature);

			// factory
			static ShCoolantPr create();
			static ShCoolantPr create(const double operating_temperature);
			
			// standard fluids
			void set_superfluid_helium();
			void set_boiling_helium3();
			void set_boiling_helium();
			void set_boiling_helium4();
			void set_boiling_helium_pressurized();
			void set_boiling_hydrogen();
			void set_boiling_neon();
			void set_boiling_nitrogen();
			void set_boiling_air();
			void set_boiling_fluorine();
			void set_boiling_argon();
			void set_boiling_oxygen();
			void set_boiling_methane();

			// set and get current
			void set_operating_temperature(const double operating_temperature);
			double get_operating_temperature() const;

			// copy
			ShCoolantPr copy() const;

			// serialization
			static std::string get_type();
			void serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const;
			void deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list);
	};

}}

#endif
