/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcsurface.hh"

// constructor
CalcSurface::CalcSurface(){

}

// constructor with model input
CalcSurface::CalcSurface(ShModelPr model){
	set_model(model);
}

// factory
ShCalcSurfacePr CalcSurface::create(){
	return std::make_shared<CalcSurface>();
}

// factory with model input
ShCalcSurfacePr CalcSurface::create(ShModelPr model){
	return std::make_shared<CalcSurface>(model);
}

// set model function
void CalcSurface::set_model(ShModelPr model){
	assert(model!=NULL);
	model_ = model;
}

// calculation function
void CalcSurface::calculate(ShLogPr lg){
	// // check input
	// assert(model_!=NULL);

	// // report header
	// lg->msg(2,"%s%s--- SURFACE CALCULATION ---%s\n",KBLD,KGRN,KNRM);
	// lg->msg(2,"%s%sFX-GEOMETRY SETUP%s\n",KBLD,KGRN,KNRM);
	// lg->msg(2,"%screating coil meshes%s\n",KBLU,KNRM);

	// // get coil meshes
	// ShCoilMeshPrList meshes = model_->create_coilmesh();

	// // count number of meshes
	// const arma::uword num_coils = meshes.n_elem;

	// // create mesh table
	// CoilMesh::display(lg,meshes);

	// // return
	// lg->msg(-2,"\n");

	// // gather surface data from the meshes
	// lg->msg(2,"%sextracting surface meshes%s\n",KBLU,KNRM);
	
	// // create surfaces
	// ShCoilSurfacePrList surfaces = CoilMesh::create_surfaces(meshes);

	// // allocate
	// arma::field<arma::Mat<double> > R(1,num_coils);
		
	// // walk over surfaces and get coordinates
	// for(arma::uword i=0;i<num_coils;i++)
	// 	R(i) = surfaces(i)->get_coords();

	// // create magnetic targets at nodes of the mesh
	// ShTargetsPr tar = MgnTargets::create(Extra::field2mat(R));

	// // return
	// lg->msg(-2,"\n");

	// // gather surface data from the meshes
	// lg->msg(2,"%screating source elements%s\n",KBLU,KNRM);
		
	// // create sources
	// ShSourcesPr src = CoilMesh::create_sources(meshes,use_line_sources_);

	// // return
	// lg->msg(-2,"\n");

	// // add sources and targets
	// ShMlfmmPr mlfmm = Mlfmm::create();
	// mlfmm->set_targets(tar);
	// mlfmm->set_sources(src);
	
	// // return
	// lg->msg(-2);

	// // run mlfmm
	// mlfmm->setup(lg);
	// mlfmm->calculate(lg);

	// // extract field on each surface
	// arma::Mat<double> A = tar->get_field("A");
	// arma::Mat<double> H = tar->get_field("H");

	// // get field and store 
	// // A_ = tar->get_field("A");
	// // H_ = tar->get_field("H");

	// // for(arma::uword i=0;i<num_coils;i++){
	// // 	surfaces(i)->set_vector_potential();
	// // }

	// // return
	// lg->msg(-2);
}

// get nodes
arma::Mat<double> CalcSurface::get_coords() const{
	return R_;
}	

// get elements
arma::Mat<arma::uword> CalcSurface::get_elements() const{
	return n_;
}

// get magnetic field
arma::Mat<double> CalcSurface::get_magnetic_field() const{
	return H_;
}

// calculate field angle with tape surface
arma::Row<double> CalcSurface::get_field_angle() const{
	// get components of magnetic field
	arma::Row<double> Hn = Extra::dot(N_,H_);
	arma::Row<double> Hd = Extra::dot(D_,H_);
	arma::Row<double> Hl = Extra::dot(L_,H_);

	// in plane component
	arma::Row<double> Hip = arma::sqrt(Hd%Hd + Hl%Hl);

	// calculate angle
	return arma::atan(Hn/Hip);
}


// gmsh interface
// note that gmsh starts counting the nodes at one
void CalcSurface::export_gmsh(ShGmshFilePr gmsh) const{
	// mesh
	gmsh->write_nodes(R_);
	gmsh->write_elements(n_,id_);

	// calculated data
	const arma::Mat<double> B = arma::datum::mu_0*Extra::vec_norm(H_);
	gmsh->write_nodedata(A_,"vector potential [Vs/m]");
	gmsh->write_nodedata(H_,"mgn. field [A/m]");
	gmsh->write_nodedata(B,"mgn. fl. dens. [T]");

	// field angle
	gmsh->write_nodedata(get_field_angle(),"mgn. fl. angle. [deg]");
}