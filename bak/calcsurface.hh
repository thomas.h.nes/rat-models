/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CALCULATE_SURFACE_HH
#define CALCULATE_SURFACE_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "model.hh"
#include "coilmesh.hh"
#include "mlfmm.hh"
#include "coilsurface.hh"
#include "mlfmm/currentsources.hh"

// shared pointer definition
typedef std::shared_ptr<class CalcSurface> ShCalcSurfacePr;
typedef arma::field<ShCalcSurfacePr> ShCalcSurfacePrList;

// template for coil
class CalcSurface{
	// properties
	protected:
		// settings
		bool use_line_sources_ = true;
		
		// model
		ShModelPr model_;

		// surface mesh
		arma::Mat<double> R_;

		// orientation
		arma::Mat<double> L_;
		arma::Mat<double> N_;
		arma::Mat<double> D_;
		
		// elements
		arma::Mat<arma::uword> n_;
		arma::Row<arma::uword> id_;

		// calculated field
		arma::Mat<double> A_;
		arma::Mat<double> H_;

	// methods
	public:
		// constructor
		CalcSurface();
		CalcSurface(ShModelPr model);

		// factory methods
		static ShCalcSurfacePr create();
		static ShCalcSurfacePr create(ShModelPr model);

		// setting
		void set_model(ShModelPr model);	
		
		// calculation function
		void calculate(ShLogPr lg = NullLog::create());

		// function that calculates field angle
		arma::Row<double> get_field_angle() const;
		arma::Mat<double> get_magnetic_field() const;
		arma::Mat<double> get_coords() const;
		arma::Mat<arma::uword> get_elements() const;

		// set field
		void set_vector_potential(const arma::Mat<double> &A);

		// write results to gmsh file
		void export_gmsh(ShGmshFilePr gmsh) const;
};

#endif
