/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef PATH_MAP_HH
#define PATH_MAP_HH

#include <armadillo> 
#include <memory>
#include <cassert>

#include "common/defines.hh"
#include "path.hh"
#include "dbgen.hh"
#include "transformations.hh"

// shared pointer definition
typedef std::shared_ptr<class PathMap> ShPathMapPr;

// map one path onto another
class PathMap: public Path, public Transformations{
	// properties
	protected:
		// base path
		ShPathPr base_; // defines overall shape
		ShPathPr path_; // the mapped path

		// select longitudinal axis
		char longitudinal_axis_ = 'z';
		char transverse_axis_ = 'x';

	// methods
	public:
		// constructor
		PathMap();
		PathMap(ShPathPr base, ShPathPr path);
		
		// factory
		static ShPathMapPr create();
		static ShPathMapPr create(ShPathPr base, ShPathPr path);

		// set properties
		void set_base(ShPathPr base);
		void set_path(ShPathPr path)

		// get generators
		virtual ShDbGenPr create_generators() const;
};

#endif
