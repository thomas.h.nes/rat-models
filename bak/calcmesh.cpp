/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "calcmesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	CalcMesh::CalcMesh(){

	}

	// constructor
	CalcMesh::CalcMesh(ShPathPr base, ShCrossPr crss){
		set_base(base); set_cross(crss);
	}

	// factory
	ShCalcMeshPr CalcMesh::create(){
		return std::make_shared<CalcMesh>();
	}

	// factory
	ShCalcMeshPr CalcMesh::create(ShPathPr base, ShCrossPr crss){
		return std::make_shared<CalcMesh>(base,crss);
	}

	// set path
	void CalcMesh::set_base(ShPathPr base){
		if(base==NULL)rat_throw_line("supplied base points to NULL");
		base_ = base;
	}

	// set path
	void CalcMesh::set_cross(ShCrossPr crss){
		if(crss==NULL)rat_throw_line("supplied cross section points to NULL");
		crss_ = crss;
	}

	// set base path manually
	void CalcMesh::set_base(const char axis, const char dir, 
		const double ell, const double x, const double y, const double z, const double dl){
		// check input
		if(ell<=0)rat_throw_line("length must be larger than zero");
		if(axis!='x' && axis!='y' && axis!='z')rat_throw_line("axis must be x,y or z");
		if(dir!='x' && dir!='y' && dir!='z')rat_throw_line("direction must be x,y or z");
		if(dl<=0)rat_throw_line("element size must be larger than zero");

		// axis of magnet for field quality calculation
		ShPathGroupPr pth = PathGroup::create();
		pth->add_path(PathStraight::create(ell,dl));
		pth->add_translation(0,-ell/2,0);

		// set line start location, direction vector and orientation (N-vector)
		// depending on the axis and direction chosen (note that D-is pointing in dir)
		if(axis=='x' && dir=='y'){
			pth->add_rotation(0,0,1,-arma::datum::pi/2);
			pth->add_rotation(1,0,0,-arma::datum::pi/2);
		}
		else if(axis=='x' && dir=='z'){
			pth->add_rotation(0,0,1,-arma::datum::pi/2);
			
		}
		else if(axis=='y' && dir=='x'){
			pth->add_rotation(0,1,0,-arma::datum::pi/2);
		}
		else if(axis=='y' && dir=='z'){
			// default orientation
		}
		else if(axis=='z' && dir=='x'){
			pth->add_rotation(1,0,0,arma::datum::pi/2);
			pth->add_rotation(0,0,1,arma::datum::pi/2);
		}
		else if(axis=='z' && dir=='y'){
			pth->add_rotation(1,0,0,arma::datum::pi/2);
			pth->add_rotation(0,0,1,arma::datum::pi);
		}
		else{
			rat_throw_line("axis and direction combination unrecognised");
		}
		
		// add translation
		pth->add_translation(x,y,z);

		// set path to self
		base_ = pth;
	}


	// setup mesh for integration
	void CalcMesh::setup(cmn::ShLogPr lg){
		// check input
		if(base_==NULL)rat_throw_line("base is not set");
		if(crss_==NULL)rat_throw_line("cross section is not set");

		// header
		lg->msg(2,"%s%sSETTING UP MESH%s\n",KBLD,KGRN,KNRM);

		// create generators for base line
		ShFramePr gen = base_->create_generators();

		// combine sections
		gen->combine();

		// create 2d area mesh
		ShAreaPr area = crss_->create_area();

		// create lofted mesh
		{	
			// get nodes
			const arma::Mat<double> R2d = area->get_nodes();
			const arma::Mat<arma::uword> n2d = area->get_elements();

			// edge matrix construction
			arma::Mat<arma::uword> s2d,i2d;	
			area->create_edge_matrix(s2d,i2d);

			// allocate non-needed
			arma::Mat<double> R,L,N,D;
			arma::Mat<arma::uword> s;

			// call loft function and 
			gen->create_loft(R,L,N,D,n_,s,R2d,n2d,s2d);
			
			// set targets
			set_target_coords(R);
		}

		// display input path
		lg->msg(2,"%sTrack Interpolation Mesh%s\n",KBLU,KNRM);
		lg->msg("number of nodes: %llu\n",num_targets());
		lg->msg("number of elements: %llu\n",n_.n_cols);
		lg->msg(-2,"\n");

		// done setup
		lg->msg(-2);
	}


	// create vtk grid object
	vtkSmartPointer<vtkUnstructuredGrid> CalcMesh::create_vtk_ugrid(cmn::ShLogPr lg) const{
		// header
		lg->msg(2,"%sPreparing Grid Data%s\n",KBLU,KNRM);
		
		// create list of node coordinates
		lg->msg("mesh node coordinates\n");
		vtkSmartPointer<vtkPoints> points = 
			vtkSmartPointer<vtkPoints>::New();
		points->SetDataTypeToDouble();
		points->Allocate(Rt_.n_elem);
		for(arma::uword i=0;i<Rt_.n_cols;i++){
			points->InsertNextPoint(Rt_(0,i),Rt_(1,i),Rt_(2,i));
		}
		
		// insert hexahedrons
		lg->msg("element indexes\n");
		const arma::Mat<arma::sword> ns = 
			arma::conv_to<arma::Mat<arma::sword> >::from(n_);
		vtkSmartPointer<vtkCellArray> nvtk = 
			vtkSmartPointer<vtkCellArray>::New();
		nvtk->Allocate(ns.n_elem + ns.n_cols);
		for(arma::uword i=0;i<ns.n_cols;i++){
			nvtk->InsertNextCell(ns.n_rows,ns.colptr(i));
		}
		
		// Create a polydata object
		vtkSmartPointer<vtkUnstructuredGrid> ugrid =	
			vtkSmartPointer<vtkUnstructuredGrid>::New();
		ugrid->SetPoints(points);
		ugrid->SetCells(12,nvtk); // 12 stands for hexahedrons

		// magnetic field data
		arma::Mat<double> B;
		if(has("H")){
			lg->msg("magnetic flux density\n");
			assert(has("H"));
			B = get_field("B");
			vtkSmartPointer<vtkDoubleArray> Bvtk = 
				vtkSmartPointer<vtkDoubleArray>::New();
			Bvtk->SetName("Mgn. Flux Density [T]");
			Bvtk->SetNumberOfComponents(B.n_rows);
			Bvtk->SetNumberOfValues(B.n_elem);
			for(arma::uword i=0;i<B.n_elem;i++)Bvtk->SetValue(i,B(i));
			//Bvtk->SetArray(B.memptr(),B.n_elem,1);
			ugrid->GetPointData()->AddArray(Bvtk);
		}

		// vector potential
		arma::Mat<double> A;
		if(has("A")){
			lg->msg("magnetic vector potential\n");
			assert(has("A"));
			A = get_field("A");
			vtkSmartPointer<vtkDoubleArray> Avtk = 
				vtkSmartPointer<vtkDoubleArray>::New();
			Avtk->SetName("Vector Potential [Vs/m]");
			Avtk->SetNumberOfComponents(A.n_rows);
			Avtk->SetNumberOfValues(A.n_elem);
			for(arma::uword i=0;i<A.n_elem;i++)Avtk->SetValue(i,A(i));
			//Avtk->SetArray(A.memptr(),A.n_elem,1);
			ugrid->GetPointData()->AddArray(Avtk);
		}

		// done preparing data
		lg->msg(-2,"\n");

		// return grid
		return ugrid;
	}

	// write surface to VTK file
	ShVTKUnstrPr CalcMesh::export_vtk_mesh(cmn::ShLogPr lg) const{
		// check if calculation was done
		if(!has_field())rat_throw_line("field is not calculated");

		// header for export
		lg->msg(2,"%s%sVTK UNSTRUCTURED GRID EXPORT%s\n",KBLD,KGRN,KNRM);
		lg->msg(2,"%sPreparing Data%s",KBLU,KNRM);

		// create data object
		ShVTKUnstrPr vtk_data = VTKUnstr::create();

		// header for data preparation
		const arma::uword hexahedron_type = 12;
		vtk_data->set_mesh(Rt_,n_,hexahedron_type);

		// wrap magnetic field
		if(has("H") && has_field()){
			lg->msg("magnetic field\n");
			const arma::Mat<double> B = get_field("B");
			vtk_data->set_nodedata(B,"Mgn. Flux Density [T]");
		}

		// wrap magnetic vector potential
		if(has("A") && has_field()){
			lg->msg("magnetic vector potential\n");
			const arma::Mat<double> A = get_field("A");
			vtk_data->set_nodedata(A,"Vector Potential [Vs/m]");
		}

		// mesh setup done
		lg->msg(-2);

		// done exporting
		lg->msg(-2,"\n");

		// return data object
		return vtk_data;
	}

}}