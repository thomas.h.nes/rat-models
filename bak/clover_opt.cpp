/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for FX-Models
#include "crossrectangle.hh"
#include "common/gmshfile.hh"
#include "modelcoil.hh"
#include "common/freecad.hh"
#include "pathclover.hh"
#include "pathcable.hh"
#include "modelgroup.hh"
#include "modelmesh.hh"
#include "modelsources.hh"
#include "modeltargets.hh"
#include "calcgrid.hh"
#include "calcharmonics.hh"
#include "conductor.hh"
#include "conductorlk.hh"
#include "conductornist.hh"
#include "conductorgroup.hh"
#include "conductorrebco.hh"
#include "circuit.hh"
#include "coolant.hh"
#include "transbend.hh"
#include "pathrectangle.hh"
#include "newtonraphson.hh"
#include "calctracks.hh"
#include "crosscircle.hh"
#include "emitterbeam.hh"

// header files for FX-MLFMM
#include "mlfmm.hh"
#include "common/log.hh"

// DESCRIPTION
// This example shows how to create a single layer 
// cloverleaf coil complemented by a racetrack coil.
// The example includes both magnet poles. Cloverleaf coils
// are probably a good solution for creating a dipole with high
// temperature superconducting (HTS) tape.

// main
int main(){
	// INPUT SETTINGS
	// switchboard for calculation types
	const bool run_optimisation = true;
	const bool run_coil_field = true; // calculate field on the coil
	const bool run_grid = false; // calculate field on a grid of points surrounding the coil
	const bool run_harmonics = true; // run harmonic coil calculation
	const bool run_tracking = true; // track particles in aperture
	const bool run_freecad = false; // export the geometry to a python freecad macro

	// operating conditions
	const double Iop = 6000; // operating current [A]
	const double Top = 20; // operating temperature [K]

	// coil geometry
	const double ellstr1 = 0.5; // straight section length [m]
	const double ellstr2 = 0.060; // width of the coil [m]
	const double endspace = 0.03; // spacing between the cloverleaf and the racetrack [m]
	const arma::uword cl_num_turns = 20; // number of turns in the cloverleaf coil
	const arma::uword rt_num_turns = 28; // number of turns in the racetrack coil
	const double dplate = 8e-3; // thickness of the plate separating the coils [m]
	const double dmid = 1e-3; // thickness of the mid-plane [m]
	const double bending_radius = 0.8; // radius for small accelerators [m] (0 is disable bending)
	const double element_size = 2e-3; // size of the elements [m]

	// cable settings
	const double wcable = 12e-3; // width of cable [m]
	const double dcable = 0.5e-3; // thickness of cable [m]
	const double dinsu = 0.15e-3/2; // thickness of insulation for each cable [m]
		
	// spline
	const double ell_trans = 0; // transition length [m]
	const double str12 = 40e-3; // control point strength [m]
	const double str34 = 20e-3; // control point strength [m]
	
	// required B1 for optimisation
	const double B1req = 3.2; // [Tm]

	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	ShLogPr lg = Log::create();
	

	// GEOMETRY SETUP	
	// create a header in the log
	lg->msg(2,"%s%s--- SETTING UP GEOMETRY ---%s\n",KBLD,KGRN,KNRM);

	// create circuit and coolant
	ShCircuitPr circ = Circuit::create(Iop);
	ShCoolantPr clnt = Coolant::create(Top);

	// calculate remaining
	const double cl_dpack = cl_num_turns*(dcable + 2*dinsu); // thickness of cloverleaf [m]
	const double rt_dpack = rt_num_turns*(dcable + 2*dinsu); // thickness of racetrack [m]

	// create model
	ShModelGroupPr pole1 = ModelGroup::create();
	pole1->set_name("p1");
	ShModelGroupPr pole2 = ModelGroup::create();
	pole2->set_name("p2");
	pole2->add_rotation(1,0,0,arma::datum::pi);
	pole2->add_reverse();

	// create conductor group
	ShConductorGroupPr rebco_cu = ConductorGroup::create();
	{
		// add nbti superconductor
		ShConductorReBCOPr rebco = ConductorReBCO::create();
		rebco->set_fujikura_te();
		rebco_cu->add_conductor(0.01,rebco);

		// add copper matrix
		ShConductorNISTPr copper = ConductorNIST::create();
		copper->set_copper_OFHC_RRR100();
		rebco_cu->add_conductor(0.99,copper);
	}

	// create cloverleaf coil
	// calculate parameters
	const double height = wcable + dplate;

	// create clover path
	ShPathCloverPr path_clover = PathClover::create(ellstr1,ellstr2,
		height,cl_dpack+0,ell_trans,str12,str34,element_size);
	if(bending_radius!=0)path_clover->set_bending_radius(bending_radius);

	// create block cross section
	ShCrossRectanglePr cross_clover = CrossRectangle::create(0,cl_dpack,0,wcable,element_size);

	// create block coil
	ShModelCoilPr coil_clover = ModelCoil::create(path_clover, cross_clover, circ, clnt, rebco_cu);
	coil_clover->set_number_turns(cl_num_turns);
	coil_clover->set_name("c1");
	coil_clover->add_translation(0,0,dmid);

	// add to model
	pole1->add_model(coil_clover);
	pole2->add_model(coil_clover);

	// create inner racetrack coil
	// coil parameters
	const double radius = ellstr2/2+cl_dpack-rt_dpack;

	// create clover path
	ShPathRectanglePr path_racetrack = PathRectangle::create(
		2*radius,ellstr1-2*cl_dpack-2*endspace,radius,element_size);
	path_racetrack->set_offset(rt_dpack);
	if(bending_radius!=0)path_racetrack->set_bending_radius(bending_radius);

	// create cross section
	ShCrossRectanglePr cross_racetrack = CrossRectangle::create(0,rt_dpack,0,wcable,element_size);

	// create block coil
	ShModelCoilPr coil_racetrack = ModelCoil::create(path_racetrack, cross_racetrack, circ, clnt, rebco_cu);
	coil_racetrack->set_number_turns(rt_num_turns); 
	coil_racetrack->add_translation(0,0,dmid+wcable+dplate);

	// add to model
	pole1->add_model(coil_racetrack);
	pole2->add_model(coil_racetrack);

	// magnet lower pole
	ShModelGroupPr model = ModelGroup::create();
	model->add_model(pole1);
	model->add_model(pole2);

	// create mesh representing all coils
	//ShModelSurfacePr mesh = ModelSurface::create();
	ShModelTargetsPr mesh = ModelTargets::create(model);
	mesh->setup(lg);

	// create sources representing all coils
	ShModelSourcesPr elements = ModelSources::create(model);
	elements->setup(lg);

	// axis of magnet for field quality calculation
	ShPathGroupPr path_beam = PathGroup::create();
	path_beam->add_path(PathStraight::create(ellstr1*2,element_size));
	path_beam->add_translation(0,-ellstr1*2.0/2,0);

	// add bending
	if(bending_radius!=0)path_beam->add_transformation(TransBend::create(0,0,1, 0,1,0, bending_radius));

	// setup done
	lg->msg(-2,"\n");

	
	// CALCULATION AND OUTPUT
	// solution of coil block positions for
	// zero B3, B5 integrated field quality
	// and required B1 field integral. Integrals
	// are performed over the beam path which should
	// cover the full length of the coils.
	if(run_optimisation){	
		// create header
		lg->msg(2,"%s%s--- OPTIMIZATION ---%s\n",KBLD,KGRN,KNRM);

		// create harmonic coil calculation
		ShCalcHarmonicsPr harm = CalcHarmonics::create();
		harm->set_compensate_curvature(true);
		harm->set_num_theta(64);
		harm->set_base(path_beam);
		harm->set_radius(10e-3); // [m]
		harm->setup();
		
		// create mlfmm settings
		ShSettingsPr sttngs = Settings::create();
		sttngs->set_num_exp(7);

		// create mlfmm
		ShMlfmmPr mlfmm = Mlfmm::create(sttngs);
		mlfmm->set_sources(elements);
		mlfmm->set_targets(harm);

		// create a newton raphson solver
		ShNewtonRaphsonPr nr = NewtonRaphson::create();
			
		// create system function
		NRSysFun sysfun = [&](const arma::Col<double> &x){
			// calculate new coil pack thicknesses
			double cl_dpack_opt = cl_dpack+x(1);
			double rt_dpack_opt = rt_dpack+x(2);

			// update the cloverleaf
			path_clover->set_ellstr2(ellstr2+x(0));
			
			// update the racetrack
			double rtrad = (ellstr2+x(0))/2 + cl_dpack_opt - rt_dpack_opt;
			path_racetrack->set_width(2*rtrad);
			path_racetrack->set_radius(rtrad);
			path_racetrack->set_height(ellstr1-2*cl_dpack_opt-2*endspace);

			// change cross section dimensions and corresponding number of turns
			cross_racetrack->set_dim_normal(0,rt_dpack_opt,element_size);
			cross_clover->set_dim_normal(0,cl_dpack_opt,element_size);
			coil_clover->set_number_turns(cl_dpack_opt/(dcable+2*dinsu));
			coil_racetrack->set_number_turns(rt_dpack_opt/(dcable+2*dinsu));

			// update elements
			elements->setup();

			// calculate harmonics
			mlfmm->setup();
			mlfmm->calculate();

			// run Fourier analysis
			harm->post_process();

			// getting integrated harmonics
			arma::Row<double> An,Bn;
			harm->get_harmonics(An,Bn);

			// return 
			return (arma::Col<double>{Bn(1)+B1req,Bn(3)*5,Bn(5)*10}).eval();
		};

		// set system function and start values
		nr->set_systemfun(sysfun);	
		nr->set_initial(arma::Col<double>{0,0,0});
		nr->set_delta(0.1e-3);
		nr->set_use_central_diff(false);
		nr->set_tolx(1e-8);
		nr->set_tolfun(1e-5);

		// use finite difference
		nr->set_finite_difference();

		// solve system
		nr->solve(lg);

		// get result and set to coils using the system function
		sysfun(nr->get_result());

		// setup mesh
		mesh->setup();

		// done
		lg->msg(-2,"\n");
	}

	// Calculate field at mesh
	if(run_coil_field){	
		// create header
		lg->msg(2,"%s%s--- CALCULATING COIL FIELD ---%s\n",KBLD,KGRN,KNRM);

		// create MLFMM object
		ShMlfmmPr mlfmm = Mlfmm::create();

		// Set sources and targets for calculation
		mlfmm->set_sources(elements);
		mlfmm->set_targets(mesh);

		// run MLFMM
		mlfmm->setup(lg);
		mlfmm->calculate(lg);

		// write data to gmsh file
		mesh->export_gmsh(GmshFile::create("clover.gmsh"));

		// write to VTK file
		ShVTKUnstrPr vtk_mesh = mesh->export_vtk(lg);
		vtk_mesh->write("clover_opt_mesh",lg);

		// done
		lg->msg(-2,"\n");
	}

	// calculate grid
	if(run_grid){
		// create header
		lg->msg(2,"%s%s--- CALCULATING GRID ---%s\n",KBLD,KGRN,KNRM);

		// Create a grid of 140x160x50 points in the volume defined by intervals:
		// x=[-0.14,0.14], y=[-0.16,0.16], z=[-0.05,0.05];
		ShCalcGridPr grid = CalcGrid::create(-0.14,0.14,140, -0.2,0.2,200, -0.05,0.05,50);
		
		// CalcGrid also requires setting up as it needs 
		// to produce the coordinates for the MLFMM
		grid->setup(lg); 

		// Create MLFMM
		ShMlfmmPr mlfmm = Mlfmm::create();
		
		// Set sources and targets for calculation
		//mlfmm->set_sources(elements);
		mlfmm->set_sources(elements);
		mlfmm->set_targets(grid);

		// run mlfmm
		mlfmm->setup(lg);
		mlfmm->calculate(lg);

		// The calculated data can be 
		ShVTKImgPr vtk_grid = grid->export_vtk(lg);
		vtk_grid->write("clover_opt_igrid",lg);

		// done
		lg->msg(-2,"\n");
	}

	// calculate harmonics
	if(run_harmonics){	
		// create header
		lg->msg(2,"%s%s--- CALCULATING HARMONICS ---%s\n",KBLD,KGRN,KNRM);

		// create harmonic coil calculation
		ShCalcHarmonicsPr harm = CalcHarmonics::create();
		harm->set_compensate_curvature(true);

		// number of points used for harmonic calculation
		harm->set_num_theta(64);

		// the axis of the magnet is set here 
		// it is a 0.5 m long line along y with 
		// main harmonics pointing along z. The line 
		// is centered at the origin and is spaced 
		// with 1 mm elements.
		// harm->set_base('y','z',0.5,0,0,0,1e-3);
		harm->set_base(path_beam);

		// radius at which harmonics are defined 
		// (commonly used is (2/3) of aperture)
		harm->set_radius(10e-3); // [m]
		
		// CalcHarmonics also requires setting up as it needs 
		// to produce the coordinates for the MLFMM
		harm->setup(lg);
		
		// calculate contribution
		harm->set_compensate_curvature(true);

		// create settings
		ShSettingsPr sttngs = Settings::create();
		
		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		sttngs->set_num_exp(7);

		// create mlfmm (and supply settings)
		ShMlfmmPr mlfmm = Mlfmm::create(sttngs);
		//mlfmm->set_sources(elements);

		// Set sources and targets for calculation
		mlfmm->set_sources(elements);
		mlfmm->set_targets(harm);

		// run mlfmm
		mlfmm->setup(lg);
		mlfmm->calculate(lg);

		// post process harmonics
		// performs fast fourier transform
		harm->post_process();

		// display harmonics
		harm->display(lg);

		// export to VTK
		ShVTKTablePr vtk_table = harm->export_vtk_table(lg);
		vtk_table->write("clover_opt_harm",lg);
		ShVTKUnstrPr vtk_harm = harm->export_vtk_coord(lg);
		vtk_harm->write("clover_opt_harm",lg);

		// done
		lg->msg(-2,"\n");
	}

	// run tracking code
	if(run_tracking){
		// create header
		lg->msg(2,"%s%s--- CALCULATING TRACKS ---%s\n",KBLD,KGRN,KNRM);

		// Create a grid of 140x160x50 points in the volume defined by intervals:
		ShModelMeshPr beam_pipe = ModelMesh::create(
			path_beam,CrossCircle::create(20e-3,element_size));
		ShModelTargetsPr interpolation_mesh = ModelTargets::create(beam_pipe);

		// CalcGrid also requires setting up as it needs 
		// to produce the coordinates for the MLFMM
		interpolation_mesh->setup(lg); 

		// create settings
		ShSettingsPr sttngs = Settings::create();
		sttngs->set_num_exp(7);

		// Create MLFMM
		ShMlfmmPr mlfmm = Mlfmm::create(sttngs);
		
		// Set sources and targets for calculation
		//mlfmm->set_sources(elements);
		mlfmm->set_sources(elements);
		mlfmm->set_targets(interpolation_mesh);

		// run mlfmm
		mlfmm->setup(lg);
		mlfmm->calculate(lg);

		// add an emitter
		ShEmitterBeamPr beam = EmitterBeam::create();
		beam->set_proton();
		beam->set_beam_energy(1.9); // GeV
		beam->set_xx(0.9, 0.004, 0*2*arma::datum::pi/360);
		beam->set_yy(0.9, 0.004, 0*2*arma::datum::pi/360);

		// create tracker
		ShCalcTracksPr tracker = CalcTracks::create(interpolation_mesh, beam);
		tracker->set_stepsize(10e-3);
		
		// track generation
		tracker->calculate(lg);

		// The calculated data can be 
		ShVTKUnstrPr vtk_beam = interpolation_mesh->export_vtk(lg);
		vtk_beam->write("clover_opt_beam",lg);
		ShVTKUnstrPr vtk_tracks = tracker->export_vtk(lg);
		vtk_tracks->write("clover_opt_tracks",lg);

		// get a particle
		//Particle p = grid->get_particle(0);
		//arma::Mat<double> Rp = p.get_track_coords();
		//std::cout<<Rp.t()<<std::endl;

		// done
		lg->msg(-2,"\n");
	}

	// write a freecad macro
	if(run_freecad){
		ShFreeCADPr fc = FreeCAD::create("clover_opt.FCMacro","clover");
		fc->set_num_sub(4);
		model->export_freecad(fc);
	}
}