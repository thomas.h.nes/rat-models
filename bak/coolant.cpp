/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "coolant.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Coolant::Coolant(){

	}

	// constructor
	Coolant::Coolant(const double operating_temperature){
		set_operating_temperature(operating_temperature);
	}

	// factory
	ShCoolantPr Coolant::create(){
		return std::make_shared<Coolant>();
	}

	// factory
	ShCoolantPr Coolant::create(const double operating_temperature){
		return std::make_shared<Coolant>(operating_temperature);
	}

	// superfluid helium as in CERN LHC
	void Coolant::set_superfluid_helium(){
		operating_temperature_ = 1.9;
	}

	// standard temperatures
	// source: https://en.wikipedia.org/wiki/Cryogenics
	// helium 3 very very very expensive
	void Coolant::set_boiling_helium3(){
		operating_temperature_ = 3.19;
	}

	// boiling helium (4)
	void Coolant::set_boiling_helium(){
		operating_temperature_ = 4.214;
	}

	// boiling helium 4
	void Coolant::set_boiling_helium4(){
		operating_temperature_ = 4.214;
	}

	// pressurized boiling helium (4)
	// this value is commonly used as the cryostats are
	// slightly pressurized
	void Coolant::set_boiling_helium_pressurized(){
		operating_temperature_ = 4.5;
	}

	// liquid hydrogen
	// look up Hindenburg disaster before using this feature
	void Coolant::set_boiling_hydrogen(){
		operating_temperature_ = 20.27;
	}

	// liquid neon
	void Coolant::set_boiling_neon(){
		operating_temperature_ = 27.09;
	}

	// liquid nitrogen
	void Coolant::set_boiling_nitrogen(){
		operating_temperature_ = 77.09;
	}

	// liquid air
	void Coolant::set_boiling_air(){
		operating_temperature_ = 78.8;
	}

	// from here down we probably 
	// need new superconductors fluorine
	void Coolant::set_boiling_fluorine(){
		operating_temperature_ = 85.24;
	}

	// liquid argon
	void Coolant::set_boiling_argon(){
		operating_temperature_ = 87.24;
	}

	// liquid oxygen
	void Coolant::set_boiling_oxygen(){
		operating_temperature_ = 90.18;
	}

	// liquid methane
	void Coolant::set_boiling_methane(){
		operating_temperature_ = 111.7;
	}

	// set temperature manually
	void Coolant::set_operating_temperature(const double operating_temperature){
		operating_temperature_ = operating_temperature;
	}

	// get temperature
	double Coolant::get_operating_temperature() const{
		return operating_temperature_;
	}

	ShCoolantPr Coolant::copy() const{
		return std::make_shared<Coolant>(*this);
	}

	// get type
	std::string Coolant::get_type(){
		return "mdl::coolant";
	}

	// method for serialization into json
	void Coolant::serialize(Json::Value &js, std::list<cmn::ShNodePr> &) const{
		js["type"] = get_type();
		js["operating_temperature"] = operating_temperature_;
	}

	// method for deserialisation from json
	void Coolant::deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &, const cmn::NodeFactoryMap &){
		set_operating_temperature(js["operating_temperature"].asDouble());
	}

}}