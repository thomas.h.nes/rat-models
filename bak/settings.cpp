/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "settings.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// constructor
	Settings::Settings(){

	}

	// factory
	ShSettingsPr Settings::create(){
		return std::make_shared<Settings>();
	}

	// set data directory
	void Settings::set_datdir(const std::string &datdir){
		if(datdir.empty())rat_throw_line("data directory is empty");
		datdir_ = datdir;
	}

	void Settings::set_model_name(const std::string &model_name){
		if(model_name.empty())rat_throw_line("model name is empty");
		model_name_ = model_name;
	}

	// get data directory
	std::string Settings::get_datdir() const{
		return datdir_;
	}
	
	// get model name
	std::string Settings::get_model_name() const{
		return model_name_;
	}

	// set output times
	void Settings::set_output_times(const arma::Row<double> &output_times){
		if(output_times.is_empty())rat_throw_line("output times array is empty");
		output_times_ = output_times;
	}

	// get output times
	arma::Row<double> Settings::get_output_times() const{
		return output_times_;
	}

	// set switchboard
	void Settings::set_enable_orientation(const bool enable_orientation){
		enable_orientation_ = enable_orientation;
	}

	void Settings::set_enable_magnetic_flux(const bool enable_magnetic_flux){
		enable_magnetic_flux_ = enable_magnetic_flux;
	}

	void Settings::set_enable_vector_potential(const bool enable_vector_potential){
		enable_vector_potential_ = enable_vector_potential;
	}

	void Settings::set_enable_current_density(const bool enable_current_density){
		enable_current_density_ = enable_current_density;
	}

	void Settings::set_enable_temperature(const bool enable_temperature){
		enable_temperature_ = enable_temperature;
	}

	void Settings::set_enable_field_angle(const bool enable_field_angle){
		enable_field_angle_ = enable_field_angle;
	}

	void Settings::set_enable_eng_current_density(const bool enable_eng_current_density){
		enable_eng_current_density_ = enable_eng_current_density;
	}

	void Settings::set_enable_percent_crit_curent(const bool enable_percent_crit_curent){
		enable_percent_crit_curent_ = enable_percent_crit_curent;
	}

	void Settings::set_enable_critical_temperature(const bool enable_critical_temperature){
		enable_critical_temperature_ = enable_critical_temperature;
	}

	void Settings::set_enable_temperature_margin(const bool enable_temperature_margin){
		enable_temperature_margin_ = enable_temperature_margin;
	}

	void Settings::set_enable_power_density(const bool enable_power_density){
		enable_power_density_ = enable_power_density;
	}

	void Settings::set_enable_force_density(const bool enable_force_density){
		enable_force_density_ = enable_force_density;
	}

	void Settings::set_enable_electric_field(const bool enable_electric_field){
		enable_electric_field_ = enable_electric_field;
	}


	// get switchboard
	bool Settings::get_enable_orientation() const{
		return enable_orientation_;
	}

	bool Settings::get_enable_magnetic_flux() const{
		return enable_magnetic_flux_;
	}
	
	bool Settings::get_enable_vector_potential() const{
		return enable_vector_potential_;
	}
	
	bool Settings::get_enable_current_density() const{
		return enable_current_density_;
	}

	bool Settings::get_enable_temperature() const{
		return enable_temperature_;
	}

	bool Settings::get_enable_field_angle() const{
		return enable_field_angle_;
	}

	bool Settings::get_enable_eng_current_density() const{
		return enable_eng_current_density_;
	}

	bool Settings::get_enable_percent_crit_curent() const{
		return enable_percent_crit_curent_;
	}

	bool Settings::get_enable_critical_temperature() const{
		return enable_critical_temperature_;
	}

	bool Settings::get_enable_temperature_margin() const{
		return enable_temperature_margin_;
	}

	bool Settings::get_enable_power_density() const{
		return enable_power_density_;
	}

	bool Settings::get_enable_force_density() const{
		return enable_force_density_;
	}

	bool Settings::get_enable_electric_field() const{
		return enable_electric_field_;
	}

	// enable all output
	void Settings::enable_all(){
		enable_orientation_ = true;
		enable_magnetic_flux_ = true;
		enable_vector_potential_ = true;
		enable_current_density_ = true;
		enable_temperature_ = true;
		enable_field_angle_ = true;
		enable_eng_current_density_ = true;
		enable_percent_crit_curent_ = true;
		enable_critical_temperature_ = true;
		enable_temperature_margin_ = true;
		enable_power_density_ = true;
		enable_force_density_ = true;
		enable_electric_field_ = true;
	}
	
	void Settings::disable_all(){
		enable_orientation_ = false;
		enable_magnetic_flux_ = false;
		enable_vector_potential_ = false;
		enable_current_density_ = false;
		enable_temperature_ = false;
		enable_field_angle_ = false;
		enable_eng_current_density_ = false;
		enable_percent_crit_curent_ = false;
		enable_critical_temperature_ = false;
		enable_temperature_margin_ = false;
		enable_power_density_ = false;
		enable_force_density_ = false;
		enable_electric_field_ = false;
	}

	// display settings
	void Settings::display(cmn::ShLogPr lg)const{
		lg->msg(2, "%soutput settings%s\n",KBLU,KNRM);
		lg->msg("directory: %s%s%s\n",KYEL,datdir_.c_str(),KNRM);
		lg->msg("model name: %s%s%s\n",KYEL,model_name_.c_str(),KNRM);
		lg->msg("time span: %8.2e - %8.2e [s]\n",KYEL,output_times_(0),output_times_(output_times_.n_elem-1),KNRM);
		lg->msg(-2,"\n");

		lg->msg(2, "%sSwitchboard%s\n",KBLU,KNRM);
		lg->msg("enable_orientation: %s%i%s\n",KYEL,enable_orientation_,KNRM);
		lg->msg("enable_magnetic_flux: %s%i%s\n",KYEL,enable_magnetic_flux_,KNRM);
		lg->msg("enable_vector_potential: %s%i%s\n",KYEL,enable_vector_potential_,KNRM);
		lg->msg("enable_current_density: %s%i%s\n",KYEL,enable_current_density_,KNRM);
		lg->msg("enable_temperature: %s%i%s\n",KYEL,enable_temperature_,KNRM);
		lg->msg("enable_field_angle: %s%i%s\n",KYEL,enable_field_angle_,KNRM);
		lg->msg("enable_eng_current_density: %s%i%s\n",KYEL,enable_eng_current_density_,KNRM);
		lg->msg("enable_percent_crit_curent: %s%i%s\n",KYEL,enable_percent_crit_curent_,KNRM);
		lg->msg("enable_critical_temperature: %s%i%s\n",KYEL,enable_critical_temperature_,KNRM);
		lg->msg("enable_temperature_margin: %s%i%s\n",KYEL,enable_temperature_margin_,KNRM);
		lg->msg("enable_power_density: %s%i%s\n",KYEL,enable_power_density_,KNRM);
		lg->msg("enable_force_density: %s%i%s\n",KYEL,enable_force_density_,KNRM);
		lg->msg("enable_electric_field: %s%i%s\n",KYEL,enable_electric_field_,KNRM);
		lg->msg(-2,"\n");
	}

	// get type
	std::string Settings::get_type(){
		return "mdl::settings";
	}

	// method for serialization into json
	void Settings::serialize(Json::Value &js, std::list<cmn::ShNodePr> &) const{
		js["type"] = get_type();
		js["datdir"] = datdir_;
		js["model_name"] = model_name_;
		js["output_times"] = Node::serialize_arma(output_times_);
		js["enable_orientation"] = enable_orientation_;
		js["enable_magnetic_flux"] = enable_magnetic_flux_;
		js["enable_vector_potential"] = enable_vector_potential_;
		js["enable_current_density"] = enable_current_density_;
		js["enable_temperature"] = enable_temperature_;
		js["enable_field_angle"] = enable_field_angle_;
		js["enable_eng_current_density"] = enable_eng_current_density_;
		js["enable_percent_crit_curent"] = enable_percent_crit_curent_;
		js["enable_critical_temperature"] = enable_critical_temperature_;
		js["enable_temperature_margin"] = enable_temperature_margin_;
		js["enable_power_density"] = enable_power_density_;
		js["enable_force_density"] = enable_force_density_;
		js["enable_electric_field"] = enable_electric_field_;
	}

	// method for deserialisation from json
	void Settings::deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &, const cmn::NodeFactoryMap &){
		datdir_ = js["datdir"].asString();
		model_name_ = js["model_name"].asString();
		output_times_ = Node::deserialize_arma(js["output_times"]);
		enable_orientation_ = js["enable_orientation"].asBool();
		enable_magnetic_flux_ = js["enable_magnetic_flux"].asBool();
		enable_vector_potential_ = js["enable_vector_potential"].asBool();
		enable_current_density_ = js["enable_current_density"].asBool();
		enable_temperature_ = js["enable_temperature"].asBool();
		enable_field_angle_ = js["enable_field_angle"].asBool();
		enable_eng_current_density_ = js["enable_eng_current_density"].asBool();
		enable_percent_crit_curent_ = js["enable_percent_crit_curent"].asBool();
		enable_critical_temperature_ = js["enable_critical_temperature"].asBool();
		enable_temperature_margin_ = js["enable_temperature_margin"].asBool();
		enable_power_density_ = js["enable_power_density"].asBool();
		enable_force_density_ = js["enable_force_density"].asBool();
		enable_electric_field_ = js["enable_electric_field"].asBool();
	}

}}