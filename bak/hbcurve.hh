/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef HB_CURVE_HH
#define HB_CURVE_HH

#include <armadillo> 
#include <cassert>
#include <algorithm>

#include "common/extra.hh"

// shared pointer definition
typedef std::shared_ptr<class HBCurve> ShHBCurvePr;
typedef arma::field<ShHBCurvePr> ShHBCurvePrList;

// hb curve class template
class HBCurve{
	// properties
	private:
		// hb curve row(0) = H, row(1) = B
		bool mono_interp_ = false;
		
		// HB data
		arma::Col<double> Hinterp_;
		arma::Col<double> Binterp_;

		// iron filling fraction
		double ff_ = 1.0;

		// finite difference
		double dH_ = 0.1;

	// methods
	public:
		// constructor
		HBCurve();

		// factory
		static ShHBCurvePr create();

		// set functions
		void set_roxie();
		void set_comsol();
		void set_opera();
		void set_extern(const arma::Row<double> &Hinterp, const arma::Row<double> &Binterp, const double ff);

		// calculation functions
		arma::Row<double> calc_magnetic_flux_density(const arma::Row<double> &Hmag) const;
		arma::Row<double> calc_magnetisation(const arma::Row<double> &Hmag) const;
		arma::Row<double> calc_susceptibility(const arma::Row<double> &Hmag) const;
		arma::Row<double> calc_susceptibility_diff(const arma::Row<double> &Hmag) const;

};

#endif