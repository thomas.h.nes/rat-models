/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef MDL_MODEL_SOURCES_HH
#define MDL_MODEL_SOURCES_HH

#include <armadillo> 
#include <memory>
#include <iomanip>

#include "common/defines.hh"
#include "modelgroup.hh"
#include "mlfmm/mgntargets.hh"
#include "mlfmm/currentsources.hh"
#include "meshcoil.hh"
#include "mesh.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// shared pointer definition
	typedef std::shared_ptr<class ModelSources> ShModelSourcesPr;
	typedef arma::field<ShModelSourcesPr> ShModelSourcesPrList;

	// template for coil
	class ModelSources: public ModelGroup, public fmm::CurrentSources, public fmm::MgnTargets{		
		// methods
		public:
			// constructor	
			ModelSources();
			ModelSources(ShModelPr model);

			// factory
			static ShModelSourcesPr create();
			static ShModelSourcesPr create(ShModelPr model);

			// merge sources into self
			void merge_sources(const fmm::ShCurrentSourcesPrList &csl);

			// setup function
			void setup(cmn::ShLogPr lg = cmn::NullLog::create());
	};

}}

#endif
