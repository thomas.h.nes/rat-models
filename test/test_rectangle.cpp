/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/defines.hh"
#include "crossrectangle.hh"
#include "area.hh"

// main
int main(){
	// settings
	double normal_inner = 0.01;
	double normal_outer = 0.04;
	double trans_inner = -0.01;
	double trans_outer = 0.02;
	double normal_elements = 5;
	double trans_elements = 10;

	// create rectangle object
	rat::mdl::ShCrossRectanglePr rect = rat::mdl::CrossRectangle::create(
		normal_inner,normal_outer,trans_inner,
		trans_outer,normal_elements,trans_elements);

	// create area
	rat::mdl::ShAreaPr area = rect->create_area();
	rat::mdl::ShPerimeterPr peri = rect->create_perimeter();

	// check area
	double A = area->calculate_area();	
	double Ach = (normal_outer-normal_inner)*(trans_outer-trans_inner);
	if(std::abs(A - Ach)>1e-6)rat_throw_line("unexpected surface area");
	
	// check perimeter
	double p = peri->calculate_length();
	double pch = 2*(normal_outer - normal_inner + trans_outer - trans_inner);
	if(std::abs(p - pch)>1e-6)rat_throw_line("unexpected perimeter length");
	
	// check number of surface elements
	arma::uword Ns = area->get_num_elements();
	arma::uword Nsch = normal_elements*trans_elements;
	if(Ns!=Nsch)rat_throw_line("unexpected number of elements");
	
	// check number of perimeter elements
	arma::uword Np = peri->get_num_elements();
	arma::uword Npch = 2*(normal_elements + trans_elements);
	if(Np!=Npch)rat_throw_line("unexpected number of elements in perimeter");
	
}