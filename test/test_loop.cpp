/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "modelcoil.hh"
#include "calcline.hh"
#include "pathaxis.hh"
#include "crosspoint.hh"

// analytical field on axis of a current loop with radius R and current I as function of z
// this function is used for cross-checking the direct calculations
arma::Mat<double> analytic_current_loop_axis(
	const arma::Mat<double> &z, 
	const double R, 
	const double I){
	
	// calculate field on axis
	const arma::Mat<double> Bz = (arma::datum::mu_0 / (4*arma::datum::pi))*
		((2*arma::datum::pi*R*R*I)/arma::pow(z%z + R*R,1.5));

	// return field on axis
	return Bz;
}

// main
int main(){
	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double delem = 2e-3; // element size [m]
	const double thickness = 1e-3; // loop cross area [m]
	const double width = 1e-3; // loop cross area [m]
	const double Iop = 100; // operating current [A]
	const double ell = 0.2; // length of axis [m]
	const arma::uword num_sections = 4; // number of coil sections
	const double tol = 1e-2; // relative tolerance
	const arma::uword num_exp = 7;
	
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossPointPr point = rat::mdl::CrossPoint::create(0,0,thickness,width);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, point);
	coil->set_number_turns(1); coil->set_operating_current(Iop);

	// create axis
	rat::mdl::ShPathAxisPr path = rat::mdl::PathAxis::create('z','x',ell,0,0,0,delem);

	// create mlfmm settings
	rat::fmm::ShSettingsPr fmm_settings = rat::fmm::Settings::create();
	fmm_settings->set_num_exp(num_exp);

	// create line calculation
	rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create(coil,path);
	line->set_field_type("H",3);
	line->set_fmm_settings(fmm_settings);
	line->calculate(lg);

	// get result
	arma::Mat<double> Rl = line->get_coords();
	arma::Mat<double> Bfmm = line->get_field("B");
	arma::Row<double> Bz = analytic_current_loop_axis(Rl.row(2),radius,Iop);
	arma::Row<double> err = arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz));

	// std::cout<<arma::join_horiz(Bfmm.row(2).t(),Bz.t(),err.t())<<std::endl;

	// compare results
	if(arma::any(arma::abs(Bfmm.row(2)-Bz)/arma::max(arma::abs(Bz))>tol))
		rat_throw_line("axial field is outside tolerance");


}