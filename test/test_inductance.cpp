/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "modeltoroid.hh"
#include "pathrectangle.hh"
#include "pathdshape.hh"
#include "material.hh"
#include "matcopper.hh"
#include "matgroup.hh"
#include "matrebco.hh"
#include "calcinductance.hh"

// DESCRIPTION
// Example geometry setup for a toroid with 
// 6 double pancake D-shaped limbs.

// main
int main(){
	// INPUT SETTINGS
	// coil geometry
	const double width = 0.25;
	const double height = 0.4;
	const double element_size = 10e-3;

	// toroid geometry
	const arma::uword num_coils = 5;
	const double toroid_radius = 5;

	// cable settings
	const double num_turns = 20;
	const double wcable = 12e-3;
	const double dcable = 1.2e-3;
	const bool use_current_sharing = true;

	// operating conditions
	const double operating_current = 8000;
	const double operating_temperature = 20;

	// calculate coil pack thickness
	const double dcoil = num_turns*dcable;


	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathDShapePr path_dshape = rat::mdl::PathDShape::create(height,width,element_size);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcoil,-wcable/2,wcable/2,1.2e-3);

	// create first coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_dshape, cross_rect);
	coil1->set_name("c1");
	coil1->add_translation(0,0,2e-2);
	coil1->set_enable_current_sharing(use_current_sharing);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);	
	coil1->set_number_turns(num_turns);
	coil1->set_drive_id(1);

	// create second coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_dshape, cross_rect);
	coil2->set_name("c2");
	coil2->add_translation(0,0,-2e-2);
	coil2->set_enable_current_sharing(use_current_sharing);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);	
	coil2->set_number_turns(num_turns);
	coil2->set_drive_id(2);
	
	// create model
	rat::mdl::ShModelGroupPr coilset = rat::mdl::ModelGroup::create();
	coilset->add_model(coil1); coilset->add_model(coil2); coilset->set_name("lm");

	// make toroid
	rat::mdl::ShModelToroidPr toroid = rat::mdl::ModelToroid::create(coilset);
	toroid->set_num_coils(num_coils);
	toroid->set_radius(toroid_radius);
	toroid->set_name("tf");

	// add toroid to a model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(toroid);


	// CALCULATION
	// calculate inductance
	// create inductance calculation
	rat::mdl::ShCalcInductancePr ind1 = rat::mdl::CalcInductance::create(model);
	ind1->set_type(rat::mdl::COIL);
	
	// add calculation
	ind1->calculate();
	
	// create inductance calculation
	rat::mdl::ShCalcInductancePr ind2 = rat::mdl::CalcInductance::create(model);
	ind2->set_type(rat::mdl::DRIVE);
	
	// add calculation
	ind2->calculate();

	// create inductance calculation
	rat::mdl::ShCalcInductancePr ind3 = rat::mdl::CalcInductance::create(model);
	ind3->set_type(rat::mdl::TOTAL);
		
	// add calculation
	ind3->calculate();


	// compare results
	const double tol = 1e-4;
	const double L1 = ind1->get_inductance();
	const double L2 = ind2->get_inductance();
	const double L3 = ind3->get_inductance();
	if(std::abs(L1/L3-1.0)>tol)rat_throw_line("inductance per coil is not equal to total inductance");
	if(std::abs(L2/L3-1.0)>tol)rat_throw_line("inductance per drive is not equal to total inductance");

}