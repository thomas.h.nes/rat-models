/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"

#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "calcharmonics.hh"
#include "modelmesh.hh"
#include "pathaxis.hh"

// main
int main(){
	// geometry settings
	const double dcable = 2e-3;
	const double wcable = 5e-3;
	const bool is_reverse = false;
	const arma::uword max_num_poles = 5; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const double radius = 0.05; // radius
	const double alpha = 30.0*2*arma::datum::pi/360; // skew angle for inner radius
	const double a = radius/std::tan(alpha); // skew amplitude
	const double delta = dcable + 0.5e-3; // spacing between turns
	const double num_turns = 10; // number of turns (can be non-integer)
	const arma::uword num_nodes_per_pole = 90;
	const double element_size = 2e-3;
	const double dradial = -5e-3; // spacing between cylinders
	const double operating_current = 4000;
	const arma::uword num_theta = 64;
	const double tolerance = 1; // tolerance on field error [units]
	const double omega = delta/std::sin(alpha);

	// walk over number of poles
	for(arma::uword i=0;i<max_num_poles;i++){
		// walk over rotation
		for(arma::uword j=0;j<2;j++){
			// number of poles
			const arma::uword num_poles = i+1;
			const arma::uword num_nodes_per_turn = num_poles*num_nodes_per_pole;

			// calculate rotation angle around z-axis for skew component
			const double phi = j*arma::datum::pi/(2*num_poles);

			// create unified path
			rat::mdl::ShPathCCTPr path_cct1 = rat::mdl::PathCCT::create(
				num_poles,radius,a,omega,
				num_turns,num_nodes_per_turn);
			path_cct1->set_is_reverse(is_reverse);

			// create unified path
			rat::mdl::ShPathCCTPr path_cct2 = rat::mdl::PathCCT::create(
				num_poles,radius+wcable+dradial,a,
				omega,num_turns,num_nodes_per_turn);
			path_cct2->set_is_reverse(!is_reverse);

			// create cross section
			rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,0,wcable,element_size);

			// create coil
			rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect);
			coil1->set_name("inner");
			coil1->set_operating_current(operating_current);
			rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect);
			coil2->set_name("outer");
			coil2->set_operating_current(operating_current);

			// create model 
			rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
			model->add_model(coil1);
			model->add_model(coil2);
			model->add_rotation(0,0,1,phi);

			// create axis
			rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',0.5,0,0,0,1e-3);

			// create mlfmm settings
			rat::fmm::ShSettingsPr fmm_settings = rat::fmm::Settings::create();
			fmm_settings->set_num_exp(9);

			// calculate harmonics
			rat::mdl::ShCalcHarmonicsPr harmonics = rat::mdl::CalcHarmonics::create(model, pth);
			harmonics->set_num_theta(num_theta);
			harmonics->set_radius((2.0/3)*radius);
			harmonics->set_fmm_settings(fmm_settings);

			// run harmonics calculation
			harmonics->calculate();

			// get integrated harmonics
			harmonics->post_process();
			arma::Row<double> An, Bn;
			harmonics->get_harmonics(An,Bn);

			// allocate reduced harmonics
			arma::Row<double> an,bn;

			// calculate in units depending on whether main or skew harmonics
			if(j==0){an = 1e4*An/Bn(num_poles); bn = 1e4*Bn/Bn(num_poles);}
			else{an = 1e4*An/An(num_poles); bn = 1e4*Bn/An(num_poles);}

			// create indices of non-main harmonics
			const arma::Row<arma::uword> a = arma::regspace<arma::Row<arma::uword> >(1,1,num_poles-1);
			const arma::Row<arma::uword> b = arma::regspace<arma::Row<arma::uword> >(num_poles+1,1,max_num_poles);
			const arma::Row<arma::uword> idx = arma::join_horiz(a,b);

			// get field error
			arma::Row<double> field_error;
			if(j==0){field_error = arma::join_horiz(bn.cols(idx),an);}
			else{field_error = arma::join_horiz(bn,an.cols(idx));}

			// check
			if(arma::any(arma::abs(field_error)>tolerance))
				rat_throw_line("field error larger than tolerance");
		}
	}

}