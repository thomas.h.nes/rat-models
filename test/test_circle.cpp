/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/defines.hh"
#include "crosscircle.hh"
#include "area.hh"

// main
int main(){
	// settings
	double radius = 0.05;
	double xc = 0.01;
	double yc = -0.04;
	double dl = 5e-3;

	// create rectangle object
	rat::mdl::ShCrossCirclePr circ = rat::mdl::CrossCircle::create(xc,yc,radius,dl);
		
	// create area
	rat::mdl::ShAreaPr area = circ->create_area();
	rat::mdl::ShPerimeterPr peri = circ->create_perimeter();

	// check area
	double A = area->calculate_area();	
	double Ach = arma::datum::pi*radius*radius;
	if(std::abs(A - Ach)/Ach>1e-2){
		rat_throw_line("unexpected surface area");
	}

	// check perimeter
	double p = peri->calculate_length();
	double pch = 2*arma::datum::pi*radius;
	if(std::abs(p - pch)/pch>1e-2){
		rat_throw_line("unexpected perimeter length");
	}

}