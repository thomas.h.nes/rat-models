/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/log.hh"
#include "rat/common/defines.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/extra.hh"

#include "rat/mlfmm/soleno.hh"
#include "rat/mlfmm/settings.hh"

#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcline.hh"
#include "pathdshape.hh"
#include "calcinductance.hh"
#include "pathaxis.hh"

// main
int main(){
	// parameters
	const double inner_radius = 0.1;
	const double outer_radius = 0.11;
	const double element_size = 2.5e-3;
	const double height = 0.1;
	const arma::uword num_sections = 4;
	const arma::sword num_gauss = 2;
	const arma::uword num_exp = 7;
	const bool volume_elements = true;

	// operating settings
	const double num_turns = 1000;
	const double operating_current = 400;

	// tolerance for comparison
	const double tol = 1e-2;

	// create circle
	rat::mdl::ShPathCirclePr path_circle = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,height/2,element_size);

	// create coil
	rat::mdl::ShModelCoilPr model_coil = rat::mdl::ModelCoil::create(path_circle, cross_rect);
	model_coil->set_number_turns(num_turns);
	model_coil->set_operating_current(operating_current);
	model_coil->set_use_volume_elements(volume_elements);
	
	// create mlfmm settings
	rat::fmm::ShSettingsPr fmm_settings = rat::fmm::Settings::create();
	fmm_settings->set_num_exp(num_exp);

	// calculate field on axis
	rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create();
	line->set_source_model(model_coil);
	line->set_base(rat::mdl::PathAxis::create('z','x',0.4,0.01,0,0,4e-3));
	line->set_fmm_settings(fmm_settings);
	//line->set_use_volume_elements(volume_elements);
	line->set_num_gauss(num_gauss);

	// run calculation
	line->calculate();

	// soleno calculation for comparison
	// setup soleno calculation
	rat::fmm::ShSolenoPr sol = rat::fmm::Soleno::create();
	sol->set_solenoid(inner_radius,outer_radius,-height/2,height/2,operating_current,num_turns,5);

	// get target points
	const arma::Mat<double> Rt = line->get_coords();

	// calculate at target points
	const arma::Mat<double> Bsol = sol->calc_B(Rt);

	// get field
	const arma::Mat<double> Bfmm = line->get_field("B");

	// compare soleno to direct
	double sol2fmm = arma::max(arma::max(arma::abs(Bsol-Bfmm)/arma::max(rat::cmn::Extra::vec_norm(Bsol)),1));


	// now we calculate the inductance
	rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(model_coil);
	ind->calculate();
	double Efmm = ind->calc_stored_energy();

	// stored energy with soleno
	double Lsol = arma::accu(sol->calc_M());
	double Esol = 0.5*Lsol*operating_current*operating_current;
	

	// checking if vector potential descending
	if(sol2fmm>tol)rat_throw_line("difference in magnetic field exceeds tolerance");
	
	// checking if vector potential descending
	if(std::abs(Esol-Efmm)/Esol>tol)rat_throw_line("difference in inductance field exceeds tolerance");
}