/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <iostream>
#include <cmath>

#include "rat/common/defines.hh"

#include "crossrectangle.hh"
#include "area.hh"
#include "pathcircle.hh"
#include "modelcoil.hh"

// main
int main(){
	// settings
	double inner_radius = 0.05;
	double outer_radius = 0.08;
	double height = 0.05;
	double element_size = 0.003;
	arma::uword num_sections = 4;

	// create rectangle object
	rat::mdl::ShCrossRectanglePr crss = rat::mdl::CrossRectangle::create(
		0,outer_radius-inner_radius,-height/2,
		height/2,element_size);

	// circular path
	rat::mdl::ShPathCirclePr base = rat::mdl::PathCircle::create(
		inner_radius,num_sections,element_size);
	base->set_offset(outer_radius-inner_radius);

	// create area
	rat::mdl::ShModelMeshPr coil = rat::mdl::ModelMesh::create(base, crss);

	// calculate volume
	double V = coil->calc_total_volume();
	double Vch = arma::datum::pi*(outer_radius*outer_radius - inner_radius*inner_radius)*height;
	if(std::abs(V - Vch)/Vch>1e-2)rat_throw_line("unexpected volume");
	

	// calculate surface area
	double S = coil->calc_total_surface_area();
	double Sch = 2*arma::datum::pi*(inner_radius + outer_radius)*height + 
		2*arma::datum::pi*(outer_radius*outer_radius - inner_radius*inner_radius);
	if(std::abs(S - Sch)/Sch>1e-2)rat_throw_line("unexpected surface area");
}