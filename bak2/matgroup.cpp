/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// include header
#include "matgroup.hh"

// code specific to Rat
namespace rat{namespace mdl{

	// default constructor
	MatGroup::MatGroup(){

	}

	// factory
	ShMatGroupPr MatGroup::create(){
		return std::make_shared<MatGroup>();
	}

	// get fraction
	arma::Col<double> MatGroup::get_fraction()const{
		return fraction_;
	}

	// setting tolerance for bisection
	void MatGroup::set_tolerance(const double tolerance){
		tolerance_ = tolerance;
	}

	// add new conductor
	void MatGroup::add_material(const double fraction, ShMaterialPr con){
		// check input
		assert(con!=NULL); 

		// allocate new conductor list
		ShMaterialPrList new_con(con_.n_elem + 1);

		// set old and new conductors
		for(arma::uword i=0;i<con_.n_elem;i++)new_con(i) = con_(i);
		new_con(con_.n_elem) = con;

		// set new conductor list
		con_ = new_con;

		// extend fractions
		fraction_ = arma::join_vert(fraction_,arma::Col<double>::fixed<1>{fraction});
	}

	// add new conductor
	void MatGroup::add_material(const arma::Col<double> fraction, ShMaterialPrList con){
		// allocate new source list
		ShMaterialPrList new_con(con_.n_elem + con.n_elem);

		// set old and new sources
		for(arma::uword i=0;i<con_.n_elem;i++)new_con(i) = con_(i);
		for(arma::uword i=0;i<con.n_elem;i++)new_con(con_.n_elem+i) = con(i);

		// set new source list
		con_ = new_con;

		// extend fractions
		fraction_ = arma::join_vert(fraction_,fraction);
	}

	// calculate current density from electric field
	arma::Row<double> MatGroup::calc_current_density(
		const arma::Row<double> &E, const arma::Row<double> &Bm, 
		const arma::Row<double> &T, const arma::Row<double> &alpha, 
		const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// check pre-calculation
		assert(Jc.n_rows==con_.n_elem);
		assert(rho.n_rows==con_.n_elem);

		// allocate
		arma::Row<double> J(E.n_rows,E.n_cols,arma::fill::zeros);
		
		// walk over conductors and accumulate current density
		for(arma::uword i=0;i<con_.n_elem;i++){
			J += fraction_(i)*con_(i)->calc_current_density(E,Bm,T,alpha,Jc.row(i),rho.row(i));
		}

		// return current density
		return J;
	}


	// calculate electric field using bi-section solver
	arma::Row<double> MatGroup::calc_electric_field(
		const arma::Row<double> &J, const arma::Row<double> &Bm, 
		const arma::Row<double> &T, const arma::Row<double> &alpha,
		const arma::Mat<double> &Jc, const arma::Mat<double> &rho) const{

		// check input
		assert(J.n_cols==Bm.n_cols); assert(J.n_cols==T.n_cols); assert(J.n_cols==alpha.n_cols);
		assert(J.n_rows==Bm.n_rows); assert(J.n_rows==T.n_rows); assert(J.n_rows==alpha.n_rows);
		assert(Jc.n_rows==con_.n_rows); assert(rho.n_rows==con_.n_rows);

		// allocate output electric fieldc
		arma::Row<double> E(J);

		// if critical current is zero then do linear equation
		const arma::Row<arma::uword> idx_normal = arma::find(arma::any(Jc==0,0) || J==0).t();
		const arma::Row<arma::uword> idx_sc = arma::find(arma::any(Jc>0,0)).t();

		// linear resistance calculation
		const arma::Mat<double> rhonc = rho.cols(idx_normal);
		if(arma::any(idx_normal))E.cols(idx_normal) = (1.0/arma::sum(fraction_/rhonc.each_col(),0))%J.cols(idx_normal);

		// check if there is any superconductors
		if(!arma::any(idx_sc))return E;

		// allocate midpoint output electric field
		arma::Row<double> Emid(idx_sc.n_elem);

		// estimate lower bound
		arma::Row<double> Elow(idx_sc.n_elem,arma::fill::zeros);
		arma::Row<double> Ehigh = arma::Row<double>(idx_sc.n_elem,arma::fill::ones)*arma::datum::inf;

		// get superconducting properties only
		const arma::Row<double> Jsc = J.cols(idx_sc);
		const arma::Mat<double> Jcsc = Jc.cols(idx_sc);
		const arma::Mat<double> rhosc = rho.cols(idx_sc);
		const arma::Row<double> Bmsc = Bm.cols(idx_sc);
		const arma::Row<double> Tsc = T.cols(idx_sc);
		const arma::Row<double> alphasc = alpha.cols(idx_sc);

		// walk over conductors
		for(arma::uword i=0;i<con_.n_elem;i++){
			// we assume all current runs through this material
			const arma::Row<double> Jcon = Jsc/fraction_(i);
			
			// calculate electric field
			const arma::Row<double> Econ = con_(i)->calc_electric_field(Jcon,Bmsc,Tsc,alphasc,Jcsc.row(i),rhosc.row(i));
			
			// find lowest electric field value
			Ehigh = arma::min(Ehigh,Econ);
		}

		// calculate current density at these bounds
		arma::Row<double> vlow = calc_current_density(Elow,Bmsc,Tsc,alphasc,Jcsc,rhosc) - Jsc;
		arma::Row<double> vhigh = calc_current_density(Ehigh,Bmsc,Tsc,alphasc,Jcsc,rhosc) - Jsc;
		
		// start bisection algorithm
		for(;;){
			// update mid point
			Emid = (Elow+Ehigh)/2;

			// calculate intermeditae value
			const arma::Row<double> vmid = calc_current_density(Emid,Bmsc,Tsc,alphasc,Jcsc,rhosc) - Jsc;

			// update lower and upper bounds
			const arma::Row<arma::uword> idx1 = arma::find(vmid<0).t();
			const arma::Row<arma::uword> idx2 = arma::find(vmid>=0).t();

			// update mid-point
			Elow(idx1) = Emid(idx1); Ehigh(idx2) = Emid(idx2);

			// check convergence
			if(arma::max(arma::max(Ehigh-Elow))<tolerance_)break;
		}

		// add to output array
		E.cols(idx_sc) = Emid;

		// find an electric field where all the 
		// supplied current density is soaked by the materials
		return E;
	}

	// critical currnet summation
	arma::Mat<double> MatGroup::calc_critical_current_density(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const arma::Row<double> &alpha, const bool combined) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// allocate
		arma::Mat<double> Je(con_.n_elem,Bm.n_elem);

		// walk over materials
		for(arma::uword i=0;i<con_.n_elem;i++){
			Je.row(i) = con_(i)->calc_critical_current_density(Bm,T,alpha); 
		}

		// combined
		if(combined){
			Je = arma::sum(Je.each_col()%fraction_,0);
		}

		// return accumulated current density
		return Je;
	}

	// critical currnet summation
	arma::Mat<double> MatGroup::calc_resistivity(
		const arma::Row<double> &Bm, const arma::Row<double> &T, const bool combined) const{

		// check filling fractions
		if(arma::accu(fraction_)>1.0)rat_throw_line("Total filling fraction exceeds 1.0");

		// allocate
		arma::Mat<double> rho(con_.n_elem,Bm.n_elem);

		// walk over materials
		for(arma::uword i=0;i<con_.n_elem;i++){
			rho.row(i) = con_(i)->calc_resistivity(Bm,T); 
		}

		// combined
		if(combined){
			rho = 1.0/arma::sum(fraction_/rho.each_col(),0);
		}

		// return accumulated current density
		return rho;
	}


	// copy this conductor group
	ShMaterialPr MatGroup::copy() const{
		// create new conductor
		ShMatGroupPr mycopy = MatGroup::create();

		// copy the stored conductors in this group
		mycopy->con_.set_size(con_.n_elem);
		for(arma::uword i=0;i<con_.n_elem;i++)
			mycopy->con_(i) = con_(i)->copy();
		
		// copy my properties
		mycopy->fraction_ = fraction_;
		mycopy->use_parallel_ = use_parallel_;
		mycopy->tolerance_ = tolerance_;

		// return copy
		return mycopy;
	}

	// specific heat output in [J m^-3 K^-1]
	arma::Row<double> MatGroup::calc_specific_heat(const arma::Row<double> &T)const{
		// allocate
		arma::Row<double> sh(T); sh.zeros();

		// add materials
		for(arma::uword i=0;i<con_.n_elem;i++)
			sh += fraction_(i)*con_(i)->calc_specific_heat(T);

		// return values
		return sh;
	}


	// thermal conductivity 
	arma::Row<double> MatGroup::calc_thermal_conductivity(const arma::Row<double> &Bm, const arma::Row<double> &T)const{
		// allocate
		arma::Row<double> k(T); k.zeros();

		// materials are stacked in parallel
		// conductivities are added
		if(is_parallel_){
			// add materials
			for(arma::uword i=0;i<con_.n_elem;i++)
				k += fraction_(i)*con_(i)->calc_thermal_conductivity(Bm,T);
		}

		// materials are stacked in series
		// resistivities are added
		else{
			// allocate thermal resistivity
			arma::Row<double> r(T); r.zeros();

			// add materials
			for(arma::uword i=0;i<con_.n_elem;i++)
				k += fraction_(i)*(1.0/con_(i)->calc_thermal_conductivity(Bm,T));

			// set k
			k = 1.0/r;
		}

		// return values
		return k;
	}

	// get type
	std::string MatGroup::get_type(){
		return "mdl::matgroup";
	}

	// method for serialization into json
	void MatGroup::serialize(Json::Value &js, std::list<cmn::ShNodePr> &list) const{
		// settings
		js["type"] = get_type();
		js["tolerance"] = tolerance_;
		js["parallel"] = use_parallel_;

		// conductor objects
		const arma::uword num_con = con_.n_elem;
		for(arma::uword i=0;i<num_con;i++){
			js["con"].append(cmn::Node::serialize_node(con_(i), list));
			js["fraction"].append(fraction_(i));
		}
	}

	// method for deserialisation from json
	void MatGroup::deserialize(const Json::Value &js, std::list<cmn::ShNodePr> &list, const cmn::NodeFactoryMap &factory_list){
		// settings
		use_parallel_ = js["parallel"].asBool();
		tolerance_ = js["tolerance"].asDouble();

		// conductors
		const arma::uword num_con = js["con"].size();
		const arma::uword num_frac = js["fraction"].size();
		if(num_con!=num_frac)rat_throw_line("conductor group inconsistent");
		con_.set_size(num_con);	fraction_.set_size(num_frac);
		for(arma::uword i=0;i<num_con;i++){
			con_(i) = cmn::Node::deserialize_node<Material>(js["con"].get(i,0), list, factory_list);
			fraction_(i) = js["fraction"].get(i,0).asDouble();
		}
	}

}}

