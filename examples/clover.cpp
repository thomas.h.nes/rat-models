/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/freecad.hh"

// header files for distmesh
#include "rat/dmsh/distfun.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrrectangle.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathclover.hh"
#include "pathcable.hh"
#include "modelgroup.hh"
#include "calcgrid.hh"
#include "calcharmonics.hh"
#include "material.hh"
#include "matcopper.hh"
#include "matgroup.hh"
#include "matrebco.hh"
#include "calcline.hh"
#include "pathaxis.hh"
#include "crossdmsh.hh"
#include "calcmesh.hh"
#include "calcgroup.hh"
#include "modelmesh.hh"
#include "calcsurface.hh"
#include "calclength.hh"

// DESCRIPTION
// This example shows how to create a small single layer 
// cloverleaf coil including both magnet poles. Cloverleaf coils
// are probably a good solution for creating a dipole with high
// temperature superconducting (HTS) tape.

// main
int main(){
	// INPUT SETTINGS
	// switchboard for calculation types
	const bool run_coil_field = true; // calculate field on the coil
	const bool run_surface_field = true; // calculate field on the surface of the meshes
	const bool run_grid = true; // calculate field on a grid of points surrounding the coil
	const bool run_harmonics = true; // run harmonic coil calculation
	const bool run_line = false; // line calculation at aperture center
	const bool run_length = true; // line calculation at aperture center
	const bool run_freecad = false; // export the geometry to a python freecad macro
	const bool run_json_export = false; // export geometry to json file

	// output directory
	const std::string output_dir = "./clover/";
	const std::string fname = "clover";

	// operating conditions
	const double Iop = 2000; // operating current [A]
	const double Top = 20; // operating temperature [K]

	// coil parameters
	const bool is_block = false; // is the coil a block or a cable?
	const double ellstr1 = 0.140; // straight section length along aperture [m]
	const double ellstr2 = 0.060; // straight section length accross aperture [m]
	const double bridge_angle = arma::datum::pi*2*7.0/360;

	// winding parameters
	// these are only used when the coil is modelled 
	// as a cable (i.e. is_block==false);
	const arma::uword idx_incr = 1; // location at which the turn is incremented to the next
	const arma::uword idx_start = 2; // section in which the cable starts 
	const arma::sword num_add = 2; // number of extra sections to add/remove at end

	// cable settings
	const double wcable = 12e-3; // width of the cable (or coil pack) [m]
	const double dcable = 0.2e-3; // thickness of the cable [m]
	const double dinsu = 0e-3/2; // thickness of insulation layer around each cable [m]
	const arma::uword num_turns = 60; // number of turns
	const double element_size = 2e-3; // size of the used elements [m]

	// the cloverleaf coil-end spline parameters
	const double ell_trans = 0; // transition length [m]
	const double height = 20e-3; // height of the bridge [m]
	const double str12 = 35e-3;	// strength for determining control point positions [m]
	const double str34 = 14e-3; // strength for determining control point positions [m]

	// calculate remaining parameters
	const double dpack = num_turns*(dcable + 2*dinsu); // thickness of the coil pack

	// former
	const double dformer = 10e-3; //12e-3;
	const double dring = 2e-3;

	// add little iron yoke
	const bool iron_yoke = false;

	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// The models defining the coil geometry are setup using a nodegraph
	// that allows reusing the same object multiple times. For this cloverleaf
	// example the structure is given as:
	//     PathClover -> PathCable -|         |-> Pole1 -|  
	// Circuit, Coolant, Mat -|-> Coil -|          |-> Model
	//                   CrossRect -|         |-> Pole2 -|
	// * make sure your text editor uses a mono-spaced font

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	{
		// add ReBCO superconductor
		rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
		rebco->set_fujikura_cern(); // one of the default scaling relations
		rebco_cu->add_material(0.01,rebco); // 1% of the cable is superconductor

		// add copper coatings
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100(); // standard copper with RRR of 100
		rebco_cu->add_material(0.20,copper); // 20 % of the cable is copper (10 mu on each side)
	}
	
	// Create clover path 
	// this is the basis which determines the shape of the coil
	// the PathClover class is actually a wrapper automatically 
	// setting all the sections based on the input parameters.
	// By default all coils are created in the XY-plane.
	// rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(ellstr1,ellstr2,
	// 	height,dpack,ell_trans,str12,str34,element_size);
	// path_clover->set_bending_radius(0.4);

	// create clover path
	rat::mdl::ShPathCloverPr path_clover = rat::mdl::PathClover::create(
		ellstr1+2*dpack,ellstr2+2*dpack,height,0,ell_trans,str12,str34,element_size);
	path_clover->add_flip();
	path_clover->set_bridge_angle(bridge_angle);

	// create magnet poles
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create(); pole1->set_name("p1");
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create(); pole2->set_name("p2");

	// these transformations flip the second pole
	pole2->add_rotation(1,0,0,arma::datum::pi);
	pole2->add_reverse();

	// create coil using cable detail this takes the cloverleaf 
	// basepath and winds a cable around up to the number of turns
	if(is_block==false){
		// create cable
		rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_clover);
		path_cable->set_turn_step(dcable + 2*dinsu);
		path_cable->set_num_turns(num_turns);
		path_cable->set_idx_incr(idx_incr);
		path_cable->set_idx_start(idx_start);
		path_cable->set_num_add(num_add);
		path_cable->set_offset(dinsu+dring);

		// create cable cross section
		rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

		// create coil based on cable
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, cross_rect); coil->set_name("c1");
		coil->set_enable_current_sharing(true);
		
		// add a translation in Z to lift the coil above the mid-plane
		coil->add_translation(0,0,8e-3);

		// set circuit
		coil->set_operating_current(Iop);
		coil->set_operating_temperature(Top);
		coil->set_material(rebco_cu);
		coil->add_flip();

		// add coil to each pole
		pole1->add_model(coil); pole2->add_model(coil);
	}

	// create coil as block
	else{
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_coil = rat::mdl::CrossRectangle::create(dring,dring+dpack,-wcable/2,wcable/2,1.2e-3);

		// create block coil
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_clover, cross_coil); coil->set_name("c1");
		
		// need to set number of turns as the coil pack represents all turns
		coil->set_number_turns(num_turns);
		
		// add a translation in Z to lift the coil above the mid-plane
		coil->add_translation(0,0,8e-3);
		
		// set circuit
		coil->set_operating_current(Iop);
		coil->set_operating_temperature(Top);
		coil->set_material(rebco_cu);
		coil->add_flip();

		// add coil to each pole
		pole1->add_model(coil);	pole2->add_model(coil);
	}

	// create former
	if(dformer>0){
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_former = rat::mdl::CrossRectangle::create(-dformer,0,-wcable/2,wcable/2,1.2e-3);

		// add former
		rat::mdl::ShModelMeshPr former = rat::mdl::ModelMesh::create(path_clover, cross_former); former->set_name("fm");
		
		// add a translation in Z to lift the coil above the mid-plane
		former->add_translation(0,0,8e-3);
		former->add_flip();

		// add coil to each pole
		pole1->add_model(former); pole2->add_model(former);
	}

	// copper ring
	if(dring>0){
		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_iring = rat::mdl::CrossRectangle::create(0,dring,-wcable/2,wcable/2,1.2e-3);

		// add former
		rat::mdl::ShModelMeshPr iring = rat::mdl::ModelMesh::create(path_clover, cross_iring); iring->set_name("ir");
		
		// add a translation in Z to lift the coil above the mid-plane
		iring->add_translation(0,0,8e-3);
		iring->add_flip();

		// add coil to each pole
		pole1->add_model(iring); pole2->add_model(iring);

		// create block cross section
		rat::mdl::ShCrossRectanglePr cross_oring = rat::mdl::CrossRectangle::create(dring+dpack,2*dring+dpack,-wcable/2,wcable/2,1.2e-3);

		// add former
		rat::mdl::ShModelMeshPr oring = rat::mdl::ModelMesh::create(path_clover, cross_oring); oring->set_name("or");
		
		// add a translation in Z to lift the coil above the mid-plane
		oring->add_translation(0,0,8e-3);
		oring->add_flip();

		// add coil to each pole
		pole1->add_model(oring); pole2->add_model(oring);
	}

	// create a model that holds both poles
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(pole1); model->add_model(pole2);

	// add iron yoke
	// distance function: subtract two rounded rectangles
	rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.09,0.09,-0.05,0.05,15e-3),
		rat::dm::DFRRectangle::create(-0.06,0.06,-0.02,0.02,8e-3));

	// yoke
	if(iron_yoke){
		// create cross section with distance function
		rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(5e-3,df,rat::dm::DFOnes::create());

		// create path
		rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',0.1,0,0,0,5e-3);

		// create mesh object
		rat::mdl::ShModelMeshPr yoke = rat::mdl::ModelMesh::create(pth,cdmsh);

		// add to model
		model->add_model(yoke);
	}

	// the axis of the magnet is set here 
	// it is a 0.5 m long line along y with 
	// main harmonics pointing along z. The line 
	// is centered at the origin and is spaced 
	// with 1 mm elements.
	rat::mdl::ShPathPr pth_axis = rat::mdl::PathAxis::create('y','z',0.5,0,0,0,element_size);



	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		
		// set output path
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->set_num_gauss(2);

		// set calculation
		mesh->set_output_type(rat::mdl::ALL);
		
		// add to root
		mesh->calculate_and_write(lg);
	}

	// surface field calculation
	if(run_surface_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcSurfacePr surf = rat::mdl::CalcSurface::create(model, model);
		
		// set output path
		surf->set_output_dir(output_dir);
		surf->set_output_fname(fname);

		// set calculation
		surf->set_output_type(rat::mdl::ALL);
		
		// add to root
		surf->calculate_and_write(lg);
	}

	// calculate grid
	if(run_grid){
		// Create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(-0.14,0.14,140, -0.16,0.16,160, -0.05,0.05,50);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);

		// add to root
		grid->calculate_and_write(lg);
	}

	// calculate harmonics
	if(run_harmonics){	
		// create harmonic coil calculation
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(model,pth_axis);

		// set output path
		harm->set_output_dir(output_dir);
		harm->set_output_fname(fname);

		// number of points used for harmonic calculation
		harm->set_num_theta(64);

		// radius at which harmonics are defined 
		// (commonly used is (2/3) of aperture)
		harm->set_radius(10e-3); // [m]

		// create settings
		rat::fmm::ShSettingsPr sttngs = rat::fmm::Settings::create();
		
		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		sttngs->set_num_exp(8);

		// set to harmonics
		harm->set_fmm_settings(sttngs);

		// add to root
		harm->calculate_and_write(lg);
	}

	// simple line calculation
	if(run_line){	
		// create harmonic coil calculation
		rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create(model, pth_axis);
		
		// set output path
		line->set_output_dir(output_dir);
		line->set_output_fname(fname);

		// add to root
		line->calculate_and_write(lg);
	}

	// simple line calculation
	if(run_length){
		// create harmonic coil calculation
		rat::mdl::ShCalcLengthPr length = rat::mdl::CalcLength::create(model);
		
		// set output path
		length->set_output_dir(output_dir);
		length->set_output_fname(fname);

		// add to root
		length->calculate_and_write(lg);
	}	

	// write a freecad macro
	if(run_freecad){
		rat::cmn::ShFreeCADPr fc = rat::cmn::FreeCAD::create("clover.FCMacro","clover");
		fc->set_num_sub(1);
		model->export_freecad(fc);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		rat::cmn::Extra::create_directory(output_dir);
		slzr->export_json(output_dir + fname + ".json");
	}

}