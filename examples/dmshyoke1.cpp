/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfcircle.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"


// DESCRIPTION
// Example showing how the 2D mesher can be used to 
// draw a mesh for an iron yoke. This can later be 
// used as input for a non-linear solver. Which,
// is high on the list of things to be implemented.
// Refer to distmesh-cpp for more information on 
// how to setup the mesh.

// main
int main(){
	// INPUT SETTINGS
	// settings
	const double element_size = 0.02; // requested size of the elements [m]
	const double Rin = 0.08; // yoke inner radius [m]
	const double Rout = 0.2; // yoke outer radius [m]
	const double dchannel = 0.04; // width of the channel [m]
	const double ell = 0.2; // length of the yoke [m]

	// GEOMETRY SETUP
	// create cross section with distance function
	// we create a circle defining the outer radius
	// and we use diff to subtract a circle with inner
	// radius as well as the two rectangular regions 
	// at the top and bottom of the yoke
	rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(element_size,rat::dm::DFDiff::create(rat::dm::DFDiff::create(
		rat::dm::DFDiff::create(rat::dm::DFCircle::create(Rout,0,0),rat::dm::DFCircle::create(Rin,0,0)),
		rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,0.17,0.21)),
		rat::dm::DFRectangle::create(-dchannel/2,dchannel/2,-0.21,-0.17)),rat::dm::DFOnes::create());

	// due to clipping need to add some fixed points in the corners
	// this process needs to be automated still
	arma::Mat<double> pfix(4,2);
	pfix.row(0) = arma::Row<double>{dchannel/2,Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(1) = arma::Row<double>{-dchannel/2,Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(2) = arma::Row<double>{dchannel/2,-Rout*std::sin(std::acos((dchannel/2)/Rout))};
	pfix.row(3) = arma::Row<double>{-dchannel/2,-Rout*std::sin(std::acos((dchannel/2)/Rout))};
	cdmsh->set_fixed(pfix);

	// create path
	//rat::mdl::ShPathStraightPr pth = rat::mdl::PathStraight::create(ell,element_size);
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ell,0,0,0,element_size);

	// create modelcoil
	rat::mdl::ShModelMeshPr model = rat::mdl::ModelMesh::create(pth,cdmsh);

	// create modelmesh
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
	mesh->set_output_dir("./dmsh/");
	mesh->set_output_fname("yoke1");
	mesh->add_output_type(rat::mdl::ORIENTATION);

	// setup and write
	mesh->setup(); mesh->write();
}