/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"

// header files for models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "transbend.hh"
#include "modelgroup.hh"
#include "pathflared.hh"
#include "calcmesh.hh"
#include "calcinductance.hh"

// DESCRIPTION
// Flared ends are a common solution for allowing access
// to the aperture of a block coil. This is an example
// showing how to create a coil with flared ends.

// main
int main(){
	// INPUT SETTINGS
	// calculation
	const bool run_coil_field = true; // calculate field on coil
	const bool run_inductance = true;

	// data storage directory and filename
	const std::string output_dir = "./flared/";
	const std::string fname = "flared";

	// cable settings
	const double wcable = 12e-3; // width of the cable [m]
	const double dcable = 1.2e-3; // thickness of the cable [m]
	const double dinsu = 0.1e-3; // thickness of the insulation layer around each cable [m]
	
	// coil geometry settings
	const double ell1 = 0.1; // length of straight section [m]
	const double ell2 = 0.15; // length of sloped section [m]
	const double arcl = arma::datum::pi/12; // angle of end [rad]
	const double radius1 = 0.2; // hardway bending radius [m]
	const double radius2 = 0.05; // softway bending radius [m]
	const double element_size = 2e-3; // size of the elements [m]
	const arma::uword num_turns = 12; // number of turns 

	// operating conditions
	const double Top = 4.5;
	const double Iop = 4000;

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// create flared end path
	rat::mdl::ShPathFlaredPr pathflared = rat::mdl::PathFlared::create(
		ell1,ell2,-arcl,radius1,radius2,element_size,num_turns*dcable);

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(pathflared);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(1);
	path_cable->set_idx_start(0);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil1->add_translation(0,0,2e-2);
	coil1->set_operating_current(Iop);
	coil1->set_operating_temperature(Top);

	// second pole
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil2->add_translation(0,0,2e-3);
	//coil2->add_rotation(1,0,0,arma::datum::pi);
	coil2->add_reflect_xy();
	coil2->add_flip();
	//coil2->add_reverse();
	coil2->set_operating_current(Iop);
	coil2->set_operating_temperature(Top);
	

	// model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1);
	model->add_model(coil2);


	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// run calculation
		mesh->calculate(lg); mesh->write(lg);
	}

	// calculate inductance
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(model);
		ind->set_type(rat::mdl::COIL);
		ind->set_output_dir(output_dir);
		ind->set_output_fname(fname);
		
		// add calculation
		ind->calculate(lg); ind->write(lg);
	}
	
}