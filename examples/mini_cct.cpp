/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "calcline.hh"
#include "calcharmonics.hh"
#include "calctracks.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "pathaxis.hh"
#include "calcsurface.hh"
#include "calcgroup.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_surface_field = true; // calculate field on surface of coil
	const bool run_harmonics = true; // calculate harmonics along aperture
	const bool run_tracking = true; // track particles through the aperture
	const bool run_freecad = false; // export model to freecad
	const bool run_opera = false; // export model to opera
	const bool run_json_export = true; // export geometry to json file

	// data storage directory and filename
	const std::string output_dir = "./mini_cct/";
	const std::string fname = "mini_cct";

	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const double dcable = 2e-3; // thickness of cable [m] (width of slot)
	const double wcable = 5e-3; // width of cable [m] (depth of slot)
	const double delta =  0.5e-3; // spacing between turns on inner radius
	const double alpha = 30.0*2*arma::datum::pi/360; // skew angle for inner radius
	
	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const double num_turns = 10; // number of turns (can be non-integer)
	const double radius = 0.025; // aperture radius in [m]
	const double dradial = 0.3e-3; // spacing between cylinders [m]
	const double dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 360;	// number of nodes each turn
	const double element_size = 2e-3; // size of the elements [m]
		
	// conductor geometry
	const arma::uword nd = 2; // number of elements in cable thickness
	const arma::uword nw = 5; // number of elements in cable width
	const double dstr = 0.825e-3; // diameter of the wire [m]
	const double fcu2sc = 1.9; // copper to superconductor fraction of the ware

	// operating conditions
	const double operating_current = 4000; // operating current [A]
	const double operating_temperature = 4.5; // operating temperature [K]

	// calculate remaining parameters
	const double r1 = radius+dformer; // inner coil radius [m]
	const double r2 = radius+2*dformer+wcable+dradial; // outer coil radius [m]

	// filling fraction
	const double ffill = nd*nw*arma::datum::pi*(dstr/2)*(dstr/2)/(dcable*wcable);

	// inner radius parameters
	const double a = radius/std::tan(alpha);
	const double omega = (dcable + delta)/std::sin(alpha);

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// create conductor
	rat::mdl::ShMatGroupPr nbti_strand = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
		nbti->set_nbti_LHC();
		nbti_strand->add_material(ffill/(1.0+fcu2sc),nbti);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100();
		nbti_strand->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	}

	// create path for inner layer
	rat::mdl::ShPathCCTPr path_cct1 = rat::mdl::PathCCT::create(
		num_poles,r1,a,omega,num_turns,num_nodes_per_turn);
	path_cct1->set_is_reverse(is_reverse); // reverse slanting
	path_cct1->set_twist(alpha/num_poles);

	// create path for outer layer
	rat::mdl::ShPathCCTPr path_cct2 = rat::mdl::PathCCT::create(
		num_poles,r2,a,omega,num_turns,num_nodes_per_turn);
	path_cct2->set_is_reverse(!is_reverse); // reverse slanting
	path_cct2->set_twist(alpha/num_poles);
	

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,0,wcable,nd,nw);

	// create inner and outer coils
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect, nbti_strand);
	coil1->set_name("inner");
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_operating_current(operating_current);
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect, nbti_strand);
	coil2->set_name("outer");
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_operating_current(operating_current);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1); model->add_model(coil2);

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',0.4,0,0,0,2e-3);
	

	// CALCULATION AND OUTPUT
	// create a root for storing all the calculations
	rat::mdl::ShCalcGroupPr root = rat::mdl::CalcGroup::create();

	// calculate field on the coil
	if(run_surface_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcSurfacePr surf = rat::mdl::CalcSurface::create(model, model);
		surf->set_output_dir(output_dir);
		surf->set_output_fname(fname);

		// add calculation
		root->add_calculation(surf);
	}

	// run tracking code
	if(run_tracking){
		// Create a grid of 140x160x50 points in the volume defined by intervals:
		rat::mdl::ShModelMeshPr beam_pipe = rat::mdl::ModelMesh::create(
			rat::mdl::PathAxis::create('z', 'y', 0.7, 0,0,0, element_size),
			rat::mdl::CrossCircle::create(radius,element_size));

		// create an emitter
		rat::mdl::ShEmitterBeamPr beam_emitter = rat::mdl::EmitterBeam::create();
		beam_emitter->set_proton();
		beam_emitter->set_beam_energy(0.948); // GeV
		beam_emitter->set_spawn_coord(
			arma::Col<double>::fixed<3>{0,0,-0.349},
			arma::Col<double>::fixed<3>{0,0,1},
			arma::Col<double>::fixed<3>{1,0,0},
			arma::Col<double>::fixed<3>{0,1,0});
		beam_emitter->set_xx(0.9, 0.005, 0.0*2*arma::datum::pi/360);
		beam_emitter->set_yy(0.9, 0.005, 0.0*2*arma::datum::pi/360);
		beam_emitter->set_start_idx(0);

		// create tracker
		rat::mdl::ShCalcTracksPr tracker = rat::mdl::CalcTracks::create(model, beam_pipe, beam_emitter);
		tracker->set_output_dir(output_dir);
		tracker->set_output_fname(fname);
		tracker->set_stepsize(10e-3);

		// add calculation
		root->add_calculation(tracker);
	}

	// harmonics calculation
	if(run_harmonics){
		// create harmonic calculator
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(model, pth);
		harm->set_output_dir(output_dir);
		harm->set_output_fname(fname);

		// settings related to harmonics calculation
		harm->set_compensate_curvature(true);
		harm->set_num_theta(64);
		harm->set_radius(radius*2.0/3.0);

		// add calculation
		root->add_calculation(harm);
	}

	// write a freecad macro
	if(run_freecad){
		model->export_freecad(rat::cmn::FreeCAD::create("mini_cct.FCMacro","mini_sextupole"));
	}
		
	// export to an opera conductor file
	if(run_opera){
		model->export_opera(rat::cmn::Opera::create("mini_cct.cond"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);
		rat::cmn::Extra::create_directory(output_dir);
		slzr->export_json(output_dir + fname + ".json");
	}

	// run model
	root->calculate_and_write(lg);

}