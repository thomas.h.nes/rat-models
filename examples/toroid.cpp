/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"
#include "rat/common/gmshfile.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "pathcable.hh"
#include "modeltoroid.hh"
#include "pathrectangle.hh"
#include "pathdshape.hh"
#include "material.hh"
#include "matcopper.hh"
#include "matgroup.hh"
#include "matrebco.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"
#include "calcgroup.hh"
#include "calcinductance.hh"

// DESCRIPTION
// Example geometry setup for a toroid with 
// 6 double pancake D-shaped limbs.

// main
int main(){
	// INPUT SETTINGS
	// calculation 
	const bool run_coil_field = true;
	const bool run_grid = true;
	const bool run_json_export = true; // export geometry to json file
	const bool run_inductance = true; 

	// data storage directory and filename
	const std::string output_dir = "./toroid/";
	const std::string fname = "toroid";

	// coil geometry
	const double width = 0.25;
	const double height = 0.4;
	const double element_size = 4e-3;

	// cable settings
	const double wcable = 12e-3;
	const double dcable = 1.2e-3;
	const double dinsu = 0.1e-3;
	const arma::uword num_turns = 12;
	const bool use_current_sharing = true;

	// cable settings
	const arma::uword idx_incr = 1;
	const arma::uword idx_start = 0;

	// operating conditions
	const double operating_current = 8000;
	const double operating_temperature = 20;

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	{
		// add ReBCO superconductor
		rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
		rebco->set_fujikura_cern(); // one of the default scaling relations
		rebco_cu->add_material(0.01,rebco); // 1% of the cable is superconductor

		// add copper coatings
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100(); // standard copper with RRR of 100
		rebco_cu->add_material(0.20,copper); // 20 % of the cable is copper (10 mu on each side)
	}

	// create unified path
	//rat::mdl::ShPathRectanglePr path_rect = rat::mdl::PathRectangle::create(width,height,radius,element_size);
	rat::mdl::ShPathDShapePr path_rect = rat::mdl::PathDShape::create(height,width,element_size);

	// create cable
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(path_rect);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns);
	path_cable1->set_idx_incr(idx_incr);
	path_cable1->set_idx_start(idx_start);

	// create cable
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(path_rect);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns);
	path_cable2->set_idx_incr(idx_incr);
	path_cable2->set_idx_start(idx_start);
	path_cable2->set_reverse_base(true);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cable1, cross_rect, rebco_cu);
	coil1->add_translation(0,0,2e-2);
	coil1->set_enable_current_sharing(use_current_sharing);
	coil1->set_name("c1");
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);	

	// create coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cable2, cross_rect, rebco_cu);
	coil2->add_translation(0,0,-2e-2);
	coil2->set_enable_current_sharing(use_current_sharing);
	coil2->set_name("c2");
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);	
	
	// create model
	rat::mdl::ShModelGroupPr coilset = rat::mdl::ModelGroup::create();
	coilset->add_model(coil1);
	coilset->add_model(coil2);
	coilset->set_name("lm");

	// make toroid
	rat::mdl::ShModelToroidPr toroid = rat::mdl::ModelToroid::create(coilset);
	toroid->set_num_coils(6);
	toroid->set_radius(0.1);
	toroid->set_name("tf");

	// add toroid to a model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(toroid);


	// CALCULATION AND OUTPUT
	// create a root for storing all the calculations
	rat::mdl::ShCalcGroupPr root = rat::mdl::CalcGroup::create();

	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// add calculation
		root->add_calculation(mesh);
	}

	// calculate grid
	if(run_grid){
		// Create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(-0.4,0.4,100,-0.4,0.4,100,-0.4,0.4,100);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);
		
		// add calculation
		root->add_calculation(grid);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);
		rat::cmn::Extra::create_directory(output_dir);
		slzr->export_json(output_dir + fname + ".json");
	}

	// calculate inductance
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(model);
		ind->set_type(rat::mdl::COIL);
		ind->set_output_dir(output_dir);
		ind->set_output_fname(fname);
		
		// add calculation
		ind->calculate(lg); ind->write(lg);
	}

	// run model
	root->calculate_and_write(lg);

}