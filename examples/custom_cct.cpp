/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header for common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"

// header files for Models
#include "pathcctcustom.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "calcharmonics.hh"
#include "transbend.hh"
#include "calcgrid.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "calcharmonics.hh"
#include "calcmesh.hh"

// DESCRIPTION
// In this example the custom CCT class is used to 
// create a combined function magnet with both
// dipole and quadrupole components.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true;
	const bool run_harmonics = true;
	const bool run_grid = true;
	const bool run_json_export = true;

	// data storage directory and filename
	const std::string output_dir = "./custom_cct/";
	const std::string fname = "custom_cct";

	// geometry settings
	const double Rbend = 0.15; // bend the magnet
	const double nt = 40; // number of turns
	const double radius1 = 0.025; // inner coil radius [m]
	const double radius2 = 0.025 + 7e-3; // outer coil radius [m]
	const double omega = 6e-3; // winding pitch [m]
	const arma::uword nnpt = 120; // number of nodes per turn

	// cable settings
	const double dcable = 2e-3; // thickness of cable [m] (slot width)
	const double wcable = 5e-3; // width of cable [m] (slot depth)
	const double element_size = 2e-3; // size of the elements [m]
	const arma::uword num_strands = 10;	

	// strength of the different components
	const double dipole_amplitude = 30e-3; // strength of dipole [m]
	const double quad_amplitude = 30e-3; // strength of quadrupole [m]

	// operating conditions
	const double operating_current = 480; // operating current [A]
	const double operating_temperature = 4.5; // operating temperature [K]

	// conductor settings
	const arma::uword nd = 2; // number of wires across cable thickness
	const arma::uword nw = 5; // number of wires across cable width
	const double dstr = 0.825e-3; // strand diameter [m]
	const double ffill = nd*nw*arma::datum::pi*(dstr/2)*(dstr/2)/(dcable*wcable); // filling fraction
	const double fcu2sc = 1.9;

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// setup conductors
	rat::mdl::ShMatGroupPr nbti_strand = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
		nbti->set_nbti_LHC();
		nbti_strand->add_material(ffill/(1.0+fcu2sc),nbti);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100();
		nbti_strand->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	}

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct1 = rat::mdl::PathCCTCustom::create();
	path_cct1->set_range(-nt/2,nt/2);
	path_cct1->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{radius1,radius1});
	path_cct1->set_num_nodes_per_turn(nnpt);
	
	// add dipole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		-arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole changing from minus 
	// to plus along the length of the magnet
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{quad_amplitude,-quad_amplitude}));

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct2 = rat::mdl::PathCCTCustom::create();
	path_cct2->set_range(-nt/2,nt/2);
	path_cct2->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{radius2,radius2});
	path_cct2->set_num_nodes_per_turn(nnpt);
	
	// add dipole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole changing from minus 
	// to plus along the length of the magnet
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,arma::Row<double>{-nt/2,nt/2},
		-arma::Row<double>{quad_amplitude,-quad_amplitude}));

	// set reverse
	path_cct2->add_reverse();
	path_cct2->add_flip();

	// axis of magnet for field quality calculation
	rat::mdl::ShPathGroupPr pth = rat::mdl::PathGroup::create();
	pth->add_path(rat::mdl::PathStraight::create(0.5,1e-3));
	pth->add_translation(0,-0.5/2,0);
	pth->add_rotation(1,0,0,arma::datum::pi/2);
	pth->add_rotation(0,0,1,arma::datum::pi);

	// add bending
	if(Rbend!=0){
		rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, Rbend);
		path_cct1->add_transformation(bending);
		path_cct2->add_transformation(bending);
		pth->add_transformation(bending);
	}

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,element_size/2);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect, nbti_strand);	
	coil1->set_name("inner");
	coil1->set_number_turns(num_strands);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect, nbti_strand);	
	coil2->set_name("outer");
	coil2->set_number_turns(num_strands);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);
	

	// create model 
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1); model->add_model(coil2);


	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// run calculation
		mesh->calculate(lg); mesh->write(lg);
	}

	// calculate grid
	if(run_grid){
		// Create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(-0.2,0.2,100,-0.1,0.1,50,-0.2,0.2,100);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);
		
		// run calculation
		grid->calculate(lg); grid->write(lg);
	}

	// calculate harmonics
	if(run_harmonics){	
		// create harmonic coil calculation
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(model,pth);
		harm->set_output_dir(output_dir);
		harm->set_output_fname(fname);

		// number of points used for harmonic calculation
		harm->set_num_theta(64);

		// radius at which harmonics are defined 
		// (commonly used is (2/3) of aperture)
		harm->set_radius(10e-3); // [m]

		// create settings
		rat::fmm::ShSettingsPr sttngs = rat::fmm::Settings::create();
		
		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		sttngs->set_num_exp(8);

		// set to harmonics
		harm->set_fmm_settings(sttngs);

		// run calculation
		harm->calculate(lg); harm->write(lg);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
		slzr->flatten_tree(model); slzr->export_json("custom_cct.json");
	}

	// done
	return 0;
}