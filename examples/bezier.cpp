/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for models
#include "pathbezier.hh"
#include "crossrectangle.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"

// DESCRIPTION
// Example that shows how the bezier 
// spline path can be used to draw three
// dimensional coil shapes. Note that the 
// code provides not check for edge regression. 
// This means thet it is possible to generate 
// a horrible mess without errors or warnings.

// main
int main(){
	// INPUT SETTINGS
	// cable settings
	const double wcable = 12e-3; // width of the cable [m]
	const double dcable = 1.2e-3; // thickness of the cable [m]

	// spline
	const double ell_trans = 0e-3; // transition length [m]
	const double element_size = 2e-3; // size of the elements [m]

	// The following design cases are provided
	// uncomment the one you wish to run
	// s-shape layer jump
	// const arma::Col<double> R0 = {0,0,0}; // start point xyz [m]
	// const arma::Col<double> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<double> N0 = {1,0,0}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<double> R1 = {40e-3,40e-3,14e-3}; // end point xyz [m]
	// const arma::Col<double> L1 = {0,1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<double> N1 = {1,0,0}; // end normal vector xyz [m] (must be normalized)
	// const double str1 = 30e-3;
	// const double str2 = 10e-3;
	// const double str3 = -10e-3;
	// const double str4 = 10e-3;
	// const double offset = 10e-3;

	// cosine theta coil end
	// const arma::Col<double> R0 = {-25e-3,0,0}; // start point xyz [m]
	// const arma::Col<double> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<double> N0 = {0,0,-1}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<double> R1 = {25e-3,0,0}; // end point xyz [m]
	// const arma::Col<double> L1 = {0,-1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<double> N1 = {0,0,-1}; // end normal vector xyz [m] (must be normalized)
	// const double str1 = 40e-3; // [m]
	// const double str2 = 10e-3; // [m]
	// const double str3 = 15e-3; // [m]
	// const double str4 = 15e-3; // [m]
	// const double offset = 0;  // [m]

	// racetrack coil layer jump
	// const arma::Col<double> R0 = {0,0,0}; // start point xyz [m]
	// const arma::Col<double> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	// const arma::Col<double> N0 = {-1,0,0}; // start normal vector xyz [m] (must be normalized)
	// const arma::Col<double> R1 = {50e-3,0,13e-3}; // end point xyz [m]
	// const arma::Col<double> L1 = {0,-1,0}; // end direction xyz [m] (must be normalized)
	// const arma::Col<double> N1 = {1,0,0}; // end normal vector xyz [m] (must be normalized)
	// const double str1 = 20e-3; // [m]
	// const double str2 = 10e-3; // [m]
	// const double str3 = 10e-3; // [m]
	// const double str4 = 10e-3; // [m]
	// const double offset = 0; // [m]

	// cloverleaf coil end
	const double height = 20e-3; // bridge height [m]
	const arma::Col<double>::fixed<3> R0 = {0,-20e-3-ell_trans,0}; // start point xyz [m]
	const arma::Col<double>::fixed<3> L0 = {0,1,0}; // start direction xyz [m] (must be normalized)
	const arma::Col<double>::fixed<3> N0 = {1,0,0}; // start normal vector xyz [m] (must be normalized)
	const arma::Col<double>::fixed<3> R1 = {20e-3+ell_trans,0,height}; // end point xyz [m]
	const arma::Col<double>::fixed<3> L1 = {1,0,0}; // end direction xyz [m] (must be normalized)
	const arma::Col<double>::fixed<3> N1 = {0,-1,0}; // end normal vector xyz [m] (must be normalized)
	const double str1 = 40e-3; // [m]
	const double str2 = 30e-3; // [m]
	const double str3 = 10e-3; // [m]
	const double str4 = 10e-3; // [m]
	const double offset = 0; // [m]

	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathBezierPr path_bezier = rat::mdl::PathBezier::create(
		R0,L0,N0, R1,L1,N1, str1,str2,str3,str4, ell_trans,element_size,offset);
	path_bezier->set_num_sections(4);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelMeshPr coil = rat::mdl::ModelMesh::create(path_bezier, cross_rect);

	// calculate field on the coil
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
	mesh->set_output_dir("./bezier/");
	mesh->set_output_fname("bezier");

	// run calculation
	mesh->setup(); mesh->write();
}