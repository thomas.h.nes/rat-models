/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// general header files
#include <armadillo>

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathequation.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelarray.hh"
#include "calcmesh.hh"
#include "calcharmonics.hh"
#include "pathaxis.hh"

// ellipse function
rat::mdl::ShPathEquationPr create_ellipse(
	const double radius, const double amplitude, const double delem){
	// elliptic path
	rat::mdl::ShPathEquationPr ellipse = rat::mdl::PathEquation::create(delem, [=](
		arma::Mat<double> &R, arma::Mat<double> &L, 
		arma::Mat<double> &D, const arma::Row<double> &t){

		// convert time to angle
		const arma::Row<double> theta = 2*arma::datum::pi*t;

		// create coordinates (position)
		R.set_size(3,t.n_elem);
		R.row(0) = radius*arma::sin(theta);
		R.row(1) = radius*arma::cos(theta);
		R.row(2) = amplitude*arma::sin(theta);

		// direction vector (velocity)
		L.set_size(3,t.n_elem);
		L.row(0) = radius*arma::cos(theta);
		L.row(1) = -radius*arma::sin(theta);
		L.row(2) = amplitude*arma::cos(theta);

		// normal direction (acceleration)
		arma::Mat<double> N(3,t.n_elem);
		N.row(0) = -radius*arma::sin(theta);
		N.row(1) = -radius*arma::cos(theta);
		N.row(2) = -amplitude*arma::sin(theta);

		// normalize
		L.each_row()/=rat::cmn::Extra::vec_norm(L);
		N.each_row()/=rat::cmn::Extra::vec_norm(N);

		// transverse direction from cross product
		D = rat::cmn::Extra::cross(L,N);
		D.each_row()/=rat::cmn::Extra::vec_norm(D);
	});

	// return path
	return ellipse;
}


// main function
int main(){
	// settings
	const double alpha = 30.0*2.0*arma::datum::pi/360; // [rad]
	const double radius = 20e-3; // [m]
	const double dcable = 0.2e-3; // two tapes [m]
	const double wcable = 4e-3; // tape width [m]
	const arma::uword num_turns = 48; // number of turns
	const double delem = 1e-3; // element size [m]
	const double operating_current = 600; // [A]
	const arma::uword num_coils = 20;
	const double dspacing = 1e-3; // spacing between coils [m]
	const double dcylinder = 2e-3; // radial spacing [m]

	// directory
	const std::string output_dir = "./cctpancake/";
	const std::string output_fname = "cctpancake";

	// switchboard
	const bool run_coil_field = true;
	const bool run_harmonics = true;

	// calculate derived properties
	const double dcoil = dcable*num_turns;
	const double pitch = (wcable + dspacing)/std::sin(alpha);
	const double dradial = dcoil*std::sin(alpha) + wcable*std::cos(alpha) + dcylinder;
	const double ampl_inner = (radius+0.5*dradial)/std::tan(alpha);
	const double ampl_outer = (radius+1.5*dradial)/std::tan(alpha);

	// create ellipses
	rat::mdl::ShPathEquationPr ellipse_inner = create_ellipse(radius,ampl_inner,delem);
	rat::mdl::ShPathEquationPr ellipse_outer = create_ellipse(radius+dcoil+dcylinder,-ampl_outer,delem);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(-dcoil/2, dcoil/2, -wcable/2, wcable/2, delem);

	// create inner coil object
	rat::mdl::ShModelCoilPr coil_inner = rat::mdl::ModelCoil::create(ellipse_inner, rectangle);
	coil_inner->set_number_turns(num_turns); coil_inner->set_operating_current(operating_current);

	// create outer coil object
	rat::mdl::ShModelCoilPr coil_outer = rat::mdl::ModelCoil::create(ellipse_outer, rectangle);
	coil_outer->set_number_turns(num_turns); coil_outer->set_operating_current(operating_current);
	coil_outer->add_reverse();

	// create modelgroup
	rat::mdl::ShModelGroupPr group = rat::mdl::ModelGroup::create();
	group->add_model(coil_inner); group->add_model(coil_outer);

	// create array
	rat::mdl::ShModelArrayPr model = rat::mdl::ModelArray::create(group,1,1,num_coils,0,0,pitch);
	model->add_translation(0,0,-(pitch*num_coils)/2);

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(output_fname);
		mesh->set_num_gauss(2);

		// perform calculation and write output file
		mesh->calculate_and_write(lg);
	}

	// calculate harmonics
	if(run_harmonics){	
		// create axis
		rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','x',pitch*num_coils+2*ampl_outer+0.3,0,0,0,1e-3);

		// create harmonic coil calculation
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(model,pth);
		harm->set_output_dir(output_dir);
		harm->set_output_fname(output_fname);

		// number of points used for harmonic calculation
		harm->set_num_theta(64);

		// radius at which harmonics are defined 
		// (commonly used is (2/3) of aperture)
		harm->set_radius((2.0/3.0)*radius); // [m]

		// create settings
		rat::fmm::ShSettingsPr sttngs = rat::fmm::Settings::create();
		
		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		sttngs->set_num_exp(8);

		// set to harmonics
		harm->set_fmm_settings(sttngs);

		// run calculation
		harm->calculate(lg); harm->write(lg);
	}
}
