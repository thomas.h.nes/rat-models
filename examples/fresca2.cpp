/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Rat-Common
#include "rat/common/serializer.hh"
#include "rat/common/gmshfile.hh"

// header files for Rat-Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "modelgroup.hh"
#include "pathflared.hh"
#include "calcinductance.hh"
#include "matgroup.hh"
#include "matnb3sn.hh"
#include "matcopper.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "calcgroup.hh"

// DESCRIPTION
// FRESCA2 is a large aperture Nb3Sn coil developed 
// at TE-MSC-MDT CERN. This example shows how to produce
// a model of its coils with fx-models. Note that this 
// demonstration model was derived from papers, 
// presentations and an Opera model and is by 
// no means the official version. 

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate magnetic field on the coil
	const bool run_inductance = true; // calculate coil inductance and stored energy
	const bool run_grid = true;
	const bool run_json_export = true; // export geometry to json file
	const bool run_plane = true;

	// data storage directory and filename
	const std::string output_dir = "./fresca2/";
	const std::string fname = "fresca2";

	// coil geometry settings
	const double wcable = 21.8e-3; // width of the cable [m] (incl insulation)
	const double dcable = 0.002334; // thickness of cable [m] (incl insulation)
	const double radius1 = 0.7679; // hardway radius of flared end [m]
	const double radius21 = 0.058; // radius at end of the central coils [m]
	const double radius22 = 0.04468; // radius at end of the outer coils [m]
	const double element_size = 6e-3; // size of the elements [m]
	const double dcoil1 = 0.084024;	// thickness of coil pack for first set of layers [m]
	const double dcoil2 = 0.098028; // thickness of coil pack for second set of layers [m]
	const double ell1 = 2*0.3640615; // straight section length [m]
	const double ell21 = 0.024; // straight section length between bend and coil-end for central coils [m]
	const double ell22 = 0.032; // straight section length between bend and coil-end for outer coils [m]
	const double arcl = 17.0*2*arma::datum::pi/360; // flared end angle [rad]
	const double dspace1 = 0.5e-3; // spacing between the single pancakes [m]
	const double dspace2 = 1.5e-3; // spacing between the double pancakes [m]
	const double operating_current = 10900; // operating current [A]
	const double operating_temperature = 1.9; // operating temperature [K]
	const double num_turns1 = 36; // number of turns in the central coils
	const double num_turns2 = 42; // number of turns in the outer coils

	// cable properties
	const double fcu2sc = 1.25; // ratio copper to superconductor
	const double Rstrand = 1.00e-3/2; // strand radius [m]
	const arma::uword num_strand = 40; // number of strands in the cable
	const double ffill = num_strand*Rstrand*Rstrand*arma::datum::pi/(wcable*dcable);

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// create conductor
	rat::mdl::ShMatGroupPr nb3sn_cable = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNb3SnPr nb3sn = rat::mdl::MatNb3Sn::create();
		nb3sn->set_RRP();
		nb3sn_cable->add_material(ffill/(1.0+fcu2sc),nb3sn);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR150();
		nb3sn_cable->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	}

	// create flared end path
	rat::mdl::ShPathFlaredPr pathflared1 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1,
		radius21,element_size,dcoil1,-wcable);
	rat::mdl::ShPathFlaredPr pathflared2 = rat::mdl::PathFlared::create(
		ell1,ell21,-arcl,radius1-wcable-dspace1,
		radius21,element_size,dcoil1,-2*wcable-dspace1);
	rat::mdl::ShPathFlaredPr pathflared3 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-2*wcable-dspace1-dspace2,
		radius22,element_size,dcoil2,-3*wcable-dspace1-dspace2);
	rat::mdl::ShPathFlaredPr pathflared4 = rat::mdl::PathFlared::create(
		ell1,ell22,-arcl,radius1-3*wcable-2*dspace1-dspace2,
		radius22,element_size,dcoil2,-4*wcable-2*dspace1-dspace2);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect1 = rat::mdl::CrossRectangle::create(0,dcoil1,-wcable,0,element_size);
	rat::mdl::ShCrossRectanglePr cross_rect2 = rat::mdl::CrossRectangle::create(0,dcoil2,-wcable,0,element_size);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(pathflared1, cross_rect1);
	coil1->set_name("l1");
	coil1->add_translation(0,0,0.0144 + wcable/2);
	coil1->set_number_turns(num_turns1);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_material(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(pathflared2, cross_rect1);
	coil2->set_name("l2");
	coil2->add_translation(0,0,0.0367 + wcable/2);
	coil2->set_number_turns(num_turns1);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_material(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil3 = rat::mdl::ModelCoil::create(pathflared3, cross_rect2);
	coil3->set_name("l3");
	coil3->add_translation(0,0,0.06 + wcable/2);
	coil3->set_number_turns(num_turns2);
	coil3->set_operating_current(operating_current);
	coil3->set_operating_temperature(operating_temperature);
	coil3->set_material(nb3sn_cable);
	
	// create coil
	rat::mdl::ShModelCoilPr coil4 = rat::mdl::ModelCoil::create(pathflared4, cross_rect2);
	coil4->set_name("l4");
	coil4->add_translation(0,0,0.0823 + wcable/2);
	coil4->set_number_turns(num_turns2);
	coil4->set_operating_current(operating_current);
	coil4->set_operating_temperature(operating_temperature);
	coil4->set_material(nb3sn_cable);
	
	// magnet upper pole
	rat::mdl::ShModelGroupPr pole1 = rat::mdl::ModelGroup::create();
	pole1->set_name("p1");
	pole1->add_model(coil1); pole1->add_model(coil2);
	pole1->add_model(coil3); pole1->add_model(coil4);

	// magnet lower pole
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create();
	pole2->set_name("p2");
	pole2->add_model(coil1); pole2->add_model(coil2);
	pole2->add_model(coil3); pole2->add_model(coil4);
	pole2->add_rotation(1,0,0,arma::datum::pi);
	pole2->add_reverse();

	// model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->set_name("f2");
	model->add_model(pole1); 
	model->add_model(pole2);


	// CALCULATION AND OUTPUT
	// create a root for storing all the calculations
	rat::mdl::ShCalcGroupPr root = rat::mdl::CalcGroup::create();

	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// add calculation
		root->add_calculation(mesh);
	}

	// calculate inductance
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(model);
		ind->set_type(rat::mdl::COIL);
		ind->set_output_dir(output_dir);
		ind->set_output_fname(fname);
		
		// add calculation
		root->add_calculation(ind);
	}

	// grid calculation
	if(run_grid){
		// create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			-0.2,0.2,100, -0.8,0.8,400, -0.2,0.2,100);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);

		// add calculation
		root->add_calculation(grid);
	}
	
	// calculate grid
	if(run_plane){
		// Create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(0,0,1, -0.8,0.8,1200, -0.2,0.2,300);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname + "plane");
		//grid->set_use_volume_elements(true);
		//grid->set_name("plane");

		// add calculation
		root->add_calculation(grid);
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(root);
		rat::cmn::Extra::create_directory(output_dir);
		slzr->export_json(output_dir + fname + ".json");
	}

	// run model
	root->calculate_and_write(lg);

}



