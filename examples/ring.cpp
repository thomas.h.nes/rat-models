/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelbar.hh"
#include "calcmesh.hh"
#include "calcgrid.hh"
#include "pathcircle.hh"

// main function
int main(){
	// switchboard
	const bool run_grid = true;
	const bool run_mesh = true;
	const bool run_plane = true;

	// output files
	const std::string output_dir = "./ring/";
	const std::string fname = "ring";

	// model geometric input parameters
	const double Rin = 10e-3; // thickness of the bar [m]
	const double Rout = 20e-3;
	const double ell = 10e-3;
	const double delem = 1e-3; // element size [m]
	const arma::uword num_sections = 4;

	// model operating parameters
	const double M = 1.0/arma::datum::mu_0;;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(Rin, num_sections, delem);
	circle->set_offset(Rout-Rin);
	
	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, Rout-Rin, -ell/2, ell/2, delem);

	// create a coil object
	rat::mdl::ShModelBarPr bar = rat::mdl::ModelBar::create(circle, rectangle);
	bar->set_magnetisation(arma::Col<double>::fixed<3>{0,M,0});

	// calculate mesh
	if(run_mesh){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(bar, bar);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->add_output_type(rat::mdl::MAGNETISATION);
		mesh->add_output_type(rat::mdl::MAGNETIC_FIELD);

		// perform calculation and write output file
		mesh->calculate_and_write(lg);
	}

	// calculate grid
	if(run_grid){
		// Create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(-0.05,0.05,150, -0.05,0.05,150, -0.05,0.05,150);
		grid->set_source_model(bar);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);
		grid->add_output_type(rat::mdl::MAGNETISATION);
		grid->add_output_type(rat::mdl::MAGNETIC_FIELD);

		// add to root
		grid->calculate_and_write(lg);
	}

	// calculate grid
	if(run_plane){
		// Create a grid
		rat::mdl::ShCalcGridPr plane = rat::mdl::CalcGrid::create(-0.05,0.05,800, -0.0,0.0,1, -0.05,0.05,800);
		plane->set_source_model(bar);
		plane->set_output_dir(output_dir);
		plane->set_output_fname(fname + "_pln");
		plane->add_output_type(rat::mdl::MAGNETISATION);
		plane->add_output_type(rat::mdl::MAGNETIC_FIELD);

		// add to root
		plane->calculate_and_write(lg);
	}
}
