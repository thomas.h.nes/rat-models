/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"

// header files for distmesh-cpp
#include "rat/dmsh/dfrrectangle.hh"
#include "rat/dmsh/dfdiff.hh"
#include "rat/dmsh/dfrectangle.hh"

// header files for models
#include "crossdmsh.hh"
#include "pathaxis.hh"
#include "calcmesh.hh"
#include "modelmesh.hh"


// DESCRIPTION
// Example showing how the 2D mesher can be used to 
// draw a mesh for an iron yoke. This can later be 
// used as input for a non-linear solver. Which,
// is high on the list of things to be implemented.
// Refer to distmesh-cpp for more information on 
// how to setup the mesh.

// main
int main(){
	// INPUT SETTINGS
	// settings
	const double element_size = 0.01; // requested size of the elements [m]
	const double ell = 0.2; // length of the yoke [m]

	// GEOMETRY SETUP
	rat::dm::ShDistFunPr df = rat::dm::DFDiff::create(
		rat::dm::DFRRectangle::create(-0.17,0.17,-0.1,0.1,element_size),
		rat::dm::DFRRectangle::create(-0.12,0.12,-0.05,0.05,element_size));

	// create cross section with distance function
	rat::mdl::ShCrossDMshPr cdmsh = rat::mdl::CrossDMsh::create(element_size,df,rat::dm::DFOnes::create());

	// create path
	//rat::mdl::ShPathStraightPr pth = rat::mdl::PathStraight::create(ell,element_size);
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('y','z',ell,0,0,0,element_size);

	// create modelcoil
	rat::mdl::ShModelMeshPr model = rat::mdl::ModelMesh::create(pth,cdmsh);

	// create modelmesh
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
	mesh->set_output_dir("./dmsh/");
	mesh->set_output_fname("yoke2");
	mesh->add_output_type(rat::mdl::ORIENTATION);
	
	// setup and write
	mesh->setup(); mesh->write();
}