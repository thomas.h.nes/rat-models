/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for common
#include "rat/common/extra.hh"

// header files for Models
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathconnect.hh"
#include "pathcable.hh"
#include "pathconnect.hh"
#include "modelmesh.hh"
#include "calcmesh.hh"

// main
int main(){
	// parameters
	const double inner_radius = 0.1;
	const double element_size = 2.5e-3;
	const arma::uword num_turns = 10;
	const double dcable = 1.2e-3;
	const double wcable = 12e-3;
	const double dinsu = 0.1e-3;
	const arma::uword num_sections = 4;
	const double operating_current = 4000;

	// create circle
	rat::mdl::ShPathCirclePr path_circle1 = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);

	// create cable
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(path_circle1);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns);
	path_cable1->set_idx_incr(1);
	path_cable1->set_idx_start(0);
	path_cable1->set_reverse_base(false);
	
	// create circle
	rat::mdl::ShPathCirclePr path_circle2 = rat::mdl::PathCircle::create(inner_radius,num_sections,element_size);
	path_circle2->add_rotation(0,-arma::datum::pi/5,0);
	path_circle2->add_translation(0.25,0.1,0.1);

	// create cable
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(path_circle2);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns);
	path_cable2->set_num_add(-2);
	path_cable2->set_idx_incr(1);
	path_cable2->set_idx_start(0);
	path_cable2->set_reverse_base(false);
	path_cable2->add_reverse();
	path_cable2->add_flip();

	// create connector
	rat::mdl::ShPathConnectPr connect = rat::mdl::PathConnect::create(path_cable1,path_cable2,100e-3,100e-3,0,0,0,element_size);

	// create cable cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,-wcable/2,wcable/2,element_size);

	// create coil
	rat::mdl::ShModelCoilPr model_coil = rat::mdl::ModelCoil::create(connect, cross_rect);
	model_coil->set_number_turns(num_turns);
	
	// create a circuit and coolant
	model_coil->set_operating_current(operating_current);

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// calculate field on the coil
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model_coil, model_coil);
	mesh->set_output_dir("./dmsh/");
	mesh->set_output_fname("connector");
	mesh->add_output_type(rat::mdl::ORIENTATION);

	// run calculation
	mesh->calculate(lg); mesh->write(lg);
}