/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
// #include "modelmesh.hh"
#include "modelcoil.hh"
#include "crossline.hh"
#include "vtkunstr.hh"
#include "calcmesh.hh"
#include "extra.hh"
#include "meshcoil.hh"
#include "calcsurface.hh"
#include "calcinductance.hh"
#include "matcopper.hh"
#include "matgroup.hh"
#include "matrebco.hh"

// main function
int main(){
	// switchboard
	const bool run_coil_field = true;
	const bool run_surface_field = true;
	const bool run_inductance = true;

	// output file
	const std::string fname = "ribbon";
	const std::string output_dir = "./ribbon/";

	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double delem = 2e-3; // element size [m]
	const double w = 12e-3;
	const arma::uword num_sections = 4; // number of coil sections
	const double Iop = 100;
	const double thickness = 1e-4;

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	{
		// add ReBCO superconductor
		rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
		rebco->set_fujikura_cern(); // one of the default scaling relations
		rebco_cu->add_material(0.01,rebco); // 1% of the cable is superconductor

		// add copper coatings
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100(); // standard copper with RRR of 100
		rebco_cu->add_material(0.20,copper); // 20 % of the cable is copper (10 mu on each side)
	}

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
		
	// create a rectangular cross section object
	rat::mdl::ShCrossLinePr line = rat::mdl::CrossLine::create(0,-w/2,0,w/2,thickness,delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, line);
	coil->set_number_turns(1); coil->set_operating_current(Iop);
	coil->set_use_volume_elements(true);
	coil->set_material(rebco_cu);

	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
		
		// set output path
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->set_num_gauss(2);

		// set calculation
		mesh->set_output_type(rat::mdl::ALL);
		
		// calculate
		mesh->calculate_and_write(lg);
	}

	// surface field calculation
	if(run_surface_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcSurfacePr surf = rat::mdl::CalcSurface::create(coil, coil);
		
		// set output path
		surf->set_output_dir(output_dir);
		surf->set_output_fname(fname);

		// set calculation
		surf->set_output_type(rat::mdl::ALL);
		
		// calculate
		surf->calculate_and_write(lg);
	}

	// calculate inductance
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(coil);
		ind->set_type(rat::mdl::COIL);
		ind->set_output_dir(output_dir);
		ind->set_output_fname(fname);
		
		// calculate
		ind->calculate_and_write(lg);
	}

}
