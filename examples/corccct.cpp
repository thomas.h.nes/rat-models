/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/serializer.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for Models
#include "crosscircle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "calcline.hh"
#include "calcharmonics.hh"
#include "driveac.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "pathaxis.hh"
#include "transbend.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"
#include "calcinductance.hh"
#include "calcsurface.hh"
#include "background.hh"
#include "calcgroup.hh"
#include "calclength.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = false; // calculate field on surface of coil
	const bool run_grid = false; 
	const bool calc_length = true;

	// data storage directory and filename
	const std::string output_dir = "./corccct/";
	const std::string fname = "corccct";

	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const arma::uword num_layers = 6; // number of layers in the coil 

	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const double bending_radius = 0.0; // coil bending in [m] (0 for no bending)
	const double ell_coil = 0.6; // length of the coil in [m]
	//const double ell_coil = 2*arma::datum::pi*bending_radius/4; // coil length calculated from angle [m]
	const double aperture_radius = 30e-3; // aperture radius in [m]
	const double dradial = 0.3e-3; // spacing between cylinders [m]
	const double dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn
	const double delem = 1e-3;

	// geometry specific per layer
	// const double alpha = 30.0*arma::datum::pi*2.0/360;

	// conductor geometryde
	const double dcable = 4e-3; // cable diameter [m]
	const double delta = 0.5e-3; // spacing between turns

	// scaling
	const double scale_x = 1.0;
	const double scale_y = 1.0/scale_x;

	// current
	const double operating_current = 400e6*arma::datum::pi*(dcable/2)*(dcable/2);
	const double operating_temperature = 4.2;
	
	// background magnetic field
	//arma::Col<double>::fixed<3> Hbg = {0,-1.0/arma::datum::mu_0,0};
	arma::Col<double>::fixed<3> Hbg = {0,0,0};

	// geometry specific per layer
	const arma::Row<double> alpha = arma::Row<double>{20.0,26.0,32.0}*2*arma::datum::pi/360; // skew angle per two layers [rad]
	const arma::Row<arma::uword> is_horizontal = {0,0,0}; // specifies whether the coil generates horizontal field


	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();



	// GEOMETRY SETUP
	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossCirclePr cross_circle = rat::mdl::CrossCircle::create(dcable/2,delem);

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, bending_radius);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	
	// walk over layers
	for(arma::uword i=0;i<num_layers;i++){
		// calculate radius
		const double coil_radius = aperture_radius + (i+1)*dformer + i*(dcable + dradial);
		const double inner_radius = aperture_radius + dformer + (i/2)*2*(dformer + dcable + dradial);

		// inner radius parameters
		const double omega = (delta + dcable)/std::sin(alpha(i/2));
		const double aest = inner_radius/(num_poles*std::tan(alpha(i/2))); // skew amplitude
		const double num_turns = 2*std::floor((ell_coil-2*aest)/(2*omega));

		// account for integer number of turns
		double a = num_poles*(ell_coil - num_turns*omega)/2;
		if(i%2==1)a+=omega/(num_poles*4); else a+=omega*(num_poles*4-1)/(num_poles*4);


		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(
			num_poles,coil_radius,a,omega,num_turns,num_nodes_per_turn);
		
		// slanting direction
		if(i%2==0)path_cct->set_is_reverse(is_reverse); // reverse slanting
		else path_cct->set_is_reverse(!is_reverse); // reverse slanting
			
		// set scaling
		path_cct->set_scale_x(scale_x);
		path_cct->set_scale_y(scale_y);

		// add bending
		path_cct->add_transformation(bending);

		// horizontal
		if(is_horizontal(i/2))path_cct->add_rotation(0,0,1,arma::datum::pi/2);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_circle);
		coil->set_name("layer" + std::to_string(i)); 
		coil->set_number_turns(1);
		coil->set_operating_current(operating_current);
		coil->set_operating_temperature(operating_temperature);
		
		// add coil to model
		model->add_model(coil);
	}


	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// add to root
		mesh->calculate_and_write(lg);
	}

	// grid calculation
	if(run_grid){
		// create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			-0.1,0.1,100, -0.1,0.1,100, -0.4,0.4,800);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);

		// add to root
		grid->calculate_and_write(lg);
	}

	// simple line calculation
	if(calc_length){
		// create harmonic coil calculation
		rat::mdl::ShCalcLengthPr length = rat::mdl::CalcLength::create(model);
		
		// set output path
		length->set_output_dir(output_dir);
		length->set_output_fname(fname);

		// add to root
		length->calculate_and_write(lg);
	}	

}