/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <armadillo>
#include <algorithm>

#include "rat/common/defines.hh"
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"
#include "rat/common/serializer.hh"

#include "factorylist.hh"
#include "model.hh"

// get input for the option
inline char* getCmdOption(char ** begin, char ** end, const std::string & option){
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)return *itr;
	return 0;
}

// function for checking whether a certain flag was provided
inline bool cmdOptionExists(char** begin, char** end, const std::string& option){
	return std::find(begin, end, option) != end;
}

// main
int main(int argc, char * argv[]){
	// get input file
	char * ifname = getCmdOption(argv, argv+argc, "-f");
	
	// create serialization object
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
	
	// register all required constructors
	rat::mdl::FactoryList::register_constructors(slzr);

	// export
	slzr->import_json(ifname); 
	rat::mdl::ShCalcGroupPr root = slzr->construct_tree<rat::mdl::CalcGroup>(); 

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// run calculation
	root->calculate_and_write(lg);

	// done
	return 0;
}