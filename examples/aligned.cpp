/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for Common
#include "rat/common/log.hh"
#include "rat/common/gmshfile.hh"

// header files for models
#include "pathprofile.hh"
#include "pathstraight.hh"
#include "patharc.hh"
#include "pathgroup.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "modelgroup.hh"
#include "pathcable.hh"
#include "pathconnect.hh"
#include "material.hh"
#include "matcopper.hh"
#include "matgroup.hh"
#include "matrebco.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"

// DESCRIPTION
// This is an example on how to create an alligned block coil 
// similar to the to Feather-M2 (CERN) coil. It uses the PathProfile
// class to merge a top view and a side view of the magnet into
// one single path.

// main
int main(){
	// SETTINGS
	// calculation settings
	const bool run_grid = true;
	const bool run_coil_field = true; // calculate field on coil

	// data storage directory and filename
	const std::string output_dir = "./aligned/";
	const std::string fname = "aligned";

	// general coil geometry
	const double element_size = 2e-3; // size of the elements [m]
	const arma::uword num_turns1 = 9; // number of turns on central deck
	const arma::uword num_turns2 = 5; // number of turns on wing deck
	const double ashear1 = 0.5*2*arma::datum::pi/360; // central deck shear angle [rad]
	const double ashear2 = 7.0*2*arma::datum::pi/360; // wing deck shear angle [rad]
	const double dspace = 1e-3; // spacing between coils [m]
	const double dmid = 2e-3; // spacing between coils on midplane [m]

	// side profile settings
	const double ellstr1 = 0.1; // straight section length [m]
	const double ellstr2 = 0.5; // length of coil-end profile [m] (not magnet length)
	const double aend = 4.0*2*arma::datum::pi/360; // max slope angle at coil end [rad]
	const double Rhard = 2.0; // hardway bending radius [m]

	// top profile settings
	const double bend = 2.0*2*arma::datum::pi/360; // max diamond shape cangle at coil end [rad]
	const double Rsoft = 0.016; // softway bending radius [m]
	const double Rmid = 0.2; // midway bending radius [m]
	const double ell1 = 0.2; // length of straight bit [m]
	const double ell2 = 0.25; // length of end [m]

	// cable settings
	const double wcable = 12e-3; // width of cable [m]
	const double dcable = 1.2e-3; // thickness of cable [m]
	const double dinsu = 0.1e-3; // thickness of insulation [m]

	// conductor settings
	const double wtape = 5.9e-3; // width of Roebel punched tapes in cable [m]
	const double dtape = 0.15e-3; // thickness of each tape [m]
	const double dsc = 1e-6; // superconductor thickness [m]
	const double dcu = 40e-6; // copper thickness [m]
	const arma::uword num_tapes = 13; // number of tapes in cable
	
	// operating conditions
	const double Iop = 12000; // operating current [A]
	const double Top = 4.5;	// operating temperature [K]

	// background field in z
	const arma::Col<double>::fixed<3> Hbg = {0,0,13.0/arma::datum::mu_0};



	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// calculate remaining properties
	// conductor filling fraction
	const double ffill = wtape*dtape*num_tapes/(dcable*wcable);

	// calculate width of coil
	const double w1 = Rsoft*std::sin(arma::datum::pi/2 - bend) + 
		std::sin(bend)*ell2 + (1.0-std::cos(bend))*Rmid;

	// calculate end radius of second coil
	const double Rsoft2 = w1 - (1.0-std::cos(bend))*Rmid + std::sin(ashear1)*wcable;

	// create side profile
	rat::mdl::ShPathGroupPr side = rat::mdl::PathGroup::create();
	side->set_align_frame(false);

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	{
		// add ReBCO superconductor
		rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
		rebco->set_fujikura_cern(); // one of the default scaling relations
		rebco_cu->add_material(ffill*(dsc/dtape),rebco);

		// add copper coatings
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100(); // standard copper with RRR of 100
		rebco_cu->add_material(ffill*(dcu/dtape),copper);
	}

	// add sections
	{
		// start point
		const arma::Col<double>::fixed<3> R0 = {0,0,0};
		const arma::Col<double>::fixed<3> L0 = {0,1,0};
		const arma::Col<double>::fixed<3> N0 = {1,0,0};
		const arma::Col<double>::fixed<3> D0 = {0,0,1};

		// side yz-profile	
		rat::mdl::ShPathGroupPr sideA = rat::mdl::PathGroup::create(R0,L0,N0,D0);

		// straight bit
		sideA->add_path(rat::mdl::PathStraight::create(ellstr1,element_size));
		
		// upward arc
		rat::mdl::ShPathArcPr arc = rat::mdl::PathArc::create(Rhard,-aend,element_size);
		arc->set_transverse(true); // enable hardway bending
		sideA->add_path(arc);

		// straight 
		sideA->add_path(rat::mdl::PathStraight::create(ellstr2,element_size));
		
		// create mirror for other side y<0
		rat::mdl::ShPathGroupPr sideB = rat::mdl::PathGroup::create();
		sideB->add_path(sideA);
		sideB->add_rotation(0,0,arma::datum::pi);
		
		// reverse this side
		sideB->add_reverse();
		sideB->add_flip();

		// combine sides
		side->add_path(sideB);
		side->add_path(sideA);
	}

	// create top profile of first coil
	rat::mdl::ShPathGroupPr top1 = rat::mdl::PathGroup::create();
	top1->add_translation(w1,0,0);

	// add sections to top profile
	// this is the diamond shape of the coil
	{
		// create unique sections
		rat::mdl::ShPathStraightPr str1 = rat::mdl::PathStraight::create(ell1/2,element_size);
		rat::mdl::ShPathArcPr arc1 = rat::mdl::PathArc::create(Rmid, bend, element_size);
		rat::mdl::ShPathStraightPr str2 = rat::mdl::PathStraight::create(ell2,element_size);
		rat::mdl::ShPathArcPr arc2 = rat::mdl::PathArc::create(Rsoft, arma::datum::pi/2 - 
			bend, element_size,num_turns1*(dcable+2*dinsu));

		// add sections to path
		top1->add_path(str1); top1->add_path(arc1); top1->add_path(str2); top1->add_path(arc2);
		top1->add_path(arc2); top1->add_path(str2); top1->add_path(arc1); top1->add_path(str1);
		top1->add_path(str1); top1->add_path(arc1); top1->add_path(str2); top1->add_path(arc2);
		top1->add_path(arc2); top1->add_path(str2); top1->add_path(arc1); top1->add_path(str1);
	}
	
	// create top profile of second coil
	rat::mdl::ShPathGroupPr top2 = rat::mdl::PathGroup::create();
	top2->add_translation(Rsoft2,0,0);

	// add sections to top profile
	{
		// create unique sections
		rat::mdl::ShPathStraightPr str1 = rat::mdl::PathStraight::create(ell1/2,element_size);
		rat::mdl::ShPathArcPr arc1 = rat::mdl::PathArc::create(Rmid, bend, element_size);
		rat::mdl::ShPathArcPr arc2 = rat::mdl::PathArc::create(Rsoft2, arma::datum::pi/2 - bend, 
			element_size,num_turns2*(dcable+2*dinsu));

		// add sections to path
		top2->add_path(str1); top2->add_path(arc1); top2->add_path(arc2);
		top2->add_path(arc2); top2->add_path(arc1); top2->add_path(str1);
		top2->add_path(str1); top2->add_path(arc1); top2->add_path(arc2);
		top2->add_path(arc2); top2->add_path(arc1); top2->add_path(str1);
	}

	// create coil path by combining top and side profiles
	rat::mdl::ShPathProfilePr profile1 = rat::mdl::PathProfile::create(top1,side);
	profile1->set_shearing(ashear1,0.7,ell1/2 + Rmid*std::cos(bend) + ell2);
	profile1->add_translation(0,0,dmid/2);

	// create coil path by combining top and side profiles
	rat::mdl::ShPathProfilePr profile2 = rat::mdl::PathProfile::create(top2,side);
	profile2->set_shearing(ashear2,0.3,ell1/2 + Rsoft2 + 0.012);
	profile2->add_translation(0,0,dmid/2 + wcable + dspace);

	// create cable for central deck
	rat::mdl::ShPathCablePr path_cable1 = rat::mdl::PathCable::create(profile1);
	path_cable1->set_turn_step(dcable + 2*dinsu);
	path_cable1->set_num_turns(num_turns1);
	path_cable1->set_idx_incr(2);
	path_cable1->set_idx_start(1);
	path_cable1->set_num_add(2);
	path_cable1->set_reverse_base(true);

	// create cable for wing deck
	rat::mdl::ShPathCablePr path_cable2 = rat::mdl::PathCable::create(profile2);
	path_cable2->set_turn_step(dcable + 2*dinsu);
	path_cable2->set_num_turns(num_turns2);
	path_cable2->set_idx_incr(3);
	path_cable2->set_idx_start(1);
	path_cable2->set_num_add(7);

	// connect paths with a layer jump
	rat::mdl::ShPathConnectPr pole_path = rat::mdl::PathConnect::create(
		path_cable1,path_cable2,70e-3,70e-3,0,0,10e-3,element_size);

	// create cable cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,0,wcable,dcable+2*dinsu);

	// create coil
	rat::mdl::ShModelCoilPr pole1 = rat::mdl::ModelCoil::create(pole_path, cross_rect, rebco_cu);
	pole1->set_operating_temperature(Top);
	pole1->set_operating_current(Iop);

	// create second magnet pole
	rat::mdl::ShModelGroupPr pole2 = rat::mdl::ModelGroup::create();
	pole2->add_model(pole1);
	pole2->add_rotation(0,1,0,arma::datum::pi);
	pole2->add_reverse();
	pole2->add_flip();

	// create coil model
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(pole1); model->add_model(pole2);

	// create background field
	rat::mdl::ShBackgroundPr bg = rat::mdl::Background::create();
	bg->set_magnetic(Hbg);


	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->add_background(bg);

		// run calculation
		mesh->calculate_and_write(lg);
	}

	// grid calculation
	if(run_grid){
		// create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			-0.1,0.1,50, -0.5,0.5,250, -0.1,0.1,50);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);
		grid->add_background(bg);

		// run calculation and write output files
		grid->calculate_and_write(lg);
	}

}