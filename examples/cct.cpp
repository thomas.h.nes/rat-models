/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files from Common
#include "rat/common/serializer.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for Models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcct.hh"
#include "modelgroup.hh"
#include "calcline.hh"
#include "calcharmonics.hh"
#include "driveac.hh"
#include "emitterbeam.hh"
#include "crosscircle.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "pathaxis.hh"
#include "transbend.hh"
#include "calcgrid.hh"
#include "calcmesh.hh"
#include "calcinductance.hh"
#include "calcsurface.hh"
#include "background.hh"
#include "calcgroup.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_line = false;
	const bool run_harmonics = false; // calculate harmonics along aperture
	const bool run_grid = false; // calculate field on a volume grid
	const bool run_freecad = false; // export model to freecad
	const bool run_inductance = false; // export model to freecad
	const bool run_opera = false; // export model to opera
	const bool run_json_export = true; // export model to json file
	const bool run_surface_field = false; // calculate field on surface of coil

	// data storage directory and filename
	const std::string output_dir = "./cct/";
	const std::string fname = "cct";

	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const arma::uword num_layers = 4; // number of layers in the coil 

	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const double bending_radius = 0.0; // ooil bending in [m] (0 for no bending)
	const double ell_coil = 0.4; // length of the coil in [m]
	//const double ell_coil = 2*arma::datum::pi*bending_radius/4; // coil length calculated from angle [m]
	const double aperture_radius = 30e-3; // aperture radius in [m]
	const double dradial = 0.3e-3; // spacing between cylinders [m]
	const double dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn

	// geometry specific per layer
	const arma::Row<double> alpha = arma::Row<double>{20.0,26.0}*2*arma::datum::pi/360; // skew angle per two layers [rad]
	const arma::Row<arma::uword> is_horizontal = {0,1}; // specifies whether the coil generates horizontal field

	// conductor geometryde
	const arma::uword nd = 2; // number of wires in width of slot
	const arma::uword nw = 4; // number of wires in depth of slot
	const double dstr = 0.825e-3; // thickness of bare wire [m]
	const double ddstr = 1.0e-3; // space allocated for insulated wire [m]
	const double fcu2sc = 1.9; // copper to superconductor fraction
	const double dcable = nd*ddstr; // thickness of cable [m] (width of slot)
	const double wcable = nw*ddstr; // width of cable [m] (depth of slot)
	const double delta = 0.5e-3; // spacing between turns

	// scaling
	const double scale_x = 1.0;
	const double scale_y = 1.0/scale_x;

	// operational settings per layer pair
	const arma::Row<arma::uword> drive_idx = {1,2}; // circuit index
	const arma::Row<double> operating_current = {420,420}; // operating current for each circuit
	const arma::Row<double> operating_temperature = {4.5,4.5};

	// background magnetic field
	//arma::Col<double>::fixed<3> Hbg = {0,-1.0/arma::datum::mu_0,0};
	arma::Col<double>::fixed<3> Hbg = {0,0,0};

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();


	// GEOMETRY SETUP
	// filling fraction of conductors in the slot
	const double ffill = nd*nw*arma::datum::pi*(dstr/2)*(dstr/2)/(dcable*wcable);

	// create conductor
	rat::mdl::ShMatGroupPr slot_conductor = rat::mdl::MatGroup::create();
	{
		// add nbti superconductor
		rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
		nbti->set_nbti_LHC();
		slot_conductor->add_material(ffill/(1.0+fcu2sc),nbti);

		// add copper matrix
		rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
		copper->set_copper_OFHC_RRR100();
		slot_conductor->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	}

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,nd,nw);

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, bending_radius);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	
	// walk over layers
	for(arma::uword i=0;i<num_layers;i++){
		// calculate radius
		const double coil_radius = aperture_radius + (i+1)*dformer + i*(wcable + dradial);
		const double inner_radius = aperture_radius + dformer + (i/2)*2*(dformer + wcable + dradial);

		// inner radius parameters
		const double omega = (delta + dcable)/std::sin(alpha(i/2));
		const double aest = inner_radius/(num_poles*std::tan(alpha(i/2))); // skew amplitude
		const double num_turns = 2*std::floor((ell_coil-2*aest)/(2*omega));

		// account for integer number of turns
		double a = num_poles*(ell_coil - num_turns*omega)/2;
		if(i%2==1)a+=omega/(num_poles*4); else a+=omega*(num_poles*4-1)/(num_poles*4);

		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(
			num_poles,coil_radius,a,omega,num_turns,num_nodes_per_turn);
		
		// slanting direction
		if(i%2==0)path_cct->set_is_reverse(is_reverse); // reverse slanting
		else path_cct->set_is_reverse(!is_reverse); // reverse slanting
			
		// set scaling
		path_cct->set_scale_x(scale_x);
		path_cct->set_scale_y(scale_y);

		// horizontal
		if(is_horizontal(i/2))path_cct->add_rotation(0,0,1,arma::datum::pi/2);

		// add bending
		path_cct->add_transformation(bending);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(
			path_cct, cross_rect, slot_conductor);
		coil->set_name("layer" + std::to_string(i)); 
		coil->set_number_turns(nd*nw);
		coil->set_operating_current(operating_current(i/2));
		coil->set_operating_temperature(operating_temperature(i/2));
		coil->set_drive_id(drive_idx(i/2));

		// add coil to model
		model->add_model(coil);
	}

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',ell_coil+0.4,0,0,0,2e-3);
	pth->add_transformation(bending);


	// CALCULATION AND OUTPUT
	// general background field
	// rat::fmm::ShBackgroundPr bg = rat::fmm::Background::create();
	// bg->set_magnetic(Hbg);

	// create drives
	rat::mdl::ShDrivePr drive1 = rat::mdl::DriveAC::create(1.0, 0.1, 0, 1);
	rat::mdl::ShDrivePr drive2 = rat::mdl::DriveAC::create(1.0, 0.1, arma::datum::pi/2, 2);

	// background fields
	rat::mdl::ShBackgroundPr bg1 = rat::mdl::Background::create(1,arma::Col<double>::fixed<3>{0,-1.0/arma::datum::mu_0,0});
	rat::mdl::ShBackgroundPr bg2 = rat::mdl::Background::create(2,arma::Col<double>::fixed<3>{1.0/arma::datum::mu_0,0,0});

	// create model settings
	const arma::uword num_times = 120; const double t1 = 0; const double t2 = 10;
	const arma::Row<double> times = arma::linspace<arma::Row<double> >(t1,t2*((num_times-1.0)/num_times),num_times);

	// create a root for storing all the calculations
	rat::mdl::ShCalcGroupPr root = rat::mdl::CalcGroup::create();

	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(model, model);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->set_output_times(times);

		// drive scaling
		mesh->add_drive(drive1); mesh->add_drive(drive2);
		mesh->add_background(bg1); mesh->add_background(bg2);

		// add to root
		root->add_calculation(mesh);
	}

	// calculate field on the coil
	if(run_surface_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcSurfacePr surf = rat::mdl::CalcSurface::create(model, model);
		surf->set_output_dir(output_dir);
		surf->set_output_fname(fname);

		// drive scaling
		surf->add_drive(drive1); surf->add_drive(drive2);

		// add to root
		root->add_calculation(surf);
	}

	// calculate field on line
	if(run_line){
		// calculate field
		rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create(model, pth);
		line->set_output_dir(output_dir);
		line->set_output_fname(fname);

		// drive scaling
		line->add_drive(drive1); line->add_drive(drive2);

		// add to root
		root->add_calculation(line);
	}

	// harmonics calculation
	if(run_harmonics){
		// create harmonic calculator
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(model, pth);
		harm->set_output_dir(output_dir);
		harm->set_output_fname(fname);

		// settings related to harmonics calculation
		harm->set_compensate_curvature(true);
		harm->set_num_theta(64);
		harm->set_radius(aperture_radius*2.0/3.0);

		// drive scaling
		harm->add_drive(drive1); harm->add_drive(drive2);

		// add to root
		root->add_calculation(harm);
	}

	// grid calculation
	if(run_grid){
		// create a grid
		rat::mdl::ShCalcGridPr grid = rat::mdl::CalcGrid::create(
			-0.1,0.1,100, -0.1,0.1,100, -0.3,0.3,600);
		grid->set_source_model(model);
		grid->set_output_dir(output_dir);
		grid->set_output_fname(fname);

		// drive scaling
		grid->add_drive(drive1); grid->add_drive(drive2);

		// add to root
		root->add_calculation(grid);
	}

	// calculate inductance
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShCalcInductancePr ind = rat::mdl::CalcInductance::create(model);
		
		// add to root
		root->add_calculation(ind);
	}

	// write a freecad macro
	if(run_freecad){
		model->export_freecad(rat::cmn::FreeCAD::create("cct.FCMacro","cct"));
	}

	// export to an opera conductor file
	if(run_opera){
		model->export_opera(rat::cmn::Opera::create("cct.cond"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create(model);
		rat::cmn::Extra::create_directory(output_dir);
		slzr->export_json(output_dir + fname + ".json");
	}

	// calculate all
	root->calculate_and_write(lg); 
}