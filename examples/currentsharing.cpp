/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>

// header files for FX-Models
#include "material.hh"
#include "matgroup.hh"
#include "matnbti.hh"
#include "matcopper.hh"
#include "matrebco.hh"
#include "matstainless316.hh"

// main
int main(){
	// settings
	const double tol_bisection = 1e-7;

	// create conductor object that contains both superconducting 
	// and normal conducting parts of the REBCO tape
	rat::mdl::ShMatGroupPr rebco_cu = rat::mdl::MatGroup::create();
	rebco_cu->set_tolerance(tol_bisection);

	// add ReBCO superconductor
	rat::mdl::ShMatReBCOPr rebco = rat::mdl::MatReBCO::create();
	rebco->set_fujikura_cern(); // one of the default scaling relations
	rebco_cu->add_material(0.01,rebco); // 1% of the cable is superconductor

	// add copper coatings
	rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
	copper->set_RRR(20); // standard copper with RRR of 100
	rebco_cu->add_material(0.20,copper); // 20 % of the cable is copper (10 mu on each side)

	// add copper coatings
	rat::mdl::ShMatStainless316Pr stst = rat::mdl::MatStainless316::create();
	rebco_cu->add_material(0.70,stst); // 70 % hastelloy layer

	// current sharing time
	// arma::Row<double> Bm = arma::Row<double>(20,arma::fill::ones)*8;
	// arma::Row<double> T = arma::Row<double>(20,arma::fill::ones)*1.9;
	// arma::Row<double> alpha = arma::Row<double>(20,arma::fill::zeros);

	// // chose critical current density of NbTi
	// arma::Row<double> Jop = arma::linspace<arma::Row<double> >(0,2e9,20);

	// std::cout<<Jop<<std::endl;

	// // calculate electric field
	// arma::Row<double> E = rebco_cu->calc_electric_field(Jop,Bm,T,alpha);
	
	// std::cout<<Jop<<std::endl;

	// // output
	// std::cout<<E<<std::endl;

	// write to VTK table
	rat::mdl::ShVTKTablePr vtkmat = rebco_cu->export_vtk(4,500,100e6);
	vtkmat->write("materialdata");
}	