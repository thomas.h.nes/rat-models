/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"
#include "patharc.hh"
#include "pathstraight.hh"
#include "pathgroup.hh"
#include "pathcable.hh"

// main function
int main(){
	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double delem = 2e-3; // element size [m]
	const double ell = 100e-3;
	const double wcable = 12e-3; // width of the cable [m]
	const double dcable = 1.2e-3;
	const double dinsu = 0.1e-3;

	// data storage directory and filename
	const std::string output_dir = "./racetrack/";
	const std::string fname = "racetrack";


	// model operating parameters
	const double operating_current = 200; // operating current in [A]
	const arma::uword num_turns = 10; // number of turns

	// winding parameters
	// these are only used when the coil is modelled 
	// as a cable (i.e. is_block==false);
	const arma::uword idx_incr = 1; // location at which the turn is incremented to the next
	const arma::uword idx_start = 2; // section in which the cable starts 
	const arma::sword num_add = 2; // number of extra sections to add/remove at end


	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create an arc
	rat::mdl::ShPathArcPr path_arc = rat::mdl::PathArc::create(radius,arma::datum::pi/2,delem,(dcable+2*dinsu)*num_turns);

	// create straight section
	rat::mdl::ShPathStraightPr path_straight = rat::mdl::PathStraight::create(ell/2, delem);

	// add sections to a pathgroup
	rat::mdl::ShPathGroupPr racetrack = rat::mdl::PathGroup::create();
	racetrack->add_path(path_straight);	racetrack->add_path(path_arc);	
	racetrack->add_path(path_arc); racetrack->add_path(path_straight);
	racetrack->add_path(path_straight);	racetrack->add_path(path_arc);	
	racetrack->add_path(path_arc); racetrack->add_path(path_straight);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcable, 0, wcable, delem);

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(racetrack);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(idx_incr);
	path_cable->set_idx_start(idx_start);
	path_cable->set_num_add(num_add);
	path_cable->set_offset(dinsu);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);
	
	// create a source representation for the coil and set them up
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
	mesh->set_output_dir(output_dir);
	mesh->set_output_fname(fname);
	mesh->set_num_gauss(3);
	
	// perform calculation and write output file
	mesh->calculate_and_write(lg);
}