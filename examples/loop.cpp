/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// DESCRIPTION
// Minimalist example of a solenoid model

// header files for common
#include "rat/common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crosspoint.hh"
#include "vtkunstr.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"
#include "calcsurface.hh"
#include "meshcoil.hh"
#include "vtkunstr.hh"

// main function
int main(){
	// switchboard
	const bool run_coil_field = true;
	const bool run_surface_field = true;

	// output file
	const std::string fname = "loop";
	const std::string output_dir = "./loop/";

	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double delem = 2e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections
	const double thickness = 1e-3;
	const double width = 1e-3;
	const double Iop = 100;

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
		
	// create a rectangular cross section object
	rat::mdl::ShCrossPointPr point = rat::mdl::CrossPoint::create(0,0,thickness,width);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, point);
	coil->set_number_turns(1); coil->set_operating_current(Iop);

	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
		
		// set output path
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);
		mesh->set_num_gauss(2);

		// set calculation
		mesh->set_output_type(rat::mdl::ALL);
		
		// add to root
		mesh->calculate_and_write(lg);
	}

	// surface field calculation
	if(run_surface_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcSurfacePr surf = rat::mdl::CalcSurface::create(coil, coil);
		
		// set output path
		surf->set_output_dir(output_dir);
		surf->set_output_fname(fname);

		// set calculation
		surf->set_output_type(rat::mdl::ALL);
		
		// add to root
		surf->calculate_and_write(lg);
	}
}
