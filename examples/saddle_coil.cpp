/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"
#include "rat/common/serializer.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// header files for models
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "pathcable.hh"
#include "pathrectangle.hh"
#include "transbend.hh"
#include "edges.hh"
#include "calcmesh.hh"

// DESCRIPTION
// This example shows how to create a simple saddle coil

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on surface of coil
	const bool run_freecad = false; // export model to freecad
	const bool run_json_export = true; // export geometry to json file

	// data storage directory and filename
	const std::string output_dir = "./saddle/";
	const std::string fname = "saddle";

	// coil geometry
	const arma::uword num_turns = 24;
	const double width = 0.25; // width of the coil [m] (before bending)
	const double height = 0.4; // height of the coil [m] (before bending)
	const double radius = 0.1; // cylinder radius on which the coil is bend [m]
	const double element_size = 4e-3; // size of the elements [m] 
	
	// cable settings
	const double wcable = 12e-3; // width of the cable [m]
	const double dcable = 1.2e-3; // thickness of the cable [m]
	const double dinsu = 0.1e-3; // thickness of the insulation around each cable [m]
	
	// operating conditions
	const double operating_current = 12e3; // current in coil [A]
	const double operating_temperature = 20; // temperature at coil [K]

	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	
	// GEOMETRY SETUP
	// create unified path
	rat::mdl::ShPathRectanglePr path_rect = rat::mdl::PathRectangle::create(width,height,radius,element_size);

	// add bending
	path_rect->add_transformation(rat::mdl::TransBend::create(0,1,0, 1,0,0, 0.15));

	// create cable
	rat::mdl::ShPathCablePr path_cable = rat::mdl::PathCable::create(path_rect);
	path_cable->set_turn_step(dcable + 2*dinsu);
	path_cable->set_num_turns(num_turns);
	path_cable->set_idx_incr(1);
	path_cable->set_idx_start(0);
	
	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(0,dcable,-wcable/2,wcable/2,1.2e-3);

	// create coil
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cable, cross_rect);
	coil->set_operating_current(operating_current);
	coil->set_operating_temperature(operating_temperature);


	// CALCULATION AND OUTPUT
	// calculate field on the coil
	if(run_coil_field){
		// create a source representation for the coil and set them up
		rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
		mesh->set_output_dir(output_dir);
		mesh->set_output_fname(fname);

		// run calculation
		mesh->calculate(lg); mesh->write(lg);
	}

	// write a freecad macro
	if(run_freecad){
		coil->export_freecad(rat::cmn::FreeCAD::create("saddle.FCMacro","saddle"));
	}

	// export json
	if(run_json_export){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
		slzr->flatten_tree(coil); slzr->export_json("clover.json");
	}
}