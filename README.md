![Logo](./figs/RATLogo.png)
# Coil Design Library
<em>Hellooo Rat is very pleased with coil modeler. Many possibilities yes?</em>

## Introduction
Setting up coil geometries for performing field calculations, optimization, quench analysis and ultimately coil construction, can prove to be a challenging task. This library aims to simplify the setup of coil geometries and provides basic classes for calculating their magnetic, vector potential at any point in space. In addition specialized calculation classes are available, allowing to calculate, for instance, the magnetic field on the surface (for peak field calculation) and coil harmonics. The generated meshes and field-maps can be exported and used in for instance quench codes. The ultimate aim is that this code holds the reference geometry of any magnet. This to avoid slight variations in the magnet geometry across different calculation tools, as is now often the case. 

The main concept of the code is by generating the cables and coil blocks by extruding their cross section along a line path. This is done through a coordinate transformation that takes into account the orientation (pitch) of the path. The path consists of one or multiple sections defined by a simple geometric objects, such as a straight line or an arc. 

## Installation
As this library is part of a collection of inter-dependent libraries a detailed description of the installation process is provided in a separate repository located here: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation). In addition it is required to set the ENABLE_CUDA flag in the cmakelists.txt file. It decides whether the code is build using NVidia Cuda or not. A quick build and installation is achieved by using

```
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```

where X is the number of cores you wish to use for the build and T is the build type. When the library is build run unit tests and install using

```
make test
sudo make install
```

## Modeling a Solenoid
This section demonstrates how to do the most basic thing with the rat-models library: creating a solenoid and calculating the magnetic field inside the conductor. The idea here is to demonstrate how the library works and what it can accomplish. This is shown from a code point of view. Later on to run the code one will need to be able to compile it and link the required libraries. In Rat any coil is modeled using its cross section in combination with a path, a space-curve with corresponding [moving frame](https://en.wikipedia.org/wiki/Moving_frame). The coil is then converted into a mesh, which is used both as sources and as targets for the MLFMM. Eventually the mesh and corresponding magnetic field is written to a VTK output file, to be analyzed with Paraview. The code reads as follows and is added to the rat-models example files under the name solenoid.cpp for convenience:
```cpp
// header files for common
#include "common/log.hh"

// header files for model
#include "pathcircle.hh"
#include "crossrectangle.hh"
#include "modelcoil.hh"
#include "calcmesh.hh"

// main function
int main(){
	// model geometric input parameters
	const double radius = 40e-3; // coil inner radius [m]
	const double dcoil = 10e-3; // thickness of the coil [m]
	const double wcable = 12e-3; // width of the cable [m]
	const double delem = 2e-3; // element size [m]
	const arma::uword num_sections = 4; // number of coil sections

	// model operating parameters
	const double operating_current = 200; // operating current in [A]
	const arma::uword num_turns = 100; // number of turns

	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

	// create a circular path object
	rat::mdl::ShPathCirclePr circle = rat::mdl::PathCircle::create(radius, num_sections, delem);
	circle->set_offset(radius);

	// create a rectangular cross section object
	rat::mdl::ShCrossRectanglePr rectangle = rat::mdl::CrossRectangle::create(0, dcoil, 0, wcable, delem);

	// create a coil object
	rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(circle, rectangle);
	coil->set_number_turns(num_turns);
	coil->set_operating_current(operating_current);
	coil->set_num_gauss(3);
	
	// create a source representation for the coil and set them up
	rat::mdl::ShCalcMeshPr mesh = rat::mdl::CalcMesh::create(coil, coil);
	mesh->line->set_output_fname("solenoid");

	// perform calculation and write output file
	mesh->calculate_and_write(lg);
}
```

The resulting VTK file solenoid_mesh.vtu visualized in Paraview, after slicing it in half, looks like this:

![Solenoid](./figs/solenoid.png)

## Modeling Advanced Geometry
Obviously one doesn't need Rat for calculating solenoids, this can be done more effectively in other codes (such as Soleno). It becomes much more interesting when complicated coil geometries are modeled. In the examples folder you find many examples demonstrating the capabilities of this code. Until a more detailed tutorial becomes available the user is referred here.

## Gallery
The output of the [rat-models](https://gitlab.com/Project-Rat/rat-models) library can be in VTK XML format and can be visualized using Kitware [VTK](https://vtk.org/) and [Paraview](https://www.paraview.org/), which are completely free and open-source. Additionally, they are available on all platforms and even offer a web-interface (need further investigation). These are excellent software packages allow for creating stunning pictures.

For instance the clover_rt.cpp example in rat-models can be visualized as:
![clover_rt](./figs/clover_rt2.png)

and the toroid.cpp example in rat-models as:
![mini_toroid](./figs/mini_toroid4.png)

and the cct.cpp example in rat-models as:
![cct_dipole](./figs/cct_dipole.png)

radially magnetized ring:
![ring](./figs/ring_radial.png)


## ToDo List
* Reconsider data containers, in some cases better to use std::list, std::set, std::map etc instead of arma::field.
* Convert some of the enum's to enum class to prevent polution of the namespace.
* Cosine theta coils and their ends.
* Improve FreeCAD interface to allow for non-rectangular cables/coil packs. Also deal better with longer sections.
* Improving the bezier spline to deal with imperfect coil windings. Bezier spline direct control point input?
* LUA scripting interface such that users can run the code without recompiling every time their model is updated. Also see: https://eliasdaler.wordpress.com/2013/10/11/lua_cpp_binder/ and http://lua-users.org/wiki/BindingCodeToLua
* Add basic quench analysis. First adiabatic like coil quench. Then one dimensional propagation and ultimately quasi 3D propagation. 
* Setup of more complicated cable geometries with strand-level detail.
* Add the velocity, acceleration and jerk as well as axial position to Frame.
* Conductor as part of cross section instead of coil. Then filling fraction is calculated using cable geometry.
* Add override specifiers.
* Move matrix display functions to Logger.

## Contribution
Not yet.

## Versioning
We're still in alpha.

## Authors
* Jeroen van Nugteren

## License
This project is licensed under the GNU general public license.